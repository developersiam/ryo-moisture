﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureDAL
{
    public static class DataAccessLayerServices
    {
        public static IStoreProcedureRepository StoreProcedureRepository()
        {
            IStoreProcedureRepository obj = new StoreProcedureRepository();
            return obj;
        }
        public static IPDRepository PDRepository()
        {
            IPDRepository obj = new PDRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDSetupRepository PDSetupRepository()
        {
            IPDSetupRepository obj = new PDSetupRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDMoistureRepository PDMoistureRepository()
        {
            IPDMoistureRepository obj = new PDMoistureRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDMoistureTypeRepository PDMoistureTypeRepository()
        {
            IPDMoistureTypeRepository obj = new PDMoistureTypeRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDMoistureTargetRepository PDMoistureTargetRepository()
        {
            IPDMoistureTargetRepository obj = new PDMoistureTargetRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDMoistureMasterboxRepository PDMoistureMasterboxRepository()
        {
            IPDMoistureMasterboxRepository obj = new PDMoistureMasterboxRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPackedGradeRepository PackedGradeRepository()
        {
            IPackedGradeRepository obj = new PackedGradeRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDMoistureMasterbagRepository PDMoistureMasterbagRepository()
        {
            IPDMoistureMasterbagRepository obj = new PDMoistureMasterbagRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDRYORemnantRepository PDRYORemnantRepository()
        {
            IPDRYORemnantRepository obj = new PDRYORemnantRepository(new StecDBMSEntities());
            return obj;
        }

        /// <summary>
        /// RYO material data access layer service.
        /// </summary>
        public static Ipd_ryo_OrderRepository pd_ryo_OrderRepository()
        {
            Ipd_ryo_OrderRepository obj = new pd_ryo_OrderRepository(new StecDBMSEntities());
            return obj;
        }
        public static IcustomerRepository customerRepository()
        {
            IcustomerRepository obj = new customerRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialIssuedCodeRepository pd_ryo_MaterialIssuedCodeRepository()
        {
            Ipd_ryo_MaterialIssuedCodeRepository obj = new pd_ryo_MaterialIssuedCodeRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialIssuedPerBoxRepository pd_ryo_MaterialIssuedPerBoxRepository()
        {
            Ipd_ryo_MaterialIssuedPerBoxRepository obj = new pd_ryo_MaterialIssuedPerBoxRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialItemRepository pd_ryo_MaterialItemRepository()
        {
            Ipd_ryo_MaterialItemRepository obj = new pd_ryo_MaterialItemRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialReceivingRepository pd_ryo_MaterialReceivingRepository()
        {
            Ipd_ryo_MaterialReceivingRepository obj = new pd_ryo_MaterialReceivingRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialReceivingTypeRepository pd_ryo_MaterialReceivingTypeRepository()
        {
            Ipd_ryo_MaterialReceivingTypeRepository obj = new pd_ryo_MaterialReceivingTypeRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialDamageRepository pd_ryo_MaterialDamageRepository()
        {
            Ipd_ryo_MaterialDamageRepository obj = new pd_ryo_MaterialDamageRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialDamageDetailRepository pd_ryo_MaterialDamageDetailRepository()
        {
            Ipd_ryo_MaterialDamageDetailRepository obj = new pd_ryo_MaterialDamageDetailRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_MaterialDamageReasonRepository pd_ryo_MaterialDamageReasonRepository()
        {
            Ipd_ryo_MaterialDamageReasonRepository obj = new pd_ryo_MaterialDamageReasonRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_RoleRepository pd_ryo_RoleRepository()
        {
            Ipd_ryo_RoleRepository obj = new pd_ryo_RoleRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_UserAccountRepository pd_ryo_UserAccountRepository()
        {
            Ipd_ryo_UserAccountRepository obj = new pd_ryo_UserAccountRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_UserRoleRepository pd_ryo_UserRoleRepository()
        {
            Ipd_ryo_UserRoleRepository obj = new pd_ryo_UserRoleRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_CustomerRepository pd_ryo_CustomerRepository()
        {
            Ipd_ryo_CustomerRepository obj = new pd_ryo_CustomerRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_InvoiceRepository pd_ryo_InvoiceRepository()
        {
            Ipd_ryo_InvoiceRepository obj = new pd_ryo_InvoiceRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_IssuedRepository pd_ryo_IssuedRepository()
        {
            Ipd_ryo_IssuedRepository obj = new pd_ryo_IssuedRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPDIssuedItemRepository PDIssuedItemRepository()
        {
            IPDIssuedItemRepository obj = new PDIssuedItemRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPd_ryo_paymenttermRepository Pd_ryo_paymenttermRepository()
        {
            IPd_ryo_paymenttermRepository obj = new Pd_ryo_paymenttermRepository(new StecDBMSEntities());
            return obj;
        }
        public static IPd_ryo_deliveryorderRepository Pd_ryo_deliveryorderepository()
        {
            IPd_ryo_deliveryorderRepository obj = new Pd_ryo_deliveryorderepository(new StecDBMSEntities());
            return obj;
        }
        public static IPd_ryo_provinceRepository Pd_ryo_provincerepository()
        {
            IPd_ryo_provinceRepository obj = new Pd_ryo_provincerepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_issuetypeRepository pd_ryo_issuetyperepository()
        {
            Ipd_ryo_issuetypeRepository obj = new pd_ryo_issuetyperepository(new StecDBMSEntities());
            return obj;
        }
        //public static IRepository Repository()
        //{
        //    IRepository obj = new Repository(new StecDBMSEntities());
        //    return obj;
        //}

        public static Ipd_ryo_customer_labelRepository pd_ryo_customer_labelRepository()
        {
            Ipd_ryo_customer_labelRepository obj = new pd_ryo_customer_labelRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_customer_label_setupRepository pd_ryo_customer_label_setupRepository()
        {
            Ipd_ryo_customer_label_setupRepository obj = new pd_ryo_customer_label_setupRepository(new StecDBMSEntities());
            return obj;
        }
        public static Ipd_ryo_InsertPaperRepository pd_ryo_InsertPaperRepository()
        {
            Ipd_ryo_InsertPaperRepository obj = new pd_ryo_InsertPaperRepository(new StecDBMSEntities());
            return obj;
        }
    }
}
