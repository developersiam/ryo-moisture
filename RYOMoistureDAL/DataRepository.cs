﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureDAL
{
    public interface IPDRepository : IGenericDataRepository<pd> { }
    public class PDRepository : GenericDataRepository<pd>, IPDRepository { public PDRepository(DbContext db) : base(db) { } }
    public interface IPDSetupRepository : IGenericDataRepository<pdsetup> { }
    public class PDSetupRepository : GenericDataRepository<pdsetup>, IPDSetupRepository { public PDSetupRepository(DbContext db) : base(db) { } }
    public interface IPDMoistureRepository : IGenericDataRepository<pd_moisture> { }
    public class PDMoistureRepository : GenericDataRepository<pd_moisture>, IPDMoistureRepository { public PDMoistureRepository(DbContext db) : base(db) { } }
    public interface IPDMoistureTargetRepository : IGenericDataRepository<pd_moisture_target> { }
    public class PDMoistureTargetRepository : GenericDataRepository<pd_moisture_target>, IPDMoistureTargetRepository { public PDMoistureTargetRepository(DbContext db) : base(db) { } }
    public interface IPDMoistureTypeRepository : IGenericDataRepository<pd_moisture_type> { }
    public class PDMoistureTypeRepository : GenericDataRepository<pd_moisture_type>, IPDMoistureTypeRepository { public PDMoistureTypeRepository(DbContext db) : base(db) { } }
    public interface IPDMoistureMasterboxRepository : IGenericDataRepository<pd_moisture_masterbox> { }
    public class PDMoistureMasterboxRepository : GenericDataRepository<pd_moisture_masterbox>, IPDMoistureMasterboxRepository { public PDMoistureMasterboxRepository(DbContext db) : base(db) { } }
    public interface IPackedGradeRepository : IGenericDataRepository<packedgrade> { }
    public class PackedGradeRepository : GenericDataRepository<packedgrade>, IPackedGradeRepository { public PackedGradeRepository(DbContext db) : base(db) { } }
    public interface IPDMoistureMasterbagRepository : IGenericDataRepository<pd_moisture_masterbag> { }
    public class PDMoistureMasterbagRepository : GenericDataRepository<pd_moisture_masterbag>, IPDMoistureMasterbagRepository { public PDMoistureMasterbagRepository(DbContext db) : base(db) { } }
    public interface IPDRYORemnantRepository : IGenericDataRepository<pd_ryo_remnant> { }
    public class PDRYORemnantRepository : GenericDataRepository<pd_ryo_remnant>, IPDRYORemnantRepository { public PDRYORemnantRepository(DbContext db) : base(db) { } }
    public interface IPDIssuedItemRepository : IGenericDataRepository<pd_ryo_issued_item> { }
    public class PDIssuedItemRepository : GenericDataRepository<pd_ryo_issued_item>, IPDIssuedItemRepository { public PDIssuedItemRepository(DbContext db) : base(db) { } }
    public interface IPd_ryo_paymenttermRepository : IGenericDataRepository<pd_ryo_paymentterm> { }
    public class Pd_ryo_paymenttermRepository : GenericDataRepository<pd_ryo_paymentterm>, IPd_ryo_paymenttermRepository { public Pd_ryo_paymenttermRepository(DbContext db) : base(db) { } }
    public interface IPd_ryo_deliveryorderRepository : IGenericDataRepository<pd_ryo_delivery_order> { }
    public class Pd_ryo_deliveryorderepository : GenericDataRepository<pd_ryo_delivery_order>, IPd_ryo_deliveryorderRepository { public Pd_ryo_deliveryorderepository(DbContext db) : base(db) { } }
    public interface IPd_ryo_provinceRepository : IGenericDataRepository<pd_ryo_province> { }
    public class Pd_ryo_provincerepository : GenericDataRepository<pd_ryo_province>, IPd_ryo_provinceRepository { public Pd_ryo_provincerepository(DbContext db) : base(db) { } }
    public interface Ipd_ryo_issuetypeRepository : IGenericDataRepository<pd_ryo_issuetype> { }
    public class pd_ryo_issuetyperepository : GenericDataRepository<pd_ryo_issuetype>, Ipd_ryo_issuetypeRepository { public pd_ryo_issuetyperepository(DbContext db) : base(db) { } }

    /// <summary>
    /// RYO material repositories interfaces.
    /// </summary>
    public interface Ipd_ryo_OrderRepository : IGenericDataRepository<pd_ryo_Order> { }
    public interface IcustomerRepository : IGenericDataRepository<customer> { }
    public interface Ipd_ryo_MaterialIssuedCodeRepository : IGenericDataRepository<pd_ryo_MaterialIssuedCode> { }
    public interface Ipd_ryo_MaterialIssuedPerBoxRepository : IGenericDataRepository<pd_ryo_MaterialIssuedPerBox> { }
    public interface Ipd_ryo_MaterialItemRepository : IGenericDataRepository<pd_ryo_MaterialItem> { }
    public interface Ipd_ryo_MaterialReceivingRepository : IGenericDataRepository<pd_ryo_MaterialReceiving> { }
    public interface Ipd_ryo_MaterialReceivingTypeRepository : IGenericDataRepository<pd_ryo_MaterialReceivingType> { }
    public interface Ipd_ryo_MaterialDamageRepository : IGenericDataRepository<pd_ryo_MaterialDamage> { }
    public interface Ipd_ryo_MaterialDamageDetailRepository : IGenericDataRepository<pd_ryo_MaterialDamageDetail> { }
    public interface Ipd_ryo_MaterialDamageReasonRepository : IGenericDataRepository<pd_ryo_MaterialDamageReason> { }
    public interface Ipd_ryo_UserAccountRepository : IGenericDataRepository<pd_ryo_UserAccount> { }
    public interface Ipd_ryo_UserRoleRepository : IGenericDataRepository<pd_ryo_UserRole> { }
    public interface Ipd_ryo_RoleRepository : IGenericDataRepository<pd_ryo_Role> { }
    public interface Ipd_ryo_CustomerRepository : IGenericDataRepository<pd_ryo_Customer> { }
    public interface Ipd_ryo_InvoiceRepository : IGenericDataRepository<pd_ryo_invoice> { }
    public interface Ipd_ryo_IssuedRepository : IGenericDataRepository<pd_ryo_issued> { }
    public interface Ipd_ryo_customer_labelRepository : IGenericDataRepository<pd_ryo_customer_label> { }
    public interface Ipd_ryo_customer_label_setupRepository : IGenericDataRepository<pd_ryo_customer_label_setup> { }
    public interface Ipd_ryo_InsertPaperRepository : IGenericDataRepository<pd_ryo_InsertPaper> { }
    //public interface Ipd_ryo_MaterialUnitRepository : IGenericDataRepository<pd_ryo_MaterialUnit> { }
    //public interface Ipd_ryo_MaterialItemUnitRepository : IGenericDataRepository<pd_ryo_MaterialItemUnit> { }


    /// <summary>
    /// RYO material repositories classes.
    /// </summary>
    public class pd_ryo_OrderRepository : GenericDataRepository<pd_ryo_Order>, Ipd_ryo_OrderRepository { public pd_ryo_OrderRepository(DbContext db) : base(db) { } }
    public class customerRepository : GenericDataRepository<customer>, IcustomerRepository { public customerRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialIssuedCodeRepository : GenericDataRepository<pd_ryo_MaterialIssuedCode>, Ipd_ryo_MaterialIssuedCodeRepository { public pd_ryo_MaterialIssuedCodeRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialIssuedPerBoxRepository : GenericDataRepository<pd_ryo_MaterialIssuedPerBox>, Ipd_ryo_MaterialIssuedPerBoxRepository { public pd_ryo_MaterialIssuedPerBoxRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialItemRepository : GenericDataRepository<pd_ryo_MaterialItem>, Ipd_ryo_MaterialItemRepository { public pd_ryo_MaterialItemRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialReceivingRepository : GenericDataRepository<pd_ryo_MaterialReceiving>, Ipd_ryo_MaterialReceivingRepository { public pd_ryo_MaterialReceivingRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialReceivingTypeRepository : GenericDataRepository<pd_ryo_MaterialReceivingType>, Ipd_ryo_MaterialReceivingTypeRepository { public pd_ryo_MaterialReceivingTypeRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialDamageRepository : GenericDataRepository<pd_ryo_MaterialDamage>, Ipd_ryo_MaterialDamageRepository { public pd_ryo_MaterialDamageRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialDamageDetailRepository : GenericDataRepository<pd_ryo_MaterialDamageDetail>, Ipd_ryo_MaterialDamageDetailRepository { public pd_ryo_MaterialDamageDetailRepository(DbContext db) : base(db) { } }
    public class pd_ryo_MaterialDamageReasonRepository : GenericDataRepository<pd_ryo_MaterialDamageReason>, Ipd_ryo_MaterialDamageReasonRepository { public pd_ryo_MaterialDamageReasonRepository(DbContext db) : base(db) { } }
    public class pd_ryo_UserAccountRepository : GenericDataRepository<pd_ryo_UserAccount>, Ipd_ryo_UserAccountRepository { public pd_ryo_UserAccountRepository(DbContext db) : base(db) { } }
    public class pd_ryo_UserRoleRepository : GenericDataRepository<pd_ryo_UserRole>, Ipd_ryo_UserRoleRepository { public pd_ryo_UserRoleRepository(DbContext db) : base(db) { } }
    public class pd_ryo_RoleRepository : GenericDataRepository<pd_ryo_Role>, Ipd_ryo_RoleRepository { public pd_ryo_RoleRepository(DbContext db) : base(db) { } }
    public class pd_ryo_CustomerRepository : GenericDataRepository<pd_ryo_Customer>, Ipd_ryo_CustomerRepository { public pd_ryo_CustomerRepository(DbContext db) : base(db) { } }
    public class pd_ryo_InvoiceRepository : GenericDataRepository<pd_ryo_invoice>, Ipd_ryo_InvoiceRepository { public pd_ryo_InvoiceRepository(DbContext db) : base(db) { } }
    public class pd_ryo_IssuedRepository : GenericDataRepository<pd_ryo_issued>, Ipd_ryo_IssuedRepository { public pd_ryo_IssuedRepository(DbContext db) : base(db) { } }
    public class pd_ryo_customer_labelRepository : GenericDataRepository<pd_ryo_customer_label>, Ipd_ryo_customer_labelRepository { public pd_ryo_customer_labelRepository(DbContext db) : base(db) { } }
    public class pd_ryo_customer_label_setupRepository : GenericDataRepository<pd_ryo_customer_label_setup>, Ipd_ryo_customer_label_setupRepository { public pd_ryo_customer_label_setupRepository(DbContext db) : base(db) { } }
    public class pd_ryo_InsertPaperRepository : GenericDataRepository<pd_ryo_InsertPaper>, Ipd_ryo_InsertPaperRepository { public pd_ryo_InsertPaperRepository(DbContext db) : base(db) { } }
    
    //public class pd_ryo_MaterialUnitRepository : GenericDataRepository<pd_ryo_MaterialUnit>, Ipd_ryo_MaterialUnitRepository { public pd_ryo_MaterialUnitRepository(DbContext db) : base(db) { } }
    //public class pd_ryo_MaterialItemUnitRepository : GenericDataRepository<pd_ryo_MaterialItemUnit>, Ipd_ryo_MaterialItemUnitRepository { public pd_ryo_MaterialItemUnitRepository(DbContext db) : base(db) { } }
}
