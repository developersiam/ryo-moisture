﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureDAL
{
    public interface IStoreProcedureRepository
    {
        List<sp_Packing_Get_Current_PD_Result> sp_Packing_Get_Current_PD();
        Int32 sp_Packing_GET_MaxPDBC(int crop);
        void sp_Packing_UPD_pdbc(int crop, int bc);
        List<sp_Packing_Get_PackingStatus_Result> sp_Packing_Get_PackingStatus(string pdno, int pdhour);
        void sp_Packing_INS_PackingStatus(string pdNo, int pdHour, DateTime pdDate);
        List<sp_GetInvoiceMasterboxByShipping_Result> sp_GetInvoiceMasterboxByShipping(string spno);
        List<sp_GetAvailableBagForIssue_Result> sp_GetAvailableBagForIssue();
        List<string> GetCutragShippingBay();
        List<sp_pd_ryo_InsertPaper_Result> GetInsertPaper(string productionYear, int fromCaseNo, int toCaseNo, string customerCode);

        #region RYO Inventory
        List<sp_pd_ryo_inv_SEL_PurchaseOrder_Result> GetPOByDateRange(DateTime from, DateTime to);
        List<sp_pd_ryo_inv_SEL_ReceiveDocument_Result> GetReceiveDocumentByDateRange(DateTime from, DateTime to);
        List<sp_pd_ryo_inv_SEL_ItemOnHand_Result> GetItemOnhand();
        List<sp_pd_ryo_inv_SEL_BalanceStock_Result> GetBalanceStock();
        List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result> GetBalanceScoreCard(string itemCode, string lotNumber);
        #endregion
    }
    public class StoreProcedureRepository : IStoreProcedureRepository
    {
        public void sp_Packing_INS_PackingStatus(string pdNo, int pdHour, DateTime pdDate)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    _context.sp_Packing_INS_PackingStatus(pdNo, pdHour, pdDate);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Packing_Get_PackingStatus_Result> sp_Packing_Get_PackingStatus(string pdno, int pdhour)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Packing_Get_PackingStatus(pdno, pdhour).ToList();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Packing_Get_Current_PD_Result> sp_Packing_Get_Current_PD()
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Packing_Get_Current_PD().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public Int32 sp_Packing_GET_MaxPDBC(int crop)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    var result = _context.sp_Packing_GET_MaxPDBC(crop).FirstOrDefault();
                    if (result > 0)
                    {
                        return Convert.ToInt32(result);
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void sp_Packing_UPD_pdbc(int crop, int bc)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    _context.sp_Packing_UPD_Pdbc(crop, bc);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_GetInvoiceMasterboxByShipping_Result> sp_GetInvoiceMasterboxByShipping(string spno)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_GetInvoiceMasterboxByShipping(spno).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_GetAvailableBagForIssue_Result> sp_GetAvailableBagForIssue()
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_GetAvailableBagForIssue().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string> GetCutragShippingBay()
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    var list = _context.sp_Shipping_SEL_ShippingBay().ToList();
                    List<string> result = new List<string>();
                    foreach (var item in list)
                        if (item.Contains("F") && (Convert.ToInt16(item.Substring(1, 3)) >= 90) && (Convert.ToInt16(item.Substring(1, 3)) <= 100))
                            result.Add(item);
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_pd_ryo_InsertPaper_Result> GetInsertPaper(string productionYear, int fromCaseNo, int toCaseNo, string customerCode)
        {
            using (StecDBMSEntities _context = new StecDBMSEntities() { })
            {
                return _context.sp_pd_ryo_InsertPaper(productionYear, fromCaseNo, toCaseNo, customerCode).ToList();
            }
        }

        #region RYO Inventory
        public List<sp_pd_ryo_inv_SEL_PurchaseOrder_Result> GetPOByDateRange(DateTime from, DateTime to)
        {
            using (StecDBMSEntities _context = new StecDBMSEntities() { })
            {
                return _context.sp_pd_ryo_inv_SEL_PurchaseOrder(from, to).ToList();
            }
        }

        public List<sp_pd_ryo_inv_SEL_ReceiveDocument_Result> GetReceiveDocumentByDateRange(DateTime from, DateTime to)
        {
            using (StecDBMSEntities _context = new StecDBMSEntities() { })
            {
                return _context.sp_pd_ryo_inv_SEL_ReceiveDocument(from, to).ToList();
            }
        }

        public List<sp_pd_ryo_inv_SEL_ItemOnHand_Result> GetItemOnhand()
        {
            using (StecDBMSEntities _context = new StecDBMSEntities() { })
            {
                return _context.sp_pd_ryo_inv_SEL_ItemOnHand().ToList();
            }
        }

        public List<sp_pd_ryo_inv_SEL_BalanceStock_Result> GetBalanceStock()
        {
            using (StecDBMSEntities _context = new StecDBMSEntities() { })
            {
                return _context.sp_pd_ryo_inv_SEL_BalanceStock().ToList();
            }
        }

        public List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result> GetBalanceScoreCard(string itemCode, string lotNumber)
        {
            using (var context = new StecDBMSEntities())
            {
                return context.sp_pd_ryo_inv_SEL_BalanceScoreCard(itemCode, lotNumber).ToList();
            }
        }
        #endregion
    }
}
