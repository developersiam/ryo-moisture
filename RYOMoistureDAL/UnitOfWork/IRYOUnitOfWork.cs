﻿using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureDAL.UnitOfWork
{
    public interface IRYOUnitOfWork
    {
        IGenericDataRepository<pd_ryo_MasterBagConfig> pd_ryo_MasterBagConfigRepo { get; }

        void Save();
    }
}
