﻿using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureDAL.UnitOfWork
{
    public class RYOUnitOfWork : IRYOUnitOfWork, System.IDisposable
    {
        private readonly StecDBMSEntities _context;

        private IGenericDataRepository<pd_ryo_MasterBagConfig> _pd_ryo_MasterBagConfigRepo;

        public RYOUnitOfWork()
        {
            _context = new StecDBMSEntities();
        }

        public IGenericDataRepository<pd_ryo_MasterBagConfig> pd_ryo_MasterBagConfigRepo
        {
            get
            {
                return _pd_ryo_MasterBagConfigRepo ?? (_pd_ryo_MasterBagConfigRepo = new GenericDataRepository<pd_ryo_MasterBagConfig>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
