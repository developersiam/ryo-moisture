﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureDAL.UnitOfWork
{
    public interface IInventoryUnitOfWork
    {
        IGenericDataRepository<pd_ryo_inventory_Employee> employeeRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Issued> issuedRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_IssuedDetail> issuedDetailRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Item> itemRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_ItemCategory> itemCategoryRepo { get; }        
        IGenericDataRepository<pd_ryo_inventory_PO> poRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_PODetail> poDetailRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Receive> receiveRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_ReceiveDetail> receiveDetailRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Return> returnRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_ReturnDetail> returnDetailRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Supplier> supplierRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Transaction> transactionRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Unit> unitRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_IssuedReference> issuedRefRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_IssuedReferenceDetail> issuedRefDetailRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_Damaged> damagedRepo { get; }
        IGenericDataRepository<pd_ryo_inventory_DamagedDetail> damagedDetailRepo { get; }
        IGenericDataRepository<pdsetup> pdsetupRepo { get; }
        IGenericDataRepository<packedgrade> packedgradeRepo { get; }

        void Save();
    }
}
