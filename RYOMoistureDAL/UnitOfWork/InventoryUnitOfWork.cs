﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureDAL.UnitOfWork
{
    public class InventoryUnitOfWork : IInventoryUnitOfWork, System.IDisposable
    {
        private readonly StecDBMSEntities _context;

        private IGenericDataRepository<pd_ryo_inventory_Employee> _employeeRepo;
        private IGenericDataRepository<pd_ryo_inventory_Issued> _issuedRepo;
        private IGenericDataRepository<pd_ryo_inventory_IssuedDetail> _issuedDetailRepo;
        private IGenericDataRepository<pd_ryo_inventory_Item> _itemRepo;
        private IGenericDataRepository<pd_ryo_inventory_ItemCategory> _itemCategoryRepo;
        private IGenericDataRepository<pd_ryo_inventory_PO> _poRepo;
        private IGenericDataRepository<pd_ryo_inventory_PODetail> _poDetailRepo;
        private IGenericDataRepository<pd_ryo_inventory_Receive> _receiveRepo;
        private IGenericDataRepository<pd_ryo_inventory_ReceiveDetail> _receiveDetailRepo;
        private IGenericDataRepository<pd_ryo_inventory_Return> _returnRepo;
        private IGenericDataRepository<pd_ryo_inventory_ReturnDetail> _returnDetailRepo;
        private IGenericDataRepository<pd_ryo_inventory_Supplier> _supplierRepo;
        private IGenericDataRepository<pd_ryo_inventory_Transaction> _transactionRepo;
        private IGenericDataRepository<pd_ryo_inventory_Unit> _unitRepo;
        private IGenericDataRepository<pd_ryo_inventory_IssuedReference> _issuedRefRepo;
        private IGenericDataRepository<pd_ryo_inventory_IssuedReferenceDetail> _issuedRefDetailRepo;
        private IGenericDataRepository<pd_ryo_inventory_Damaged> _damagedRepo;
        private IGenericDataRepository<pd_ryo_inventory_DamagedDetail> _damagedDetailRepo;
        private IGenericDataRepository<pdsetup> _pdsetupRepo;
        private IGenericDataRepository<packedgrade> _packedgradeRepo;


        public InventoryUnitOfWork()
        {
            _context = new StecDBMSEntities();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        public IGenericDataRepository<pd_ryo_inventory_Employee> employeeRepo
        {
            get
            {
                return _employeeRepo ?? (_employeeRepo = new GenericDataRepository<pd_ryo_inventory_Employee>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Issued> issuedRepo
        {
            get
            {
                return _issuedRepo ?? (_issuedRepo = new GenericDataRepository<pd_ryo_inventory_Issued>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_IssuedDetail> issuedDetailRepo
        {
            get
            {
                return _issuedDetailRepo ?? (_issuedDetailRepo = new GenericDataRepository<pd_ryo_inventory_IssuedDetail>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Item> itemRepo
        {
            get
            {
                return _itemRepo ?? (_itemRepo = new GenericDataRepository<pd_ryo_inventory_Item>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_ItemCategory> itemCategoryRepo
        {
            get
            {
                return _itemCategoryRepo ?? (_itemCategoryRepo = new GenericDataRepository<pd_ryo_inventory_ItemCategory>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_PO> poRepo
        {
            get
            {
                return _poRepo ?? (_poRepo = new GenericDataRepository<pd_ryo_inventory_PO>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_PODetail> poDetailRepo
        {
            get
            {
                return _poDetailRepo ?? (_poDetailRepo = new GenericDataRepository<pd_ryo_inventory_PODetail>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Receive> receiveRepo
        {
            get
            {
                return _receiveRepo ?? (_receiveRepo = new GenericDataRepository<pd_ryo_inventory_Receive>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_ReceiveDetail> receiveDetailRepo
        {
            get
            {
                return _receiveDetailRepo ?? (_receiveDetailRepo = new GenericDataRepository<pd_ryo_inventory_ReceiveDetail>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Return> returnRepo
        {
            get
            {
                return _returnRepo ?? (_returnRepo = new GenericDataRepository<pd_ryo_inventory_Return>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_ReturnDetail> returnDetailRepo
        {
            get
            {
                return _returnDetailRepo ?? (_returnDetailRepo = new GenericDataRepository<pd_ryo_inventory_ReturnDetail>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Supplier> supplierRepo
        {
            get
            {
                return _supplierRepo ?? (_supplierRepo = new GenericDataRepository<pd_ryo_inventory_Supplier>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Transaction> transactionRepo
        {
            get
            {
                return _transactionRepo ?? (_transactionRepo = new GenericDataRepository<pd_ryo_inventory_Transaction>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Unit> unitRepo
        {
            get
            {
                return _unitRepo ?? (_unitRepo = new GenericDataRepository<pd_ryo_inventory_Unit>(_context));
            }
        }

        public IGenericDataRepository<pdsetup> pdsetupRepo
        {
            get
            {
                return _pdsetupRepo ?? (_pdsetupRepo = new GenericDataRepository<pdsetup>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_IssuedReference> issuedRefRepo
        {
            get
            {
                return _issuedRefRepo ?? (_issuedRefRepo = new GenericDataRepository<pd_ryo_inventory_IssuedReference>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_IssuedReferenceDetail> issuedRefDetailRepo
        {
            get
            {
                return _issuedRefDetailRepo ?? (_issuedRefDetailRepo = new GenericDataRepository<pd_ryo_inventory_IssuedReferenceDetail>(_context));
            }
        }

        public IGenericDataRepository<packedgrade> packedgradeRepo
        {
            get
            {
                return _packedgradeRepo ?? (_packedgradeRepo = new GenericDataRepository<packedgrade>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_Damaged> damagedRepo
        {
            get
            {
                return _damagedRepo ?? (_damagedRepo = new GenericDataRepository<pd_ryo_inventory_Damaged>(_context));
            }
        }

        public IGenericDataRepository<pd_ryo_inventory_DamagedDetail> damagedDetailRepo
        {
            get
            {
                return _damagedDetailRepo ?? (_damagedDetailRepo = new GenericDataRepository<pd_ryo_inventory_DamagedDetail>(_context));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}
