﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOStore.Form.Materials
{
    /// <summary>
    /// Interaction logic for Receivings.xaml
    /// </summary>
    public partial class Receivings : Window
    {
        pd_ryo_MaterialReceiving _receiving;
        short _crop;
        string _customer;
        public Receivings(short crop,string customer)
        {
            try
            {
                InitializeComponent();

                _receiving = new pd_ryo_MaterialReceiving();
                _crop = crop;
                _customer = customer;

                CropTextBlock.Text = _crop.ToString();

                ReceivingTypeCombobox.ItemsSource = null;
                ReceivingTypeCombobox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialReceivingTypeBL().GetAll();

                ItemCombobox.ItemsSource = null;
                ItemCombobox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialItemBL()
                    .GetAll()
                    .OrderBy(x => x.ItemName);

                ReceivedDatePicker.SelectedDate = DateTime.Now;
                ReceivingDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReceivingDataGrideBinding()
        {
            try
            {
                ReceivingDataGride.ItemsSource = null;
                ReceivingDataGride.ItemsSource = BusinessLayerServices.pd_ryo_MaterialReceivingBL()
                    .GetByReceivedCrop(_crop);

                TotalTextBlock.Text = ReceivingDataGride.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                PcsTextBox.Clear();
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;

                _receiving = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReceivingTypeCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Receiving Type");

                if (ItemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Item");

                if (PcsTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอกจำนวนชิ้นที่รับเข้า (pcs.)");

                if (ReceivedDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดเลือกวันที่รับ (Received Date)");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(PcsTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                BusinessLayerServices.pd_ryo_MaterialReceivingBL()
                    .Add(new pd_ryo_MaterialReceiving {
                        Crop = _crop,
                        Customer = _customer,
                        ReceivingTypeCode = ReceivingTypeCombobox.SelectedValue.ToString(),
                        ItemCode = ItemCombobox.SelectedValue.ToString(),
                        Quantity = Convert.ToInt32(PcsTextBox.Text),
                        ReceivedDate = Convert.ToDateTime(ReceivedDatePicker.SelectedDate),
                        ReceivedBy = user_setting.User.Username,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                });

                ReceivingDataGrideBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                if (ReceivingTypeCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Receiving Type");

                if (ItemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Item");

                if (PcsTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอกจำนวนชิ้นที่รับเข้า (pcs.)");

                if (ReceivedDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดเลือกวันที่รับ (Received Date)");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(PcsTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                _receiving.Customer = _customer;
                _receiving.ReceivingTypeCode = ReceivingTypeCombobox.SelectedValue.ToString();
                _receiving.ItemCode = ItemCombobox.SelectedValue.ToString();
                _receiving.Quantity = Convert.ToInt32(PcsTextBox.Text);
                _receiving.ReceivedDate = Convert.ToDateTime(ReceivedDatePicker.SelectedDate);
                _receiving.ModifiedBy = user_setting.User.Username;
                _receiving.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialReceivingBL().Edit(_receiving);

                ReceivingDataGrideBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReceivingDataGride.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_MaterialReceiving)ReceivingDataGride.SelectedItem;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?","Confirm",MessageBoxButton.YesNo,MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MaterialReceivingBL().Delete(model.ReceivingCode);

                ReceivingDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReceivingDataGride.SelectedIndex < 0)
                    return;

                _receiving = (pd_ryo_MaterialReceiving)ReceivingDataGride.SelectedItem;

                /// Binding to controls.
                /// 
                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                ReceivingTypeCombobox.SelectedValue = _receiving.ReceivingTypeCode;
                ItemCombobox.SelectedValue = _receiving.ItemCode;
                PcsTextBox.Text = _receiving.Quantity.ToString();
                ReceivedDatePicker.SelectedDate = _receiving.ReceivedDate;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ReceivingDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Items window = new Items();
                window.ShowDialog();
                ReceivingDataGrideBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
