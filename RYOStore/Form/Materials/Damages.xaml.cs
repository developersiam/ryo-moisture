﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOStore.Form.Materials
{
    /// <summary>
    /// Interaction logic for Damages.xaml
    /// </summary>
    public partial class Damages : Page
    {
        pd_ryo_MaterialDamage _damage;

        public Damages()
        {
            try
            {
                InitializeComponent();

                _damage = new pd_ryo_MaterialDamage();

                CropCombobox.ItemsSource = null;
                CropCombobox.ItemsSource = user_setting.Crops;
                CropCombobox.SelectedValue = DateTime.Now.Year;

                DamageReasonComboBox.ItemsSource = null;
                DamageReasonComboBox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialDamageReasonBL()
                    .GetAll()
                    .OrderBy(x => x.DamageReasonName);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (CropCombobox.SelectedValue == null)
                    return;

                DamageDataGrid.ItemsSource = null;
                DamageDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_MaterialDamageBL()
                    .GetByCrop(Convert.ToInt16(CropCombobox.SelectedValue.ToString()))
                    .OrderBy(x => x.DamageCode);

                TotalTextBlock.Text = DamageDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
                pdnoTextBox.Clear();

                _damage = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Crop");

                if (DamageReasonComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Damage Reason");

                if (pdnoTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ pdno");

                BusinessLayerServices.pd_ryo_MaterialDamageBL()
                    .Add(new pd_ryo_MaterialDamage
                    {
                        Crop = Convert.ToInt16(CropCombobox.SelectedValue.ToString()),
                        pdno = pdnoTextBox.Text,
                        DamageReasonID = Guid.Parse(DamageReasonComboBox.SelectedValue.ToString()),
                        CreateDate = DateTime.Now,
                        CreateBy = user_setting.User.Username,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_MaterialDamage)DamageDataGrid.SelectedItem;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MaterialDamageBL().Delete(model.DamageCode);
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageDataGrid.SelectedIndex < 0)
                    return;

                _damage = (pd_ryo_MaterialDamage)DamageDataGrid.SelectedItem;

                /// Binding to controls.
                /// 
                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                DamageReasonComboBox.SelectedValue = _damage.DamageReasonID.ToString();
                pdnoTextBox.Text = _damage.pdno;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void AddNewReasonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DamageReasons window = new DamageReasons();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Crop");

                if (DamageReasonComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Damage Reason");

                if (pdnoTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ pdno");

                _damage.DamageReasonID = Guid.Parse(DamageReasonComboBox.SelectedValue.ToString());
                _damage.pdno = pdnoTextBox.Text;
                _damage.ModifiedBy = user_setting.User.Username;
                _damage.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialDamageBL().Edit(_damage);

                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DamageDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (DamageDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_MaterialDamage)DamageDataGrid.SelectedItem;

                DamageDetails window = new DamageDetails(model);
                window.ShowDialog();

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void pdnoTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Crop");

                DamagePdnos._crop = Convert.ToInt16(CropCombobox.SelectedValue.ToString());
                var returnPdno = DamagePdnos.GetPdno();

                if (returnPdno == null)
                    return;

                pdnoTextBox.Text = returnPdno;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void DamageDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_MaterialDamage)DamageDataGrid.SelectedItem;

                DamageDetails window = new DamageDetails(model);
                window.ShowDialog();

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
