﻿using RYOStore.ViewModel;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Materials
{
    /// <summary>
    /// Interaction logic for ReceivingDetails.xaml
    /// </summary>
    public partial class ReceivingDetails : Window
    {
        MaterialStockSummaryViewModel _model;

        public ReceivingDetails(MaterialStockSummaryViewModel model)
        {
            try
            {
                InitializeComponent();

                _model = new MaterialStockSummaryViewModel();
                _model = model;

                ItemCodeTextBox.Text = model.ItemCode;
                ItemNameTextBox.Text = model.ItemName;
                TotalReceivedTextBox.Text = model.TotalReceived.ToString("N0");
                TotalIssuedTextBox.Text = model.TotalIssued.ToString("N0");
                OnHandTextBox.Text = model.OnHand.ToString("N0");

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                ReceivingDataGride.ItemsSource = null;
                ReceivingDataGride.ItemsSource = BusinessLayerServices.pd_ryo_MaterialReceivingBL()
                    .GetByReceivedCrop(_model.Crop)
                    .Where(x => x.ItemCode == _model.ItemCode)
                    .OrderByDescending(x => x.ReceivedDate);

                TotalTextBlock.Text = ReceivingDataGride.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
