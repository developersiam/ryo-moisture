﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for ItemUnits.xaml
    /// </summary>
    public partial class ItemUnits : Page
    {
        public ItemUnits()
        {
            InitializeComponent();
            ItemUnitDataBinding();
        }

        private void ItemUnitDataBinding()
        {
            try
            {
                ItemCombobox.ItemsSource = null;
                ItemCombobox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialItemBL()
                    .GetAll()
                    .OrderBy(x => x.ItemName);

                UnitCombobox.ItemsSource = null;
                UnitCombobox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialUnitBL()
                    .GetAll()
                    .OrderBy(x => x.UnitName);

                ItemUnitDataGrid.ItemsSource = null;
                ItemUnitDataGrid.ItemsSource = Helper.MaterialItemUnitHelper.GetAll();

                TotalTextBlock.Text = ItemUnitDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemUnitDataGrid.SelectedIndex < 0)
                    return;
                
                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialItemUnit)ItemUnitDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialItemUnitBL().Delete(model.ID);

                ItemUnitDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Item");

                if (UnitCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Unit");

                BusinessLayerServices.pd_ryo_MaterialItemUnitBL().Add(new pd_ryo_MaterialItemUnit
                {
                    ID = Guid.NewGuid(),
                    ItemCode = ItemCombobox.SelectedValue.ToString(),
                    UnitCode = Convert.ToInt16(UnitCombobox.SelectedValue.ToString()),
                    CreateBy = user_setting.Username,
                    ModifiedBy = user_setting.Username,
                    CreateDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                });
                ItemUnitDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Items window = new Items();
                window.ShowDialog();
                ItemUnitDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewUnitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Units window = new Units();
                window.ShowDialog();
                ItemUnitDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
