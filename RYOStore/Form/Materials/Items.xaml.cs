﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Materials
{
    /// <summary>
    /// Interaction logic for Items.xaml
    /// </summary>
    public partial class Items : Window
    {
        pd_ryo_MaterialItem _item;

        public Items()
        {
            try
            {
                InitializeComponent();
                ItemDataGridBinding();

                _item = new pd_ryo_MaterialItem();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ItemDataGridBinding()
        {
            try
            {
                ItemDataGrid.ItemsSource = null;
                ItemDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_MaterialItemBL().GetAll();

                TotalTextBlock.Text = ItemDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
                ItemCodeTextBox.IsEnabled = true;
                ItemCodeTextBox.Clear();
                ItemNameTextBox.Clear();

                _item = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Item");

                BusinessLayerServices.pd_ryo_MaterialItemBL()
                    .Add(new pd_ryo_MaterialItem
                    {
                        ItemCode = ItemCodeTextBox.Text,
                        ItemName = ItemNameTextBox.Text,
                        CreateBy = user_setting.User.Username,
                        CreateDate = DateTime.Now,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                ItemDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialItem)ItemDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialItemBL().Delete(model.ItemCode);

                ItemDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Item");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _item.ItemName = ItemNameTextBox.Text;
                _item.ModifiedBy = user_setting.User.Username;
                _item.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialItemBL().Edit(_item);

                ItemDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                ItemCodeTextBox.IsEnabled = false;

                _item = (pd_ryo_MaterialItem)ItemDataGrid.SelectedItem;

                ItemCodeTextBox.Text = _item.ItemCode;
                ItemNameTextBox.Text = _item.ItemName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
