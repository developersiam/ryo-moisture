﻿using DomainModelStecDbms;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Invoice
{
    /// <summary>
    /// Interaction logic for MastebagWindow.xaml
    /// </summary>
    public partial class MasteboxWindow : Window
    {
        public List<pd_moisture_masterbox> selectedBox;
        public MasteboxWindow()
        {
            InitializeComponent();
            ShippingNumberTextBox.Focus();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var spno = ShippingNumberTextBox.Text.Trim();
                if (spno == "") throw new Exception("Please mention Shipping number!");
                var masterbagList = BusinessLayerServices.InvoiceBL().GetMasterboxListForInvoice(spno);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = masterbagList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShippingNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) ReloadDatagrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void InformationDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SelectedItemCount();
        }

        private void InformationDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            SelectedItemCount();
        }

        private void SelectedItemCount()
        {
            try
            {
                var selected = InformationDataGrid.SelectedItems;
                SelectTextblock.Text = string.Format("Select ({0}) Items", selected.Count);
                SelectButton.IsEnabled = selected.Count > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedBox = new List<pd_moisture_masterbox>();
                var selected = InformationDataGrid.SelectedItems;
                if(selected.Count > 0)
                {
                    foreach (var item in selected)
                    {
                        var bc = ((sp_GetInvoiceMasterboxByShipping_Result)item).bc;
                        var masterBox = BusinessLayerServices.InvoiceBL().GetSingleMasterbox(bc);
                        if(masterBox != null) selectedBox.Add(masterBox);
                    }
                }
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
