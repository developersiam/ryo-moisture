﻿using RYOStore.Helper;
using RYOMoistureBL;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOStore.Form.Invoice
{
    /// <summary>
    /// Interaction logic for InvoicePage.xaml
    /// </summary>
    public partial class InvoicePage : Page
    {
        public InvoicePage()
        {
            try
            {
                InitializeComponent();
                CropCombobox.ItemsSource = null;
                CropCombobox.ItemsSource = user_setting.Crops;
                CropCombobox.SelectedValue = DateTime.Now.Year;
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var crop = Convert.ToInt16(CropCombobox.SelectedValue);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.InvoiceBL().GetInvoiceListByCrop(crop);
                TotalTextBlock.Text = InformationDataGrid.Items.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_invoice)InformationDataGrid.SelectedItem;
                var window = new InvoiceUpdateWindow(selected);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0) throw new Exception("โปรดเลือกปีการผลิตจากตัวเลือก");
                var window = new InvoiceAddWindow(Convert.ToInt16(CropCombobox.SelectedValue));
                window.ShowDialog();
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //var selected = (pd_ryo_invoice)InformationDataGrid.SelectedItem;
                    //if (selected != null)
                    //{
                    //    var masterboxlist = BusinessLayerServices.InvoiceBL().GetMasterboxByInvoiceno(selected.invoiceno);
                    //    foreach (var masterBox in masterboxlist)
                    //    {
                    //        masterBox.IssuedCode = "";
                    //        BusinessLayerServices.InvoiceBL().UpdateMasterbox(masterBox);
                    //    }
                    //    BusinessLayerServices.InvoiceBL().RemoveInvoice(selected);
                    //    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    ReloadDatagrid();
                    //}
                    throw new NotImplementedException("Not Implement yet");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }
    }
}
