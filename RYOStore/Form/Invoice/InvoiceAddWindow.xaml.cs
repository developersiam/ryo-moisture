﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Invoice
{
    /// <summary>
    /// Interaction logic for InvoiceAddWindow.xaml
    /// </summary>
    public partial class InvoiceAddWindow : Window
    {
        List<sp_GetInvoiceMasterboxByShipping_Result> masterboxList;
        public InvoiceAddWindow()
        {
            InitializeComponent();
        }

        public InvoiceAddWindow(short crop)
        {
            InitializeComponent();
            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = crop;
            InvoiceNoTextBox.Text = GetNewInvoiceNo(crop);
            masterboxList = new List<sp_GetInvoiceMasterboxByShipping_Result>();
            ReloadDatagrid();
        }

        private string GetNewInvoiceNo(short crop)
        {
            string newInvoice = "";
            try
            {
                var maxInvoice = BusinessLayerServices.InvoiceBL().GetMaxInvoiceNumber(crop);
                var shortCrop = Regex.Match(crop.ToString(), @"(.{2})\s*$");
                if (maxInvoice == "")
                {
                    newInvoice = string.Format("INV{0}-0001", shortCrop);
                }
                else
                {
                    var cutInvoiceNumber = Regex.Match(maxInvoice, @"(.{4})\s*$").ToString();
                    var newInvoiceNumber = Convert.ToInt32(cutInvoiceNumber) + 1;
                    var newInvoiceString = newInvoiceNumber.ToString().PadLeft(4, '0');
                    newInvoice = string.Format("INV{0}-{1}", shortCrop, newInvoiceString);
                }
                return newInvoice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return newInvoice;
            }
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void InvoiceNoTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var invoiceno = InvoiceNoTextBox.Text.Trim();
                if (invoiceno == "") return;
                //if (invoiceno == "") throw new Exception("Please mention Invoice number!");
                masterboxList = BusinessLayerServices.InvoiceBL().GetMasterboxListForInvoice(invoiceno);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = masterboxList;
                SaveTextblock.Text = string.Format("Confirm ({0})", masterboxList.Count);
                SaveButton.IsEnabled = masterboxList.Any();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedItem == null) throw new Exception("Please mention Crop");
                if (InvoiceNoTextBox.Text == "") throw new Exception("Please mention Invoice no.");
                if (CustomerTextBox.Text == "") throw new Exception("Please mention Customer");
                if (!masterboxList.Any()) return;
                var invoice = new pd_ryo_invoice
                {
                    crop = Convert.ToInt16(CropCombobox.SelectedValue),
                    invoiceno = InvoiceNoTextBox.Text,
                    invoicedate = DateTime.Now,
                    customer = CustomerTextBox.Text,
                    modifieddate = DateTime.Now,
                    modifieduser = user_setting.User.Username
                };
                BusinessLayerServices.InvoiceBL().AddInvoice(invoice);

                foreach (var box in masterboxList)
                {
                    var masterbagList = BusinessLayerServices.InvoiceBL().GetMasterbagByMasterbox(box.bc);
                    foreach (var bag in masterbagList)
                    {
                        bag.invoiceno = InvoiceNoTextBox.Text;
                        BusinessLayerServices.InvoiceBL().UpdateMasterbag(bag);
                    }
                }
                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
