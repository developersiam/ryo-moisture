﻿using DomainModelStecDbms;
using RYOMoistureBL;
using RYOStore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Invoice
{
    /// <summary>
    /// Interaction logic for InvoiceUpdateWindow.xaml
    /// </summary>
    public partial class InvoiceUpdateWindow : Window
    {
        public InvoiceUpdateWindow()
        {
            InitializeComponent();
        }

        public InvoiceUpdateWindow(pd_ryo_invoice invoice)
        {
            InitializeComponent();
            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = invoice.crop;
            InvoiceNoTextBox.Text = invoice.invoiceno;
            CustomerTextBox.Text = invoice.customer;
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var masterboxList = BusinessLayerServices.InvoiceBL().GetMasterboxByInvoiceno(InvoiceNoTextBox.Text);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = masterboxList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
