﻿using DomainModelStecDbms;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Invoice
{
    /// <summary>
    /// Interaction logic for CustomerWindow.xaml
    /// </summary>
    public partial class CustomerWindow : Window
    {
        public pd_ryo_Customer selectedCustomer;
        List<pd_ryo_Customer> customerList;
        public CustomerWindow()
        {
            InitializeComponent();
            customerList = BusinessLayerServices.customerBL().GetAllRYOCustomer();
            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = customerList;
        }

        private void CustomerTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            FilterDatagrid();
        }

        private void AddMasterBagButton_Click(object sender, RoutedEventArgs e)
        {
            FilterDatagrid();
        }

        private void FilterDatagrid()
        {
            try
            {
                var searchText = CustomerTextBox.Text.Trim();
                List<pd_ryo_Customer> filteredList;
                if (searchText == "") filteredList = customerList;
                else filteredList = customerList.Where(w => w.CustomerName.Contains(searchText)).ToList();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filteredList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_Customer)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    selectedCustomer = selected;
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
