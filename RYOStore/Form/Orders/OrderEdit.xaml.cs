﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Orders
{
    /// <summary>
    /// Interaction logic for EditOrder.xaml
    /// </summary>
    public partial class OrderEdit : Window
    {
        pd_ryo_Order _order;

        public OrderEdit(pd_ryo_Order model)
        {
            try
            {
                InitializeComponent();

                _order = new pd_ryo_Order();
                _order = model;

                StatusCombobox.ItemsSource = null;
                StatusCombobox.ItemsSource = user_setting.Status;

                CustomerCombobox.ItemsSource = null;
                CustomerCombobox.ItemsSource = BusinessLayerServices.customerBL()
                    .GetAll()
                    .OrderBy(x => x.code);

                OrderDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OrderDataBinding()
        {
            try
            {
                if (_order == null)
                    return;

                PONoTextBox.Text = _order.PONo;
                InvoiceNoTextBox.Text = _order.InvoiceNo;
                CustomerCombobox.SelectedValue = _order.Customer;
                QuantityTextBox.Text = _order.Quantity.ToString();
                UnitPriceTextBox.Text = _order.UnitPrice.ToString();
                AmountTextBox.Text = (_order.Quantity * _order.UnitPrice).ToString("N2");
                ProductionYearTextBox.Text = _order.ProductionYear;
                ShippingCaseNoFromTextBox.Text = _order.ShippingCaseFrom.ToString();
                ShippingCaseNoToTextBox.Text = _order.ShippingCaseTo.ToString();
                CustomerPaymentDatePicker.SelectedDate = _order.CustomerPaymentDate;
                DeliveryDatePicker.SelectedDate = _order.DeliveryDate;
                ReceivedDatePicker.SelectedDate = _order.ReceivedDate;
                ArrivalDatePicker.SelectedDate = _order.ArrivalDate;
                CourierPaymentDatePicker.SelectedDate = _order.CourierPaymentDate;
                CourierTextBox.Text = _order.Courier;
                StatusCombobox.SelectedValue = _order.DeliveryStatus;
                DescriptionOfGoodsTextBox.Text = _order.DescriptionOfGoods;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OrderDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PONoTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก PO no.");

                if (InvoiceNoTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Invoice no.");

                if (CustomerCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดกรอก Customer");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Quantity");

                if (UnitPriceTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Unit Price");

                if (ProductionYearTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Production Year");

                if (ShippingCaseNoFromTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Shipping Case No From");

                if (ShippingCaseNoToTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Shipping Case No To");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsDecimalCharacter(UnitPriceTextBox.Text) == false)
                    throw new ArgumentException("Unit price จะต้องเป็นตัวเลขเท่านั้น");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ProductionYearTextBox.Text) == false)
                    throw new ArgumentException("Production year จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    throw new ArgumentException("shippint case no from จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    throw new ArgumentException("shippint case no to จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                var msg01 = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg01 == MessageBoxResult.No)
                    return;

                if (_order.ShippingCaseFrom != Convert.ToInt16(ShippingCaseNoFromTextBox.Text) ||
                    _order.ShippingCaseTo != Convert.ToInt16(ShippingCaseNoToTextBox.Text) ||
                    _order.Quantity != Convert.ToInt16(QuantityTextBox.Text))
                {
                    var msg02 = MessageBox.Show("เมื่อมีการเปลี่ยนแปลงข้อมูล Shipping case no from, shipping case no to หรือ quantity" +
                        " รหัสบาร์โค้ตของ insert paper ในระบบอาจมีการเปลี่ยนแปลงหมายเลข running number ท่านแน่ใจหรือไม่ที่จะแก้ไขข้อมูลดังกล่าว", "confirm",
                        MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (msg02 == MessageBoxResult.No)
                        return;
                }

                _order.InvoiceNo = InvoiceNoTextBox.Text;
                _order.Quantity = Convert.ToInt16(QuantityTextBox.Text);
                _order.UnitPrice = Convert.ToDecimal(UnitPriceTextBox.Text);
                _order.ProductionYear = ProductionYearTextBox.Text;
                _order.ShippingCaseFrom = Convert.ToInt16(ShippingCaseNoFromTextBox.Text);
                _order.ShippingCaseTo = Convert.ToInt16(ShippingCaseNoToTextBox.Text);
                _order.CustomerPaymentDate = Convert.ToDateTime(CustomerPaymentDatePicker.SelectedDate);
                _order.DeliveryDate = Convert.ToDateTime(DeliveryDatePicker.SelectedDate);
                _order.ReceivedDate = Convert.ToDateTime(ReceivedDatePicker.SelectedDate);
                _order.ArrivalDate = Convert.ToDateTime(ArrivalDatePicker.SelectedDate);
                _order.CourierPaymentDate = Convert.ToDateTime(CourierPaymentDatePicker.SelectedDate);
                _order.Courier = CourierTextBox.Text;
                _order.DeliveryStatus = Convert.ToBoolean(StatusCombobox.SelectedValue);
                _order.DescriptionOfGoods = DescriptionOfGoodsTextBox.Text;

                BusinessLayerServices.pd_ryo_OrderBL().Edit(_order);

                MessageBox.Show("บันทึกข้อมูลสำเร็จ", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void QuantityTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (QuantityTextBox.Text == "")
                {
                    UnitPriceTextBox.Clear();
                    AmountTextBox.Clear();
                    ShippingCaseNoToTextBox.Clear();
                    return;
                }

                /// Calculate amount.
                /// 
                AmountTextBox.Text = (Convert.ToDecimal(QuantityTextBox.Text) *
                    Convert.ToDecimal(UnitPriceTextBox.Text == "" ?
                    UnitPriceTextBox.Text = "0" : UnitPriceTextBox.Text)).ToString("N2");

                /// Calculate shipping case no to.
                /// 
                ShippingCaseNoToTextBox.Text = (Convert.ToInt16(QuantityTextBox.Text) +
                    Convert.ToInt16(ShippingCaseNoFromTextBox.Text == "" ?
                    "1" : ShippingCaseNoFromTextBox.Text) - 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnitPriceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UnitPriceTextBox.Text == "")
                {
                    AmountTextBox.Clear();
                    return;
                }

                /// Calculate amount.
                /// 
                AmountTextBox.Text = (Convert.ToDecimal(QuantityTextBox.Text) *
                    Convert.ToDecimal(UnitPriceTextBox.Text)).ToString("N2");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShippingCaseNoFromTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (ShippingCaseNoFromTextBox.Text == "" || QuantityTextBox.Text == "")
                    return;

                /// Calculate shipping case no to.
                /// 
                ShippingCaseNoToTextBox.Text = (Convert.ToInt16(QuantityTextBox.Text) +
                    Convert.ToInt16(ShippingCaseNoFromTextBox.Text) - 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
