﻿using DomainModelStecDbms;
using RYOStore.ViewModel;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Orders
{
    /// <summary>
    /// Interaction logic for PrintMasterBagBarcode.xaml
    /// </summary>
    /// 

    public partial class PrintMasterBagBarcode : Window
    {
        pd_ryo_Order _order;

        public PrintMasterBagBarcode(pd_ryo_Order order)
        {
            InitializeComponent();

            _order = new pd_ryo_Order();
            _order = order;

            PONoTextBox.Text = _order.PONo;
            ShippingCaseNoFromTextBox.Text = _order.ShippingCaseFrom.ToString();
            ShippingCaseNoToTextBox.Text = _order.ShippingCaseTo.ToString();
        }

        private void Print(MasterBagBarcodeTemplateViewModel model)
        {
            try
            {
                var application = new Microsoft.Office.Interop.Word.Application();
                var document = new Microsoft.Office.Interop.Word.Document();

                document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");

                application.Visible = false;

                foreach (Microsoft.Office.Interop.Word.Shape shape in document.Shapes)
                {
                    if (shape.Name == "MBTextBox1")
                        shape.TextFrame.ContainingRange.Text = model.Barcode01;
                    if (shape.Name == "MBTextBox2")
                        shape.TextFrame.ContainingRange.Text = model.Barcode02;
                    if (shape.Name == "MBTextBox3")
                        shape.TextFrame.ContainingRange.Text = model.Barcode03;
                    if (shape.Name == "MBTextBox4")
                        shape.TextFrame.ContainingRange.Text = model.Barcode04;
                    if (shape.Name == "MBTextBox5")
                        shape.TextFrame.ContainingRange.Text = model.Barcode05;
                    if (shape.Name == "MBTextBox6")
                        shape.TextFrame.ContainingRange.Text = model.Barcode06;
                    if (shape.Name == "MBTextBox7")
                        shape.TextFrame.ContainingRange.Text = model.Barcode07;
                    if (shape.Name == "MBTextBox8")
                        shape.TextFrame.ContainingRange.Text = model.Barcode08;

                    if (shape.Name == "BCTextBox1")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode01 + "*";
                    if (shape.Name == "BCTextBox2")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode02 + "*";
                    if (shape.Name == "BCTextBox3")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode03 + "*";
                    if (shape.Name == "BCTextBox4")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode04 + "*";
                    if (shape.Name == "BCTextBox5")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode05 + "*";
                    if (shape.Name == "BCTextBox6")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode06 + "*";
                    if (shape.Name == "BCTextBox7")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode07 + "*";
                    if (shape.Name == "BCTextBox8")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode08 + "*";
                }

                object saveOption = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                object originalFormat = Microsoft.Office.Interop.Word.WdOriginalFormat.wdOriginalDocumentFormat;
                object routDocument = false;


                object missingValue = Type.Missing;

                object myTrue = true;
                object myFalse = false;

                document.PrintOut(ref myFalse, ref myFalse, ref missingValue, ref missingValue,
                    ref missingValue, missingValue, ref missingValue, ref missingValue, ref missingValue,
                    ref missingValue, ref myFalse, ref missingValue, ref missingValue, ref missingValue);

                document.Close(ref saveOption, ref originalFormat, ref routDocument);

                while (application.BackgroundPrintingStatus > 0)
                {
                    System.Threading.Thread.Sleep(250);
                }

                application.Quit(ref missingValue, ref missingValue, ref missingValue);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MasterBagBarcodeDataGrid.Items.Count < 1)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการพิมพ์");

                if (MasterBagBarcodeDataGrid.SelectedItems.Count < 1)
                    throw new ArgumentException("โปรดเลือก master bag barcode ที่ท่านต้องการจะพิมพ์");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    throw new ArgumentException("shippint case no from จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    throw new ArgumentException("shippint case no to จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                List<MasterBagBarcodeViewModel> list = new List<MasterBagBarcodeViewModel>();
                foreach (var item in MasterBagBarcodeDataGrid.SelectedItems)
                    list.Add((MasterBagBarcodeViewModel)item);

                /// Generate info list for fill on the template.
                /// 
                List<MasterBagBarcodeTemplateViewModel> templateList = new List<MasterBagBarcodeTemplateViewModel>();
                MasterBagBarcodeTemplateViewModel obj = new MasterBagBarcodeTemplateViewModel();
                int columnCount = 1;

                foreach (var item in list)
                {
                    if (columnCount <= 8 && !item.Equals(list.Last()))
                    {
                        if (columnCount == 1)
                            obj.Barcode01 = item.MasterBagCode;
                        else if (columnCount == 2)
                            obj.Barcode02 = item.MasterBagCode;
                        else if (columnCount == 3)
                            obj.Barcode03 = item.MasterBagCode;
                        else if (columnCount == 4)
                            obj.Barcode04 = item.MasterBagCode;
                        else if (columnCount == 5)
                            obj.Barcode05 = item.MasterBagCode;
                        else if (columnCount == 6)
                            obj.Barcode06 = item.MasterBagCode;
                        else if (columnCount == 7)
                            obj.Barcode07 = item.MasterBagCode;
                        else if (columnCount == 8)
                        {
                            obj.Barcode08 = item.MasterBagCode;
                            templateList.Add(new MasterBagBarcodeTemplateViewModel
                            {
                                Barcode01 = obj.Barcode01,
                                Barcode02 = obj.Barcode02,
                                Barcode03 = obj.Barcode03,
                                Barcode04 = obj.Barcode04,
                                Barcode05 = obj.Barcode05,
                                Barcode06 = obj.Barcode06,
                                Barcode07 = obj.Barcode07,
                                Barcode08 = obj.Barcode08
                            });
                            columnCount = 1;
                            obj.Barcode01 = "";
                            obj.Barcode02 = "";
                            obj.Barcode03 = "";
                            obj.Barcode04 = "";
                            obj.Barcode05 = "";
                            obj.Barcode06 = "";
                            obj.Barcode07 = "";
                            obj.Barcode08 = "";
                            continue;
                        }
                        columnCount = columnCount + 1;
                    }
                    else
                    {
                        if (columnCount == 1)
                            obj.Barcode01 = item.MasterBagCode;
                        else if (columnCount == 2)
                            obj.Barcode02 = item.MasterBagCode;
                        else if (columnCount == 3)
                            obj.Barcode03 = item.MasterBagCode;
                        else if (columnCount == 4)
                            obj.Barcode04 = item.MasterBagCode;
                        else if (columnCount == 5)
                            obj.Barcode05 = item.MasterBagCode;
                        else if (columnCount == 6)
                            obj.Barcode06 = item.MasterBagCode;
                        else if (columnCount == 7)
                            obj.Barcode07 = item.MasterBagCode;
                        else if (columnCount == 8)
                            obj.Barcode08 = item.MasterBagCode;

                        templateList.Add(obj);
                    }
                }

                foreach (var item in templateList)
                {
                    var test = item;
                    Print(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReprintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MasterBagBarcodeDataGrid.SelectedIndex < 0)
                    return;

                var model = (MasterBagBarcodeViewModel)MasterBagBarcodeDataGrid.SelectedItem;

                /// print barcode by barcode.
                /// 

                Print(new MasterBagBarcodeTemplateViewModel
                {
                    Barcode01 = model.MasterBagCode
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void GenerateBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShippingCaseNoFromTextBox.Text == "" || ShippingCaseNoToTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุเลข Shipping case no from และ to ก่อนการค้นหา");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    throw new ArgumentException("shippint case no from จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    throw new ArgumentException("shippint case no to จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                List<MasterBagBarcodeViewModel> barcodes = new List<MasterBagBarcodeViewModel>();

                int rowNumber = 1;
                for (int caseno = Convert.ToInt16(ShippingCaseNoFromTextBox.Text); caseno <= Convert.ToInt16(ShippingCaseNoToTextBox.Text); caseno++)
                {
                    for (int bag = 1; bag <= 20; bag++)
                    {
                        barcodes.Add(new MasterBagBarcodeViewModel
                        {
                            MasterBagCode = _order.ProductionYear + caseno.ToString().PadLeft(6, '0') + bag.ToString().PadLeft(2, '0'),
                            CreateDate = DateTime.Now,
                            RowNumber = rowNumber
                        });
                        rowNumber = rowNumber + 1;
                    }
                }

                MasterBagBarcodeDataGrid.ItemsSource = null;
                MasterBagBarcodeDataGrid.ItemsSource = barcodes;

                TotalTextBlock.Text = MasterBagBarcodeDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MasterBagBarcodeDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectedItemsTextBlock.Text = MasterBagBarcodeDataGrid.SelectedItems.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
