﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOStore.Form.Orders
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : Page
    {
        public Order()
        {
            try
            {
                InitializeComponent();

                CropCombobox.ItemsSource = null;
                CropCombobox.ItemsSource = user_setting.Crops;
                CropCombobox.SelectedValue = DateTime.Now.Year;
                

                //user_setting.User.Username = "eakkaluck";

                OrderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OrderDataGridBinding()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (CropCombobox.SelectedValue == null)
                    return;

                OrderDataGrid.ItemsSource = null;
                OrderDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_OrderBL()
                    .GetByCrop(Convert.ToInt16(CropCombobox.SelectedValue))
                    .OrderBy(x => x.InvoiceNo);               

                TotalTextBlock.Text = OrderDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OrderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือกปีการผลิตจากตัวเลือก");

                OrderAdd window = new OrderAdd(Convert.ToInt16(CropCombobox.SelectedValue));
                window.ShowDialog();
                OrderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OrderDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_Order)OrderDataGrid.SelectedItem;
                if (model == null)
                    return;

                BusinessLayerServices.pd_ryo_OrderBL().Delete(model.PONo);
                OrderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void OrderDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (OrderDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_Order)OrderDataGrid.SelectedItem;
                if (model == null)
                    return;

                OrderEdit window = new OrderEdit(model);
                window.ShowDialog();
                OrderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintInsertPaperButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (OrderDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_Order)OrderDataGrid.SelectedItem;

                PrintMasterBagBarcode window = new PrintMasterBagBarcode(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
