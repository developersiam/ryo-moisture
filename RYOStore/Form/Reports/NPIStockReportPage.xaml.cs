﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelStecDbms;
using RYOMoistureBL;
using RYOStore.ViewModel;

namespace RYOStore.Form.Reports
{
    /// <summary>
    /// Interaction logic for NPIStockReportPage.xaml
    /// </summary>
    public partial class NPIStockReportPage : Page
    {
        public NPIStockReportPage()
        {
            InitializeComponent();
            var statusList = new List<pd_moisture_type>();
            statusList.Add(new pd_moisture_type { MoistureTypeID = 0, MoistureType = "-- All --" });
            statusList.Add(new pd_moisture_type { MoistureTypeID = 1, MoistureType = "Issued" });
            statusList.Add(new pd_moisture_type { MoistureTypeID = 2, MoistureType = "Avialable" });
            StatusCombobox.ItemsSource = statusList;
            StatusCombobox.SelectedIndex = 0;
        }

        private void StatusCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var status = (pd_moisture_type)StatusCombobox.SelectedItem;
                if (status == null) throw new Exception("Please select status.");
                var masterbagList = BusinessLayerServices.InvoiceBL().GetRemaingingBagReport(status.MoistureTypeID);

                var groupedBox = masterbagList.GroupBy(g => g.masterbox_bc).Select(s => s.Key).ToList();
                var remainingList = new List<RemainingReportVM>();
                foreach (var box in groupedBox)
                {
                    var i = masterbagList.Where(w => w.masterbox_bc == box);
                    var remaining = new RemainingReportVM();
                    remaining.invoiceno = i.FirstOrDefault().invoiceno;
                    remaining.masterbox_bc = i.FirstOrDefault().masterbox_bc;
                    remaining.remainingbag = i.Count();
                    if (remaining.remainingbag > 0) remainingList.Add(remaining);
                }

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = remainingList;
                TotalTextBlock.Text = remainingList.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (RemainingReportVM)InformationDataGrid.SelectedItem;
                var w = new RemainingReportWindow(selected.masterbox_bc);
                w.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
