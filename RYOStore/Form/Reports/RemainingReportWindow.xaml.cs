﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RYOMoistureBL;

namespace RYOStore.Form.Reports
{
    /// <summary>
    /// Interaction logic for RemainingReportWindow.xaml
    /// </summary>
    public partial class RemainingReportWindow : Window
    {
        public RemainingReportWindow()
        {
            InitializeComponent();
        }
        public RemainingReportWindow(string masterbox_bc)
        {
            InitializeComponent();
            MasterboxTextBox.Text = masterbox_bc;
            MasterboxTextBox.IsEnabled = false;
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                var masterbox_bc = MasterboxTextBox.Text.Trim();
                if (masterbox_bc == null) throw new Exception("Please select masterbox barcode.");
                var masterbagList = BusinessLayerServices.InvoiceBL().GetMasterbagByMasterbox(masterbox_bc);

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = masterbagList;
                TotalTextBlock.Text = masterbagList.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
