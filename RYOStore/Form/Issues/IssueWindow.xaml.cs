﻿using DomainModelStecDbms;
using RYOMoistureBL;
using RYOStore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Issues
{
    /// <summary>
    /// Interaction logic for IssueWindow.xaml
    /// </summary>
    public partial class IssueWindow : Window
    {
        List<pd_ryo_issued_item> issueditemList;
        List<pd_moisture_masterbag> masterbagList;

        public IssueWindow()
        {
            InitializeComponent();
        }

        public IssueWindow(short crop)
        {
            InitializeComponent();
            BindCombobox();
        }

        private void BindCombobox()
        {
            issueditemList = new List<pd_ryo_issued_item>();
            masterbagList = new List<pd_moisture_masterbag>();
            PaymenttermCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllPaymentterm();
            IssueTypeCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllIssueType();
            ProvinceCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllProvince();
            BillDateDatePicker.DisplayDateStart = DateTime.Now;
        }

        private void ProvinceCombobox_DropDownClosed(object sender, EventArgs e)
        {
            IssueNumberTextBox.Text = GetNewIssueNo();
            DeliveryNumberTextBox.Text = GetNewDeliveryNo();
        }

        private string GetNewIssueNo()
        {
            string newIssue = "";
            try
            {
                var province = ProvinceCombobox.SelectedValue.ToString();
                var shortCrop = DateTime.Now.ToString("yy");
                var shortMonth = DateTime.Now.ToString("MM");
                var prefix = string.Format("{0}{1}{2}", province, shortCrop, shortMonth);
                var maxIssue = BusinessLayerServices.InvoiceBL().GetMaxIssueNumber(prefix);
                if (maxIssue == "")
                {
                    newIssue = string.Format("{0}0001", prefix);
                }
                else
                {
                    var cutIssueNumber = Regex.Match(maxIssue, @"(.{4})\s*$").ToString();
                    var newIssueNumber = Convert.ToInt32(cutIssueNumber) + 1;
                    var newIssueString = newIssueNumber.ToString().PadLeft(4, '0');
                    newIssue = string.Format("{0}{1}", prefix, newIssueString);
                }
                return newIssue;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return newIssue;
            }
        }

        private string GetNewDeliveryNo()
        {
            string newDelivery = "";
            try
            {
                var shortCrop = DateTime.Now.ToString("yy");
                var shortMonth = DateTime.Now.ToString("MM");
                var province = ProvinceCombobox.SelectedValue.ToString();
                var prefix = string.Format("INV-{0}{1}{2}", province, shortCrop, shortMonth);
                var maxDeliveryNumber = BusinessLayerServices.InvoiceBL().GetMaxDeliveryNumber(prefix);
                if (maxDeliveryNumber == "")
                {
                    newDelivery = string.Format("{0}0001", prefix);
                }
                else
                {
                    var cutDeliveryNumber = Regex.Match(maxDeliveryNumber, @"(.{4})\s*$").ToString();
                    var newDeliveryNumber = Convert.ToInt32(cutDeliveryNumber) + 1;
                    var newDeliveryString = newDeliveryNumber.ToString().PadLeft(4, '0');
                    newDelivery = string.Format("{0}{1}", prefix, newDeliveryString);
                }
                return newDelivery;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return newDelivery;
            }
        }

        private void CustomerNameTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var w = new Form.Invoice.CustomerWindow();
                w.ShowDialog();
                var customer = w.selectedCustomer;
                if (customer != null)
                {
                    CustomerIDTextBox.Text = customer.CustomerID.ToString();
                    CustomerNameTextBox.Text = customer.CustomerName;
                    CustomerAddressTextBox.Text = customer.Address;
                    CustomerTaxidTextBox.Text = customer.TaxID;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (pd_ryo_issued_item)InformationDataGrid.SelectedItem;
                    if (selected != null)
                    {
                        masterbagList.RemoveAll(r => r.issueitemno == selected.issueitemno);
                        issueditemList.Remove(selected);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var crop = Convert.ToInt16(DateTime.Now.Year);
                var issueno = IssueNumberTextBox.Text;
                if (issueno == "") throw new Exception("Please mention issue number");
                var deliveryno = DeliveryNumberTextBox.Text;
                if (deliveryno == "") throw new Exception("Please mention delivery number");
                var customer = CustomerIDTextBox.Text.Trim();
                if (customer == "") throw new Exception("Please select customer");
                var customerID = Convert.ToInt32(customer);
                var paymentterm = (pd_ryo_paymentterm)PaymenttermCombobox.SelectedItem;
                if (paymentterm == null) throw new Exception("Please mention Payment Term");
                var issueType = IssueTypeCombobox.SelectedValue;
                if (issueType == null) throw new Exception("Please mention Issue Type");

                if (MessageBox.Show("ยืนยันการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var delivery = new pd_ryo_delivery_order();
                    delivery.deliveryno = deliveryno;
                    //delivery.issuedno = issueno;
                    delivery.modified_date = DateTime.Now;
                    delivery.modified_by = user_setting.User.Username;
                    BusinessLayerServices.InvoiceBL().AddDeliveryOrder(delivery);

                    var iss = new pd_ryo_issued();
                    iss.crop = crop;
                    iss.issuedno = issueno;
                    iss.deliveryno = deliveryno;
                    iss.issuetype_id = (int)issueType;
                    iss.issuedate = DateTime.Now;
                    iss.customerid = customerID;
                    iss.issuedstatus = "";
                    iss.billdate = BillDateDatePicker.SelectedDate;
                    iss.billstatus = "";
                    iss.remark = RemarkTextBox.Text;
                    iss.paymentterm_id = paymentterm.paymentterm_id;
                    iss.modifieddate = DateTime.Now;
                    iss.modifieduser = user_setting.User.Username;
                    BusinessLayerServices.InvoiceBL().AddIssue(iss);

                    foreach (var issueitem in issueditemList)
                    {
                        BusinessLayerServices.InvoiceBL().AddIssueItem(issueitem);
                    }

                    foreach (var bag in masterbagList)
                    {

                        BusinessLayerServices.InvoiceBL().UpdateMasterbag(bag);
                    }

                    issueditemList.Clear();
                    masterbagList.Clear();
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var deliveryNumber = DeliveryNumberTextBox.Text;
                var w = new IssueItemWindow(deliveryNumber, issueditemList, masterbagList);
                w.ShowDialog();
                var _issueditem = w.issued_item;
                var _masterbagList = w.masterbagList;
                if (_issueditem != null && _masterbagList.Any())
                {
                    issueditemList.Add(_issueditem);
                    masterbagList.AddRange(_masterbagList);
                }
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = issueditemList;
                TotalAmountTextBox.Text = issueditemList.Count.ToString();
                AddButton.IsEnabled = issueditemList.Count > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_issued_item)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    var w = new MasterbagWindow(selected);
                    w.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
