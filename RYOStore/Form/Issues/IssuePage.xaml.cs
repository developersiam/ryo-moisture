﻿using DomainModelStecDbms;
using RYOStore.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOStore.Form.Issues
{
    /// <summary>
    /// Interaction logic for IssuePage.xaml
    /// </summary>
    public partial class IssuePage : Page
    {
        public IssuePage()
        {
            try
            {
                InitializeComponent();
                CropCombobox.ItemsSource = null;
                CropCombobox.ItemsSource = user_setting.Crops;
                CropCombobox.SelectedValue = DateTime.Now.Year;
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllIssued().OrderBy(o => o.issuedate);
                TotalTextBlock.Text = InformationDataGrid.Items.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_issued)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    var w = new IssueUpdateWindow(selected);
                    w.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0) throw new Exception("โปรดเลือกปีการผลิตจากตัวเลือก");
                //var window = new IssueWindow(Convert.ToInt16(CropCombobox.SelectedValue));
                var w = new IssueUpdateWindow();
                w.ShowDialog();
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    //var selected = (pd_ryo_invoice)InformationDataGrid.SelectedItem;
                    //if (selected != null)
                    //{
                    //    var masterboxlist = BusinessLayerServices.InvoiceBL().GetMasterboxByInvoiceno(selected.invoiceno);
                    //    foreach (var masterBox in masterboxlist)
                    //    {
                    //        masterBox.IssuedCode = "";
                    //        BusinessLayerServices.InvoiceBL().UpdateMasterbox(masterBox);
                    //    }
                    //    BusinessLayerServices.InvoiceBL().RemoveInvoice(selected);
                    //    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    //    ReloadDatagrid();
                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void ExportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_issued)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    ExportHelper.PrintReceipt(selected);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146827284) return;
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
