﻿using DomainModelStecDbms;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Issues
{
    /// <summary>
    /// Interaction logic for MasterbagWindow.xaml
    /// </summary>
    public partial class MasterbagWindow : Window
    {
        List<pd_moisture_masterbag> listedMasterbag;
        public List<pd_moisture_masterbag> selectedBag;
        List<sp_GetAvailableBagForIssue_Result> filterbagList;
        public MasterbagWindow()
        {

        }

        public MasterbagWindow(pd_ryo_issued_item issueditem)
        {
            InitializeComponent();
            SearchStackpanel.Visibility = Visibility.Collapsed;
            SelectButton.Visibility = Visibility.Collapsed;
            InformationDataGrid.ItemsSource = null;
            InformationDataGrid.ItemsSource = BusinessLayerServices.InvoiceBL().GetMasterbagByIssueitemno(issueditem.issueitemno);
        }

        public MasterbagWindow(List<pd_moisture_masterbag> _listedMasterbag)
        {
            InitializeComponent();
            InvoiceNumberTextBox.Focus();
            filterbagList = new List<sp_GetAvailableBagForIssue_Result>();
            listedMasterbag = new List<pd_moisture_masterbag>();
            listedMasterbag = _listedMasterbag;
        }

        private void ReloadDatagrid()
        {
            try
            {
                var masterbagList = new List<sp_GetAvailableBagForIssue_Result>();
                if(!masterbagList.Any()) masterbagList = BusinessLayerServices.InvoiceBL().sp_GetAvailableBagForIssue();

                if (listedMasterbag.Any())
                {
                    foreach (var listed in listedMasterbag)
                    {
                        masterbagList.RemoveAll(r => r.masterbag_bc == listed.masterbag_bc);
                    }
                }

                var invoiceNo = InvoiceNumberTextBox.Text.Trim();
                if (invoiceNo != "") filterbagList = masterbagList.Where(w => w.invoiceno == invoiceNo).ToList();
                else filterbagList = masterbagList;

                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = filterbagList.OrderBy(o => o.invoiceno).ThenBy(t => t.masterbag_bc).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InvoiceNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) ReloadDatagrid();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDatagrid();
        }

        private void InformationDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SelectedItemCount();
        }

        private void InformationDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            SelectedItemCount();
        }

        private void SelectedItemCount()
        {
            try
            {
                var selected = InformationDataGrid.SelectedItems;
                SelectTextblock.Text = string.Format("Select ({0}) Items", selected.Count);
                SelectButton.IsEnabled = selected.Count > 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selectedBag = new List<pd_moisture_masterbag>();
                var selected = InformationDataGrid.SelectedItems;
                if (selected.Count > 0)
                {
                    foreach (var item in selected)
                    {
                        var bc = ((sp_GetAvailableBagForIssue_Result)item).masterbag_bc;
                        var masterBag = BusinessLayerServices.InvoiceBL().GetSingleMasterbag(bc);
                        if (masterBag != null) selectedBag.Add(masterBag);
                    }
                }
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
