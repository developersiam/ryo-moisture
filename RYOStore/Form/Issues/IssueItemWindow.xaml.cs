﻿using DomainModelStecDbms;
using RYOMoistureBL;
using RYOStore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Issues
{
    /// <summary>
    /// Interaction logic for IssueItemWindow.xaml
    /// </summary>
    public partial class IssueItemWindow : Window
    {
        public pd_ryo_issued_item issued_item;
        public List<pd_moisture_masterbag> masterbagList;
        List<pd_ryo_issued_item> _inprogressItemList;
        List<pd_moisture_masterbag> _inprogressBagList;

        public IssueItemWindow()
        {
            InitializeComponent();
        }

        public IssueItemWindow(string deliveryno, List<pd_ryo_issued_item> _issueitemList, List<pd_moisture_masterbag> _masterbagList)
        {
            InitializeComponent();
            DeliveryNumberTextBox.Text = deliveryno;
            _inprogressItemList = _issueitemList;
            _inprogressBagList = _masterbagList;
            masterbagList = new List<pd_moisture_masterbag>();
        }

        private void AddMasterBagButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var restrictedBag = new List<pd_moisture_masterbag>();
                restrictedBag.AddRange(masterbagList);
                restrictedBag.AddRange(_inprogressBagList);
                var window = new MasterbagWindow(restrictedBag);
                window.ShowDialog();
                var selectedBag = window.selectedBag;
                if (selectedBag == null) return;

                foreach (var item in selectedBag)
                {
                    masterbagList.Add(item);
                }
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Summary()
        {
            try
            {

                var deliveryno = DeliveryNumberTextBox.Text;
                var desc = DescriptionTextBox.Text.Trim();
                var unitprice = UnitPriceTextBox.Text.Trim();
                decimal unitPrice = 0;
                var parseUnitPrice = decimal.TryParse(unitprice, out unitPrice);
                var masterbagCount = masterbagList.Count();

                QuatityTextBox.Text = masterbagCount.ToString();
                TotalTextBox.Text = String.Format("{0:n}", (unitPrice * masterbagCount));
                AddButton.IsEnabled = (desc != "") && masterbagList.Any() && parseUnitPrice;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnitPriceTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            Summary();
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (pd_moisture_masterbag)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    masterbagList.Remove(selected);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                masterbagList = masterbagList.OrderBy(o => o.masterbox_bc).ToList();
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = masterbagList;

                Summary();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทีกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var deliveryno = DeliveryNumberTextBox.Text;
                    var desc = DescriptionTextBox.Text.Trim();
                    var unitprice = UnitPriceTextBox.Text.Trim();
                    decimal unitPrice = 0;
                    var parseUnitPrice = decimal.TryParse(unitprice, out unitPrice);
                    if (desc == "") throw new Exception("Please mention description");
                    if (unitprice == "") throw new Exception("Please mention unit price");
                    if (!parseUnitPrice) throw new Exception("Please check unit price");
                    if (!masterbagList.Any()) throw new Exception("Please select master bag");

                    issued_item = new pd_ryo_issued_item();
                    issued_item.deliveryno = deliveryno;
                    issued_item.issueitemno = GetNewIssueItemNo();
                    issued_item.description = desc;
                    issued_item.unitprice = unitPrice;
                    issued_item.modified_date = DateTime.Now;
                    issued_item.modified_by = user_setting.User.Username;

                    foreach (var bag in masterbagList)
                    {
                        bag.issueitemno = issued_item.issueitemno;
                    }
                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private string GetNewIssueItemNo()
        {
            var crop = Convert.ToInt16(DateTime.Now.Year);
            string newIssueItem = "";
            try
            {
                var shortCrop = DateTime.Now.ToString("yy");
                var shortMonth = DateTime.Now.ToString("MM");
                var prefix = string.Format("ISS-I-{0}{1}", shortCrop, shortMonth);
                
                var maxIssueItem = _inprogressItemList.Any() ? _inprogressItemList.Max(m => m.issueitemno) : BusinessLayerServices.InvoiceBL().GetMaxIssueItemNumber(prefix);
                if (maxIssueItem == "")
                {
                    newIssueItem = string.Format("{0}0001", prefix);
                }
                else
                {
                    var cutIssueNumber = Regex.Match(maxIssueItem, @"(.{4})\s*$").ToString();
                    var newIssueNumber = Convert.ToInt32(cutIssueNumber) + 1;
                    var newIssueString = newIssueNumber.ToString().PadLeft(4, '0');
                    newIssueItem = string.Format("{0}{1}", prefix, newIssueString);
                }
                return newIssueItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return newIssueItem;
            }
        }

        private void DescriptionTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            Summary();
        }
    }
}
