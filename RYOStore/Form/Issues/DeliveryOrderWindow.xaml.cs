﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelStecDbms;
using RYOMoistureBL;
using System.Text.RegularExpressions;
using RYOStore.Helper;

namespace RYOStore.Form.Issues
{
    /// <summary>
    /// Interaction logic for DeliveryOrderWindow.xaml
    /// </summary>
    public partial class DeliveryOrderWindow : Window
    {
        public pd_ryo_delivery_order deliveryOrder;
        public List<pd_ryo_issued_item> issueditemList;
        public List<pd_moisture_masterbag> masterbagList;
        List<pd_ryo_delivery_order> _inprogressDeliveryList;

        public DeliveryOrderWindow()
        {
            InitializeComponent();
        }

        public DeliveryOrderWindow(string issueno, string province, List<pd_ryo_delivery_order> _deliveryList, List<pd_ryo_issued_item> _issueitemList, List<pd_moisture_masterbag> _masterbagList)
        {
            InitializeComponent();
            IssueNumberTextBox.Text = issueno;
            issueditemList = new List<pd_ryo_issued_item>();
            masterbagList = new List<pd_moisture_masterbag>();
            _inprogressDeliveryList = new List<pd_ryo_delivery_order>();
            _inprogressDeliveryList = _deliveryList;
            issueditemList = _issueitemList;
            masterbagList = _masterbagList;

            var deliverTypeList = new List<pd_ryo_issued_item>();
            deliverTypeList.Add(new pd_ryo_issued_item { deliveryno = "N", description = "ใบส่งสินค้า" });
            deliverTypeList.Add(new pd_ryo_issued_item { deliveryno = "T", description = "ใบส่งสินค้าชั้วคราว" });
            DeliveryTypeCombobox.ItemsSource = deliverTypeList;
            var provinceList = new List<pd_ryo_issued_item>();
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "CR", description = "Chiang Rai" });
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "CM", description = "Chiang Mai" });
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "LP", description = "Lamphun" });
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "PH", description = "Phrae" });
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "PA", description = "Payao" });
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "SU", description = "Sukhothai" });
            provinceList.Add(new pd_ryo_issued_item { deliveryno = "PE", description = "Phetchabun" });
            ProvinceCombobox.ItemsSource = provinceList;
            ProvinceCombobox.SelectedValue = province;
            DeliveryNumberTextBox.Text = GetNewDeliveryNo();
        }

        private string GetNewDeliveryNo()
        {
            string newDelivery = "";
            try
            {
                var shortCrop = DateTime.Now.ToString("yy");
                var shortMonth = DateTime.Now.ToString("MM");
                var province = ProvinceCombobox.SelectedValue.ToString();
                var prefix = string.Format("INV-{0}{1}{2}", province, shortCrop, shortMonth);
                var maxDeliveryNumber = _inprogressDeliveryList.Any() ? _inprogressDeliveryList.Max(m => m.deliveryno) : BusinessLayerServices.InvoiceBL().GetMaxDeliveryNumber(prefix);
                if (maxDeliveryNumber == "")
                {
                    newDelivery = string.Format("{0}0001", prefix);
                }
                else
                {
                    var cutDeliveryNumber = Regex.Match(maxDeliveryNumber, @"(.{4})\s*$").ToString();
                    var newDeliveryNumber = Convert.ToInt32(cutDeliveryNumber) + 1;
                    var newDeliveryString = newDeliveryNumber.ToString().PadLeft(4, '0');
                    newDelivery = string.Format("{0}{1}", prefix, newDeliveryString);
                }
                return newDelivery;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return newDelivery;
            }
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var deliveryNumber = DeliveryNumberTextBox.Text;
                var w = new IssueItemWindow(deliveryNumber, issueditemList, masterbagList);
                w.ShowDialog();
                var _issueditem = w.issued_item;
                var _masterbagList = w.masterbagList;
                if (_issueditem != null && _masterbagList.Any())
                {
                    issueditemList.Add(_issueditem);
                    masterbagList.AddRange(_masterbagList);
                }
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (pd_ryo_issued_item)InformationDataGrid.SelectedItem;
                    if (selected != null)
                    {
                        masterbagList.RemoveAll(r => r.issueitemno == selected.issueitemno);
                        issueditemList.Remove(selected);
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = issueditemList;
                TotalAmountTextBox.Text = issueditemList.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทีกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var deliveryno = DeliveryNumberTextBox.Text;
                    var issuedno = IssueNumberTextBox.Text;
                    //var deliveryType = (pd_ryo_issued_item)DeliveryTypeCombobox.SelectedItem;

                    if (deliveryno == "") throw new Exception("Please mention description");
                    if (issuedno == "") throw new Exception("Please mention unit price");
                    if (issueditemList == null || !masterbagList.Any()) throw new Exception("Please add delivery detail");

                    deliveryOrder = new pd_ryo_delivery_order();
                    deliveryOrder.deliveryno = deliveryno;
                    //deliveryOrder.issuedno = issuedno;
                    deliveryOrder.modified_date = DateTime.Now;
                    deliveryOrder.modified_by = user_setting.User.Username;

                    Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
