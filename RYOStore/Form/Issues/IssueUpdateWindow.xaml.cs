﻿using DomainModelStecDbms;
using RYOMoistureBL;
using RYOStore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOStore.Form.Issues
{
    /// <summary>
    /// Interaction logic for IssueUpdateWindow.xaml
    /// </summary>
    public partial class IssueUpdateWindow : Window
    {
        List<pd_ryo_issued_item> issueditemList;
        List<pd_moisture_masterbag> masterbagList;
        pd_ryo_issued _issued;

        public IssueUpdateWindow()
        {
            InitializeComponent();
            BindCombobox();
        }

        public IssueUpdateWindow(pd_ryo_issued issued)
        {
            InitializeComponent();
            _issued = issued;
            BindCombobox();
            BindData();
            ReloadDatagrid();
        }

        private void BindCombobox()
        {
            issueditemList = new List<pd_ryo_issued_item>();
            masterbagList = new List<pd_moisture_masterbag>();
            PaymenttermCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllPaymentterm();
            IssueTypeCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllIssueType();
            ProvinceCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllProvince();
            BillDateDatePicker.DisplayDateStart = DateTime.Now;
        }

        private void CustomerNameTextBox_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (_issued == null)
                {
                    var w = new Invoice.CustomerWindow();
                    w.ShowDialog();
                    var customer = w.selectedCustomer;
                    if (customer != null)
                    {
                        CustomerIDTextBox.Text = customer.CustomerID.ToString();
                        CustomerNameTextBox.Text = customer.CustomerName;
                        CustomerAddressTextBox.Text = customer.Address;
                        CustomerTaxidTextBox.Text = customer.TaxID;
                        ProvinceCombobox.SelectedValue = customer.ProvinceCode;
                        IssueNumberTextBox.Text = IdentityHelper.GetNewIssueNo(customer.ProvinceCode);
                        DeliveryNumberTextBox.Text = IdentityHelper.GetNewDeliveryNo(customer.ProvinceCode);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var deliveryNumber = DeliveryNumberTextBox.Text;
                if (deliveryNumber == "") throw new Exception("Please fill issue detail first");
                var w = new IssueItemWindow(deliveryNumber, issueditemList, masterbagList);
                w.ShowDialog();
                var _issueditem = w.issued_item;
                var _masterbagList = w.masterbagList;
                if (_issueditem != null && _masterbagList.Any())
                {
                    issueditemList.Add(_issueditem);
                    masterbagList.AddRange(_masterbagList);
                }
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var updatePhase = issueditemList.Any() || _issued == null;
                var itemList = new List<pd_ryo_issued_item>();
                itemList = BusinessLayerServices.InvoiceBL().GetIssueitemListbyDeliveryno(DeliveryNumberTextBox.Text);
                itemList.AddRange(issueditemList);
                SaveStackPanel.Visibility = updatePhase ? Visibility.Visible : Visibility.Collapsed;
                PrintStackPanel.Visibility = updatePhase ? Visibility.Collapsed : Visibility.Visible;
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = itemList;
                TotalAmountTextBox.Text = itemList.Count.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BindData()
        {
            try
            {
                var issue = BusinessLayerServices.InvoiceBL().GetSingleIsued(_issued.issuedno);
                IssueNumberTextBox.Text = issue.issuedno;
                IssueTypeCombobox.SelectedValue = issue.issuetype_id;
                IssueTypeCombobox.IsEnabled = false;
                DeliveryNumberTextBox.Text = issue.pd_ryo_delivery_order.deliveryno;
                ProvinceCombobox.SelectedValue = issue.pd_ryo_Customer.ProvinceCode;
                ProvinceCombobox.IsEnabled = false;
                CustomerIDTextBox.Text = issue.pd_ryo_Customer.CustomerID.ToString();
                CustomerIDTextBox.IsEnabled = false;
                CustomerNameTextBox.Text = issue.pd_ryo_Customer.CustomerName;
                CustomerNameTextBox.IsEnabled = false;
                CustomerAddressTextBox.Text = issue.pd_ryo_Customer.Address;
                CustomerAddressTextBox.IsEnabled = false;
                CustomerTaxidTextBox.Text = issue.pd_ryo_Customer.TaxID;
                CustomerTaxidTextBox.IsEnabled = false;
                BillDateDatePicker.SelectedDate = issue.billdate;
                BillDateDatePicker.IsEnabled = false;
                PaymenttermCombobox.SelectedValue = issue.paymentterm_id;
                PaymenttermCombobox.IsEnabled = false;
                RemarkTextBox.Text = issue.remark;
                RemarkTextBox.IsEnabled = false;
                TemporaryPrintButton.Visibility = issue.issuetype_id == 1 ? Visibility.Collapsed : Visibility.Visible;
                DeliveryPrintButton.Width = issue.issuetype_id == 1 ? 288 : 135;
                DeliveryPrintContext.Text = issue.issuetype_id == 1 ? "Delivery & Receipt" : "Delivery";
                AddItemButton.IsEnabled = !issue.finished.GetValueOrDefault();
                InformationDataGrid.Columns[0].Visibility = !issue.finished.GetValueOrDefault() ? Visibility.Visible : Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_issued_item)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    var w = new MasterbagWindow(selected);
                    w.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (pd_ryo_issued_item)InformationDataGrid.SelectedItem;
                    if (selected != null)
                    {
                        var isModel = issueditemList.FirstOrDefault(f => f.issueitemno == selected.issueitemno);
                        if (isModel != null)
                        {
                            masterbagList.RemoveAll(r => r.issueitemno == selected.issueitemno);
                            issueditemList.Remove(selected);
                        }
                        else
                        {
                            var returnBagList = BusinessLayerServices.InvoiceBL().GetMasterbagByIssueitemno(selected.issueitemno);
                            foreach (var bag in returnBagList)
                            {
                                bag.issueitemno = null;
                                BusinessLayerServices.InvoiceBL().UpdateMasterbag(bag);
                            }
                            BusinessLayerServices.InvoiceBL().DeleteIssueItem(selected);
                        }
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ยืนยันการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var crop = Convert.ToInt16(DateTime.Now.Year);
                    var issueno = IssueNumberTextBox.Text;
                    if (issueno == "") throw new Exception("Please mention issue number");
                    var deliveryno = DeliveryNumberTextBox.Text;
                    if (deliveryno == "") throw new Exception("Please mention delivery number");
                    var customer = CustomerIDTextBox.Text.Trim();
                    if (customer == "") throw new Exception("Please select customer");
                    var customerID = Convert.ToInt32(customer);
                    var paymentterm = (pd_ryo_paymentterm)PaymenttermCombobox.SelectedItem;
                    if (paymentterm == null) throw new Exception("Please mention Payment Term");
                    var issueType = IssueTypeCombobox.SelectedValue;
                    if (issueType == null) throw new Exception("Please mention Issue Type");

                    var issued = BusinessLayerServices.InvoiceBL().GetSingleIsued(issueno);
                    if (issued == null)
                    {
                        var delivery = new pd_ryo_delivery_order();
                        delivery.deliveryno = deliveryno;
                        //delivery.issuedno = issueno;
                        delivery.modified_date = DateTime.Now;
                        delivery.modified_by = user_setting.User.Username;
                        BusinessLayerServices.InvoiceBL().AddDeliveryOrder(delivery);

                        var iss = new pd_ryo_issued();
                        iss.crop = crop;
                        iss.issuedno = issueno;
                        iss.deliveryno = deliveryno;
                        iss.issuetype_id = (int)issueType;
                        iss.issuedate = DateTime.Now;
                        iss.customerid = customerID;
                        iss.issuedstatus = "";
                        iss.billdate = BillDateDatePicker.SelectedDate;
                        iss.billstatus = "";
                        iss.remark = RemarkTextBox.Text;
                        iss.paymentterm_id = paymentterm.paymentterm_id;
                        iss.modifieddate = DateTime.Now;
                        iss.modifieduser = user_setting.User.Username;
                        BusinessLayerServices.InvoiceBL().AddIssue(iss);
                        _issued = iss;
                        BindData();
                    }

                    foreach (var issueitem in issueditemList)
                    {
                        BusinessLayerServices.InvoiceBL().AddIssueItem(issueitem);
                    }

                    foreach (var bag in masterbagList)
                    {

                        BusinessLayerServices.InvoiceBL().UpdateMasterbag(bag);
                    }

                    issueditemList.Clear();
                    masterbagList.Clear();
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeliveryPrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportHelper.PrintDeliveryOrder("Delivery", _issued);
                if (_issued.issuetype_id == 1)
                {
                    ExportHelper.PrintReceipt(_issued);
                    BindData();
                }
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146827284) return;
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            };
        }

        private void TemporaryPrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ExportHelper.PrintDeliveryOrder("Temporary", _issued);
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146827284) return;
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
