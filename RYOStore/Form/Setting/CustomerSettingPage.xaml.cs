﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RYOMoistureBL;
using DomainModelStecDbms;

namespace RYOStore.Form.Setting
{
    /// <summary>
    /// Interaction logic for StoreSettingPage.xaml
    /// </summary>
    public partial class CustomerSettingPage : Page
    {
        public CustomerSettingPage()
        {
            InitializeComponent();
            ProvinceCombobox.ItemsSource = BusinessLayerServices.InvoiceBL().GetAllProvince();
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = BusinessLayerServices.customerBL().GetAllRYOCustomer();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการบันทึกข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {

                    var newItem = new pd_ryo_Customer
                    {
                        CustomerName = CustomerNameTHTextBox.Text,
                        TaxID = CustomerTaxidTextBox.Text,
                        ProvinceCode = ProvinceCombobox.SelectedValue.ToString(),
                        Address = AddressTextBox.Text,
                        PhoneNo = TelephoneTextBox.Text,
                        Remark = RemarkTextBox.Text
                    };
                    if (CustomerIDTHTextBox.Text != "") newItem.CustomerID = Convert.ToInt32(CustomerIDTHTextBox.Text);
                    if (CustomerIDTHTextBox.Text == "") BusinessLayerServices.customerBL().Insert(newItem);
                    else BusinessLayerServices.customerBL().Update(newItem);
                    MessageBox.Show("บันทึกข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            try
            {
                CustomerIDTHTextBox.Text = "";
                CustomerNameTHTextBox.Text = "";
                CustomerTaxidTextBox.Text = "";
                ProvinceCombobox.SelectedIndex = -1;
                AddressTextBox.Text = "";
                TelephoneTextBox.Text = "";
                RemarkTextBox.Text = "";
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (pd_ryo_Customer)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    CustomerIDTHTextBox.Text = selected.CustomerID.ToString();
                    CustomerNameTHTextBox.Text = selected.CustomerName;
                    CustomerTaxidTextBox.Text = selected.TaxID;
                    ProvinceCombobox.SelectedValue = selected.ProvinceCode;
                    AddressTextBox.Text = selected.Address;
                    TelephoneTextBox.Text = selected.PhoneNo;
                    RemarkTextBox.Text = selected.Remark;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ต้องการลบข้อมูลหรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var selected = (pd_ryo_Customer)InformationDataGrid.SelectedItem;
                    if (selected != null) BusinessLayerServices.customerBL().Delete(selected);
                    MessageBox.Show("ลบข้อมูลสำเร็จ", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ReloadDatagrid();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
