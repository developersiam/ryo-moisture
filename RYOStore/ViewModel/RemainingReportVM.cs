﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOStore.ViewModel
{
    public class RemainingReportVM
    {
        public string invoiceno { get; set; }
        public string masterbox_bc { get; set; }
        public int remainingbag { get; set; }
    }
}
