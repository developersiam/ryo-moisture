﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOStore.ViewModel
{
    public class MasterBagBarcodeTemplateViewModel
    {
        public string Barcode01 { get; set; }
        public string Barcode02 { get; set; }
        public string Barcode03 { get; set; }
        public string Barcode04 { get; set; }
        public string Barcode05 { get; set; }
        public string Barcode06 { get; set; }
        public string Barcode07 { get; set; }
        public string Barcode08 { get; set; }
    }
}
