﻿using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOStore.ViewModel
{
    public class ItemUnitViewModel : pd_ryo_MaterialItem
    {
        public int TotalReceived { get; set; }
        public int OnHand { get; set; }
        public int Balance { get; set; }
    }
}
