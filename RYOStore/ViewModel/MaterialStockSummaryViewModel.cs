﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOStore.ViewModel
{
    public class MaterialStockSummaryViewModel
    {
        public short Crop { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int TotalReceived { get; set; }
        public int TotalIssued { get; set; }
        public int TotalDamage { get; set; }
        public int OnHand { get; set; }
        public decimal RemainingPercentages { get; set; }
    }
}
