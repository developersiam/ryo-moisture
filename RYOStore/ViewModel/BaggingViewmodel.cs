﻿using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOStore.ViewModel
{
    public class BaggingViewmodel
    {
        public pd Pds { get; set; }
        public pd_moisture PdMois { get; set; }
        public string Status { get; set; }
    }
}
