﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOStore.ViewModel
{
    public class MoistureVM
    {
        public int Crop { get; set; }
        public string Pdno { get; set; }
        public decimal? TargetMoisture { get; set; }
        public TimeSpan? TestTime { get; set; }
        public int? Fromcaseno { get; set; } 
        public string Barcode { get; set; }
        public int? Caseno { get; set; }
        public decimal? PackedMoisture { get; set; }
        public string PackedStatus { get; set; }
        public decimal? RetestMoisture { get; set; }
        public string RestestStatus { get; set; }
        public decimal? FeedingMoister { get; set; }
        public string FeedingStatus { get; set; }
        public decimal? MasterBoxMoisture { get; set; }
        public string MasterBoxStatus { get; set; }
        public decimal? BaggingMoisture { get; set; }
        public string BaggingStatus { get; set; }
        public pd_moisture pdMoisture { get; set; }
        //public pd Pds { get; set; }
    }
}
