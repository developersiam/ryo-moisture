﻿using DomainModelStecDbms;
using OfficeOpenXml;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOStore.Helper
{
    public static class ExportHelper
    {
        static Microsoft.Office.Interop.Excel.Application excelApplication = new Microsoft.Office.Interop.Excel.Application();
        static Microsoft.Office.Interop.Excel.Workbook excelWorkBook;
        static Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet;
        static Microsoft.Office.Interop.Excel.Range excelRange;

        public static void PrintReceipt(pd_ryo_issued selected)
        {
            try
            {
                excelWorkBook = excelApplication.Workbooks.Open(@"C:\RYO_Template\Issue_Template.xlsx");

                excelWorkSheet = excelWorkBook.ActiveSheet;
                excelWorkSheet.PageSetup.Zoom = false;
                excelWorkSheet.PageSetup.FitToPagesWide = 1;
                excelApplication.Visible = false;

                var s = BusinessLayerServices.InvoiceBL().GetSingleIsued(selected.issuedno);
                PutDataintoCell("W6", s.issuedno); //เลขที่ Issue
                PutDataintoCell("F9", s.pd_ryo_Customer.CustomerName); //ชื่อลูกค้า
                PutDataintoCell("E10", s.pd_ryo_Customer.Address); //ที่อยู่ลูกค้า
                PutDataintoCell("K11", s.pd_ryo_Customer.TaxID); //เลขประจำตัวผู้เสียภาษีอากร
                PutDataintoCell("E12", s.billdate.GetValueOrDefault().ToString("dd/MM/yyyy")); //วันที่ออกบิล

                int inputRow = 16;
                decimal totalPrice = 0;
                int index = 1;

                var itemList = BusinessLayerServices.InvoiceBL().GetIssueitemListbyDeliveryno(selected.deliveryno);
                foreach (var i in itemList)
                {
                    //var i = BusinessLayerServices.InvoiceBL().GetSingleIssueItem(item);
                    var baglist = BusinessLayerServices.InvoiceBL().GetMasterbagByIssueitemno(i.issueitemno);
                    var sumPrice = i.unitprice.GetValueOrDefault() * baglist.Count;

                    PutDataintoCell("A" + inputRow, index.ToString());
                    PutDataintoCell("C" + inputRow, i.description); //รายละเอียด
                    PutDataintoCell("P" + inputRow, baglist.Count.ToString());//จำนวน
                    PutDataintoCell("S" + inputRow, i.unitprice.ToString()); //ราคาต่อหน่วย
                    PutDataintoCell("V" + inputRow, sumPrice.ToString()); //รวม
                    totalPrice += sumPrice;
                    inputRow++;
                    index++;
                }

                PutDataintoCell("T26", totalPrice.ToString()); //ยอดรวม

                excelWorkSheet.PrintOutEx();
                excelWorkBook.Close(0);
                excelApplication.Quit();

                selected.printdate = DateTime.Now;
                selected.finished = true;
                selected.modifieddate = DateTime.Now;
                selected.modifieduser = user_setting.User.Username;
                BusinessLayerServices.InvoiceBL().UpdateIssue(selected);
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146827284)
                {
                    excelWorkBook.Close(0);
                    excelApplication.Quit();
                }throw;
            }
        }

        public static void PrintDeliveryOrder(string form, pd_ryo_issued issue)
        {
            try
            {
                var delivery = BusinessLayerServices.InvoiceBL().GetSingleDeliveryOrder(issue.deliveryno);
                var templateName = form == "Delivery" ? "Delivery_Order_Template.xlsx" : "Temporary_Delivery_Order_Template.xlsx";
                excelWorkBook = excelApplication.Workbooks.Open(@"C:\RYO_Template\" + templateName);

                excelWorkSheet = excelWorkBook.ActiveSheet;
                excelWorkSheet.PageSetup.Zoom = false;
                excelWorkSheet.PageSetup.FitToPagesWide = 1;
                excelApplication.Visible = false;

                var s = BusinessLayerServices.InvoiceBL().GetSingleIsued(issue.issuedno);
                if (form == "Delivery") PutDataintoCell("C11", s.pd_ryo_Customer.CustomerName);//ชื่อลูกค้า
                if (form == "Temporary") PutDataintoCell("D11", s.pd_ryo_Customer.CustomerName);//ชื่อลูกค้า
                PutDataintoCell("B12", s.pd_ryo_Customer.Address); //ที่อยู่ลูกค้า
                PutDataintoCell("D13", s.pd_ryo_Customer.PhoneNo); //เบอร์โทรศัพท์ลูกค้า
                PutDataintoCell("F14", s.pd_ryo_Customer.TaxID); //เลขประจำตัวผู้เสียภาษีอากร
                //PutDataintoCell("E12", s.billdate.GetValueOrDefault().ToString("dd/MM/yyyy")); //วันที่ออกบิล

                int inputRow = 18;
                decimal totalPrice = 0;
                int index = 1;

                var itemList = new List<pd_ryo_issued_item>();
                itemList = BusinessLayerServices.InvoiceBL().GetIssueitemListbyDeliveryno(issue.deliveryno);
                foreach (var i in itemList)
                {
                    //var i = BusinessLayerServices.InvoiceBL().GetSingleIssueItem(item);
                    var baglist = BusinessLayerServices.InvoiceBL().GetMasterbagByIssueitemno(i.issueitemno);
                    var sumPrice = i.unitprice.GetValueOrDefault() * baglist.Count;

                    PutDataintoCell("A" + inputRow, index.ToString()); // Index
                    PutDataintoCell("B" + inputRow, i.description); //รายละเอียด
                    if (form == "Temporary") PutDataintoCell("G" + inputRow, "");//หน่วยนับ
                    if (form == "Delivery") PutDataintoCell("G" + inputRow, baglist.Count.ToString("N0"));//จำนวน
                    if (form == "Temporary") PutDataintoCell("I" + inputRow, baglist.Count.ToString("N0"));//จำนวน
                    if (form == "Delivery") PutDataintoCell("H" + inputRow, i.unitprice.ToString()); //ราคาต่อหน่วย
                    if (form == "Delivery") PutDataintoCell("I" + inputRow, sumPrice.ToString()); //รวม
                    totalPrice += sumPrice;
                    inputRow++;
                    index++;
                }

                if (form == "Delivery") PutDataintoCell("I27", RegularExpressionHelper.ThaiBahtTextFormat(totalPrice.ToString())); //ยอดรวมอักษร
                if (form == "Delivery") PutDataintoCell("I27", totalPrice.ToString()); //ยอดรวม
                if (form == "Delivery") PutDataintoCell("B28", ""); //paymenterm
                if (form == "Delivery") PutDataintoCell("B29", ""); //หมายเหตุ

                excelWorkSheet.PrintOutEx();
                excelWorkBook.Close(0);
                excelApplication.Quit();

                if (form == "Temporary") delivery.temporary_print_date = DateTime.Now;
                if (form == "Delivery") delivery.delivery_print_date = DateTime.Now;
                delivery.modified_date = DateTime.Now;
                delivery.modified_by = user_setting.User.Username;
                BusinessLayerServices.InvoiceBL().UpdateDeliveryOrder(delivery);
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146827284)
                {
                    excelWorkBook.Close(0);
                    excelApplication.Quit();
                }
                throw;
            }
        }

        private static void PutDataintoCell(string strCell, string value)
        {
            excelRange = excelWorkSheet.Range[strCell];
            excelWorkSheet.Range[strCell].Value = value;
        }


    }
}
