﻿using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RYOStore.Helper
{
    public static class IdentityHelper
    {
        public static string GetNewIssueNo(string provinceCode)
        {
            string newIssue = "";
            try
            {
                var shortCrop = DateTime.Now.ToString("yy");
                var shortMonth = DateTime.Now.ToString("MM");
                var prefix = string.Format("{0}{1}{2}", provinceCode, shortCrop, shortMonth);
                var maxIssue = BusinessLayerServices.InvoiceBL().GetMaxIssueNumber(prefix);
                if (maxIssue == "")
                {
                    newIssue = string.Format("{0}0001", prefix);
                }
                else
                {
                    var cutIssueNumber = Regex.Match(maxIssue, @"(.{4})\s*$").ToString();
                    var newIssueNumber = Convert.ToInt32(cutIssueNumber) + 1;
                    var newIssueString = newIssueNumber.ToString().PadLeft(4, '0');
                    newIssue = string.Format("{0}{1}", prefix, newIssueString);
                }
                return newIssue;
            }
            catch (Exception)
            {
                return newIssue;
            }
        }

        public static string GetNewDeliveryNo(string provinceCode)
        {
            string newDelivery = "";
            try
            {
                var shortCrop = DateTime.Now.ToString("yy");
                var shortMonth = DateTime.Now.ToString("MM");
                var prefix = string.Format("INV-{0}{1}{2}", provinceCode, shortCrop, shortMonth);
                var maxDeliveryNumber = BusinessLayerServices.InvoiceBL().GetMaxDeliveryNumber(prefix);
                if (maxDeliveryNumber == "")
                {
                    newDelivery = string.Format("{0}0001", prefix);
                }
                else
                {
                    var cutDeliveryNumber = Regex.Match(maxDeliveryNumber, @"(.{4})\s*$").ToString();
                    var newDeliveryNumber = Convert.ToInt32(cutDeliveryNumber) + 1;
                    var newDeliveryString = newDeliveryNumber.ToString().PadLeft(4, '0');
                    newDelivery = string.Format("{0}{1}", prefix, newDeliveryString);
                }
                return newDelivery;
            }
            catch (Exception)
            {
                return newDelivery;
            }
        }
    }
}
