﻿using RYOStore.Form;
using RYOStore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOStore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PermissionCheck()
        {
            if (user_setting.User == null)
            {
                MessageBox.Show("โปรดทำการล็อคอินเข้าใช้งานก่อน", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                MainFrame.NavigationService.Navigate(new Form.Login());
                return;
            }

            if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");
        }

        private void OrderMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.Navigate(new Form.Orders.Order());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReceivedMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.Navigate(new Form.Materials.ReceivingsBalanceStock());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void IssuedPerBoxMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.Navigate(new Form.Materials.IssuedCodes());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DamageItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.Navigate(new Form.Materials.Damages());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void LofOffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                user_setting.UserRoles = null;
                MainFrame.NavigationService.Navigate(new Form.Login());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        
        private void ManageCustomerMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.NavigationService.Navigate(new Form.Setting.CustomerSettingPage());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InvoiceMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.NavigationService.Navigate(new Form.Invoice.InvoicePage());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void IssueMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermissionCheck();
                MainFrame.NavigationService.Navigate(new Form.Issues.IssuePage());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RemainingReoprtMenu_Click(object sender, RoutedEventArgs e)
        {
            PermissionCheck();
            MainFrame.NavigationService.Navigate(new Form.Reports.NPIStockReportPage());
        }
    }
}
