﻿using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureBL.InventoryBL
{
    public interface ISupplierBL
    {
        void Add(pd_ryo_inventory_Supplier model);
        void Delete(string supplierCode);
        void Update(pd_ryo_inventory_Supplier model);
        List<pd_ryo_inventory_Supplier> Get();
        pd_ryo_inventory_Supplier GetSingle(string supplierCode);
    }

    public class SupplierBL : ISupplierBL
    {
        InventoryUnitOfWork uow;

        public SupplierBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Supplier model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.SupplierCode))
                    throw new ArgumentException("โปรดระบุ supplier code.");

                if (string.IsNullOrEmpty(model.Name))
                    throw new ArgumentException("โปรดระบุ supplier name.");

                if (string.IsNullOrEmpty(model.Address))
                    throw new ArgumentException("โปรดระบุ address.");

                if (string.IsNullOrEmpty(model.TelephoneNo))
                    throw new ArgumentException("โปรดระบุ telephone number.");

                var item = GetSingle(model.SupplierCode);
                if (item != null)
                    throw new ArgumentException("Dupplicate supplier code.");

                model.ModifiedDate = DateTime.Now;

                uow.supplierRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string supplierCode)
        {
            try
            {
                if (string.IsNullOrEmpty(supplierCode))
                    throw new ArgumentException("Supplier code cannot be empty.");

                var item = GetSingle(supplierCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                //if (uow.poRepo.Get(x => x.ContractPerson == employeeCode).Count() > 0 ||
                //    uow.receiveRepo.Get(x => x.ReceiveUser == employeeCode).Count() > 0 ||
                //    uow.issuedRepo.Get(x => x.IssuedUser == employeeCode).Count() > 0 ||
                //    uow.returnRepo.Get(x => x.ReturnUser == employeeCode).Count() > 0)
                //    throw new ArgumentException("The system cannot delete.This employee use to reference with another transaction.");

                uow.supplierRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Supplier> Get()
        {
            return uow.supplierRepo.Get().ToList();
        }

        public pd_ryo_inventory_Supplier GetSingle(string supplierCode)
        {
            return uow.supplierRepo.GetSingle(x=>x.SupplierCode == supplierCode);
        }

        public void Update(pd_ryo_inventory_Supplier model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.SupplierCode))
                    throw new ArgumentException("โปรดระบุ supplier code.");

                if (string.IsNullOrEmpty(model.Name))
                    throw new ArgumentException("โปรดระบุ supplier name.");

                if (string.IsNullOrEmpty(model.Address))
                    throw new ArgumentException("โปรดระบุ address.");

                if (string.IsNullOrEmpty(model.TelephoneNo))
                    throw new ArgumentException("โปรดระบุ telephone number.");

                var item = GetSingle(model.SupplierCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                item.Name = model.Name;
                item.Address = model.Address;
                item.TelephoneNo = model.TelephoneNo ;
                item.ModifiedDate = DateTime.Now;

                uow.supplierRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
