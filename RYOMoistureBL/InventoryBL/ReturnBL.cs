﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IReturnBL
    {
        pd_ryo_inventory_Return GetSingle(string returnCode);
        List<pd_ryo_inventory_Return> GetByDateRange(DateTime from, DateTime to);
        string GetNewReturnCode();
        void Add(pd_ryo_inventory_Return model);
        void Update(string returnCode, string returnUser, string description,
           DateTime returnDate, string modifiedBy);
        void Delete(string returnCode);

        bool IsDupplicateReturnCode(string issuedCode);
    }

    public class ReturnBL : IReturnBL
    {
        IInventoryUnitOfWork uow;

        public ReturnBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Return model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReturnCode))
                    throw new ArgumentException("โปรดระบุ return code");

                if (string.IsNullOrEmpty(model.ReturnUser))
                    throw new ArgumentException("โปรดระบุ return user");

                if (string.IsNullOrEmpty(model.IssuedCode))
                    throw new ArgumentException("โปรดระบุ issued code");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("โปรดระบุ modified by");

                if (IsDupplicateReturnCode(model.IssuedCode) == true)
                    throw new ArgumentException("Issued code " + model.IssuedCode + " เคยมีการคืนของกลับเข้าคลังสินค้าแล้ว");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                uow.returnRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsDupplicateReturnCode(string issuedCode)
        {
            try
            {
                var items = uow.returnRepo.Get(x => x.IssuedCode == issuedCode);
                if (items.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string returnCode)
        {
            try
            {
                var item = GetSingle(returnCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (item.pd_ryo_inventory_ReturnDetail.Count() > 0)
                    throw new ArgumentException("มีรายการสินค้าอยู่ใน return document นี้ ไม่สามารถลบข้อมูลได้");

                uow.returnRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Return> GetByDateRange(DateTime from, DateTime to)
        {
            return uow.returnRepo
                .Get(x => x.ReturnDate.Day >= from.Day &&
                x.ReturnDate.Month >= from.Month &&
                x.ReturnDate.Year >= from.Year &&
                x.ReturnDate.Day <= to.Day &&
                x.ReturnDate.Month <= to.Month &&
                x.ReturnDate.Year <= to.Year,
                null,
                x => x.pd_ryo_inventory_Employee)
                .ToList();
        }

        public string GetNewReturnCode()
        {
            try
            {
                int max = 0;
                var year = DateTime.Now.Year.ToString();
                var list = uow.returnRepo
                    .Get(x => x.ReturnCode.Substring(2, 4) == year)
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.ReturnCode.Substring(7, 3))
                    })
                    .ToList();

                if (list.Count > 0)
                    max = list.Max(x => x.RunNumber);

                return "RT" + year + "-" + (max + 1).ToString().PadLeft(3, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public pd_ryo_inventory_Return GetSingle(string returnCode)
        {
            return uow.returnRepo.GetSingle(x => x.ReturnCode == returnCode,
                x => x.pd_ryo_inventory_ReturnDetail,
                x => x.pd_ryo_inventory_Employee);
        }

        public void Update(string returnCode, string returnUser, string description, DateTime returnDate, string modifiedBy)
        {
            try
            {
                var item = uow.returnRepo
                    .GetSingle(x => x.ReturnCode == returnCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (string.IsNullOrEmpty(returnCode))
                    throw new ArgumentException("โปรดระบุ return code");

                if (string.IsNullOrEmpty(returnUser))
                    throw new ArgumentException("โปรดระบุ return user");

                item.ReturnDate = returnDate;
                item.ReturnUser = returnUser;
                item.Description = description;
                item.ModifiedBy = modifiedBy;
                item.ModifiedDate = DateTime.Now;

                uow.returnRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
