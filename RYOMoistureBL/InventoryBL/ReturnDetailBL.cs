﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IReturnDetailBL
    {
        void Add(pd_ryo_inventory_ReturnDetail model);
        void Delete(string returnCode, string itemCode, string lotNumber);
        List<pd_ryo_inventory_ReturnDetail> GetByReturnCode(string returnCode);
        List<pd_ryo_inventory_ReturnDetail> GetByIssuedCode(string issuedCode);
    }

    public class ReturnDetailBL : IReturnDetailBL
    {
        InventoryUnitOfWork uow;

        public ReturnDetailBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_ReturnDetail model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReturnCode))
                    throw new ArgumentException("Return code cannot be empty.");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity ไม่ควรน้อยกว่า 1");

                if (model.UnitPrice <= 0)
                    throw new ArgumentException("UnitPrice ไม่ควรน้อยกว่า 1");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("Modified by cannot be empty.");

                if (string.IsNullOrEmpty(model.LotNo))
                    throw new ArgumentException("Lot number cannot be empty.");

                model.ReturnID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;
                uow.returnDetailRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string returnCode,string itemCode,string lotNumber)
        {
            try
            {
                var item = uow.returnDetailRepo
                    .GetSingle(x => x.ReturnCode == returnCode && 
                    x.ItemCode == itemCode && 
                    x.LotNo == lotNumber);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                uow.returnDetailRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_ReturnDetail> GetByIssuedCode(string issuedCode)
        {
            return uow.returnDetailRepo
                .Get(x => x.pd_ryo_inventory_Return.IssuedCode == issuedCode,
                null,
                x => x.pd_ryo_inventory_Return,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory)
                .ToList();
        }

        public List<pd_ryo_inventory_ReturnDetail> GetByReturnCode(string returnCode)
        {
            return uow.returnDetailRepo
                .Get(x => x.ReturnCode == returnCode,
                null,
                x => x.pd_ryo_inventory_Return,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory)
                .ToList();
        }
    }
}
