﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IIssuedReferenceDetailBL
    {
        pd_ryo_inventory_IssuedReferenceDetail GetSingle(Guid id);
        List<pd_ryo_inventory_IssuedReferenceDetail> GetByReferenceCode(string refCode);
        void Add(pd_ryo_inventory_IssuedReferenceDetail model);
        void Delete(Guid id);
    }

    public class IssuedReferenceDetailBL : IIssuedReferenceDetailBL
    {
        InventoryUnitOfWork uow;

        public IssuedReferenceDetailBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_IssuedReferenceDetail model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReferenceCode))
                    throw new ArgumentException("โปรดระบุ Issued Reference Code");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("โปรดระบุ Modified By");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity ไม่ควรน้อยกว่า 1");

                model.ReferenceDetailID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;
                uow.issuedRefDetailRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var item = GetSingle(id);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                uow.issuedRefDetailRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_IssuedReferenceDetail> GetByReferenceCode(string refCode)
        {
            return uow.issuedRefDetailRepo
                .Get(x => x.ReferenceCode == refCode,
                null,
                x => x.pd_ryo_inventory_IssuedReference,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit)
                .ToList();
        }

        public pd_ryo_inventory_IssuedReferenceDetail GetSingle(Guid id)
        {
            return uow.issuedRefDetailRepo
                .GetSingle(x => x.ReferenceDetailID == id,
                x => x.pd_ryo_inventory_IssuedReference,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit);
        }
    }
}
