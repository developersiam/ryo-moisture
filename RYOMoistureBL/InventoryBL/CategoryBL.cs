﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RYOMoistureDAL.UnitOfWork;
using DomainModelStecDbms;

namespace RYOMoistureBL.InventoryBL
{
    public interface ICategoryBL
    {
        void Add(pd_ryo_inventory_ItemCategory model);
        void Delete(string categoryCode);
        void Update(pd_ryo_inventory_ItemCategory model);
        pd_ryo_inventory_ItemCategory GetSingle(string categoryCode);
        List<pd_ryo_inventory_ItemCategory> Get();
    }

    public class CategoryBL : ICategoryBL
    {
        InventoryUnitOfWork uow;

        public CategoryBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_ItemCategory model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ItemCategoryCode))
                    throw new ArgumentException("โปรดระบุ category code.");

                if (string.IsNullOrEmpty(model.ItemCategoryName))
                    throw new ArgumentException("โปรดระบุ category name.");

                var item = GetSingle(model.ItemCategoryCode);
                if (item != null)
                    throw new ArgumentException("Dupplicate category code.");

                model.ModifiedDate = DateTime.Now;

                uow.itemCategoryRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string categoryCode)
        {
            try
            {
                if (string.IsNullOrEmpty(categoryCode))
                    throw new ArgumentException("Category code cannot be empty.");

                var item = GetSingle(categoryCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                ///Check association table.
                ///

                uow.itemCategoryRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_ItemCategory> Get()
        {
            return uow.itemCategoryRepo.Get().ToList();
        }

        public pd_ryo_inventory_ItemCategory GetSingle(string categoryCode)
        {
            return uow.itemCategoryRepo
                .GetSingle(x => x.ItemCategoryCode == categoryCode);
        }

        public void Update(pd_ryo_inventory_ItemCategory model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ItemCategoryCode))
                    throw new ArgumentException("โปรดระบุ category code.");

                if (string.IsNullOrEmpty(model.ItemCategoryName))
                    throw new ArgumentException("โปรดระบุ category name.");

                var item = GetSingle(model.ItemCategoryCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                item.ItemCategoryName = model.ItemCategoryName;
                model.ModifiedDate = DateTime.Now;

                uow.itemCategoryRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
