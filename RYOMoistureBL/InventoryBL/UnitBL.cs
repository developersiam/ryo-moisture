﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IUnitBL
    {
        void Add(pd_ryo_inventory_Unit model);
        void Delete(string unitCode);
        void Update(pd_ryo_inventory_Unit model);
        List<pd_ryo_inventory_Unit> Get();
        pd_ryo_inventory_Unit GetSingle(string unitCode);
    }

    public class UnitBL : IUnitBL
    {
        InventoryUnitOfWork uow;

        public UnitBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Unit model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.UnitCode))
                    throw new ArgumentException("โปรดระบุ unit code.");

                if (string.IsNullOrEmpty(model.UnitName))
                    throw new ArgumentException("โปรดระบุ unit name.");

                if (string.IsNullOrEmpty(model.Description))
                    throw new ArgumentException("โปรดระบุ description.");

                var item = GetSingle(model.UnitCode);
                if (item != null)
                    throw new ArgumentException("Dupplicate unit code.");

                model.ModifiedDate = DateTime.Now;

                uow.unitRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string unitCode)
        {
            try
            {
                if (string.IsNullOrEmpty(unitCode))
                    throw new ArgumentException("Unit code cannot be empty.");

                var item = GetSingle(unitCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                //if (uow.poRepo.Get(x => x.ContractPerson == employeeCode).Count() > 0 ||
                //    uow.receiveRepo.Get(x => x.ReceiveUser == employeeCode).Count() > 0 ||
                //    uow.issuedRepo.Get(x => x.IssuedUser == employeeCode).Count() > 0 ||
                //    uow.returnRepo.Get(x => x.ReturnUser == employeeCode).Count() > 0)
                //    throw new ArgumentException("The system cannot delete.This employee use to reference with another transaction.");

                uow.unitRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Unit> Get()
        {
            return uow.unitRepo.Get().ToList();
        }

        public pd_ryo_inventory_Unit GetSingle(string unitCode)
        {
            return uow.unitRepo.GetSingle(x => x.UnitCode == unitCode);
        }

        public void Update(pd_ryo_inventory_Unit model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.UnitCode))
                    throw new ArgumentException("โปรดระบุ unit code.");

                if (string.IsNullOrEmpty(model.UnitName))
                    throw new ArgumentException("โปรดระบุ unit name.");

                if (string.IsNullOrEmpty(model.Description))
                    throw new ArgumentException("โปรดระบุ description.");

                var item = GetSingle(model.UnitCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                item.UnitName = model.UnitName;
                item.Description = model.Description;
                model.ModifiedDate = DateTime.Now;

                uow.unitRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
