﻿using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureBL.InventoryBL
{
    public interface IPurchaseOrderBL
    {
        void Add(pd_ryo_inventory_PO model);
        void Edit(pd_ryo_inventory_PO model);
        void ChangeStatus(string pono);
        void Delete(string pono);
        string GetNewPONo();
        pd_ryo_inventory_PO GetSingle(string pono);
        List<pd_ryo_inventory_PO> GetByDateRange(DateTime from, DateTime to);
        List<pd_ryo_inventory_PO> GetByStatus(bool status);
    }

    public class PurchaseOrderBL : IPurchaseOrderBL
    {
        InventoryUnitOfWork uow;

        public PurchaseOrderBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_PO model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.SupplierCode))
                    throw new ArgumentException("โปรดระบุ supplier");

                if (string.IsNullOrEmpty(model.ContractPerson))
                    throw new ArgumentException("โปรดระบุผู้ทำสัญญา (Contract person cannot be empty.)");

                if (string.IsNullOrEmpty(model.PONo))
                    throw new ArgumentException("โปรดระบุ pono");

                if (GetSingle(model.PONo) != null)
                    throw new ArgumentException("PONo มีอยู่ในระบบ");

                if (model.DeliveryDate <= model.RequestedDate)
                    throw new ArgumentException("Delivery date จะต้องเป็นวันที่หลังจาก requested date.");

                if (model.RequestedDate >= model.DeliveryDate)
                    throw new ArgumentException("Requested date จะต้องเป็นวันที่ก่อน delivery date.");

                model.POStatus = true;
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                uow.poRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeStatus(string pono)
        {
            try
            {
                var item = GetSingle(pono);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                item.POStatus = !item.POStatus;
                item.ModifiedDate = DateTime.Now;

                uow.poRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string pono)
        {
            try
            {
                var item = GetSingle(pono);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (item.pd_ryo_inventory_PODetail.Count() > 0)
                    throw new ArgumentException("มีรายการสินค้าอยู่ใน PO นี้ ไม่สามารถลบข้อมูลได้");

                uow.poRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_inventory_PO model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.PONo))
                    throw new ArgumentException("pono cannot be null.");

                var po = GetSingle(model.PONo);
                if (po == null)
                    throw new ArgumentException("ไม่พบ PO นี้ในระบบ");

                po.SupplierCode = model.SupplierCode;
                po.Description = model.Description;
                po.QuotationRefNo = model.QuotationRefNo;
                po.PaymentTerm = model.PaymentTerm;
                po.FaxDate = model.FaxDate;
                po.DeliveryDate = model.DeliveryDate;
                po.RequestedDate = model.RequestedDate;
                po.ModifiedBy = model.ModifiedBy;
                po.ModifiedDate = DateTime.Now;

                uow.poRepo.Update(po);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_PO> GetByDateRange(DateTime from, DateTime to)
        {
            return uow.poRepo
                .Get(x => x.CreateDate.Day >= from.Day &&
                x.CreateDate.Month >= from.Month &&
                x.CreateDate.Year >= from.Year &&
                x.CreateDate.Day <= to.Day &&
                x.CreateDate.Month <= to.Month &&
                x.CreateDate.Year <= to.Year)
                .ToList();
        }

        public List<pd_ryo_inventory_PO> GetByStatus(bool status)
        {
            return uow.poRepo.Get(x => x.POStatus == status).ToList();
        }

        public string GetNewPONo()
        {
            try
            {
                int max = 0;
                var year = DateTime.Now.Year.ToString();
                var list = uow.poRepo
                    .Get(x => x.PONo.Substring(7, 4) == year)
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.PONo.Substring(3, 3))
                    })
                    .ToList();

                if (list.Count > 0)
                    max = list.Max(x => x.RunNumber);

                return "NPI" + (max + 1).ToString().PadLeft(3, '0') + "/" + year;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public pd_ryo_inventory_PO GetSingle(string pono)
        {
            return uow.poRepo.GetSingle(x => x.PONo == pono,
                x => x.pd_ryo_inventory_PODetail,
                x => x.pd_ryo_inventory_Supplier);
        }
    }
}
