﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public static class InventoryService
    {
        /// <summary>
        /// RYO inventory system business layer class.
        /// </summary>
        public static ICategoryBL CategoryBL()
        {
            ICategoryBL obj = new CategoryBL();
            return obj;
        }
        public static IEmployeeBL EmployeeBL()
        {
            IEmployeeBL obj = new EmployeeBL();
            return obj;
        }
        public static IIssuedBL IssuedBL()
        {
            IIssuedBL obj = new IssuedBL();
            return obj;
        }
        public static IIssuedDetailBL IssuedDetailsBL()
        {
            IIssuedDetailBL obj = new IssuedDetailBL();
            return obj;
        }
        public static IItemBL ItemBL()
        {
            IItemBL obj = new ItemBL();
            return obj;
        }
        public static IPurchaseOrderBL POBL()
        {
            IPurchaseOrderBL obj = new PurchaseOrderBL();
            return obj;
        }
        public static IPurchasOrderDetailBL PODetailsBL()
        {
            IPurchasOrderDetailBL obj = new PurchaseOrderDetailBL();
            return obj;
        }
        public static IReceiveBL ReceiveBL()
        {
            IReceiveBL obj = new ReceiveBL();
            return obj;
        }
        public static IReceiveDetailBL ReceiveDetailsBL()
        {
            IReceiveDetailBL obj = new ReceiveDetailBL();
            return obj;
        }
        public static IReturnBL ReturnBL()
        {
            IReturnBL obj = new ReturnBL();
            return obj;
        }
        public static IReturnDetailBL ReturnDetailsBL()
        {
            IReturnDetailBL obj = new ReturnDetailBL();
            return obj;
        }
        public static ISupplierBL SupplierBL()
        {
            ISupplierBL obj = new SupplierBL();
            return obj;
        }
        public static ITransactionBL TransactionBL()
        {
            ITransactionBL obj = new TransactionBL();
            return obj;
        }
        public static IUnitBL UnitBL()
        {
            IUnitBL obj = new UnitBL();
            return obj;
        }
        public static IpdsetupBL pdsetupBL()
        {
            IpdsetupBL obj = new pdsetupBL();
            return obj;
        }
        public static IIssuedReferenceBL IssuedReferenceBL()
        {
            IIssuedReferenceBL obj = new IssuedReferenceBL();
            return obj;
        }
        public static IIssuedReferenceDetailBL IssuedReferenceDetailBL()
        {
            IIssuedReferenceDetailBL obj = new IssuedReferenceDetailBL();
            return obj;
        }
        public static IpackedgradeBL packedgradeBL()
        {
            IpackedgradeBL obj = new packedgradeBL();
            return obj;
        }
        public static IReportBL ReportBL()
        {
            IReportBL obj = new ReportBL();
            return obj;
        }
        public static IDamagedBL DamagedBL()
        {
            IDamagedBL obj = new DamagedBL();
            return obj;
        }
        public static IDamagedDetailBL DamagedDetailBL()
        {
            IDamagedDetailBL obj = new DamagedDetailBL();
            return obj;
        }
        /// <returns></returns>
    }
}
