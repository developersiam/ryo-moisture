﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IIssuedBL
    {
        pd_ryo_inventory_Issued GetSingle(string issuedCode);
        List<pd_ryo_inventory_Issued> GetByDateRange(DateTime from, DateTime to);
        string GetNewIssuedCode();
        void Add(string issuedCode, DateTime issuedDate, string issuedUser,
            string topdno, int fromCaseNo, int toCaseNo, string description, string currentUser);
        void Update(string issuedCode, string issuedUser, string description,
           DateTime issuedDate, string topdno, int fromCaseNo, int toCaseNo, string modifiedBy);
        void Delete(string issuedCode);
    }

    public class IssuedBL : IIssuedBL
    {
        InventoryUnitOfWork uow;

        public IssuedBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(string issuedCode, DateTime issuedDate, string issuedUser,
            string topdno, int fromCaseNo, int toCaseNo, string description, string currentUser)
        {
            try
            {
                if (string.IsNullOrEmpty(issuedCode))
                    throw new ArgumentException("โปรดระบุ issued code");

                if (string.IsNullOrEmpty(issuedUser))
                    throw new ArgumentException("โปรดระบุ issued user");

                if (string.IsNullOrEmpty(topdno))
                    throw new ArgumentException("โปรดระบุ topdno");

                if (fromCaseNo <= 0)
                    throw new ArgumentException("from caseno ต้องมากกว่า 0");

                if (toCaseNo <= 0)
                    throw new ArgumentException("to caseno ต้องมากกว่า 0");

                if (toCaseNo < fromCaseNo)
                    throw new ArgumentException("to caseno ต้องมากกว่า from caseno");

                uow.issuedRepo
                    .Add(new pd_ryo_inventory_Issued
                    {
                        IssuedCode = issuedCode,
                        IssuedDate = issuedDate,
                        IssuedUser = issuedUser,
                        ToPdno = topdno,
                        FromCaseNo = fromCaseNo,
                        ToCaseNo = toCaseNo,
                        Description = description,
                        CreateDate = DateTime.Now,
                        IssuedStatus = true,
                        ModifiedBy = currentUser,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string issuedCode)
        {
            try
            {
                var item = GetSingle(issuedCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (item.pd_ryo_inventory_IssuedDetail.Count() > 0)
                    throw new ArgumentException("มีรายการสินค้าอยู่ใน issued document นี้ ไม่สามารถลบข้อมูลได้");

                uow.issuedRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Issued> GetByDateRange(DateTime from, DateTime to)
        {
            return uow.issuedRepo
                .Get(x => x.IssuedDate >= from && x.IssuedDate <= to,
                null,
                x => x.pd_ryo_inventory_Employee)
                .ToList();
        }

        public string GetNewIssuedCode()
        {
            try
            {
                int max = 0;
                var year = DateTime.Now.Year.ToString();
                var list = uow.issuedRepo
                    .Get(x => x.IssuedCode.Substring(2, 4) == year)
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.IssuedCode.Substring(7, 3))
                    })
                    .ToList();

                if (list.Count > 0)
                    max = list.Max(x => x.RunNumber);

                return "IS" + year + "-" + (max + 1).ToString().PadLeft(3, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public pd_ryo_inventory_Issued GetSingle(string issuedCode)
        {
            return uow.issuedRepo.GetSingle(x => x.IssuedCode == issuedCode,
                x => x.pd_ryo_inventory_IssuedDetail,
                x => x.pd_ryo_inventory_Employee);
        }

        public void Update(string issuedCode, string issuedUser, string description,
           DateTime issuedDate, string topdno, int fromCaseNo, int toCaseNo, string modifiedBy)
        {
            try
            {
                var item = uow.issuedRepo
                    .GetSingle(x => x.IssuedCode == issuedCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (string.IsNullOrEmpty(issuedCode))
                    throw new ArgumentException("โปรดระบุ issued code");

                if (string.IsNullOrEmpty(issuedUser))
                    throw new ArgumentException("โปรดระบุ issued user");

                if (string.IsNullOrEmpty(topdno))
                    throw new ArgumentException("โปรดระบุ topdno");

                if (fromCaseNo <= 0)
                    throw new ArgumentException("from caseno ต้องมากกว่า 0");

                if (toCaseNo <= 0)
                    throw new ArgumentException("to caseno ต้องมากกว่า 0");

                if (toCaseNo < fromCaseNo)
                    throw new ArgumentException("to caseno ต้องมากกว่า from caseno");

                item.IssuedDate = issuedDate;
                item.IssuedUser = issuedUser;
                item.Description = description;
                item.ToPdno = topdno;
                item.FromCaseNo = fromCaseNo;
                item.ToCaseNo = toCaseNo;
                item.ModifiedBy = modifiedBy;
                item.ModifiedDate = DateTime.Now;

                uow.issuedRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
