﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IIssuedDetailBL
    {
        void Add(pd_ryo_inventory_IssuedDetail model);
        List<pd_ryo_inventory_IssuedDetail> GetByIssuedCode(string issuedCode);
    }

    public class IssuedDetailBL : IIssuedDetailBL
    {
        InventoryUnitOfWork uow;

        public IssuedDetailBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_IssuedDetail model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.IssuedCode))
                    throw new ArgumentException("Issued code cannot be empty.");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity ไม่ควรน้อยกว่า 1");

                if (model.UnitPrice <= 0)
                    throw new ArgumentException("UnitPrice ไม่ควรน้อยกว่า 1");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("Modified by cannot be empty.");

                if (string.IsNullOrEmpty(model.LotNo))
                    throw new ArgumentException("Lot number cannot be empty.");

                model.IssuedID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;
                uow.issuedDetailRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_IssuedDetail> GetByIssuedCode(string issuedCode)
        {
            return uow.issuedDetailRepo
                .Get(x => x.IssuedCode == issuedCode,
                null,
                x => x.pd_ryo_inventory_Issued,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory)
                .ToList();
        }
    }
}
