﻿using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureBL.InventoryBL
{
    public interface IPurchasOrderDetailBL
    {
        void Add(pd_ryo_inventory_PODetail model);
        void Delete(Guid id);
        List<pd_ryo_inventory_PODetail> GetByPONo(string pono);
        pd_ryo_inventory_PODetail GetSingle(Guid id);
    }

    public class PurchaseOrderDetailBL : IPurchasOrderDetailBL
    {
        InventoryUnitOfWork uow;

        public PurchaseOrderDetailBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_PODetail model)
        {
            try
            {
                //if (uow.poDetailRepo
                //    .Get(x => x.PONo == model.PONo && 
                //    x.ItemCode == model.ItemCode)
                //    .ToList()
                //    .Count() > 0)
                //    throw new ArgumentException("Item code # was " + model.ItemCode + " dupplicated.");

                if (string.IsNullOrEmpty(model.PONo))
                    throw new ArgumentException("โปรดระบุ PO no.");

                if (string.IsNullOrEmpty(model.ItemCode))
                    throw new ArgumentException("โปรดระบุ item code.");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity จะต้องมากกว่า 0");

                if (model.UnitPrice <= 0)
                    throw new ArgumentException("Unit price จะต้องมากกว่า 0");

                uow.poDetailRepo
                    .Add(new pd_ryo_inventory_PODetail
                    {
                        PONo = model.PONo,
                        ItemCode = model.ItemCode,
                        UnitPrice = model.UnitPrice,
                        Quantity = model.Quantity,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedDate = DateTime.Now,
                        PODetailsID = Guid.NewGuid()
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var item = GetSingle(id);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                uow.poDetailRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_PODetail> GetByPONo(string pono)
        {
            return uow.poDetailRepo.Get(x => x.PONo == pono,
                null,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit)
                .ToList();
        }

        public pd_ryo_inventory_PODetail GetSingle(Guid id)
        {
            return uow.poDetailRepo.GetSingle(x => x.PODetailsID == id);
        }
    }
}
