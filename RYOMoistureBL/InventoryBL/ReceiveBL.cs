﻿using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureBL.Models.Inventory;

namespace RYOMoistureBL.InventoryBL
{
    public interface IReceiveBL
    {
        void Add(pd_ryo_inventory_Receive model);
        void Edit(pd_ryo_inventory_Receive model);
        void Delete(string receiveCode);
        string GetNewReceiveCode();
        List<pd_ryo_inventory_Receive> GetByDateRange(DateTime from, DateTime to);
        pd_ryo_inventory_Receive GetSingle(string receiveCode);
        bool IsReceived(string poNo);
    }

    public class ReceiveBL : IReceiveBL
    {
        InventoryUnitOfWork uow;

        public ReceiveBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Receive model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReceiveUser))
                    throw new ArgumentException("โปรดระบุ receive user");

                if (!string.IsNullOrEmpty(model.PONo) && model.ReceiveType == "BF")
                    throw new ArgumentException("กรณีการรับเข้าแบบ Bougth Forward ไม่ต้องระบุ PONo");

                if (string.IsNullOrEmpty(model.PONo) && model.ReceiveType == "PO")
                    throw new ArgumentException("กรณีการรับเข้าจาก PO ต้องระบุ PONo ด้วย");

                model.ReceiveStatus = true;
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                uow.receiveRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string receiveCode)
        {
            try
            {
                var item = GetSingle(receiveCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (item.pd_ryo_inventory_ReceiveDetail.Count() > 0)
                    throw new ArgumentException("มีรายการสินค้าอยู่ใน receive document นี้ ไม่สามารถลบข้อมูลได้");

                uow.receiveRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_inventory_Receive model)
        {
            try
            {
                var edit = GetSingle(model.ReceiveCode);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล receive code " + model.ReceiveCode + " นี้ในระบบ");

                edit.pd_ryo_inventory_Employee = null;
                edit.ReceiveUser = model.ReceiveUser;
                edit.InvoiceNo = model.InvoiceNo;
                edit.Description = model.Description;
                edit.ModifiedBy = model.ModifiedBy;
                edit.ModifiedDate = DateTime.Now;

                uow.receiveRepo.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Receive> GetByDateRange(DateTime from, DateTime to)
        {
            return uow.receiveRepo
                .Get(x => x.CreateDate.Day >= from.Day &&
                x.CreateDate.Month >= from.Month &&
                x.CreateDate.Year >= from.Year &&
                x.CreateDate.Day <= to.Day &&
                x.CreateDate.Month <= to.Month &&
                x.CreateDate.Year <= to.Year)
                .ToList();
        }

        public string GetNewReceiveCode()
        {
            try
            {
                int max = 0;
                var year = DateTime.Now.Year.ToString();
                var list = uow.receiveRepo
                    .Get(x => x.ReceiveCode.Substring(2, 4) == year)
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.ReceiveCode.Substring(7, 3))
                    })
                    .ToList();

                if (list.Count > 0)
                    max = list.Max(x => x.RunNumber);

                return "RC" + year + "-" + (max + 1).ToString().PadLeft(3, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public pd_ryo_inventory_Receive GetSingle(string receiveCode)
        {
            return uow.receiveRepo.GetSingle(x => x.ReceiveCode == receiveCode,
                x => x.pd_ryo_inventory_PO,
                x => x.pd_ryo_inventory_ReceiveDetail,
                x => x.pd_ryo_inventory_Employee);
        }

        public bool IsReceived(string poNo)
        {
            if (uow.receiveRepo.Get(x => x.PONo == poNo).Count() > 0)
                return true;
            else
                return false;
        }
    }
}
