﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IpackedgradeBL
    {
        List<packedgrade> GetByCrop(int crop);
    }
    public class packedgradeBL : IpackedgradeBL
    {
        InventoryUnitOfWork uow;

        public packedgradeBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public List<packedgrade> GetByCrop(int crop)
        {
            return uow.packedgradeRepo.Get(x => x.crop == crop).ToList();
        }
    }
}
