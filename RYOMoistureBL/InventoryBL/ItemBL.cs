﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IItemBL
    {
        void Add(pd_ryo_inventory_Item model);
        void Delete(string itemCode);
        void Update(pd_ryo_inventory_Item model);
        List<pd_ryo_inventory_Item> Get();
        List<pd_ryo_inventory_Item> GetByCategory(string categoryCode);
        pd_ryo_inventory_Item GetSingle(string itemCode);
    }

    public class ItemBL : IItemBL
    {
        InventoryUnitOfWork uow;

        public ItemBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Item model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ItemCode))
                    throw new ArgumentException("โปรดระบุ item code.");

                if (string.IsNullOrEmpty(model.Description))
                    throw new ArgumentException("โปรดระบุ description.");

                if (string.IsNullOrEmpty(model.ItemCategoryCode))
                    throw new ArgumentException("โปรดระบุ category.");

                if (string.IsNullOrEmpty(model.Unit))
                    throw new ArgumentException("โปรดระบุ unit.");

                var item = GetSingle(model.ItemCode);
                if (item != null)
                    throw new ArgumentException("Dupplicate item code.");

                model.ModifiedDate = DateTime.Now;

                uow.itemRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string itemCode)
        {
            try
            {
                if (string.IsNullOrEmpty(itemCode))
                    throw new ArgumentException("item code cannot be empty.");

                var item = GetSingle(itemCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                //if (uow.poRepo.Get(x => x.ContractPerson == employeeCode).Count() > 0 ||
                //    uow.receiveRepo.Get(x => x.ReceiveUser == employeeCode).Count() > 0 ||
                //    uow.issuedRepo.Get(x => x.IssuedUser == employeeCode).Count() > 0 ||
                //    uow.returnRepo.Get(x => x.ReturnUser == employeeCode).Count() > 0)
                //    throw new ArgumentException("The system cannot delete.This employee use to reference with another transaction.");

                uow.itemRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Item> Get()
        {
            return uow.itemRepo
                .Get(
                null,
                null, 
                x => x.pd_ryo_inventory_ItemCategory,
                x => x.pd_ryo_inventory_Unit)
                .ToList();
        }

        public List<pd_ryo_inventory_Item> GetByCategory(string categoryCode)
        {
            return uow.itemRepo
                .Get(x => x.ItemCategoryCode == categoryCode)
                .ToList();
        }

        public pd_ryo_inventory_Item GetSingle(string itemCode)
        {
            return uow.itemRepo.GetSingle(x => x.ItemCode == itemCode);
        }

        public void Update(pd_ryo_inventory_Item model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ItemCode))
                    throw new ArgumentException("โปรดระบุ item code.");

                if (string.IsNullOrEmpty(model.Description))
                    throw new ArgumentException("โปรดระบุ description.");

                if (string.IsNullOrEmpty(model.ItemCategoryCode))
                    throw new ArgumentException("โปรดระบุ category.");

                if (string.IsNullOrEmpty(model.Unit))
                    throw new ArgumentException("โปรดระบุ unit.");

                var item = GetSingle(model.ItemCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                item.Description = model.Description;
                item.Unit = model.Unit;
                item.ItemCategoryCode = model.ItemCategoryCode;
                model.ModifiedDate = DateTime.Now;

                uow.itemRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
