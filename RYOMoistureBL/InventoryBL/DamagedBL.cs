﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IDamagedBL
    {
        pd_ryo_inventory_Damaged GetSingle(string damagedCode);
        List<pd_ryo_inventory_Damaged> GetByDateRange(DateTime from, DateTime to);
        string GetNewDamagedCode();
        void Add(pd_ryo_inventory_Damaged model);
        void Update(string damagedCode, string reportUser, string description, string modifiedBy);
        void Delete(string damagedCode);
        bool IsDupplicateIssuedCode(string issuedCode);
    }

    public class DamagedBL : IDamagedBL
    {
        InventoryUnitOfWork uow;

        public DamagedBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Damaged model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.DamagedCode))
                    throw new ArgumentException("โปรดระบุ damaged code");

                if (string.IsNullOrEmpty(model.ReportUser))
                    throw new ArgumentException("โปรดระบุ report user");

                if (string.IsNullOrEmpty(model.IssuedCode))
                    throw new ArgumentException("โปรดระบุ issued code");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("โปรดระบุ modified by");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                uow.damagedRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string damagedCode)
        {
            try
            {
                var item = GetSingle(damagedCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (item.pd_ryo_inventory_DamagedDetail.Count() > 0)
                    throw new ArgumentException("มีรายการสินค้าอยู่ใน damaged document นี้ ไม่สามารถลบข้อมูลได้");

                uow.damagedRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Damaged> GetByDateRange(DateTime from, DateTime to)
        {
            return uow.damagedRepo
                .Get(x => x.CreateDate.Day >= from.Day &&
                x.CreateDate.Month >= from.Month &&
                x.CreateDate.Year >= from.Year &&
                x.CreateDate.Day <= to.Day &&
                x.CreateDate.Month <= to.Month &&
                x.CreateDate.Year <= to.Year,
                null,
                x => x.pd_ryo_inventory_Issued,
                x => x.pd_ryo_inventory_Employee)
                .ToList();
        }

        public string GetNewDamagedCode()
        {
            try
            {
                int max = 0;
                var year = DateTime.Now.Year.ToString();
                var list = uow.damagedRepo
                    .Get(x => x.DamagedCode.Substring(2, 4) == year)
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.DamagedCode.Substring(7, 3))
                    })
                    .ToList();

                if (list.Count > 0)
                    max = list.Max(x => x.RunNumber);

                return "DM" + year + "-" + (max + 1).ToString().PadLeft(3, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public pd_ryo_inventory_Damaged GetSingle(string damagedCode)
        {
            return uow.damagedRepo.GetSingle(x => x.DamagedCode == damagedCode,
               x => x.pd_ryo_inventory_Issued,
               x => x.pd_ryo_inventory_Employee);
        }

        public bool IsDupplicateIssuedCode(string issuedCode)
        {
            try
            {
                var item = uow.damagedRepo.Get(x => x.IssuedCode == issuedCode);
                if (item.Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(string damagedCode, string reportUser, string description, string modifiedBy)
        {
            try
            {
                var item = uow.damagedRepo
                    .GetSingle(x => x.DamagedCode == damagedCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (string.IsNullOrEmpty(damagedCode))
                    throw new ArgumentException("โปรดระบุ return code");

                if (string.IsNullOrEmpty(reportUser))
                    throw new ArgumentException("โปรดระบุ return user");

                item.ReportUser = reportUser;
                item.Description = description;
                item.ModifiedBy = modifiedBy;
                item.ModifiedDate = DateTime.Now;

                uow.damagedRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
