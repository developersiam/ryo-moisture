﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IDamagedDetailBL
    {
        void Add(pd_ryo_inventory_DamagedDetail model);
        void Delete(string itemCode, string lotNumber, string damagedCode);
        List<pd_ryo_inventory_DamagedDetail> GetByDamagedCode(string damagedCode);
    }

    public class DamagedDetailBL : IDamagedDetailBL
    {
        InventoryUnitOfWork uow;

        public DamagedDetailBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_DamagedDetail model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.DamagedCode))
                    throw new ArgumentException("Damaged code cannot be empty.");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity ไม่ควรน้อยกว่า 1");

                if (model.UnitPrice <= 0)
                    throw new ArgumentException("UnitPrice ไม่ควรน้อยกว่า 1");

                if (string.IsNullOrEmpty(model.Description))
                    throw new ArgumentException("โปรดระบุ Damage remark");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("Modified by cannot be empty.");

                if (string.IsNullOrEmpty(model.LotNo))
                    throw new ArgumentException("Lot number cannot be empty.");

                if (uow.damagedDetailRepo
                    .GetSingle(x => x.ItemCode == model.ItemCode) != null)
                    throw new ArgumentException("มีรายการที่ซ้ำ หากต้องการแก้ไขจำนวนโปรดลบรายการเก่าออกก่อน");

                model.ID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;
                uow.damagedDetailRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string itemCode, string lotNumber, string damagedCode)
        {
            try
            {
                var item = uow.damagedDetailRepo
                    .GetSingle(x => x.ItemCode == itemCode && 
                    x.LotNo == lotNumber && 
                    x.DamagedCode == damagedCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                uow.damagedDetailRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_DamagedDetail> GetByDamagedCode(string damagedCode)
        {
            return uow.damagedDetailRepo
                .Get(x => x.DamagedCode == damagedCode,
                null,
                x => x.pd_ryo_inventory_Damaged,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory)
                .ToList();
        }
    }
}
