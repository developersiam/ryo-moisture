﻿using RYOMoistureDAL.UnitOfWork;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IEmployeeBL
    {
        void Add(pd_ryo_inventory_Employee model);
        void Delete(string employeeCode);
        void Update(pd_ryo_inventory_Employee model);
        pd_ryo_inventory_Employee GetSingle(string employeeCode);
        List<pd_ryo_inventory_Employee> Get();
        List<pd_ryo_inventory_Employee> GetByDepartment(string department);
    }

    public class EmployeeBL : IEmployeeBL
    {
        InventoryUnitOfWork uow;

        public EmployeeBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_Employee model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.EmployeeCode))
                    throw new ArgumentException("โปรดระบุ employee code.");

                if (string.IsNullOrEmpty(model.FirstName))
                    throw new ArgumentException("โปรดระบุ first name.");

                if (string.IsNullOrEmpty(model.LastName))
                    throw new ArgumentException("โปรดระบุ last name.");

                if (string.IsNullOrEmpty(model.Department))
                    throw new ArgumentException("โปรดระบุ department.");

                var item = GetSingle(model.EmployeeCode);
                if (item != null)
                    throw new ArgumentException("Dupplicate employee code.");

                model.ModifiedDate = DateTime.Now;

                uow.employeeRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string employeeCode)
        {
            try
            {
                if (string.IsNullOrEmpty(employeeCode))
                    throw new ArgumentException("Employee cannot be empty.");

                var item = GetSingle(employeeCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                //if (uow.poRepo.Get(x => x.ContractPerson == employeeCode).Count() > 0 ||
                //    uow.receiveRepo.Get(x => x.ReceiveUser == employeeCode).Count() > 0 ||
                //    uow.issuedRepo.Get(x => x.IssuedUser == employeeCode).Count() > 0 ||
                //    uow.returnRepo.Get(x => x.ReturnUser == employeeCode).Count() > 0)
                //    throw new ArgumentException("The system cannot delete.This employee use to reference with another transaction.");

                uow.employeeRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_Employee> Get()
        {
            return uow.employeeRepo.Get().ToList();
        }

        public List<pd_ryo_inventory_Employee> GetByDepartment(string department)
        {
            return uow.employeeRepo.Get(x => x.Department == department).ToList();
        }

        public pd_ryo_inventory_Employee GetSingle(string employeeCode)
        {
            return uow.employeeRepo.GetSingle(x => x.EmployeeCode == employeeCode);
        }

        public void Update(pd_ryo_inventory_Employee model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.EmployeeCode))
                    throw new ArgumentException("โปรดระบุ employee code.");

                if (string.IsNullOrEmpty(model.FirstName))
                    throw new ArgumentException("โปรดระบุ first name.");

                if (string.IsNullOrEmpty(model.LastName))
                    throw new ArgumentException("โปรดระบุ last name.");

                if (string.IsNullOrEmpty(model.Department))
                    throw new ArgumentException("โปรดระบุ department.");

                var item = GetSingle(model.EmployeeCode);
                if (item == null)
                    throw new ArgumentException("This item cannot be null.");

                item.FirstName = model.FirstName;
                item.LastName = model.LastName;
                item.Department = model.Department;
                model.ModifiedDate = DateTime.Now;

                uow.employeeRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
