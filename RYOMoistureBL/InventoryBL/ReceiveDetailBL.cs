﻿using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureBL.Models.Inventory;

namespace RYOMoistureBL.InventoryBL
{
    public interface IReceiveDetailBL
    {
        void Add(pd_ryo_inventory_ReceiveDetail model);
        List<pd_ryo_inventory_ReceiveDetail> GetByReceiveCode(string receiveCode);
        pd_ryo_inventory_ReceiveDetail GetSingle(Guid id);
        string GetLotNumber(string itemCode);
    }

    public class ReceiveDetailBL : IReceiveDetailBL
    {
        InventoryUnitOfWork uow;

        public ReceiveDetailBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_ReceiveDetail model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ItemCode))
                    throw new ArgumentException("Item code cannot ne empty.");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Qantity จะต้องมากกว่า 0");

                if (model.UnitPrice <= 0)
                    throw new ArgumentException("Unit price จะต้องมากกว่า 0");

                if (string.IsNullOrEmpty(model.LotNo))
                    throw new ArgumentException("Lot number cannot be empty.");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("Modified by cannot be empty.");

                //GetLotNumber(model.ItemCode);
                model.ModifiedDate = DateTime.Now;
                uow.receiveDetailRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetLotNumber(string itemCode)
        {
            try
            {
                var year = DateTime.Now.Year.ToString();
                var list = uow.transactionRepo
                    .Get(x => x.LotNo.Substring(0, 4) == year &&
                    x.ItemCode == itemCode)
                    .GroupBy(x => x.LotNo.Substring(5, 3))
                    .Select(x => new m_ItemTransaction
                    {
                        LotNumber = x.Key
                    })
                    .ToList();

                int max = 1;
                if (list.Count() <= 0)
                    return year + "-" + "001";
                else
                {
                    int currentNumber;
                    foreach (var item in list)
                    {
                        currentNumber = Convert.ToInt16(item.LotNumber);
                        if (currentNumber > max)
                            max = currentNumber;
                    }
                }

                return year + "-" + (max + 1).ToString().PadLeft(3, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_ReceiveDetail> GetByReceiveCode(string receiveCode)
        {
            return uow.receiveDetailRepo.Get(x => x.ReceiveCode == receiveCode,
                null,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit,
                x => x.pd_ryo_inventory_Receive)
                .ToList();
        }

        public pd_ryo_inventory_ReceiveDetail GetSingle(Guid id)
        {
            return uow.receiveDetailRepo.GetSingle(x => x.ReceiveDetailID == id,
                x => x.pd_ryo_inventory_Item,
                x => x.pd_ryo_inventory_Receive);
        }
    }
}
