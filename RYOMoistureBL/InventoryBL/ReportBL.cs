﻿using RYOMoistureBL.Models.Inventory;
using RYOMoistureDAL;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IReportBL
    {
        List<m_ItemTransaction> GetBalanceStock();
        List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result> GetBalanceScoreCard(string itemCode,string lotNumber);
    }
    public class ReportBL : IReportBL
    {
        public List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result> GetBalanceScoreCard(string itemCode, string lotNumber)
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetBalanceScoreCard(itemCode, lotNumber);
        }

        public List<m_ItemTransaction> GetBalanceStock()
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetBalanceStock()
                .Select(x => new m_ItemTransaction
                {
                    ItemCode = x.ItemCode,
                    Description = x.Description,
                    UnitName = x.UnitName,
                    CategoryName = x.ItemCategoryName,
                    Quantity = Convert.ToInt32(x.BalanceQty),
                    Price = Convert.ToDecimal(x.BalanceValue)
                })
                .ToList();
        }
    }
}
