﻿using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface ITransactionBL
    {
    }

    public class TransactionBL : ITransactionBL
    {
        InventoryUnitOfWork uow;

        public TransactionBL()
        {
            uow = new InventoryUnitOfWork();
        }
    }
}
