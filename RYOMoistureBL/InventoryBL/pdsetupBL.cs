﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;

namespace RYOMoistureBL.InventoryBL
{
    public interface IpdsetupBL
    {
        List<pdsetup> GetByCrop(short crop);
    }

    public class pdsetupBL : IpdsetupBL
    {
        InventoryUnitOfWork uow;

        public pdsetupBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public List<pdsetup> GetByCrop(short crop)
        {
            return uow.pdsetupRepo.Get(x => x.crop == crop).ToList();
        }
    }
}
