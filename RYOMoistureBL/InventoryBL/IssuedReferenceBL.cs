﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.InventoryBL
{
    public interface IIssuedReferenceBL
    {
        pd_ryo_inventory_IssuedReference GetSingle(string issuedRefCode);
        List<pd_ryo_inventory_IssuedReference> Get();
        void Add(pd_ryo_inventory_IssuedReference model);
        void Delete(string issuedRefCode);
        void Update(pd_ryo_inventory_IssuedReference model);
    }

    public class IssuedReferenceBL : IIssuedReferenceBL
    {
        InventoryUnitOfWork uow;

        public IssuedReferenceBL()
        {
            uow = new InventoryUnitOfWork();
        }

        public void Add(pd_ryo_inventory_IssuedReference model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReferenceCode))
                    throw new ArgumentException("โปรดระบุ Issued Reference Code");

                if (string.IsNullOrEmpty(model.ModifiedBy))
                    throw new ArgumentException("โปรดระบุ Modified By");

                if (GetSingle(model.ReferenceCode) != null)
                    throw new ArgumentException("มี Issued Reference Code นี้แล้วในระบบ");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                uow.issuedRefRepo.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string issuedRefCode)
        {
            try
            {
                if (string.IsNullOrEmpty(issuedRefCode))
                    throw new ArgumentException("Issued Reference Code cannot be empty.");

                var item = GetSingle(issuedRefCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                uow.issuedRefRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_inventory_IssuedReference> Get()
        {
            return uow.issuedRefRepo
                .Get(
                null,
                null, 
                x => x.pd_ryo_inventory_IssuedReferenceDetail)
                .ToList();
        }

        public pd_ryo_inventory_IssuedReference GetSingle(string issuedRefCode)
        {
            return uow.issuedRefRepo
                .GetSingle(x => x.ReferenceCode == issuedRefCode,
                x => x.pd_ryo_inventory_IssuedReferenceDetail);
        }

        public void Update(pd_ryo_inventory_IssuedReference model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReferenceCode))
                    throw new ArgumentException("Ref code cannot be empty.");

                var item = GetSingle(model.ReferenceCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                item.Description = model.Description;
                item.ModifiedBy = model.ModifiedBy;
                item.ModifiedDate = DateTime.Now;
                uow.issuedRefRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
