﻿using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Models.Inventory;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.Helper
{
    public static class PurchaseOrderHelper
    {
        public static List<m_PurchaseOrder> GetByDateRange(DateTime from, DateTime to)
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetPOByDateRange(from, to)
                .Select(x => new m_PurchaseOrder
                {
                    PONo = x.PONo,
                    QuotationRefNo = x.QuotationRefNo,
                    POStatus = x.POStatus,
                    CreateDate = x.CreateDate,
                    FaxDate = x.FaxDate,
                    RequestedDate = x.RequestedDate,
                    DeliveryDate = x.DeliveryDate,
                    Description = x.Description,
                    PaymentTerm = x.PaymentTerm,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedDate = x.ModifiedDate,
                    SupplierCode = x.SupplierCode,
                    SupplierName = x.SupplierName,
                    ContractPersonName = x.ContractPerson,
                    ContractPerson = x.EmployeeCode,
                    TotalPrice = Convert.ToDecimal(x.TotalPrice)
                })
                .ToList();
        }
    }
}
