﻿using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.Helper
{
    public static class ReceiveDetailHelper
    {
        public static List<m_ItemTransaction> GetByReceiveCode(string receiveCode)
        {
            return InventoryService.ReceiveDetailsBL()
                .GetByReceiveCode(receiveCode)
                .Select(x => new m_ItemTransaction
                {
                    ItemCode = x.ItemCode,
                    Description = x.pd_ryo_inventory_Item.Description,
                    UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                    Unit = x.pd_ryo_inventory_Item.Unit,
                    Quantity = x.Quantity,
                    UnitPrice = x.UnitPrice,
                    Price = x.Quantity * x.UnitPrice,
                    LotNumber = x.LotNo
                })
                .ToList();
        }
    }
}
