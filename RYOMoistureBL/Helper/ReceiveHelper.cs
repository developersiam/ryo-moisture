﻿using RYOMoistureBL.Models.Inventory;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.Helper
{
    public static class ReceiveHelper
    {
        public static List<m_Receive> GetByDateRange(DateTime from,DateTime to)
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetReceiveDocumentByDateRange(from,to)
                .Select(x => new m_Receive
                {
                    PONo = x.PONo,
                    ReceiveCode = x.ReceiveCode,
                    ReceiveStatus = x.ReceiveStatus,
                    CreateDate = x.CreateDate,
                    ReceiveDate = x.ReceiveDate,
                    ReceiveType = x.ReceiveType,
                    ReceiveUser = x.ReceiveUserCode,
                    ReceiveUserName = x.ReceiveUserName,
                    Description = x.Description,
                    InvoiceNo = x.InvoiceNo,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedDate = x.ModifiedDate,
                    TotalPrice = Convert.ToDecimal(x.TotalPrice)
                })
                .ToList();
        }
    }
}
