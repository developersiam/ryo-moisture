﻿using RYOMoistureBL.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.Helper
{
    public static class ReceiveTypeHelper
    {
        public static List<m_ReceiveType> GetReceiveType()
        {
            var types = new List<m_ReceiveType>();

            types.Add(new m_ReceiveType { Type = "Brought Forward" });
            types.Add(new m_ReceiveType { Type = "PO" });

            return types;
        }
    }
}
