﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureDAL;
using RYOMoistureBL.Models.Inventory;

namespace RYOMoistureBL.Helper
{
    public static class ItemHelper
    {
        public static List<m_ItemTransaction> GetByOnHand()
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetItemOnhand()
                .Select(x => new m_ItemTransaction
                {
                    ItemCode = x.ItemCode,
                    Description = x.Description,
                    Unit = x.UnitCode,
                    UnitName = x.UnitName,
                    ItemCategoryCode = x.ItemCategoryCode,
                    CategoryName = x.ItemCategoryName,
                    UnitPrice = x.UnitPrice,
                    Quantity = Convert.ToInt32(x.Quantity),
                    Price = Convert.ToDecimal(x.Price),
                    LotNumber = x.LotNo
                }).ToList();
        }

        public static List<m_ItemTransaction> GetByIssuedCode(string issuedCode)
        {
            return new List<m_ItemTransaction>();
        }
    }
}
