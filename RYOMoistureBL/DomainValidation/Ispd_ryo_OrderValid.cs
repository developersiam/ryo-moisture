﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_OrderValid : Validator<pd_ryo_Order>
    {
        public Ispd_ryo_OrderValid()
        {

            Add("PONoIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.PONo), "PONo field is missing"));
            Add("InvoiceNoIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.InvoiceNo), "InvoiceNo field is missing"));
            Add("CustomerIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.Customer), "Customer field is missing"));
            Add("CropIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.Crop), "Crop field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.Quantity), "Quantity field is missing"));
            Add("UnitPriceIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.UnitPrice), "UnitPrice field is missing"));
            Add("ProductionYearIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.ProductionYear), "ProductionYear field is missing"));
            Add("ShippingCaseFromIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.ShippingCaseFrom), "ShippingCaseFrom field is missing"));
            Add("ShippingCaseToIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.ShippingCaseTo), "ShippingCaseTo field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.CreateBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_Order>(new IsNotNull<pd_ryo_Order>(x => x.ModifiedBy), "ModifiedBy field is missing"));

            Add("QuantityValid", new Rule<pd_ryo_Order>(new IsExpressionValid<pd_ryo_Order>(x => x.Quantity > 0), "Quantity จะต้องมากกว่า 0"));
            Add("CasenoRangeValid", new Rule<pd_ryo_Order>(new IsExpressionValid<pd_ryo_Order>(x => x.ShippingCaseFrom <= x.ShippingCaseTo), "การกำหนดหมายเลขกล่องระหว่าง ShippingCaseNoFrom กับ ShippingCaseNoTo ไม่ถูกต้อง"));
            Add("ShippingCasenoFromValid", new Rule<pd_ryo_Order>(new IsExpressionValid<pd_ryo_Order>(x => x.ShippingCaseFrom > 0), "หมายเลขกล่องเริ่มต้นจะต้องเริ่มต้นที่หมายเลช 1 เป็นต้นไป"));
            Add("ShippingCasenoCompareQuantityValid", new Rule<pd_ryo_Order>(new IsExpressionValid<pd_ryo_Order>(x => x.ShippingCaseTo - x.ShippingCaseFrom + 1 == x.Quantity), "การกำหนดหมายเลขกล่องระหว่าง ShippingCaseNoFrom กับ ShippingCaseNoTo ไม่ถูกต้อง เมื่อนับรวมกันแล้วไม่เท่ากับจำนวนกล่องที่ระบุ"));
            Add("UnitPriceValid", new Rule<pd_ryo_Order>(new IsExpressionValid<pd_ryo_Order>(x => x.UnitPrice >= 0), "UnitPrice จะต้องไม่น้อยกว่า 0"));
            Add("ProductionYearLenghtValid", new Rule<pd_ryo_Order>(new IsExpressionValid<pd_ryo_Order>(x => x.ProductionYear.Length == 2), "Production Year จะต้องเป็นเลข 2 หลัก"));
        }
    }
}
