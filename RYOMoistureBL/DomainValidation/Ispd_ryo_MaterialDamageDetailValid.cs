﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialDamageDetailValid : Validator<pd_ryo_MaterialDamageDetail>
    {
        public Ispd_ryo_MaterialDamageDetailValid()
        {
            Add("ItemCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamageDetail>(new IsNotNull<pd_ryo_MaterialDamageDetail>(x => x.ItemCode), "ItemCode field is missing"));
            Add("DamageCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamageDetail>(new IsNotNull<pd_ryo_MaterialDamageDetail>(x => x.DamageCode), "DamageCode field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamageDetail>(new IsNotNull<pd_ryo_MaterialDamageDetail>(x => x.Quantity), "Quantity field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamageDetail>(new IsNotNull<pd_ryo_MaterialDamageDetail>(x => x.RecordBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamageDetail>(new IsNotNull<pd_ryo_MaterialDamageDetail>(x => x.ModifiedBy), "ModifiedBy field is missing"));

            Add("QuantityValid", new Rule<pd_ryo_MaterialDamageDetail>(new IsExpressionValid<pd_ryo_MaterialDamageDetail>(x => x.Quantity > 0), "Quantity จะต้องมีค่ามากกว่า 0"));
        }
    }
}
