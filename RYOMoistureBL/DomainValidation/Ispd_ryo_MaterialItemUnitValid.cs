﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialItemUnitValid : Validator<pd_ryo_MaterialItemUnit>
    {
        public Ispd_ryo_MaterialItemUnitValid()
        {
            Add("ItemCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItemUnit>(new IsNotNull<pd_ryo_MaterialItemUnit>(x => x.ItemCode), "ItemCode field is missing"));
            Add("UnitCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItemUnit>(new IsNotNull<pd_ryo_MaterialItemUnit>(x => x.UnitCode), "UnitCode field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItemUnit>(new IsNotNull<pd_ryo_MaterialItemUnit>(x => x.CreateBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItemUnit>(new IsNotNull<pd_ryo_MaterialItemUnit>(x => x.ModifiedBy), "ModifiedBy field is missing"));
        }
    }
}
