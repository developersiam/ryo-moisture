﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialReceivingValid : Validator<pd_ryo_MaterialReceiving>
    {
        public Ispd_ryo_MaterialReceivingValid()
        {
            Add("CropIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.Crop), "Crop field is missing"));
            Add("CustomerIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.Customer), "Customer field is missing"));
            Add("ReceivingCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.ReceivingCode), "ReceivingCode field is missing"));
            Add("ReceivingTypeCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.ReceivingTypeCode), "ReceivingTypeCode field is missing"));
            Add("ReceivedDateIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.ReceivedDate), "ReceivedDate field is missing"));
            Add("ItemCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.ItemCode), "Item field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.Quantity), "Quantity field is missing"));
            Add("ReceivedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.ReceivedBy), "ReceivedBy field is missing"));            
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialReceiving>(new IsNotNull<pd_ryo_MaterialReceiving>(x => x.ModifiedBy), "ModifiedBy field is missing"));

            Add("QuantityValid", new Rule<pd_ryo_MaterialReceiving>(new IsExpressionValid<pd_ryo_MaterialReceiving>(x => x.Quantity > 0), "Quantity จะต้องมีจำนวนอย่างน้อย 1 pcs. ขึ้นไป"));
        }
    }
}
