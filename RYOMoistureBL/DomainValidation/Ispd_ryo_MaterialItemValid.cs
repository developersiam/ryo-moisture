﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialItemValid : Validator<pd_ryo_MaterialItem>
    {
        public Ispd_ryo_MaterialItemValid() {
            Add("ItemCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItem>(new IsNotNull<pd_ryo_MaterialItem>(x => x.ItemCode), "ItemCode field is missing"));
            Add("ItemNameIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItem>(new IsNotNull<pd_ryo_MaterialItem>(x => x.ItemName), "ItemName field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItem>(new IsNotNull<pd_ryo_MaterialItem>(x => x.CreateBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialItem>(new IsNotNull<pd_ryo_MaterialItem>(x => x.ModifiedBy), "ModifiedBy field is missing"));

            Add("ItemCodeLenghtValid", new Rule<pd_ryo_MaterialItem>(new IsExpressionValid<pd_ryo_MaterialItem>(x => x.ItemCode.Length == 5), "Item Code จะต้องไม่เกิน 5 ตัวอักษร"));
        }
    }
}
