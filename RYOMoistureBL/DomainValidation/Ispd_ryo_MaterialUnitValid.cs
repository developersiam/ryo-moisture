﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialUnitValid : Validator<pd_ryo_MaterialUnit>
    {
        public Ispd_ryo_MaterialUnitValid()
        {
            Add("UnitNameIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialUnit>(new IsNotNull<pd_ryo_MaterialUnit>(x => x.UnitName), "UnitName field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialUnit>(new IsNotNull<pd_ryo_MaterialUnit>(x => x.CreateBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialUnit>(new IsNotNull<pd_ryo_MaterialUnit>(x => x.ModifiedBy), "ModifiedBy field is missing"));
        }
    }
}
