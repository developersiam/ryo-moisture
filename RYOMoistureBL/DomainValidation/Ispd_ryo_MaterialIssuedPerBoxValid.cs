﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialIssuedPerBoxValid : Validator<pd_ryo_MaterialIssuedPerBox>
    {
        public Ispd_ryo_MaterialIssuedPerBoxValid()
        {
            Add("ItemCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialIssuedPerBox>(new IsNotNull<pd_ryo_MaterialIssuedPerBox>(x => x.ItemCode), "ItemCode field is missing"));
            Add("IssuedCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialIssuedPerBox>(new IsNotNull<pd_ryo_MaterialIssuedPerBox>(x => x.IssuedCode), "IssuedCode field is missing"));
            Add("PcsPerBoxIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialIssuedPerBox>(new IsNotNull<pd_ryo_MaterialIssuedPerBox>(x => x.PcsPerBox), "PcsPerBox field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialIssuedPerBox>(new IsNotNull<pd_ryo_MaterialIssuedPerBox>(x => x.CreateBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialIssuedPerBox>(new IsNotNull<pd_ryo_MaterialIssuedPerBox>(x => x.ModifiedBy), "ModifiedBy field is missing"));

            Add("PcsPerBoxValid", new Rule<pd_ryo_MaterialIssuedPerBox>(new IsExpressionValid<pd_ryo_MaterialIssuedPerBox>(x => x.PcsPerBox > 0), "Pcs per box จะต้องมากกว่า 0"));
        }
    }
}
