﻿using DomainModelStecDbms;
using DomainValidation.Validation;
using RYOMoistureBL.DomainValidation.Expression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.DomainValidation
{
    public class Ispd_ryo_MaterialDamageValid : Validator<pd_ryo_MaterialDamage>
    {
        public Ispd_ryo_MaterialDamageValid()
        {
            Add("DamageCodeIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamage>(new IsNotNull<pd_ryo_MaterialDamage>(x => x.DamageCode), "DamageCode field is missing"));
            Add("DamageReasonIDIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamage>(new IsNotNull<pd_ryo_MaterialDamage>(x => x.DamageReasonID), "DamageReasonID field is missing"));
            Add("pdnoIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamage>(new IsNotNull<pd_ryo_MaterialDamage>(x => x.pdno), "pdno field is missing"));
            Add("CreateByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamage>(new IsNotNull<pd_ryo_MaterialDamage>(x => x.CreateBy), "CreateBy field is missing"));
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<pd_ryo_MaterialDamage>(new IsNotNull<pd_ryo_MaterialDamage>(x => x.ModifiedBy), "ModifiedBy field is missing"));
        }
    }
}
