﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface IInvoiceBL
    {
        string GetMaxInvoiceNumber(short crop);
        List<sp_GetInvoiceMasterboxByShipping_Result> GetMasterboxListForInvoice(string spno);
        pd_moisture_masterbox GetSingleMasterbox(string masterbox_bc);
        List<pd> GetMasterboxByInvoiceno(string invoiceno);
        void UpdateMasterbox(pd_moisture_masterbox masterbox);
        pd_ryo_invoice GetSingleInvoice(string invoiceNo);
        List<pd_ryo_invoice> GetInvoiceListByCrop(short crop);
        void AddInvoice(pd_ryo_invoice invoice);
        void RemoveInvoice(pd_ryo_invoice invoice);
        List<pd_moisture_masterbag> GetMasterbagByMasterbox(string masterboxBC);
        List<pd_moisture_masterbag> GetMasterbagByIssueitemno(string issueitemno);
        List<pd_moisture_masterbag> GetRemaingingBagReport(int status_id);
        pd_moisture_masterbag GetSingleMasterbag(string masterbagBC);
        void UpdateMasterbag(pd_moisture_masterbag masterbag);
        List<sp_GetAvailableBagForIssue_Result> sp_GetAvailableBagForIssue();
        string GetMaxIssueNumber(string startsWith);
        string GetMaxIssueItemNumber(string startsWith);
        List<pd_ryo_issued_item> GetIssueitemListbyDeliveryno(string deliveryno);
        pd_ryo_issued_item GetSingleIssueItem(pd_ryo_issued_item item);
        void AddIssueItem(pd_ryo_issued_item item);
        void DeleteIssueItem(pd_ryo_issued_item item);
        void AddIssue(pd_ryo_issued issue);
        void UpdateIssue(pd_ryo_issued issue);
        List<pd_ryo_issued> GetIssued();
        pd_ryo_issued GetSingleIsued(string issueno);
        List<pd_ryo_paymentterm> GetPaymentterm();
        pd GetSinglePDByBarcode(string barcode);
        pd_ryo_delivery_order GetSingleDeliveryOrder(string deliveryno);
        string GetMaxDeliveryNumber(string startWith);
        void AddDeliveryOrder(pd_ryo_delivery_order item);
        void UpdateDeliveryOrder(pd_ryo_delivery_order item);
        List<pd_ryo_province> GetProvince();
        List<pd_ryo_issuetype> GetIssueType();
    }

    public class InvoiceBL : IInvoiceBL
    {
        public void AddInvoice(pd_ryo_invoice invoice)
        {
            var invoiced = GetSingleInvoice(invoice.invoiceno);
            if (invoiced == null)
            {
                DataAccessLayerServices.pd_ryo_InvoiceRepository().Add(invoice);
            }
            else throw new Exception("Invoice Already exist.");
        }

        public List<pd_ryo_invoice> GetInvoiceListByCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_InvoiceRepository().Get(g => g.crop == crop).ToList();
        }

        public List<sp_GetInvoiceMasterboxByShipping_Result> GetMasterboxListForInvoice(string spno)
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetInvoiceMasterboxByShipping(spno);
        }

        public List<pd> GetMasterboxByInvoiceno(string invoiceno)
        {
            var result = new List<pd>();
            var bagList = DataAccessLayerServices.PDMoistureMasterbagRepository().Get(g => g.invoiceno == invoiceno).ToList();
            var groupedBox = bagList.GroupBy(g => g.masterbox_bc).ToList();
            foreach (var box in groupedBox)
            {
                var singleBox = GetSinglePDByBarcode(box.Key.ToString());
                if (singleBox != null) result.Add(singleBox);
            }
            return result;
        }

        public string GetMaxInvoiceNumber(short crop)
        {
            var invoiceList = DataAccessLayerServices.pd_ryo_InvoiceRepository().Get(g => g.crop == crop).ToList();
            //var y = x.Where(w => w.invoiceno != "").ToList();
            if (!invoiceList.Any()) return "";
            var maxInvoice = invoiceList.Max(m => m.invoiceno);
            return maxInvoice.ToString();
        }

        public string GetMaxIssueNumber(string startsWith)
        {
            var issueList = DataAccessLayerServices.pd_ryo_IssuedRepository().Get(g => g.issuedno.StartsWith(startsWith)).ToList();
            if (!issueList.Any()) return "";
            var maxIssue = issueList.Max(m => m.issuedno);
            return maxIssue.ToString();
        }

        public string GetMaxIssueItemNumber(string startsWith)
        {
            var issueList = DataAccessLayerServices.PDIssuedItemRepository().Get(g => g.issueitemno.StartsWith(startsWith)).ToList();
            if (!issueList.Any()) return "";
            var maxIssueItem = issueList.Max(m => m.issueitemno);
            return maxIssueItem.ToString();
        }

        public pd_ryo_invoice GetSingleInvoice(string invoiceNo)
        {
            return DataAccessLayerServices.pd_ryo_InvoiceRepository().GetSingle(g => g.invoiceno == invoiceNo);
        }

        public pd_moisture_masterbox GetSingleMasterbox(string masterbox_bc)
        {
            return DataAccessLayerServices.PDMoistureMasterboxRepository().GetSingle(g => g.masterbox_bc == masterbox_bc);
        }

        public void UpdateMasterbox(pd_moisture_masterbox masterbox)
        {
            var selected = GetSingleMasterbox(masterbox.masterbox_bc);
            if (selected != null)
            {
                selected.IssuedCode = masterbox.IssuedCode;
                DataAccessLayerServices.PDMoistureMasterboxRepository().Update(selected);
            }
            else throw new Exception("Masterbox bc : " + masterbox.masterbox_bc + " not found");
        }

        public void RemoveInvoice(pd_ryo_invoice invoice)
        {
            var invoiced = GetSingleInvoice(invoice.invoiceno);
            if (invoiced != null)
            {
                DataAccessLayerServices.pd_ryo_InvoiceRepository().Remove(invoiced);
            }
            else throw new Exception("Invoice not found.");
        }

        public List<pd_moisture_masterbag> GetMasterbagByMasterbox(string masterboxBC)
        {
            return DataAccessLayerServices.PDMoistureMasterbagRepository().Get(g => g.masterbox_bc == masterboxBC).ToList();
        }

        public void UpdateMasterbag(pd_moisture_masterbag masterbag)
        {
            var selected = GetSingleMasterbag(masterbag.masterbag_bc);
            if (selected != null)
            {
                //selected.invoiceno = masterbag.invoiceno;
                DataAccessLayerServices.PDMoistureMasterbagRepository().Update(masterbag);
            }
            else throw new Exception("Masterbag bc : " + masterbag.masterbox_bc + " not found");
        }

        public pd_moisture_masterbag GetSingleMasterbag(string masterbagBC)
        {
            return DataAccessLayerServices.PDMoistureMasterbagRepository().GetSingle(g => g.masterbag_bc == masterbagBC);
        }

        public List<sp_GetAvailableBagForIssue_Result> sp_GetAvailableBagForIssue()
        {
            return DataAccessLayerServices.StoreProcedureRepository().sp_GetAvailableBagForIssue().ToList();
        }

        public void AddIssueItem(pd_ryo_issued_item item)
        {
            var selected = GetSingleIssueItem(item);
            if (selected == null) DataAccessLayerServices.PDIssuedItemRepository().Add(item);
        }

        public pd_ryo_issued_item GetSingleIssueItem(pd_ryo_issued_item item)
        {
            return DataAccessLayerServices.PDIssuedItemRepository().GetSingle(g => g.issueitemno == item.issueitemno);
        }

        public List<pd_ryo_issued_item> GetIssueitemListbyDeliveryno(string deliveryno)
        {
            return DataAccessLayerServices.PDIssuedItemRepository().Get(g => g.deliveryno == deliveryno).ToList();
        }

        public void AddIssue(pd_ryo_issued issue)
        {
            var selected = GetSingleIsued(issue.issuedno);
            if (selected == null) DataAccessLayerServices.pd_ryo_IssuedRepository().Add(issue);
        }

        public List<pd_ryo_issued> GetIssued()
        {
            return DataAccessLayerServices.pd_ryo_IssuedRepository()
                .Get(null,
                null,
                g => g.pd_ryo_Customer,
                g => g.pd_ryo_issuetype)
                .ToList();
        }

        public pd_ryo_issued GetSingleIsued(string issueno)
        {
            return DataAccessLayerServices.pd_ryo_IssuedRepository()
                .GetSingle(g => g.issuedno == issueno,
                g => g.pd_ryo_Customer,
                g => g.pd_ryo_delivery_order);
        }

        public List<pd_moisture_masterbag> GetMasterbagByIssueitemno(string issueitemno)
        {
            var result = DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Get(g => g.issueitemno == issueitemno)
                .ToList();
            return result;
        }

        public List<pd_ryo_paymentterm> GetPaymentterm()
        {
            return DataAccessLayerServices.Pd_ryo_paymenttermRepository()
                .Get()
                .ToList();
        }

        public pd GetSinglePDByBarcode(string barcode)
        {
            return DataAccessLayerServices.PDRepository()
                .GetSingle(g => g.bc == barcode);
        }

        public List<pd_moisture_masterbag> GetRemaingingBagReport(int status_id)
        {
            var invoicedBag = new List<pd_moisture_masterbag>();
            invoicedBag = DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Get(g => g.invoiceno != null).ToList();
            if (status_id == 1)
                invoicedBag = invoicedBag
                    .Where(w => w.issueitemno != null)
                    .ToList();

            if (status_id == 2)
                invoicedBag = invoicedBag
                    .Where(w => w.issueitemno == null)
                    .ToList();

            return invoicedBag;
        }

        public string GetMaxDeliveryNumber(string startWith)
        {
            var searchList = DataAccessLayerServices.Pd_ryo_deliveryorderepository()
                .Get(g => g.deliveryno.StartsWith(startWith))
                .ToList();
            if (!searchList.Any()) return "";

            var maxItemNumber = searchList.Max(m => m.deliveryno);
            return maxItemNumber.ToString();
        }

        public pd_ryo_delivery_order GetSingleDeliveryOrder(string deliveryno)
        {
            return DataAccessLayerServices.Pd_ryo_deliveryorderepository()
                .GetSingle(g => g.deliveryno == deliveryno,
                g => g.pd_ryo_issued);
        }

        public void AddDeliveryOrder(pd_ryo_delivery_order item)
        {
            var selected = GetSingleDeliveryOrder(item.deliveryno);
            if (selected == null)
                DataAccessLayerServices.Pd_ryo_deliveryorderepository().Add(item);
        }

        public void UpdateIssue(pd_ryo_issued issue)
        {
            var selected = GetSingleIsued(issue.issuedno);
            if (selected != null)
                DataAccessLayerServices.pd_ryo_IssuedRepository().Update(issue);
        }

        public void DeleteIssueItem(pd_ryo_issued_item item)
        {
            var selected = GetSingleIssueItem(item);
            if (selected != null)
                DataAccessLayerServices.PDIssuedItemRepository().Remove(item);
        }

        public void UpdateDeliveryOrder(pd_ryo_delivery_order item)
        {
            var selected = GetSingleDeliveryOrder(item.deliveryno);
            if (selected != null)
                DataAccessLayerServices.Pd_ryo_deliveryorderepository().Update(item);
        }

        public List<pd_ryo_province> GetProvince()
        {
            return DataAccessLayerServices.Pd_ryo_provincerepository().Get().ToList();
        }

        public List<pd_ryo_issuetype> GetIssueType()
        {
            return DataAccessLayerServices.pd_ryo_issuetyperepository().Get().ToList();
        }
    }
}
