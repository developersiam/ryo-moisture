﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialItemUnitBL
    {
        void Add(pd_ryo_MaterialItemUnit model);
        void Delete(Guid id);
        void Edit(pd_ryo_MaterialItemUnit model);
        List<pd_ryo_MaterialItemUnit> GetAll();
        pd_ryo_MaterialItemUnit GetSingle(Guid id);
    }

    public class pd_ryo_MaterialItemUnitBL : Ipd_ryo_MaterialItemUnitBL
    {
        public void Add(pd_ryo_MaterialItemUnit model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialItemUnitValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = GetAll();
                if (list.Where(x => x.ItemCode == model.ItemCode && 
                x.UnitCode == model.UnitCode).Count() > 0)
                    throw new ArgumentNullException("มีข้อมูล Item นี้อยู่แล้วในระบบ");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialItemUnitRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                var list = DataAccessLayerServices.pd_ryo_MaterialReceivingRepository()
                    .GetList(x => x.ItemUnitID == id);

                if (list.Count() > 0)
                    throw new ArgumentException("มีข้อมูลการการรับ item นี้เข้าสต๊อกแล้วจำนวน" +
                        list.Count + " รายการ ไม่สามารถลบข้อมูลนี้ได้");

                DataAccessLayerServices.pd_ryo_MaterialItemUnitRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialItemUnit model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialItemUnitValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.ID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                editModel.ItemCode = model.ItemCode;
                editModel.UnitCode = model.UnitCode;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialItemUnitRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialItemUnit> GetAll()
        {
            return DataAccessLayerServices.pd_ryo_MaterialItemUnitRepository()
                .GetAll(x => x.pd_ryo_MaterialItem,
                x => x.pd_ryo_MaterialUnit)
                .ToList();
        }

        public pd_ryo_MaterialItemUnit GetSingle(Guid id)
        {
            return DataAccessLayerServices.pd_ryo_MaterialItemUnitRepository()
                .GetSingle(x => x.ID == id,
                x => x.pd_ryo_MaterialItem,
                x => x.pd_ryo_MaterialUnit);
        }
    }
}
