﻿using DomainModelStecDbms;
using RYOMoistureDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MasterBagConfigBL
    {
        List<pd_ryo_MasterBagConfig> GetAll();
        pd_ryo_MasterBagConfig GetSingle(string code);
        pd_ryo_MasterBagConfig GetByDefaultStatus();
        void Add(string code, string description, decimal tare, decimal min, decimal max, bool defaultStatus, string user);
        void Edit(string code, string description, decimal tare, decimal min, decimal max, bool defaultStatus, string user);
        void Delete(string code);
        void SetDefault(string code, string user);
    }

    public class pd_ryo_MasterBagConfigBL : Ipd_ryo_MasterBagConfigBL
    {
        IRYOUnitOfWork uow;
        public pd_ryo_MasterBagConfigBL()
        {
            uow = new RYOUnitOfWork();
        }

        public void Add(string code, string description, decimal tare, decimal min, decimal max, bool defaultStatus, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                    throw new ArgumentException("โปรดระบุ config code");

                if (min > max)
                    throw new ArgumentException("Min weight (น้ำหนักค่าต่ำสุดของ Master Bag) จะต้องน้อยกว่า Max weight (น้ำหนักค่าสูงสุดของ Master Bag)");

                if (string.IsNullOrWhiteSpace(user))
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้งานระบบ โปรดล็อคอินใหม่อีกครั้ง");

                var config = GetSingle(code);
                if (config != null)
                    throw new ArgumentException("มี config code นี้แล้วในระบบ");

                uow.pd_ryo_MasterBagConfigRepo
                    .Add(new pd_ryo_MasterBagConfig
                    {
                        ConfigCode = code,
                        Description = description,
                        TareWeight = tare,
                        MinWeight = min,
                        MaxWeight = max,
                        DefaultStatus = defaultStatus,
                        ModifiedBy = user,
                        ModifiedDate = DateTime.Now,
                    });
                uow.Save();

                if (defaultStatus == true)
                    SetDefault(code, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string code)
        {
            try
            {
                var item = GetSingle(code);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูล code " + code + " นี้ในระบบ");

                uow.pd_ryo_MasterBagConfigRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string code, string description, decimal tare, decimal min, decimal max, bool defaultStatus, string user)
        {
            try
            {
                var item = GetSingle(code);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูล code " + code + " นี้ในระบบ");

                if (min > max)
                    throw new ArgumentException("Min weight (น้ำหนักค่าต่ำสุดของ Master Bag) จะต้องน้อยกว่า Max weight (น้ำหนักค่าสูงสุดของ Master Bag)");

                if (string.IsNullOrWhiteSpace(user))
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้งานระบบ โปรดล็อคอินใหม่อีกครั้ง");

                item.Description = description;
                item.TareWeight = tare;
                item.MinWeight = min;
                item.MaxWeight = max;
                item.DefaultStatus = defaultStatus;
                item.ModifiedDate = DateTime.Now;
                item.ModifiedBy = user;

                uow.pd_ryo_MasterBagConfigRepo.Update(item);
                uow.Save();

                if (defaultStatus == true)
                    SetDefault(code, user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MasterBagConfig> GetAll()
        {
            return uow.pd_ryo_MasterBagConfigRepo.Get();
        }

        public pd_ryo_MasterBagConfig GetByDefaultStatus()
        {
            return uow.pd_ryo_MasterBagConfigRepo.GetSingle(x => x.DefaultStatus == true);
        }

        public pd_ryo_MasterBagConfig GetSingle(string code)
        {
            return uow.pd_ryo_MasterBagConfigRepo.GetSingle(x => x.ConfigCode == code);
        }

        public void SetDefault(string code, string user)
        {
            var list = GetAll();
            foreach (var item in list)
            {
                if (item.ConfigCode == code)
                {
                    item.DefaultStatus = true;
                    item.ModifiedDate = DateTime.Now;
                    item.ModifiedBy = user;
                }
                else
                {
                    item.DefaultStatus = false;
                }

                uow.pd_ryo_MasterBagConfigRepo.Update(item);
            }
            uow.Save();
        }
    }
}
