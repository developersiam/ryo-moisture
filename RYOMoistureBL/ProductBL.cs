﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureDAL;

namespace RYOMoistureBL
{
    public interface IProductBL
    {
        int GetCaseNo_Normal(string packedGrade);
        int GetLatestBarcode(int crop);
        List<sp_Packing_Get_Current_PD_Result> GetCurrentProduction();
        pd GetSingleProduct(string barcode);
        List<pd> GetGroupedGradeByDate(DateTime packingDate);
        //List<string> GetGradeByCrop(int crop);
        List<pdsetup> GetGradeByfrompdDate(DateTime frompdDate);
        List<pd> GetPDByGradeAndDate(string grade, DateTime packingDate);
        List<pd> GetPdinfoByGrade(string grade);
        List<pd> GetPdInfoByFromPdno(string frompdno);
        List<pd> GetPdInfoByCrop(short crop);
        List<pd> GetPdInfoByCustomer(short crop, string customer);
        List<pd> GetPDByGradeAndFromPdDate(string grade, DateTime frompdDate);
        List<pd> GetAB4GradeByCrop(int crop);
        pd GetSinglePdinfoByGrade(string grade, int caseno);
        List<int?> GetPdHours(string pdno);
        decimal GetNetAvg(string fromPRgrade, DateTime fromPDdate);
        List<pd> GetPdList(string pdno, int pdhour);
        void AddNewPacking(pd items);
        void UpdatePacking(pd items);
        void UpdatePdBC(int crop, int bc);
        List<sp_Packing_Get_PackingStatus_Result> GetPackingStatus(string pdno, int pdhour);
        void InsertPackingStatus(string pdno, int pdhour, DateTime pdDate);
        pd GetSingleProductByCaseandGrade(string grade, int caseno);
        List<string> GetCutragShippingBay();
    }

    public class ProductBL : IProductBL
    {
        public pd GetSingleProductByCaseandGrade(string grade, int caseno)
        {
            return DataAccessLayerServices.PDRepository()
                .GetSingle(g => g.grade == grade && g.caseno == caseno);
        }
        public void InsertPackingStatus(string pdNo, int pdHour, DateTime pdDate)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            sp.sp_Packing_INS_PackingStatus(pdNo, pdHour, pdDate);
        }
        public List<sp_Packing_Get_PackingStatus_Result> GetPackingStatus(string pdno, int pdhour)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Packing_Get_PackingStatus(pdno, pdhour);
        }
        public void UpdatePdBC(int crop, int bc)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            sp.sp_Packing_UPD_pdbc(crop, bc);
        }
        public void UpdatePacking(pd items)
        {
            DataAccessLayerServices.PDRepository().Update(items);
        }
        public void AddNewPacking(pd items)
        {
            DataAccessLayerServices.PDRepository().Add(items);
        }
        public int GetCaseNo_Normal(string packedGrade)
        {
            int caseno = 1;
            var MaxCase = DataAccessLayerServices.PDRepository()
                .Get(w => w.grade == packedGrade);
            if (MaxCase.Any())
                caseno = MaxCase.Max(w => w.caseno.HasValue ? w.caseno.Value : 0) + 1;
            return caseno;
        }
        public int GetLatestBarcode(int crop)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Packing_GET_MaxPDBC(crop);
        }
        public List<pd> GetPdList(string pdno, int pdhour)
        {
            var PdBaleList = DataAccessLayerServices.PDRepository()
                .Get(w => w.frompdno == pdno &&
                w.frompdhour == pdhour &&
                (w.bc.Contains("PC") || w.bc.Contains("LN") || w.bc.Contains("RN")));
            return PdBaleList.ToList();
        }
        public List<int?> GetPdHours(string pdno)
        {
            var AllPd = new List<int?>();
            AllPd = DataAccessLayerServices.PDRepository()
                .Get(w => w.frompdno == pdno &&
                w.pdtype == "Lamina")
                .OrderBy(w => w.frompdhour)
                .Select(w => w.frompdhour)
                .Distinct()
                .ToList();
            if (AllPd.Count() == 0 || AllPd == null)
                AllPd.Add(1);
            else
                AllPd.Add(AllPd.Count() + 1);

            return AllPd;
        }
        public decimal GetNetAvg(string fromPRgrade, DateTime fromPDdate)
        {
            decimal NetAvg;
            NetAvg = 0;
            var avg = DataAccessLayerServices.PDRepository()
                .Get(w => w.fromprgrade == fromPRgrade &&
                w.frompddate == fromPDdate &&
                w.pdtype == "Lamina");
            if (avg.Count() > 0)
                NetAvg = avg.Average(w => w.netreal.HasValue ? w.netreal.Value : 0);

            return NetAvg;
        }
        public List<sp_Packing_Get_Current_PD_Result> GetCurrentProduction()
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Packing_Get_Current_PD();
        }
        public List<pd> GetGroupedGradeByDate(DateTime packingDate)
        {
            var pdList = DataAccessLayerServices.PDRepository()
                .Get(g => g.packingdate == packingDate)
                .ToList();
            var groupedGrade = pdList
                .GroupBy(g => g.grade)
                .Select(s => new pd { grade = s.Key })
                .ToList();
            return groupedGrade;
        }
        public List<pdsetup> GetGradeByfrompdDate(DateTime frompdDate)
        {
            var pdList = DataAccessLayerServices.PDSetupRepository()
                .Get(g => g.date == frompdDate)
                .ToList();
            var groupedGrade = pdList
                .GroupBy(g => g.packedgrade)
                .Select(s => new pdsetup { packedgrade = s.Key })
                .ToList();
            return groupedGrade;
        }

        public List<pd> GetPDByGradeAndDate(string grade, DateTime packingDate)
        {
            return DataAccessLayerServices.PDRepository()
                .Get(g => g.packingdate == packingDate &&
                g.grade == grade && g.caseno != 0,
                null,
                g => g.pd_moisture,
                g => g.pd_moisture_target)
                .OrderBy(o => o.caseno)
                .ToList();
        }
        public List<pd> GetPdinfoByGrade(string grade)
        {
            return DataAccessLayerServices.PDRepository()
                .Get(g => g.grade == grade && g.caseno != 0,
                null, g => g.pd_moisture,
                g => g.pd_moisture_target)
                .OrderBy(o => o.caseno)
                .ToList();
        }
        public List<pd> GetPDByGradeAndFromPdDate(string grade, DateTime frompdDate)
        {
            return DataAccessLayerServices.PDRepository()
                .Get(g => g.frompddate == frompdDate &&
                g.grade == grade && g.caseno != 0,
                null,
                g => g.pd_moisture,
                g => g.pd_moisture_target)
                .OrderBy(o => o.caseno)
                .ToList();
        }

        public pd GetSingleProduct(string barcode)
        {
            var x = DataAccessLayerServices.PDRepository().GetSingle(g => g.bc == barcode);
            return x;
        }
        public pd GetSinglePdinfoByGrade(string grade, int caseno)
        {
            var x = DataAccessLayerServices.PDRepository()
                .GetSingle(g => g.grade == grade &&
                g.caseno == caseno);
            return x;
        }
        public List<pd> GetAB4GradeByCrop(int crop)
        {
            var pdList = DataAccessLayerServices.PDRepository()
                .Get(g => g.crop == crop)
                .Where(a => a.grade.Contains("AB"))
                .ToList();
            var groupedGrade = pdList
                .GroupBy(g => g.grade)
                .Select(s => new pd { grade = s.Key })
                .ToList();
            return groupedGrade;
        }

        public List<pd> GetPdInfoByFromPdno(string frompdno)
        {
            return DataAccessLayerServices.PDRepository()
                .Get(x => x.frompdno == frompdno)
                .ToList();
        }

        public List<pd> GetPdInfoByCrop(short crop)
        {
            return DataAccessLayerServices.PDRepository()
                .Get(x => x.crop == crop)
                .ToList();
        }

        public List<pd> GetPdInfoByCustomer(short crop, string customer)
        {
            return DataAccessLayerServices.PDRepository()
                .Get(x => x.crop == crop && x.customer == customer)
                .ToList();
        }

        public List<string> GetCutragShippingBay()
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetCutragShippingBay();
        }
    }
}
