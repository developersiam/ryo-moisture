﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureDAL;

namespace RYOMoistureBL
{
    public interface IMoistureBL
    {
        List<pd_moisture_type> GetMoistureType();
        List<pd_moisture> GetMoistureListByBC(string barcode);
        pd_moisture_target GetMoistureTargetByBC(string barcode);
        pd_moisture GetSingleMoisture(string barcode, int moistureTypeID);
        void UpdateMoistureTarget(pd_moisture_target target);
        void UpdateMoisture(pd_moisture moisture);
        List<pd_moisture_masterbox> GetMasterBoxListByBC(string barcode);
        List<pd_moisture_masterbox> GetMasterBoxListByHasIssuedCode(short crop);
        pd_moisture_masterbox GetSingleMasterBoxByMasterBox(string barcode);
        void AddMasterBox(pd_moisture_masterbox masterbox);
        void UpdateMasterBox(pd_moisture_masterbox item);
        void DeleteMasterBox(pd_moisture_masterbox masterbox);
        void DeleteMoisture(pd_moisture moisture);
        List<pd_moisture_masterbag> GetMasterBagByMasterBox(string barcode);
        pd_moisture_masterbag GetSingleMasterBagByBC(string bc);
        void UpdateMasterBag(pd_moisture_masterbag item);
        void AddMasterBag(pd_moisture_masterbag item);
        void RemoveMasterBag(pd_moisture_masterbag item);
        void RemoveAllMasterBagByMasterBox(string barcode);
        int GetLastestMasterBagBC();
        pd_ryo_remnant GetSingleRemnant(string pdno, DateTime packingdate);
        void UpdateRemnant(pd_ryo_remnant item);
        void AddRemnant(pd_ryo_remnant item);
        void RemoveRemnant(pd_ryo_remnant item);
        List<pd_ryo_remnant> GetRemnant();
        List<pd_ryo_remnant> GetRemnantByPdno(string pdno);
        pd_moisture_masterbag GetSingleMasterBagByCase(string masterbag_bc, int caseno);
    }
    public class MoistureBL : IMoistureBL
    {
        public void RemoveAllMasterBagByMasterBox(string Barcode)
        {
            var lstMB = DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Get(g => g.masterbox_bc == Barcode)
                .ToList();
            if (lstMB.Count() > 0)
            {
                foreach (var i in lstMB)
                {
                    DataAccessLayerServices.PDMoistureMasterbagRepository()
                        .Remove(i);
                }
            }
        }
        public List<pd_ryo_remnant> GetRemnantByPdno(string pdno)
        {
            return DataAccessLayerServices.PDRYORemnantRepository()
                .Get(r => r.pdno == pdno).ToList();
        }
        public List<pd_ryo_remnant> GetRemnant()
        {
            return DataAccessLayerServices.PDRYORemnantRepository()
                .Get()
                .ToList();
        }
        public void RemoveRemnant(pd_ryo_remnant item)
        {
            DataAccessLayerServices.PDRYORemnantRepository()
                .Remove(item);
        }
        public void AddRemnant(pd_ryo_remnant item)
        {
            DataAccessLayerServices.PDRYORemnantRepository()
                .Add(item);
        }
        public void UpdateRemnant(pd_ryo_remnant item)
        {
            DataAccessLayerServices.PDRYORemnantRepository()
                .Update(item);
        }
        public pd_ryo_remnant GetSingleRemnant(string pdno, DateTime packingdate)
        {
            return DataAccessLayerServices.PDRYORemnantRepository()
                .GetSingle(g => g.pdno == pdno &&
                g.packingdate == packingdate);
        }
        public int GetLastestMasterBagBC()
        {
            string tmpBC;
            int maxBC = 1;
            var lastestBC = DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Get().OrderByDescending(h => h.masterbag_bc)
                .FirstOrDefault();
            if (lastestBC != null)
            {
                tmpBC = lastestBC.masterbag_bc.Substring(5, 6);
                maxBC = Convert.ToInt32(tmpBC) + 1;
            }
            return maxBC;

        }
        public void RemoveMasterBag(pd_moisture_masterbag item)
        {
            DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Remove(item);
        }
        public void AddMasterBag(pd_moisture_masterbag item)
        {
            DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Add(item);
        }
        public void UpdateMasterBag(pd_moisture_masterbag item)
        {
            DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Update(item);
        }

        public pd_moisture_masterbag GetSingleMasterBagByCase(string masterbox_bc, int masterbag_case)
        {
            return DataAccessLayerServices.PDMoistureMasterbagRepository()
                .GetSingle(g => g.masterbox_bc == masterbox_bc &&
                g.masterbag_caseno == masterbag_case);
        }
        public pd_moisture_masterbag GetSingleMasterBagByBC(string barcode)
        {
            return DataAccessLayerServices.PDMoistureMasterbagRepository()
                .GetSingle(g => g.masterbag_bc == barcode);
        }
        public List<pd_moisture_masterbag> GetMasterBagByMasterBox(string bc)
        {
            return DataAccessLayerServices.PDMoistureMasterbagRepository()
                .Get(g => g.masterbox_bc == bc).ToList();
        }
        public void AddMasterBox(pd_moisture_masterbox masterbox)
        {
            DataAccessLayerServices.PDMoistureMasterboxRepository()
                .Add(masterbox);
        }
        public void UpdateMasterBox(pd_moisture_masterbox masterbox)
        {
            DataAccessLayerServices.PDMoistureMasterboxRepository()
                .Update(masterbox);
        }
        public void DeleteMasterBox(pd_moisture_masterbox masterbox)
        {
            DataAccessLayerServices.PDMoistureMasterboxRepository()
                .Remove(masterbox);
        }

        public List<pd_moisture_type> GetMoistureType()
        {
            return DataAccessLayerServices.PDMoistureTypeRepository()
                .Get()
                .ToList();
        }

        public List<pd_moisture_masterbox> GetMasterBoxListByBC(string barcode)
        {
            return DataAccessLayerServices.PDMoistureMasterboxRepository()
                .Get(g => g.frombc == barcode)
                .ToList();
        }

        public List<pd_moisture> GetMoistureListByBC(string barcode)
        {
            return DataAccessLayerServices.PDMoistureRepository()
                .Get(g => g.bc == barcode,
                null,
                g => g.pd_moisture_type)
                .ToList();
        }

        public pd_moisture_target GetMoistureTargetByBC(string barcode)
        {
            return DataAccessLayerServices.PDMoistureTargetRepository()
                .GetSingle(g => g.bc == barcode);
        }

        public pd_moisture_masterbox GetSingleMasterBoxByMasterBox(string barcode)
        {
            return DataAccessLayerServices.PDMoistureMasterboxRepository()
                .GetSingle(g => g.masterbox_bc == barcode);
        }

        public pd_moisture GetSingleMoisture(string barcode, int moistureTypeID)
        {
            return DataAccessLayerServices.PDMoistureRepository()
                .GetSingle(g => g.bc == barcode &&
                g.moistureType == moistureTypeID);
        }

        public void UpdateMoisture(pd_moisture moisture)
        {
            var oldItem = GetSingleMoisture(moisture.bc, moisture.moistureType);
            if (oldItem != null) DataAccessLayerServices.PDMoistureRepository()
                    .Remove(oldItem);
            DataAccessLayerServices.PDMoistureRepository().Add(moisture);
        }
        public void UpdateMoistureTarget(pd_moisture_target target)
        {
            var oldItem = GetMoistureTargetByBC(target.bc);
            if (oldItem != null) DataAccessLayerServices.PDMoistureTargetRepository()
                    .Remove(oldItem);
            DataAccessLayerServices.PDMoistureTargetRepository()
                .Add(target);
        }
        public void DeleteMoisture(pd_moisture moisture)
        {
            var existedMC = GetSingleMoisture(moisture.bc, moisture.moistureType);
            if (existedMC != null) DataAccessLayerServices.PDMoistureRepository()
                    .Remove(existedMC);
        }

        public List<pd_moisture_masterbox> GetMasterBoxListByHasIssuedCode(short crop)
        {
            if (DataAccessLayerServices.PDMoistureMasterboxRepository()
                .Get(x => x.IssuedCode != null).Count <= 0)
                return new List<pd_moisture_masterbox>();

            return DataAccessLayerServices.PDMoistureMasterboxRepository()
                .Get(x => x.IssuedCode != null)
                .Where(x => Convert.ToInt16(x.IssuedCode.Substring(3, 4)) == crop)
                .ToList();
        }
    }
}
