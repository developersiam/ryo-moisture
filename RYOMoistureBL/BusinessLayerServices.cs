﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public static class BusinessLayerServices
    {
        public static IProductBL ProductBL()
        {
            IProductBL obj = new ProductBL();
            return obj;
        }
        public static IMoistureBL MoistureBL()
        {
            IMoistureBL obj = new MoistureBL();
            return obj;
        }
        public static IPdSetupBL PdSetupBL()
        {
            IPdSetupBL obj = new PdSetupBL();
            return obj;
        }
        public static IPackedGradeBL PackedGradeBL()
        {
            IPackedGradeBL obj = new PackedGradeBL();
            return obj;
        }


        /// <summary>
        /// RYO material and Order.
        /// </summary>
        public static Ipd_ryo_OrderBL pd_ryo_OrderBL()
        {
            Ipd_ryo_OrderBL obj = new pd_ryo_OrderBL();
            return obj;
        }
        public static IcustomerBL customerBL()
        {
            IcustomerBL obj = new customerBL();
            return obj;
        }
        public static Ipd_ryo_MaterialIssuedCodeBL pd_ryo_MaterialIssuedCodeBL()
        {
            Ipd_ryo_MaterialIssuedCodeBL obj = new pd_ryo_MaterialIssuedCodeBL();
            return obj;
        }
        public static Ipd_ryo_MaterialIssuedPerBoxBL pd_ryo_MaterialIssuedPerBoxBL()
        {
            Ipd_ryo_MaterialIssuedPerBoxBL obj = new pd_ryo_MaterialIssuedPerBoxBL();
            return obj;
        }
        public static Ipd_ryo_MaterialItemBL pd_ryo_MaterialItemBL()
        {
            Ipd_ryo_MaterialItemBL obj = new pd_ryo_MaterialItemBL();
            return obj;
        }
        public static Ipd_ryo_MaterialReceivingBL pd_ryo_MaterialReceivingBL()
        {
            Ipd_ryo_MaterialReceivingBL obj = new pd_ryo_MaterialReceivingBL();
            return obj;
        }
        public static Ipd_ryo_MaterialReceivingTypeBL pd_ryo_MaterialReceivingTypeBL()
        {
            Ipd_ryo_MaterialReceivingTypeBL obj = new pd_ryo_MaterialReceivingTypeBL();
            return obj;
        }
        public static Ipd_ryo_MaterialDamageBL pd_ryo_MaterialDamageBL()
        {
            Ipd_ryo_MaterialDamageBL obj = new pd_ryo_MaterialDamageBL();
            return obj;
        }
        public static Ipd_ryo_MaterialDamageDetailBL pd_ryo_MaterialDamageDetailBL()
        {
            Ipd_ryo_MaterialDamageDetailBL obj = new pd_ryo_MaterialDamageDetailBL();
            return obj;
        }
        public static Ipd_ryo_MaterialDamageReasonBL pd_ryo_MaterialDamageReasonBL()
        {
            Ipd_ryo_MaterialDamageReasonBL obj = new pd_ryo_MaterialDamageReasonBL();
            return obj;
        }
        public static Ipd_ryo_UserAccountBL pd_ryo_UserAccountBL()
        {
            Ipd_ryo_UserAccountBL obj = new pd_ryo_UserAccountBL();
            return obj;
        }
        public static Ipd_ryo_UserRoleBL pd_ryo_UserRoleBL()
        {
            Ipd_ryo_UserRoleBL obj = new pd_ryo_UserRoleBL();
            return obj;
        }
        public static Ipd_ryo_RoleBL pd_ryo_RoleBL()
        {
            Ipd_ryo_RoleBL obj = new pd_ryo_RoleBL();
            return obj;
        }
        public static IInvoiceBL InvoiceBL()
        {
            IInvoiceBL obj = new InvoiceBL();
            return obj;
        }
        public static Ipd_ryo_customer_labelBL pd_ryo_customer_labelBL()
        {
            Ipd_ryo_customer_labelBL obj = new pd_ryo_customer_labelBL();
            return obj;
        }
        public static Ipd_ryo_customer_label_setupBL pd_ryo_customer_label_setupBL()
        {
            Ipd_ryo_customer_label_setupBL obj = new pd_ryo_customer_label_setupBL();
            return obj;
        }
        //public static Ipd_ryo_MaterialUnitBL pd_ryo_MaterialUnitBL()
        //{
        //    Ipd_ryo_MaterialUnitBL obj = new pd_ryo_MaterialUnitBL();
        //    return obj;
        //}
        //public static Ipd_ryo_MaterialItemUnitBL pd_ryo_MaterialItemUnitBL()
        //{
        //    Ipd_ryo_MaterialItemUnitBL obj = new pd_ryo_MaterialItemUnitBL();
        //    return obj;
        //}
        public static Ipd_ryo_InsertPaperBL pd_ryo_InsertPaperBL()
        {
            Ipd_ryo_InsertPaperBL obj = new pd_ryo_InsertPaperBL();
            return obj;
        }
        /// <returns></returns>


        public static Ipd_ryo_MasterBagConfigBL pd_ryo_MasterBagConfigBL()
        {
            Ipd_ryo_MasterBagConfigBL obj = new pd_ryo_MasterBagConfigBL();
            return obj;
        }

    }
}
