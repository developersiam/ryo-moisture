﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialIssuedPerBoxBL
    {
        void Add(pd_ryo_MaterialIssuedPerBox model);
        void Delete(string issuedCode, string itemCode);
        void Edit(pd_ryo_MaterialIssuedPerBox model);
        pd_ryo_MaterialIssuedPerBox GetSingle(string issuedCode, string itemCode);
        List<pd_ryo_MaterialIssuedPerBox> GetByIssuedCode(string issuedCode);
        List<pd_ryo_MaterialIssuedPerBox> GetByCrop(short crop);
    }

    public class pd_ryo_MaterialIssuedPerBoxBL : Ipd_ryo_MaterialIssuedPerBoxBL
    {
        public void Add(pd_ryo_MaterialIssuedPerBox model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialIssuedPerBoxValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = GetByIssuedCode(model.IssuedCode);
                if (list.Where(x => x.ItemCode == model.ItemCode).Count() > 0)
                    throw new ArgumentNullException("มีข้อมูล Item Code " + model.ItemCode + " อยู่แล้วใน issued code " + model.IssuedCode);

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string issuedCode, string itemCode)
        {
            try
            {
                var model = GetSingle(issuedCode, itemCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialIssuedPerBox model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialIssuedPerBoxValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.IssuedCode, model.ItemCode);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล item code " + model.ItemCode + " ในระบบ");

                editModel.PcsPerBox = model.PcsPerBox;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialIssuedPerBox> GetByCrop(short crop)
        {
            try
            {
                return DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository()
                    .Get(x => x.pd_ryo_MaterialIssuedCode.Crop == crop,
                null,
                    x => x.pd_ryo_MaterialIssuedCode)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialIssuedPerBox> GetByIssuedCode(string issuedCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository()
                .Get(x => x.IssuedCode == issuedCode,
                null,
                x => x.pd_ryo_MaterialItem)
                .ToList();
        }

        public pd_ryo_MaterialIssuedPerBox GetSingle(string issuedCode, string itemCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository()
                .GetSingle(x => x.IssuedCode == issuedCode &&
                x.ItemCode == itemCode,
                x => x.pd_ryo_MaterialItem);
        }
    }
}
