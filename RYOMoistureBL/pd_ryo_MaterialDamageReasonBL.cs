﻿using RYOMoistureDAL;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialDamageReasonBL
    {
        void Add(pd_ryo_MaterialDamageReason model);
        void Edit(pd_ryo_MaterialDamageReason model);
        void Delete(Guid id);
        pd_ryo_MaterialDamageReason GetSingle(Guid id);
        List<pd_ryo_MaterialDamageReason> Get();
    }

    public class pd_ryo_MaterialDamageReasonBL : Ipd_ryo_MaterialDamageReasonBL
    {
        public void Add(pd_ryo_MaterialDamageReason model)
        {
            try
            {
                var list = Get();
                if (list.Where(x => x.DamageReasonName == model.DamageReasonName).Count() > 0)
                    throw new ArgumentNullException("มีข้อมูล Damage Reason " + model.DamageReasonName + " อยู่แล้วในระบบ");

                model.DamageReasonID = Guid.NewGuid();
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialDamageReasonRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                var list = DataAccessLayerServices.pd_ryo_MaterialDamageRepository()
                    .Get(x => x.DamageReasonID == id);

                if (list.Count() > 0)
                    throw new ArgumentException("มีการนำข้อมูลนี้ไปใช้อ้างในการบันทึก Damage แล้ว ไม่สามารถลบข้อมูลนี้ได้");

                DataAccessLayerServices.pd_ryo_MaterialDamageReasonRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialDamageReason model)
        {
            try
            {
                var editModel = GetSingle(model.DamageReasonID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                editModel.DamageReasonName = model.DamageReasonName;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialDamageReasonRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialDamageReason> Get()
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageReasonRepository()
                .Get()
                .ToList();
        }

        public pd_ryo_MaterialDamageReason GetSingle(Guid id)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageReasonRepository()
                .GetSingle(x => x.DamageReasonID == id);
        }
    }
}
