﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_UserRoleBL
    {
        void Add(pd_ryo_UserRole model);
        void Delete(string username, Guid roleID);
        pd_ryo_UserRole GetSingle(string username, Guid roleID);
        List<pd_ryo_UserRole> Get();
        List<pd_ryo_UserRole> GetByUsername(string username);
    }

    public class pd_ryo_UserRoleBL : Ipd_ryo_UserRoleBL
    {
        public void Add(pd_ryo_UserRole model)
        {
            try
            {
                var list = GetByUsername(model.Username);
                if (list.Where(x => x.RoleID == model.RoleID).Count() > 0)
                    throw new ArgumentException("มีการกำหนด role นี้ซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                DataAccessLayerServices.pd_ryo_UserRoleRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, Guid roleID)
        {
            try
            {
                var model = GetSingle(username, roleID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.pd_ryo_UserRoleRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_UserRole> Get()
        {
            return DataAccessLayerServices.pd_ryo_UserRoleRepository()
                .Get(null,null,x => x.pd_ryo_Role)
                .ToList();
        }

        public List<pd_ryo_UserRole> GetByUsername(string username)
        {
            return DataAccessLayerServices.pd_ryo_UserRoleRepository()
                .Get(x => x.Username == username,
                null,
                x => x.pd_ryo_Role).ToList();
        }

        public pd_ryo_UserRole GetSingle(string username, Guid roleID)
        {
            return DataAccessLayerServices.pd_ryo_UserRoleRepository()
                .GetSingle(x => x.Username == username && x.RoleID == roleID);
        }
    }
}
