﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialIssuedCodeBL
    {
        void Add(short crop, string customer, string username);
        void Delete(string issuedCode);
        void SetCurrentStatus(string issuedCode, string username);
        List<pd_ryo_MaterialIssuedCode> Get();
        List<pd_ryo_MaterialIssuedCode> GetByCrop(short crop);
        List<pd_ryo_MaterialIssuedCode> GetByCustomer(string customer);
        pd_ryo_MaterialIssuedCode GetSingle(string issuedCode);
        pd_ryo_MaterialIssuedCode GetCurrentStatus(string customer);
    }

    public class pd_ryo_MaterialIssuedCodeBL : Ipd_ryo_MaterialIssuedCodeBL
    {
        public void Add(short crop, string customer, string username)
        {
            try
            {
                var list = GetByCrop(crop);
                int maxRecord = list.Count() <= 0 ? 0 : list.Max(x => Convert.ToInt16(x.IssuedCode.Substring(8, 3)));
                string code = "IS-" + crop + "-" + (maxRecord + 1).ToString().PadLeft(3, '0');

                DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository()
                    .Add(new pd_ryo_MaterialIssuedCode
                    {
                        IssuedCode = code,
                        Crop = crop,
                        Customer = customer,
                        CurrentStatus = false,
                        CreateDate = DateTime.Now,
                        CreateBy = username,
                        ModifiedBy = username,
                        ModifiedDate = DateTime.Now
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string issuedCode)
        {
            try
            {
                var model = GetSingle(issuedCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var list = DataAccessLayerServices.PDMoistureMasterboxRepository()
                    .Get(x => x.IssuedCode == issuedCode);

                if (list.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบข้อมูล issued code " +
                        issuedCode + " นี้ได้เนื่องจากถูกนำไปใช้อ้างอิงในการตัดสต๊อก material แล้ว");

                var list2 = DataAccessLayerServices.pd_ryo_MaterialIssuedPerBoxRepository()
                    .Get(x => x.IssuedCode == issuedCode);

                if (list2.Count() > 0)
                    throw new ArgumentException("Issued code นี้มีรายการข้อมูล Issued per box อยู่ภายใน ไม่สามารถลบข้อมูลนี้ได้");

                DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialIssuedCode> Get()
        {
            return DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository().Get().ToList();
        }

        public List<pd_ryo_MaterialIssuedCode> GetByCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository()
                .Get(x => x.Crop == crop)
                .ToList();
        }
        public List<pd_ryo_MaterialIssuedCode> GetByCustomer(string customer)
        {
            return DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository()
                .Get(x => x.Customer == customer)
                .ToList();
        }

        public pd_ryo_MaterialIssuedCode GetCurrentStatus(string customer)
        {
            var issueCode = DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository()
                .GetSingle(x => x.CurrentStatus == true && x.Customer == customer);

            if (issueCode == null)
                throw new ArgumentException("ไม่พบข้อมูล การตั้ง status สำหรับตัด stock ของลูกค้ารายนี้");

            return issueCode;
        }

        public pd_ryo_MaterialIssuedCode GetSingle(string issuedCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository()
                .GetSingle(x => x.IssuedCode == issuedCode);
        }

        public void SetCurrentStatus(string issuedCode, string username)
        {
            try
            {
                var model = GetSingle(issuedCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                foreach (var item in GetByCustomer(model.Customer))
                {
                    item.CurrentStatus = false;
                    DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository().Update(item);
                }

                model.CurrentStatus = true;
                model.ModifiedBy = username;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialIssuedCodeRepository().Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
