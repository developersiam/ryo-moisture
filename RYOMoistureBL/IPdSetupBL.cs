﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureDAL;

namespace RYOMoistureBL
{
    public interface IPdSetupBL
    {
        pdsetup GetSinglePDSetup(string pdno);
        pdsetup GetByDefault();
        List<pdsetup> GetProductionByDate();
        List<pdsetup> GetProductionByCrop(short crop);
        List<string> GetPdNoByCrop(short crop);
        void UpdatePdSetup(pdsetup item);
        
    }
    public class PdSetupBL : IPdSetupBL
    {
        public pdsetup GetSinglePDSetup(string pdno)
        {
            var x = DataAccessLayerServices.PDSetupRepository().GetSingle(g => g.pdno == pdno);
            return x;
        }
        public List<pdsetup> GetProductionByDate()
        {
            return DataAccessLayerServices.PDSetupRepository().Get(g => g.date == DateTime.Now || g.def == true).ToList();
        }
        public void UpdatePdSetup(pdsetup items)
        {
            DataAccessLayerServices.PDSetupRepository().Update(items);
        }

        public List<pdsetup> GetProductionByCrop(short crop)
        {
            return DataAccessLayerServices.PDSetupRepository()
                .Get(x => x.crop == crop && 
                x.packedgrade.Contains("CHAMP"))
                .ToList();
        }
        public List<string> GetPdNoByCrop(short crop)
        {
            return DataAccessLayerServices.PDSetupRepository()
                .Get(x => x.crop == crop).OrderByDescending(x => x.date).Select(x => x.pdno)
                .ToList();
        }
        public pdsetup GetByDefault()
        {
            return DataAccessLayerServices.PDSetupRepository().GetSingle(x => x.def == true);
        }
    }
}
