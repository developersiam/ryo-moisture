﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_UserAccountBL
    {
        void Add(pd_ryo_UserAccount model);
        void Delete(string username);
        pd_ryo_UserAccount GetSingle(string username);
        List<pd_ryo_UserAccount> Get();
    }

    public class pd_ryo_UserAccountBL : Ipd_ryo_UserAccountBL
    {
        public void Add(pd_ryo_UserAccount model)
        {
            try
            {
                if (DataAccessLayerServices.pd_ryo_UserAccountRepository()
                    .Get(x => x.Username == model.Username)
                    .Count() > 0)
                    throw new ArgumentException("username " + model.Username + " นี้มีซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                DataAccessLayerServices.pd_ryo_UserAccountRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username)
        {
            try
            {
                var model = GetSingle(username);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.pd_ryo_UserRole.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบผู้ใช้นี้ได้เนื่องจากยังมีข้อมูลการกำหนดสิทธิ์การใช้งานระบบอยู่ โปรดลบสิทธิ์การใช้งานระบบออกก่อน");

                DataAccessLayerServices.pd_ryo_UserAccountRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_UserAccount> Get()
        {
            return DataAccessLayerServices.pd_ryo_UserAccountRepository().Get().ToList();
        }

        public pd_ryo_UserAccount GetSingle(string username)
        {
            return DataAccessLayerServices.pd_ryo_UserAccountRepository()
                .GetSingle(x => x.Username == username);
        }
    }
}
