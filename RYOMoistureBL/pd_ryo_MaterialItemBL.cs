﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialItemBL
    {
        void Add(pd_ryo_MaterialItem model);
        void Delete(string itemCode);
        void Edit(pd_ryo_MaterialItem model);
        List<pd_ryo_MaterialItem> Get();
        pd_ryo_MaterialItem GetSingle(string itemCode);
    }

    public class pd_ryo_MaterialItemBL : Ipd_ryo_MaterialItemBL
    {
        public void Add(pd_ryo_MaterialItem model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialItemValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = Get();
                if (list.Where(x => x.ItemCode == model.ItemCode).Count() > 0)
                    throw new ArgumentNullException("มีข้อมูล Item Code " + model.ItemCode + " อยู่แล้วในระบบ");

                if (list.Where(x => x.ItemName == model.ItemName).Count() > 0)
                    throw new ArgumentNullException("มีข้อมูล Item Name " + model.ItemName + " อยู่แล้วในระบบ");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialItemRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string itemCode)
        {
            try
            {
                var model = GetSingle(itemCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล Item Code " + itemCode + " นี้ในระบบ");

                var list = DataAccessLayerServices.pd_ryo_MaterialItemRepository()
                    .Get(x => x.ItemCode == itemCode);

                if (list.Count() > 0)
                    throw new ArgumentException("มีข้อมูลการการรับ item นี้เข้าสต๊อกแล้วจำนวน" +
                        list.Count + " รายการ ไม่สามารถลบข้อมูลนี้ได้");

                DataAccessLayerServices.pd_ryo_MaterialItemRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialItem model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialItemValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.ItemCode);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล item code " + model.ItemCode + " ในระบบ");

                editModel.ItemName = model.ItemName;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialItemRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialItem> Get()
        {
            return DataAccessLayerServices.pd_ryo_MaterialItemRepository()
                .Get()
                .ToList();
        }

        public pd_ryo_MaterialItem GetSingle(string itemCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialItemRepository()
                .GetSingle(x => x.ItemCode == itemCode);
        }
    }
}
