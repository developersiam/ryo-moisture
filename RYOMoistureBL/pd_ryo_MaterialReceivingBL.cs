﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialReceivingBL
    {
        void Add(pd_ryo_MaterialReceiving model);
        void Edit(pd_ryo_MaterialReceiving model);
        void Delete(string ReceivingCode);
        pd_ryo_MaterialReceiving GetSingle(string receivingCode);
        List<pd_ryo_MaterialReceiving> GetByReceivedCrop(short crop);
        List<pd_ryo_MaterialReceiving> GetByReceivedDate(DateTime receivingDate);
        List<pd_ryo_MaterialReceiving> GetByReceivedDateRange(DateTime from, DateTime to);
        List<pd_ryo_MaterialReceiving> GetByItem(string itemCode);
    }

    public class pd_ryo_MaterialReceivingBL : Ipd_ryo_MaterialReceivingBL
    {
        public void Add(pd_ryo_MaterialReceiving model)
        {
            try
            {
                /// Generate receiving code.
                /// 
                var list = GetByReceivedCrop(model.Crop);
                var maxRecord = list.Count < 1 ? 0 : list.Max(x => Convert.ToInt16(x.ReceivingCode.Substring(8, 5)));
                var receivedCode = "RC-" + model.Crop + "-" + (maxRecord + 1).ToString().PadLeft(5, '0');

                model.ReceivingCode = receivedCode;

                var validation = new DomainValidation.Ispd_ryo_MaterialReceivingValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialReceivingRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string receivingCode)
        {
            try
            {
                var model = GetSingle(receivingCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.pd_ryo_MaterialReceivingRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialReceiving model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialReceivingValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.ReceivingCode);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Crop = model.Crop;
                editModel.Customer = model.Customer;
                editModel.ItemCode = model.ItemCode;
                editModel.Quantity = model.Quantity;
                editModel.ReceivingTypeCode = model.ReceivingTypeCode;
                editModel.ReceivedDate = model.ReceivedDate;
                editModel.ModifiedBy = model.ModifiedBy;

                DataAccessLayerServices.pd_ryo_MaterialReceivingRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialReceiving> GetByItem(string itemCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialReceivingRepository()
                .Get(x => x.ItemCode == itemCode).ToList();
        }

        public List<pd_ryo_MaterialReceiving> GetByReceivedCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_MaterialReceivingRepository()
                .Get(x => x.Crop == crop,
                null,
                x => x.pd_ryo_MaterialItem,
                x => x.pd_ryo_MaterialReceivingType)
                .ToList();
        }

        public List<pd_ryo_MaterialReceiving> GetByReceivedDate(DateTime receivingDate)
        {
            return DataAccessLayerServices.pd_ryo_MaterialReceivingRepository()
                .Get(x => x.ReceivedDate.Date == receivingDate.Date)
                .ToList();
        }

        public List<pd_ryo_MaterialReceiving> GetByReceivedDateRange(DateTime from, DateTime to)
        {
            return DataAccessLayerServices.pd_ryo_MaterialReceivingRepository()
                .Get(x => x.ReceivedDate.Date >= from.Date &&
                x.ReceivedDate.Date <= to.Date)
                .ToList();
        }

        public pd_ryo_MaterialReceiving GetSingle(string receivingCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialReceivingRepository()
                .GetSingle(x => x.ReceivingCode == receivingCode);
        }
    }
}
