﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_InsertPaperBL
    {
        List<sp_pd_ryo_InsertPaper_Result> GetInsertPaper(string productionYear, int fromCaseNo, int toCaseNo, string customerCode);
        pd_ryo_InsertPaper GetSingle(string masterbag_bc);
    }

    public class pd_ryo_InsertPaperBL : Ipd_ryo_InsertPaperBL
    {
        public List<sp_pd_ryo_InsertPaper_Result> GetInsertPaper(string productionYear, int fromCaseNo, int toCaseNo, string customerCode)
        {
            return DataAccessLayerServices.StoreProcedureRepository()
                .GetInsertPaper(productionYear, fromCaseNo, toCaseNo, customerCode)
                .ToList();
        }

        public pd_ryo_InsertPaper GetSingle(string masterbag_bc)
        {
            return DataAccessLayerServices.pd_ryo_InsertPaperRepository()
                .GetSingle(x => x.masterbag_bc == masterbag_bc);
        }
    }
}
