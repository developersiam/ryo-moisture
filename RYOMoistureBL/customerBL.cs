﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface IcustomerBL {
        pd_ryo_Customer Getsingle(int customerID);
        List<customer> Get();
        List<pd_ryo_Customer> GetRYOCustomer();
        void Insert(pd_ryo_Customer i);
        void Update(pd_ryo_Customer i);
        void Delete(pd_ryo_Customer i);
    }

    public class customerBL : IcustomerBL
    {
        public void Delete(pd_ryo_Customer i)
        {
            var item = Getsingle(i.CustomerID);
            if (item != null) DataAccessLayerServices.pd_ryo_CustomerRepository().Remove(item);
            else throw new Exception("Data not found.");
        }

        public List<customer> Get()
        {
            return DataAccessLayerServices.customerRepository().Get().ToList();
        }

        public List<pd_ryo_Customer> GetRYOCustomer()
        {
            return DataAccessLayerServices.pd_ryo_CustomerRepository()
                .Get(g => g.ProvinceCode != "",
                null,
                g => g.pd_ryo_province).ToList();
        }

        public pd_ryo_Customer Getsingle(int customerID)
        {
            return DataAccessLayerServices.pd_ryo_CustomerRepository().GetSingle(g => g.CustomerID == customerID);
        }

        public void Insert(pd_ryo_Customer i)
        {
            var item = Getsingle(i.CustomerID);
            if (item == null) DataAccessLayerServices.pd_ryo_CustomerRepository().Add(i);
            else throw new Exception("Data exist.");
        }

        public void Update(pd_ryo_Customer i)
        {
            var item = Getsingle(i.CustomerID);
            if (item != null) DataAccessLayerServices.pd_ryo_CustomerRepository().Update(i);
            else throw new Exception("Data not found.");
        }
    }
}
