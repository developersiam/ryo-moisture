﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialReceivingTypeBL {
        List<pd_ryo_MaterialReceivingType> Get();
    }

    public class pd_ryo_MaterialReceivingTypeBL : Ipd_ryo_MaterialReceivingTypeBL
    {
        public List<pd_ryo_MaterialReceivingType> Get()
        {
            return DataAccessLayerServices.pd_ryo_MaterialReceivingTypeRepository().Get().ToList();
        }
    }
}
