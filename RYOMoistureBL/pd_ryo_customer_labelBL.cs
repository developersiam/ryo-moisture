﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_customer_labelBL
    {
        List<pd_ryo_customer_label> Get();
        void Add(pd_ryo_customer_label model);
        pd_ryo_customer_label GetSingle(string code);
        void Delete(string code);
        void Edit(pd_ryo_customer_label model);
    }
    public class pd_ryo_customer_labelBL : Ipd_ryo_customer_labelBL
    {
         public List<pd_ryo_customer_label> Get()
        {
            return DataAccessLayerServices.pd_ryo_customer_labelRepository().Get().ToList();
        }
        public void Add(pd_ryo_customer_label model)
        {
            try
            {
                var setupPacked = GetSingle(model.customerID);
                if (setupPacked != null) throw new ArgumentNullException("มี Brand Code นี้ไปแล้ว ไม่สามารถ Add ซ้ำได้");

                model.modifiedDate = DateTime.Now;
                DataAccessLayerServices.pd_ryo_customer_labelRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public pd_ryo_customer_label GetSingle(string code)
        {
            return DataAccessLayerServices.pd_ryo_customer_labelRepository().GetSingle(x => x.customerID == code);
        }
        public void Delete(string code)
        {
            try
            {
                var setupPacked = GetSingle(code);
                if (setupPacked == null) throw new ArgumentNullException("ไม่พบข้อมุลในระบบ");

                DataAccessLayerServices.pd_ryo_customer_labelRepository().Remove(setupPacked);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Edit(pd_ryo_customer_label model)
        {
            try
            {
                var setupPacked = GetSingle(model.customerID);
                if (setupPacked == null) throw new ArgumentNullException("ไม่พบข้อมุลในระบบ");

                setupPacked.customerName = model.customerName;
                setupPacked.pouch = model.pouch;
                setupPacked.User = model.User;
                setupPacked.modifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_customer_labelRepository().Update(setupPacked);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }


}
