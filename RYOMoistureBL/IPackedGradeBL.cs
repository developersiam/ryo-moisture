﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureDAL;

namespace RYOMoistureBL
{
    public interface IPackedGradeBL
    {
        packedgrade GetPackedGrade(string packedgrade);
    }
    public class PackedGradeBL : IPackedGradeBL
    {
        public packedgrade GetPackedGrade(string packedgrade)
        {
            return DataAccessLayerServices.PackedGradeRepository().GetSingle(p => p.packedgrade1 == packedgrade);
        }
    }
}
