﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_OrderBL
    {
        void Add(pd_ryo_Order model);
        void Delete(string PONo);
        void Edit(pd_ryo_Order model);
        List<pd_ryo_Order> Get();
        List<pd_ryo_Order> GetByCrop(short crop);
        pd_ryo_Order GetByPO(string pono);
        pd_ryo_Order GetByInvoiceNo(string invoiceNo);
    }

    public class pd_ryo_OrderBL : Ipd_ryo_OrderBL
    {
        public void Add(pd_ryo_Order model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_OrderValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                /// Get all order for check a dupplicate PONo and a dupplicate invoice no.
                var allOrder = DataAccessLayerServices.pd_ryo_OrderRepository().Get();
                if (allOrder.Where(x => x.PONo == model.PONo).Count() > 0)
                {
                    var exitsPONo = allOrder.Where(x => x.PONo == model.PONo).FirstOrDefault();
                    throw new ArgumentException("PONo รหัส " + model.PONo +
                        " นี้มีอยู่แล้วในระบบเป็น Order ของปี " + exitsPONo.Crop);
                }

                if (allOrder.Where(x => x.InvoiceNo == model.InvoiceNo).Count() > 0)
                {
                    var exitsOrder = allOrder
                        .Where(x => x.InvoiceNo == model.InvoiceNo).FirstOrDefault();
                    throw new ArgumentException("Invoice no " + model.InvoiceNo +
                        " ซ้ำกับที่มีอยู่แล้วในระบบ " + Environment.NewLine +
                        "Crop: " + exitsOrder.Crop + Environment.NewLine +
                        "PONo: " + exitsOrder.PONo + Environment.NewLine +
                        "Customer: " + exitsOrder.Customer);
                }

                var customerOrderList = allOrder.Where(x => x.Crop == model.Crop && x.Customer == model.Customer);

                /// For each a customer order, The shipping case number must be continuous.
                if (customerOrderList
                    .Where(x => model.ShippingCaseFrom >= x.ShippingCaseFrom &&
                    model.ShippingCaseFrom <= x.ShippingCaseFrom)
                    .Count() > 0)
                    throw new ArgumentException("มีหมายเลขกล่อง " +
                        model.ShippingCaseFrom + " นี้แล้วในระบบ โปรดตรวจสอบอีกครั้ง");

                if (customerOrderList
                    .Where(x => model.ShippingCaseTo >= x.ShippingCaseTo &&
                    model.ShippingCaseTo <= x.ShippingCaseTo)
                    .Count() > 0)
                    throw new ArgumentException("มีหมายเลขกล่อง " +
                        model.ShippingCaseFrom + " นี้แล้วในระบบ โปรดตรวจสอบอีกครั้ง");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_OrderRepository().Add(model);

                /// Gennerate master bag barcode.
                /// ไม่ต้องมีการบันทึกข้อมูลบาร์โค้ตของ insert paper ในระบบ
                ///                

                //for (int caseno = model.ShippingCaseFrom; caseno <= model.ShippingCaseTo; caseno++)
                //{
                //    for (int masterbag = 1; masterbag <= 20; masterbag++)
                //    {
                //        string masterBagCode = model.ProductionYear +
                //            caseno.ToString().PadLeft(5, '0') +
                //            masterbag.ToString().PadLeft(2, '0');

                //        DataAccessLayerServices.pd_ryo_OrderDetailRepository()
                //            .Add(new pd_ryo_OrderDetail
                //            {
                //                MasterBagCode = masterBagCode,
                //                PONo = model.PONo,
                //                CreateDate = DateTime.Now,
                //                CreateBy = model.CreateBy,
                //                ModifiedDate = DateTime.Now,
                //                ModifiedBy = model.ModifiedBy
                //            });
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string PONo)
        {
            try
            {
                var model = DataAccessLayerServices.pd_ryo_OrderRepository()
                    .GetSingle(x => x.PONo == PONo);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล PO หมายเลข " + PONo + " ในระบบ");

                DataAccessLayerServices.pd_ryo_OrderRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_Order model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_OrderValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = DataAccessLayerServices.pd_ryo_OrderRepository()
                    .GetSingle(x => x.PONo == model.PONo);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล PO หมายเลข " + model.PONo + " ในระบบ");

                /// Get all order for check a dupplicate invoice no.
                var allOrder = DataAccessLayerServices.pd_ryo_OrderRepository().Get();
                if (model.InvoiceNo != editModel.InvoiceNo &&
                    allOrder.Where(x => x.InvoiceNo == model.InvoiceNo).Count() > 0)
                {
                    var exitsOrder = allOrder
                        .Where(x => x.InvoiceNo == model.InvoiceNo)
                        .FirstOrDefault();
                    throw new ArgumentException("Invoice no " + model.InvoiceNo +
                        " ซ้ำกับที่มีอยู่แล้วในระบบ " + Environment.NewLine +
                        "Crop: " + exitsOrder.Crop + Environment.NewLine +
                        "PONo: " + exitsOrder.PONo + Environment.NewLine +
                        "Customer: " + exitsOrder.Customer);
                }

                var customerOrderList = allOrder
                    .Where(x => x.Crop == model.Crop && x.Customer == model.Customer);

                if (editModel.ShippingCaseFrom != model.ShippingCaseFrom ||
                   editModel.ShippingCaseTo != model.ShippingCaseTo ||
                   editModel.Quantity != model.Quantity)
                {
                    var list = DataAccessLayerServices.pd_ryo_OrderRepository()
                        .Get(x => x.Crop == editModel.Crop && x.Customer == editModel.Customer);

                    if (list.Where(x => model.ShippingCaseFrom >= x.ShippingCaseFrom &&
                    model.ShippingCaseFrom <= x.ShippingCaseFrom).Count() > 0)
                        throw new ArgumentException("มีหมายเลขกล่อง " + model.ShippingCaseFrom + " นี้แล้วในระบบ โปรดตรวจสอบอีกครั้ง");

                    if (list.Where(x => model.ShippingCaseTo >= x.ShippingCaseTo &&
                    model.ShippingCaseTo <= x.ShippingCaseTo).Count() > 0)
                        throw new ArgumentException("มีหมายเลขกล่อง " + model.ShippingCaseFrom + " นี้แล้วในระบบ โปรดตรวจสอบอีกครั้ง");
                }

                /// ถ้าเป็นการแก้ไขข้อมูลหมายเลขกล่อง หรือจำนวนกล่อง ให้ไปลบข้อมูลบาร์โค้ตที่เคยสร้างไว้แล้วก่อน
                /// ไม่ต้องมีการบันทึกข้อมูลบาร์โค้ตของ insert paper ในระบบ
                /// 
                //if (editModel.pd_ryo_OrderDetail.Count() > 0)
                //{
                //    if (editModel.Quantity != model.Quantity ||
                //        editModel.ShippingCaseFrom != model.ShippingCaseFrom ||
                //        editModel.ShippingCaseTo != model.ShippingCaseTo)
                //    //throw new ArgumentException("เมื่อมีการเปลี่ยนแปลงข้อมูล Quantity หรือ ShippingCaseFrom " +
                //    //    "หรือ ShippingCaseTo จะต้องเข้าไปลบข้อมูล Master bag barcode ให้หมดก่อน");
                //    {
                //        var list1 = DataAccessLayerServices.pd_ryo_OrderDetailRepository()
                //            .Get(x => x.PONo == model.PONo)
                //            .ToList();

                //        pd_ryo_OrderDetail[] arr = list1.ToArray<pd_ryo_OrderDetail>();
                //        DataAccessLayerServices.pd_ryo_OrderDetailRepository()
                //            .Remove(arr);
                //    }
                //}

                editModel.InvoiceNo = model.InvoiceNo;
                editModel.Crop = model.Crop;
                editModel.Quantity = model.Quantity;
                editModel.UnitPrice = model.UnitPrice;
                editModel.ShippingCaseFrom = model.ShippingCaseFrom;
                editModel.ShippingCaseTo = model.ShippingCaseTo;
                editModel.CustomerPaymentDate = model.CustomerPaymentDate;
                editModel.DeliveryDate = model.DeliveryDate;
                editModel.ReceivedDate = model.ReceivedDate;
                editModel.ArrivalDate = model.ArrivalDate;
                editModel.DeliveryStatus = model.DeliveryStatus;
                editModel.Courier = model.Courier;
                editModel.CourierPaymentDate = model.CourierPaymentDate;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_OrderRepository().Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_Order> Get()
        {
            return DataAccessLayerServices.pd_ryo_OrderRepository().Get().ToList();
        }

        public List<pd_ryo_Order> GetByCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_OrderRepository()
                .Get(x => x.Crop == crop).ToList();
        }

        public pd_ryo_Order GetByInvoiceNo(string invoiceNo)
        {
            return DataAccessLayerServices.pd_ryo_OrderRepository()
                .GetSingle(x => x.InvoiceNo == invoiceNo);
        }

        public pd_ryo_Order GetByPO(string pono)
        {
            return DataAccessLayerServices.pd_ryo_OrderRepository()
                .GetSingle(x => x.PONo == pono);
        }
    }
}
