﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.Models
{
    public class FinishPacked
    {
        public string pdno { get; set; }
        public string packedgrade { get; set; }
        public string packinglocked { get; set; }
    }
}
