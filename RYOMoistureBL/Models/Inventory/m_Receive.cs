﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureBL.Models.Inventory
{
    public class m_Receive : pd_ryo_inventory_Receive
    {
        public decimal TotalPrice { get; set; }
        public string ReceiveUserName { get; set; }
    }
}
