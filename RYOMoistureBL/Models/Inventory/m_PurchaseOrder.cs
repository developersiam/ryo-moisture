﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureBL.Models.Inventory
{
    public class m_PurchaseOrder : pd_ryo_inventory_PO
    {
        public string SupplierName { get; set; }
        public string ContractPersonName { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
