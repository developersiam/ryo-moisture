﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;

namespace RYOMoistureBL.Models.Inventory
{
    public class m_ItemTransaction : pd_ryo_inventory_Item
    {
        public string UnitName { get; set; }
        public string CategoryName { get; set; }
        public string LotNumber { get; set; }
        public int OnHand { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public int IssuedQuantity { get; set; }
        public int ReturnQuantity { get; set; }
        public int DamagedQuantity { get; set; }
        public string DamagedDescription { get; set; }
    }
}
