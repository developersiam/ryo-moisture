﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL.Models
{
    public class pdPacked
    {
        public string ab4_bc { get; set; }
        public int? ab4_caseno { get; set; }
        public string bc { get; set; }
        public int caseno { get; set; }
        public decimal grossreal { get; set; }
        public double? boxtare { get; set; }
        public decimal? taredef { get; set; }
        public decimal? netreal { get; set; }
        public DateTime? packingtime { get; set; }
        public string box { get; set; }
        public DateTime? packingdate { get; set; }
        public DateTime? dtrecord { get; set; }
        public string bay { get; set; }
    }
}
