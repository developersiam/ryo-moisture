﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_customer_label_setupBL
    {
        List<pd_ryo_customer_label_setup> GetByCrop(short crop);
        void Add(pd_ryo_customer_label_setup model);
        void Edit(pd_ryo_customer_label_setup model);
        void Delete(string pdno);
        pd_ryo_customer_label_setup GetSingle(string pdno);
        pd_ryo_customer_label_setup GetSingleByCode(string code);

    }
    public class pd_ryo_customer_label_setupBL : Ipd_ryo_customer_label_setupBL
    {
        public List<pd_ryo_customer_label_setup> GetByCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_customer_label_setupRepository()
                .Get(x => x.crop == crop).OrderByDescending(x => x.pdno)
                .ToList();
        }
        public void Add(pd_ryo_customer_label_setup model)
        {
            try
            {

                var setupPacked = GetSingle(model.pdno);
                if (setupPacked != null) throw new ArgumentNullException("ข้อมูล " + model.pdno + " ถูก setup Label ไปแล้ว ไม่สามารถ Add ซ้ำได้");

                model.modifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_customer_label_setupRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public pd_ryo_customer_label_setup GetSingle(string pd_no)
        {
            return DataAccessLayerServices.pd_ryo_customer_label_setupRepository()
            .GetSingle(x => x.pdno == pd_no);
        }
        public void Edit(pd_ryo_customer_label_setup model)
        {
            try
            {
                var setupPacked = GetSingle(model.pdno);
                if (setupPacked == null) throw new ArgumentNullException("ไม่พบข้อมุลในระบบ");

                setupPacked.crop = model.crop;
                setupPacked.pdno = model.pdno;
                setupPacked.pdDate = model.pdDate;
                setupPacked.packedgrade = model.packedgrade;
                setupPacked.customerID = model.customerID;
                setupPacked.customerName = model.customerName;
                setupPacked.User = model.User;
                setupPacked.modifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_customer_label_setupRepository().Update(setupPacked);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string pdno)
        {
            try
            {
                var setupPacked = GetSingle(pdno);
                if (setupPacked == null) throw new ArgumentNullException("ไม่พบข้อมุลในระบบ");

                DataAccessLayerServices.pd_ryo_customer_label_setupRepository().Remove(setupPacked);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public pd_ryo_customer_label_setup GetSingleByCode(string code)
        {
            return DataAccessLayerServices.pd_ryo_customer_label_setupRepository()
                .Get(x => x.customerID == code).FirstOrDefault();
        }
    }
}
