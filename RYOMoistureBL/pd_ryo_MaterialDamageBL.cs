﻿using RYOMoistureDAL;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialDamageBL
    {
        void Add(pd_ryo_MaterialDamage model);
        void Edit(pd_ryo_MaterialDamage model);
        void Delete(string damageCode);
        List<pd_ryo_MaterialDamage> GetByCrop(short crop);
        List<pd_ryo_MaterialDamage> GetBypdno(string pdno);
        pd_ryo_MaterialDamage GetSingle(string damageCode);
    }

    public class pd_ryo_MaterialDamageBL : Ipd_ryo_MaterialDamageBL
    {
        public void Add(pd_ryo_MaterialDamage model)
        {
            try
            {
                /// Generate receiving code.
                /// 
                var list = GetByCrop(model.Crop);
                var maxRecord = list.Count < 1 ? 0 : list.Max(x => Convert.ToInt16(x.DamageCode.Substring(8, 5)));
                var code = "DM-" + model.Crop + "-" + (maxRecord + 1).ToString().PadLeft(5, '0');

                model.DamageCode = code;

                var validation = new DomainValidation.Ispd_ryo_MaterialDamageValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialDamageRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string damageCode)
        {
            try
            {
                var model = GetSingle(damageCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.pd_ryo_MaterialDamageDetail.Count() > 0)
                    throw new ArgumentException("มีข้อมูลรายการ material ที่บันทึกไว้ใน damage code " +
                        model.DamageCode + " ไม่สามารถลบข้อมูลนี้ได้");

                DataAccessLayerServices.pd_ryo_MaterialDamageRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialDamage model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialDamageValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.DamageCode);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Crop = model.Crop;
                editModel.DamageReasonID = model.DamageReasonID;
                editModel.pdno = model.pdno;
                editModel.ModifiedBy = model.ModifiedBy;

                DataAccessLayerServices.pd_ryo_MaterialDamageRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialDamage> GetByCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageRepository()
                .Get(x => x.Crop == crop,
                null,
                x => x.pd_ryo_MaterialDamageReason)
                .ToList();
        }

        public List<pd_ryo_MaterialDamage> GetBypdno(string pdno)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageRepository()
                .Get(x => x.pdno == pdno,
                null,
                x => x.pd_ryo_MaterialDamageReason)
                .ToList();
        }

        public pd_ryo_MaterialDamage GetSingle(string damageCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageRepository()
                .GetSingle(x => x.DamageCode == damageCode,
                x => x.pd_ryo_MaterialDamageDetail,
                x => x.pd_ryo_MaterialDamageReason);
        }
    }
}
