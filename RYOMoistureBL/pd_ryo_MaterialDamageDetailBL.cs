﻿using RYOMoistureDAL;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialDamageDetailBL
    {
        void Add(pd_ryo_MaterialDamageDetail model);
        void Edit(pd_ryo_MaterialDamageDetail model);
        void Delete(string damageCode, string itemCode);
        List<pd_ryo_MaterialDamageDetail> GetByCrop(short crop);
        List<pd_ryo_MaterialDamageDetail> GetByDamageCode(string damageCode);
        pd_ryo_MaterialDamageDetail GetSingle(string damageCode, string itemCode);
    }

    public class pd_ryo_MaterialDamageDetailBL : Ipd_ryo_MaterialDamageDetailBL
    {
        public void Add(pd_ryo_MaterialDamageDetail model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialDamageDetailValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var list = GetByDamageCode(model.DamageCode);
                if (list.Where(x => x.ItemCode == model.ItemCode).Count() > 0)
                    throw new ArgumentNullException("มีข้อมูล Item Code " + model.ItemCode +
                        " อยู่แล้วใน damage code " + model.DamageCode);

                model.RecordDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialDamageDetailRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string damageCode, string itemCode)
        {
            try
            {
                var model = GetSingle(damageCode, itemCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                DataAccessLayerServices.pd_ryo_MaterialDamageDetailRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialDamageDetail model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialDamageDetailValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.DamageCode, model.ItemCode);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูล item code " + model.ItemCode + " ในระบบ");

                editModel.Quantity = model.Quantity;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_MaterialDamageDetailRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialDamageDetail> GetByCrop(short crop)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageDetailRepository()
                .Get(x => x.pd_ryo_MaterialDamage.Crop == crop,
                null,
                x => x.pd_ryo_MaterialItem,
                x => x.pd_ryo_MaterialDamage)
                .ToList();
        }

        public List<pd_ryo_MaterialDamageDetail> GetByDamageCode(string damageCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageDetailRepository()
                .Get(x => x.DamageCode == damageCode,
                null,
                x => x.pd_ryo_MaterialItem,
                x => x.pd_ryo_MaterialDamage)
                .ToList();
        }

        public pd_ryo_MaterialDamageDetail GetSingle(string damageCode, string itemCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialDamageDetailRepository()
                .GetSingle(x => x.DamageCode == damageCode &&
                x.ItemCode == itemCode,
                x => x.pd_ryo_MaterialItem,
                x => x.pd_ryo_MaterialDamage);
        }
    }
}
