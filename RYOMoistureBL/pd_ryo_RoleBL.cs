﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_RoleBL
    {
        void Add(pd_ryo_Role model);
        void Edit(pd_ryo_Role model);
        void Delete(Guid id);
        pd_ryo_Role GetSingle(Guid id);
        List<pd_ryo_Role> Get();
    }

    public class pd_ryo_RoleBL : Ipd_ryo_RoleBL
    {
        public void Add(pd_ryo_Role model)
        {
            try
            {
                if (DataAccessLayerServices.pd_ryo_RoleRepository()
                    .Get(x => x.RoleName == model.RoleName)
                    .Count() > 0)
                    throw new ArgumentException("role " + model.RoleName + " นี้มีซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = model.CreateBy;

                DataAccessLayerServices.pd_ryo_RoleRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.pd_ryo_UserRole.Count() > 0)
                    throw new ArgumentException("Role นี้ถูกกำหนดให้ user บางรายไปแล้ว โปรดลบข้อมูลการกำหนดสิทธิ์ออก่กอน");

                DataAccessLayerServices.pd_ryo_RoleRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_Role model)
        {
            try
            {
                var editModel = GetSingle(model.RoleID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.RoleName = model.RoleName;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = DateTime.Now;

                DataAccessLayerServices.pd_ryo_RoleRepository().Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_Role> Get()
        {
            return DataAccessLayerServices.pd_ryo_RoleRepository().Get().ToList();
        }

        public pd_ryo_Role GetSingle(Guid id)
        {
            return DataAccessLayerServices.pd_ryo_RoleRepository().GetSingle(x => x.RoleID == id);
        }
    }
}
