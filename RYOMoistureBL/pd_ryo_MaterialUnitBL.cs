﻿using DomainModelStecDbms;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoistureBL
{
    public interface Ipd_ryo_MaterialUnitBL
    {
        void Add(pd_ryo_MaterialUnit model);
        void Edit(pd_ryo_MaterialUnit model);
        void Delete(short unitCode);
        pd_ryo_MaterialUnit GetSingle(short unitCode);
        List<pd_ryo_MaterialUnit> GetAll();
    }

    public class pd_ryo_MaterialUnitBL : Ipd_ryo_MaterialUnitBL
    {
        public void Add(pd_ryo_MaterialUnit model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialUnitValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var dupplicated = DataAccessLayerServices.pd_ryo_MaterialUnitRepository()
                    .GetSingle(x => x.UnitName == model.UnitName);

                if (dupplicated != null)
                    throw new ArgumentException("มีข้อมูล " + model.UnitName + " นี้แล้วในระบบ");

                DataAccessLayerServices.pd_ryo_MaterialUnitRepository().Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short unitCode)
        {
            try
            {
                var model = GetSingle(unitCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var items = DataAccessLayerServices.pd_ryo_MaterialItemUnitRepository()
                    .GetList(x=>x.UnitCode == unitCode);

                if (items.Count() > 0)
                    throw new ArgumentException("ข้อมูลนี้เชื่อมโยงกับข้อมูลอื่นๆ ในระบบ ไม่สามารถลบข้อมูลนี้ได้");

                DataAccessLayerServices.pd_ryo_MaterialUnitRepository().Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(pd_ryo_MaterialUnit model)
        {
            try
            {
                var validation = new DomainValidation.Ispd_ryo_MaterialUnitValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var editModel = GetSingle(model.UnitCode);

                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.UnitName = model.UnitName;
                editModel.ModifiedBy = model.ModifiedBy;
                editModel.ModifiedDate = model.ModifiedDate;

                DataAccessLayerServices.pd_ryo_MaterialUnitRepository().Update(editModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd_ryo_MaterialUnit> GetAll()
        {
            return DataAccessLayerServices.pd_ryo_MaterialUnitRepository().GetAll().ToList();
        }

        public pd_ryo_MaterialUnit GetSingle(short unitCode)
        {
            return DataAccessLayerServices.pd_ryo_MaterialUnitRepository().GetSingle(x => x.UnitCode == unitCode);
        }
    }
}
