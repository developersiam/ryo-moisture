//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelStecDbms
{
    using System;
    using System.Collections.Generic;
    
    public partial class pd_ryo_inventory_Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pd_ryo_inventory_Employee()
        {
            this.pd_ryo_inventory_Issued = new HashSet<pd_ryo_inventory_Issued>();
            this.pd_ryo_inventory_Receive = new HashSet<pd_ryo_inventory_Receive>();
            this.pd_ryo_inventory_Return = new HashSet<pd_ryo_inventory_Return>();
            this.pd_ryo_inventory_Damaged = new HashSet<pd_ryo_inventory_Damaged>();
        }
    
        public string EmployeeCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_Issued> pd_ryo_inventory_Issued { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_Receive> pd_ryo_inventory_Receive { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_Return> pd_ryo_inventory_Return { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_Damaged> pd_ryo_inventory_Damaged { get; set; }
    }
}
