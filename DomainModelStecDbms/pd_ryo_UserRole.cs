//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelStecDbms
{
    using System;
    using System.Collections.Generic;
    
    public partial class pd_ryo_UserRole
    {
        public string Username { get; set; }
        public System.Guid RoleID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
    
        public virtual pd_ryo_Role pd_ryo_Role { get; set; }
        public virtual pd_ryo_UserAccount pd_ryo_UserAccount { get; set; }
    }
}
