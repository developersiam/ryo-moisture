//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelStecDbms
{
    using System;
    using System.Collections.Generic;
    
    public partial class pd_ryo_MasterBagConfig
    {
        public string ConfigCode { get; set; }
        public string Description { get; set; }
        public decimal TareWeight { get; set; }
        public decimal MinWeight { get; set; }
        public decimal MaxWeight { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public bool DefaultStatus { get; set; }
    }
}
