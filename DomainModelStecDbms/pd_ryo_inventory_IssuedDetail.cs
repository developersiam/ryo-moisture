//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelStecDbms
{
    using System;
    using System.Collections.Generic;
    
    public partial class pd_ryo_inventory_IssuedDetail
    {
        public System.Guid IssuedID { get; set; }
        public string IssuedCode { get; set; }
        public string ItemCode { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
        public string LotNo { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    
        public virtual pd_ryo_inventory_Issued pd_ryo_inventory_Issued { get; set; }
        public virtual pd_ryo_inventory_Item pd_ryo_inventory_Item { get; set; }
    }
}
