//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModelStecDbms
{
    using System;
    using System.Collections.Generic;
    
    public partial class pd_ryo_inventory_Item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public pd_ryo_inventory_Item()
        {
            this.pd_ryo_inventory_IssuedDetail = new HashSet<pd_ryo_inventory_IssuedDetail>();
            this.pd_ryo_inventory_IssuedReferenceDetail = new HashSet<pd_ryo_inventory_IssuedReferenceDetail>();
            this.pd_ryo_inventory_PODetail = new HashSet<pd_ryo_inventory_PODetail>();
            this.pd_ryo_inventory_ReceiveDetail = new HashSet<pd_ryo_inventory_ReceiveDetail>();
            this.pd_ryo_inventory_ReturnDetail = new HashSet<pd_ryo_inventory_ReturnDetail>();
            this.pd_ryo_inventory_Transaction = new HashSet<pd_ryo_inventory_Transaction>();
            this.pd_ryo_inventory_DamagedDetail = new HashSet<pd_ryo_inventory_DamagedDetail>();
        }
    
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public string ItemCategoryCode { get; set; }
        public string Unit { get; set; }
        public string ModifiedBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_IssuedDetail> pd_ryo_inventory_IssuedDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_IssuedReferenceDetail> pd_ryo_inventory_IssuedReferenceDetail { get; set; }
        public virtual pd_ryo_inventory_ItemCategory pd_ryo_inventory_ItemCategory { get; set; }
        public virtual pd_ryo_inventory_Unit pd_ryo_inventory_Unit { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_PODetail> pd_ryo_inventory_PODetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_ReceiveDetail> pd_ryo_inventory_ReceiveDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_ReturnDetail> pd_ryo_inventory_ReturnDetail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_Transaction> pd_ryo_inventory_Transaction { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<pd_ryo_inventory_DamagedDetail> pd_ryo_inventory_DamagedDetail { get; set; }
    }
}
