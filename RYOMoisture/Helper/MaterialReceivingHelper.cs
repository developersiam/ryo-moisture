﻿using DomainModelStecDbms;
using RYOMoisture.Model;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoisture.Helper
{
    public static class MaterialReceivingHelper
    {
        public static List<m_MaterialStockSummary> GetCurrentStockByCrop(short crop)
        {
            try
            {
                /// Get total of master box per issued code.
                /// 
                var masterboxPerIssuedCode = BusinessLayerServices.MoistureBL()
                    .GetMasterBoxListByHasIssuedCode(crop)
                    .GroupBy(x => new { x.IssuedCode })
                    .Select(x => new
                    {
                        IssuedCode = x.Key.IssuedCode,
                        TotalMasterBox = x.Count()
                    });

                /// Get item per box by issued code.
                /// 
                var issuedPerBoxList = BusinessLayerServices.pd_ryo_MaterialIssuedPerBoxBL().GetByCrop(crop);

                var realIssuedlist = (from a in issuedPerBoxList
                                      from b in masterboxPerIssuedCode
                                      where a.IssuedCode == b.IssuedCode
                                      select new
                                      {
                                          IssuedCode = a.IssuedCode,
                                          ItemCode = a.ItemCode,
                                          PcsPerBox = a.PcsPerBox,
                                          TotalMasterBox = b.TotalMasterBox,
                                          NumberOfIssued = a.PcsPerBox * b.TotalMasterBox
                                      }).ToList();

                /// Get the all crop received.
                /// 
                var receivedList = BusinessLayerServices.pd_ryo_MaterialReceivingBL()
                    .GetByReceivedCrop(crop)
                    .GroupBy(x => new
                    {
                        x.Crop,
                        x.ItemCode,
                        x.pd_ryo_MaterialItem.ItemName
                    })
                    .Select(x => new m_MaterialStockSummary
                    {
                        Crop = x.Key.Crop,
                        ItemCode = x.Key.ItemCode,
                        ItemName = x.Key.ItemName,
                        TotalReceived = x.Sum(y => y.Quantity)
                    }).ToList();

                var returnList = (from a in receivedList
                                  join b in realIssuedlist
                                  on a.ItemCode equals b.ItemCode into s
                                  from b in s.DefaultIfEmpty()
                                  select new m_MaterialStockSummary
                                  {
                                      Crop = a.Crop,
                                      ItemCode = a.ItemCode,
                                      ItemName = a.ItemName,
                                      TotalReceived = a.TotalReceived,
                                      TotalIssued = b == null ? 0 : b.NumberOfIssued,
                                      OnHand = a.TotalReceived - (b == null ? 0 : b.NumberOfIssued),
                                      RemainingPercentages = Convert.ToDecimal(a.TotalReceived - (b == null ? 0 : b.NumberOfIssued)) / Convert.ToDecimal(a.TotalReceived) * 100
                                  }).ToList();

                var list = BusinessLayerServices.pd_ryo_MaterialDamageDetailBL().GetByCrop(crop);
                if (list.Count() < 1)
                    return returnList;

                /// Get total of damage material group by item code.
                /// 
                var damageMaterial = BusinessLayerServices.pd_ryo_MaterialDamageDetailBL()
                    .GetByCrop(crop)
                    .GroupBy(x => new { x.pd_ryo_MaterialDamage.Crop, x.ItemCode })
                    .Select(x => new
                    {
                        Crop = x.Key.Crop,
                        ItemCode = x.Key.ItemCode,
                        Quantity = x.Sum(y => y.Quantity)
                    });

                /// นำข้อมูลด้านบนรวมกับ damage ทั้งหมดเพื่อ return กลับ
                /// 
                var returnList1 = (from a in returnList
                                   join b in damageMaterial
                                   on new { a.Crop, a.ItemCode } equals new { b.Crop, b.ItemCode }
                                   into s
                                   from b in s.DefaultIfEmpty()
                                   select new m_MaterialStockSummary
                                   {
                                       Crop = a.Crop,
                                       ItemCode = a.ItemCode,
                                       ItemName = a.ItemName,
                                       TotalReceived = a.TotalReceived,
                                       TotalIssued = a.TotalIssued,
                                       TotalDamage = b == null ? 0 : b.Quantity,
                                       OnHand = a.OnHand,
                                       RemainingPercentages = Convert.ToDecimal(a.OnHand) / Convert.ToDecimal(a.TotalReceived) * 100
                                   })
                                   .ToList();

                return returnList1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
