﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace RYOMoisture.Helper
{
    public struct ComportParameter
    {
        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public Parity Parity { get; set; }
        public short DataBits { get; set; }
        public StopBits StopBits { get; set; }
        public short StrLength { get; set; }
    }

    public static class DigitalScaleHelper
    {
        public static SerialPort serialPort = new SerialPort();
        static string _lineReadIn;
        static short _strLength;
        static int _theardSleep;

        //private delegate void preventCrossThreading(string x);
        //private static preventCrossThreading accessControlFromCentralThread;

        public static void Setup()
        {
            try
            {
                var ports = SerialPort.GetPortNames();
                if (ports.Count() <= 0)
                    throw new ArgumentException("ไม่พบพอร์ตสำหรับการเขื่อมต่อเครื่องชั่งบนเครื่องคอมพิวเตอร์ของท่าน");

                var hasPortName = false;
                for (int i = 0; i < ports.Length; i++)
                {
                    if (ports[i] == Properties.Settings.Default.PortName)
                    {
                        hasPortName = true;
                        break;
                    }
                }

                if (!hasPortName)
                    throw new ArgumentException("ไม่พบพอร์ต " + Properties.Settings.Default.PortName +
                        " เชื่อมต่อกับเครื่องคอมพิวเตอร์ของคุณ โปรดตรวจสอบการตั้งค่าเครื่องชั่งอีกครั้ง");

                serialPort.PortName = Properties.Settings.Default.PortName;
                serialPort.BaudRate = Properties.Settings.Default.BaudRate;
                serialPort.Parity = Properties.Settings.Default.Parity;
                serialPort.DataBits = Properties.Settings.Default.DataBit;
                serialPort.StopBits = Properties.Settings.Default.StopBits;

                _theardSleep = Properties.Settings.Default.ThreadSleep;
                _strLength = Properties.Settings.Default.SubStringLength;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWeightResultFromTigerModel()
        {
            try
            {
                _lineReadIn = "";
                _lineReadIn += serialPort.ReadExisting();
                Thread.Sleep(_theardSleep);

                /// display what we've acquired.
                string result = _lineReadIn;

                if (result.Length <= _strLength)
                    return "0";

                string[] splitString;
                string[] stringSeparators = new string[] { "\r\n" };

                splitString = result.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 2)
                    return "Scale Error";

                result = splitString[1];

                result = result.Replace(" ", string.Empty);
                result = result.Replace("", string.Empty);
                
                /// replace charctor ทีละตัว ตัวไหนที่ไม่ใช่ตัวเลข จุด และเครื่องหมายลบ ให้ทำการ replace ด้วย ""
                /// 
                char[] replaceOperator = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', '-' };
                foreach (var str in result)
                {
                    bool flag = true;
                    foreach (var opr in replaceOperator)
                    {
                        if (str == opr)
                        {
                            flag = false;
                            break;
                        }
                    }

                    if (flag == true)
                        result = result.Replace(str.ToString(), string.Empty);
                }

                /// ตรวจสอบว่าข้อมูลที่ตัดออกมากไม่อยู่ในรูปแบบ decimal ให้ return เลข Error ออกไปแสดงที่หน้าจอ
                decimal resultWeight = 0;
                if (result != "")
                {
                    decimal value;
                    if (Decimal.TryParse(result, out value))
                    {
                        resultWeight = Convert.ToDecimal(result);
                        result = resultWeight.ToString("N1");
                    }
                    else
                        return "Scale Error";
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
