﻿using DomainModelStecDbms;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoisture.Helper
{
    public struct OrderCrop
    {
        public short Crop { get; set; }
    }

    public struct TrueFalseStatus
    {
        public bool Status { get; set; }
        public string StatusName { get; set; }
    }

    public static class user_setting
    {
        public static List<OrderCrop> Crops
        {
            get
            {
                List<OrderCrop> list = new List<OrderCrop>();
                foreach (var item in BusinessLayerServices.pd_ryo_OrderBL()
                    .Get()
                    .GroupBy(x => x.Crop)
                    .Select(x => new pd_ryo_Order { Crop = x.Key }))
                {
                    list.Add(new OrderCrop { Crop = item.Crop });
                }

                if (list.Where(x => x.Crop == DateTime.Now.Year).Count() <= 0)
                    list.Add(new OrderCrop { Crop = Convert.ToInt16(DateTime.Now.Year) });

                return list;
            }
        }
        public static List<TrueFalseStatus> Status
        {
            get
            {
                List<TrueFalseStatus> list = new List<TrueFalseStatus>();
                list.Add(new TrueFalseStatus { Status = true, StatusName = "Finish" });
                list.Add(new TrueFalseStatus { Status = false, StatusName = "Not Finish" });

                return list;
            }
        }
        public static pd_ryo_UserAccount User { get; set; }
        public static List<pd_ryo_Role> UserRoles { get; set; }
    }
}
