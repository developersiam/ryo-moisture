﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoisture.Model
{
    public class m_MasterBagBarcodeTemplate
    {
        public string Barcode01 { get; set; }
        public string Barcode02 { get; set; }
        public string Barcode03 { get; set; }
        public string Barcode04 { get; set; }
        public string Barcode05 { get; set; }
        public string Barcode06 { get; set; }
        public string Barcode07 { get; set; }
        public string Barcode08 { get; set; }
        public string Barcode09 { get; set; }
        public string Barcode10 { get; set; }
        public string Barcode11 { get; set; }
        public string Barcode12 { get; set; }
        public string Barcode13 { get; set; }
        public string Barcode14 { get; set; }
        public string Barcode15 { get; set; }
        public string Barcode16 { get; set; }
        public string Barcode17 { get; set; }
        public string Barcode18 { get; set; }
        public string Barcode19 { get; set; }
        public string Barcode20 { get; set; }
    }
}
