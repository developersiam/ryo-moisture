﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoisture.Model
{
    public class m_MasterBagBarcode
    {
        public string MasterBagCode { get; set; }
        public string PONo { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int RowNumber { get; set; }
    }
}
