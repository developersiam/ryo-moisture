﻿using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoisture.Model
{
    public class m_Bagging
    {
        public pd Pds { get; set; }
        public pd_moisture PdMois { get; set; }
        public string Status { get; set; }
    }
}
