﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.View.MasterBagConfig
{
    /// <summary>
    /// Interaction logic for MasterBagConfigList.xaml
    /// </summary>
    public partial class MasterBagConfigList : Page
    {
        public MasterBagConfigList()
        {
            InitializeComponent();
            DataContext = new ViewModel.MasterBagConfig.vm_MasterBagConfigList();
        }
    }
}
