﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.View.Order
{
    /// <summary>
    /// Interaction logic for InsertPaper.xaml
    /// </summary>
    public partial class InsertPaper : Window
    {
        public InsertPaper()
        {
            InitializeComponent();
            DataContext = new vm_InsertPaper();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_InsertPaper)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.CaseNoFrom):
                    CaseNoFromTextBox.SelectAll();
                    CaseNoFromTextBox.Focus();
                    break;
                case nameof(vm.CaseNoTo):
                    CaseNoToTextBox.SelectAll();
                    CaseNoToTextBox.Focus();
                    break;
                case nameof(vm.CustomerCode):
                    CustomerComboBox.Focus();
                    break;
                case nameof(vm.Order.PONo):
                    PoNoTextBox.SelectAll();
                    PoNoTextBox.Focus();
                    break;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CaseNoFromTextBox.Focus();

            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }
    }
}
