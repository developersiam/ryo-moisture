﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Issued;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Issued
{
    /// <summary>
    /// Interaction logic for IssuedDetails.xaml
    /// </summary>
    public partial class IssuedDetail : Window
    {
        public IssuedDetail()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_IssuedDetail)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.FromCaseNo):
                    FromCaseNoTextBox.SelectAll();
                    FromCaseNoTextBox.Focus();
                    break;
                case nameof(vm.ToCaseNo):
                    ToCaseNoTextBox.SelectAll();
                    ToCaseNoTextBox.Focus();
                    break;
                case nameof(vm.TotalCase):
                    TotalCaseTextBox.SelectAll();
                    TotalCaseTextBox.Focus();
                    break;
                case nameof(vm.IssuedUser):
                    IssuedUserComboBox.Focus();
                    break;
                case nameof(vm.Topdno):
                    ToPdnoComboBox.Focus();
                    break;
                case nameof(vm.Quantity):
                    QuantityTextBox.SelectAll();
                    QuantityTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
