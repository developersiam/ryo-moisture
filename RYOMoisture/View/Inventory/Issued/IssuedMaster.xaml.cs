﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RYOMoisture.ViewModel.Inventory.Issued;
using RYOMoisture.MVVM;

namespace RYOMoisture.View.Inventory.Issued
{
    /// <summary>
    /// Interaction logic for IssuedMaster.xaml
    /// </summary>
    public partial class IssuedMaster : Window
    {
        public IssuedMaster()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_IssuedMaster)DataContext;
            switch (e.PropertyName)
            {
                default:
                    break;
            }
        }
    }
}
