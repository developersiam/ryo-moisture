﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Issued;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Issued
{
    /// <summary>
    /// Interaction logic for IssuedReferenceDetail.xaml
    /// </summary>
    public partial class IssuedReferenceDetail : Window
    {
        public IssuedReferenceDetail()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_IssuedReferenceDetail)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.RefCode):
                    RefCodeTextBox.SelectAll();
                    RefCodeTextBox.Focus();
                    break;
                case nameof(vm.ItemCode):
                    ItemCodeTextBox.SelectAll();
                    ItemCodeTextBox.Focus();
                    break;
                case nameof(vm.Quantity):
                    QuantityTextBox.SelectAll();
                    QuantityTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
