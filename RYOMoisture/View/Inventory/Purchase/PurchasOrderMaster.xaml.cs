﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Purchase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Purchase
{
    /// <summary>
    /// Interaction logic for POMaster.xaml
    /// </summary>
    public partial class PurchaseOrderMaster : Window
    {
        public PurchaseOrderMaster()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_PurchaseOrderMaster)DataContext;
            switch (e.PropertyName)
            {
                default:
                    break;
            }
        }
    }
}
