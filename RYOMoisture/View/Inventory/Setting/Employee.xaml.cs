﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Setting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Setting
{
    /// <summary>
    /// Interaction logic for Employee.xaml
    /// </summary>
    public partial class Employee : Window
    {
        public Employee()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Employee)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.EmployeeCode):
                    EmployeeCodeTextBox.SelectAll();
                    EmployeeCodeTextBox.Focus();
                    break;
                case nameof(vm.FirstName):
                    FirstNameTextBox.SelectAll();
                    FirstNameTextBox.Focus();
                    break;
                case nameof(vm.LastName):
                    LastNameTextBox.SelectAll();
                    LastNameTextBox.Focus();
                    break;
                case nameof(vm.Department):
                    DepartmentTextBox.SelectAll();
                    DepartmentTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
