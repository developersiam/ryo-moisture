﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Setting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Setting
{
    /// <summary>
    /// Interaction logic for Supplier.xaml
    /// </summary>
    public partial class Supplier : Window
    {
        public Supplier()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_supplier)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.SupplierCode):
                    SupplierCodeTextBox.SelectAll();
                    SupplierCodeTextBox.Focus();
                    break;
                case nameof(vm.SupplierName):
                    SupplierNameTextBox.SelectAll();
                    SupplierNameTextBox.Focus();
                    break;
                case nameof(vm.Address):
                    AddressTextBox.SelectAll();
                    AddressTextBox.Focus();
                    break;
                case nameof(vm.TelephoneNumber):
                    TelephoneNumberTextBox.SelectAll();
                    TelephoneNumberTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
