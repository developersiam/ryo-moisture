﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Damaged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Damaged
{
    /// <summary>
    /// Interaction logic for DamagedDetail.xaml
    /// </summary>
    public partial class DamagedDetail : Window
    {
        public DamagedDetail()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_DamagedDetail)DataContext;
            switch (e.PropertyName)
            {
                case (nameof(vm.IssuedCode)):
                    IssuedCodeComboBox.Focus();
                    break;
                case (nameof(vm.DamagedQuantity)):
                    DamageQuantityTextBox.Focus();
                    DamageQuantityTextBox.SelectAll();
                    break;
                case (nameof(vm.DamagedDescription)):
                    DamagedDescriptionTextBox.SelectAll();
                    DamagedDescriptionTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
