﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Return;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Return
{
    /// <summary>
    /// Interaction logic for ReturnDetails.xaml
    /// </summary>
    public partial class ReturnDetail : Window
    {
        public ReturnDetail()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_ReturnDetail)DataContext;
            switch (e.PropertyName)
            {
                case (nameof(vm.IssuedCode)):
                    IssuedCodeComboBox.Focus();
                    break;
                case (nameof(vm.ReturnQuantity)):
                    ReturnQuantityTextBox.SelectAll();
                    ReturnQuantityTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
