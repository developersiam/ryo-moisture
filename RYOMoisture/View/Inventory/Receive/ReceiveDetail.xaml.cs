﻿using RYOMoisture.MVVM;
using RYOMoisture.ViewModel.Inventory.Receive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.View.Inventory.Receive
{
    /// <summary>
    /// Interaction logic for ReceiveDetails.xaml
    /// </summary>
    public partial class ReceiveDetail : Window
    {
        public ReceiveDetail()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            PONoTextBox.Focus();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_ReceiveDetail)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.ReceiveType):
                    ReceiveTypeComboBox.Focus();
                    break;
                case nameof(vm.ReceiveCode):
                    ReceiveCodeTextBox.SelectAll();
                    ReceiveCodeTextBox.Focus();
                    break;
                case nameof(vm.ReceiveDate):
                    ReceiveDateDtaePicker.Focus();
                    break;
                case nameof(vm.InvoiceNo):
                    InvoiceNoTextBox.SelectAll();
                    InvoiceNoTextBox.Focus();
                    break;
                case nameof(vm.ReceiveUser):
                    ReceiveUserComboBox.Focus();
                    break;
                case nameof(vm.ReceiveDescription):
                    DescriptionTextBox.SelectAll();
                    DescriptionTextBox.Focus();
                    break;
                case nameof(vm.ItemCode):
                    ItemCodeTextBox.SelectAll();
                    ItemCodeTextBox.Focus();
                    break;
                case nameof(vm.Quantity):
                    QuantityTextBox.SelectAll();
                    QuantityTextBox.Focus();
                    break;
                case nameof(vm.UnitPrice):
                    UnitPriceTextBox.SelectAll();
                    UnitPriceTextBox.Focus();
                    break;
                case nameof(vm.PONo):
                    PONoTextBox.SelectAll();
                    PONoTextBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
