﻿using RYOMoisture.Form;
using RYOMoisture.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            //user_setting.User = new DomainModelStecDbms.pd_ryo_UserAccount { Username = "eakkaluck" };
            //var window = new View.Inventory.Receive.ReceiveDetail();
            //var vm = new ViewModel.Inventory.Receive.vm_ReceiveDetail();
            //window.DataContext = vm;
            //vm.Window = window;
            //vm.ReceiveType = "Brought Forward";
            //window.ShowDialog();
        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "QC").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Moisture.MoisturePage());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BaggingMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "QC").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Moisture.BaggingPage());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OrderMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Orders.Order());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceivedMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Materials.ReceivingsBalanceStock());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void IssuedPerBoxMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Materials.IssuedCodes());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DamageItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Materials.Damages());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ManageUserMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.UserAccounts.UserAccounts());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LofOffMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                user_setting.User = null;
                user_setting.UserRoles = null;
                MainFrame.NavigationService.Navigate(new Form.Login());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void HomeReportMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new Form.Home());
        }

        private void CustomerLabelItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Production.MasterBoxLabelWindow());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackingItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Production.ProductionPage());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerLCItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new Form.Materials.CustomerCodeLabel());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SetupDigitalScaleItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Form.Production.SetupDigitalScale window = new Form.Production.SetupDigitalScale();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #region RYO Inventory
        private void InventoryPOMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Purchase.PurchaseOrderMaster();
                var vm = new ViewModel.Inventory.Purchase.vm_PurchaseOrderMaster();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryReceiveMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Receive.ReceiveMaster();
                var vm = new ViewModel.Inventory.Receive.vm_ReceiveMaster();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryIssuedMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Issued.IssuedMaster();
                var vm = new ViewModel.Inventory.Issued.vm_IssuedMaster();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryIssuedRefMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Issued.IssuedReference();
                var vm = new ViewModel.Inventory.Issued.vm_IssuedReference();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryReturnMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Return.ReturnMaster();
                var vm = new ViewModel.Inventory.Return.vm_ReturnMaster();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryDamagedMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Damaged.DamagedMaster();
                var vm = new ViewModel.Inventory.Damaged.vm_DamagedMaster();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryBalanceStockReportMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Report.BalanceStock();
                var vm = new ViewModel.Inventory.Report.vm_BalanceStock();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryBalanceScoreCard_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Report.BalanceScoreCard();
                var vm = new ViewModel.Inventory.Report.vm_BalanceScoreCard();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #region Setting Menu
        private void InventoryCategoryMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Setting.Category();
                var vm = new ViewModel.Inventory.Setting.vm_Category();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Setting.Item();
                var vm = new ViewModel.Inventory.Setting.vm_Item();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventorySupplierMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Setting.Supplier();
                var vm = new ViewModel.Inventory.Setting.vm_supplier();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryEmployeeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Setting.Employee();
                var vm = new ViewModel.Inventory.Setting.vm_Employee();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InventoryUnitMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Inventory Admin").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                var window = new View.Inventory.Setting.Unit();
                var vm = new ViewModel.Inventory.Setting.vm_Unit();
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion

        #endregion

        private void SetupTareWeightMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new Form.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() <= 0 &&
                    user_setting.UserRoles.Where(x => x.RoleName == "CUTRAG").Count() <= 0)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานเมนูนี้ได้");

                MainFrame.Navigate(new View.MasterBagConfig.MasterBagConfigList());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
