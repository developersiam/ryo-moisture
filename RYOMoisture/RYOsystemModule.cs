﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace RYOMoisture
{
    class RYOsystemModule
    {
        [DllImport("tsclib.dll", EntryPoint = "openport", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long openport(string PrinterName);
        [DllImport("tsclib.dll", EntryPoint = "closeport", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long closeport();
        [DllImport("tsclib.dll", EntryPoint = "sendcommand", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long sendcommand(string command);
        [DllImport("tsclib.dll", EntryPoint = "setup", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long setup(string LabelWidth, string LabelHeight, string Speed, string Density, string Sensor, string Vertical, string Offset);
        [DllImport("tsclib.dll", EntryPoint = "downloadpcx", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long downloadpcx(string Filename, string ImageName);
        [DllImport("tsclib.dll", EntryPoint = "barcode", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long barcode(string X, string Y, string CodeType, string Height, string Readable, string rotation, string Narrow, string Wide, string Code);
        [DllImport("tsclib.dll", EntryPoint = "printerfont", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long printerfont(string X, string Y, string FontName, string rotation, string Xmul, string Ymul, string Content);
        [DllImport("tsclib.dll", EntryPoint = "clearbuffer", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long clearbuffer();
        [DllImport("tsclib.dll", EntryPoint = "printlabel", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long printlabel(string NumberOfSet, string NumberOfCopy);
        [DllImport("tsclib.dll", EntryPoint = "formfeed", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long formfeed();
        [DllImport("tsclib.dll", EntryPoint = "nobackfeed", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long nobackfeed(string PrinterName);
        [DllImport("tsclib.dll", EntryPoint = "windowsfont", ExactSpelling = false, CharSet = CharSet.Ansi, SetLastError = true)]
        public static extern long windowsfont(int X, int Y, int fontheight, int rotation, int fontstyle, int fontunderline, string FaceName, string TextContent);
    }
}
