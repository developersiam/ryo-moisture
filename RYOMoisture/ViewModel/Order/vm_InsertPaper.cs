﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.Model;
using RYOMoisture.MVVM;
using RYOMoisture.View.Order;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Order
{
    public class vm_InsertPaper : ObservableObject, IRequestFocus
    {
        public vm_InsertPaper()
        {
            _loadingLabelVisibility = Visibility.Collapsed;
            _dataGridVisibility = Visibility.Visible;
            _printButtonIsEnabled = false;
            _stopWatch = new Stopwatch();
            RaisePropertyChangedEvent(nameof(LoadingLabelVisibility));
            RaisePropertyChangedEvent(nameof(DataGridVisibility));
            RaisePropertyChangedEvent(nameof(PrintButtonIsEnabled));
        }

        #region Properties
        private pd_ryo_Order _order;

        public pd_ryo_Order Order
        {
            get { return _order; }
            set { _order = value; }
        }

        private string _customerCode;

        public string CustomerCode
        {
            get { return _customerCode; }
            set { _customerCode = value; }
        }

        private int _caseNoFrom;

        public int CaseNoFrom
        {
            get { return _caseNoFrom; }
            set { _caseNoFrom = value; }
        }

        private int _caseNoTo;

        public int CaseNoTo
        {
            get { return _caseNoTo; }
            set { _caseNoTo = value; }
        }

        private int _totalRecords;

        public int TotalRecords
        {
            get { return _totalRecords; }
            set { _totalRecords = value; }
        }

        private int _selectedItem;

        public int SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }

        private Visibility _loadingLabelVisibility;

        public Visibility LoadingLabelVisibility
        {
            get { return _loadingLabelVisibility; }
            set { _loadingLabelVisibility = value; }
        }

        private Visibility _dataGridVisibility;

        public Visibility DataGridVisibility
        {
            get { return _dataGridVisibility; }
            set { _dataGridVisibility = value; }
        }

        private bool _printButtonIsEnabled;

        public bool PrintButtonIsEnabled
        {
            get { return _printButtonIsEnabled; }
            set { _printButtonIsEnabled = value; }
        }

        private Stopwatch _stopWatch;

        public Stopwatch StopWatch
        {
            get { return _stopWatch; }
            set { _stopWatch = value; }
        }

        private TimeSpan _loadingTime;

        public TimeSpan LoadingTime
        {
            get { return _loadingTime; }
            set { _loadingTime = value; }
        }
        #endregion


        #region List
        public List<pd_ryo_customer_label> CustomerList
        {
            get
            {
                return BusinessLayerServices.pd_ryo_customer_labelBL()
                    .Get();
            }
        }

        private List<sp_pd_ryo_InsertPaper_Result> _insertPaperList;

        public List<sp_pd_ryo_InsertPaper_Result> InsertPaperList
        {
            get { return _insertPaperList; }
            set { _insertPaperList = value; }
        }

        private List<sp_pd_ryo_InsertPaper_Result> _selectedItemList;

        public List<sp_pd_ryo_InsertPaper_Result> SelectedItemList
        {
            get { return _selectedItemList; }
            set
            {
                _selectedItemList = value;
                _selectedItem = _selectedItemList.Count();
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
        }
        #endregion


        #region Command
        private ICommand _onGenerateCommand;

        public ICommand OnGenerateCommand
        {
            get { return _onGenerateCommand ?? (_onGenerateCommand = new RelayCommand(OnGenerate)); }
            set { _onGenerateCommand = value; }
        }

        private ICommand _onPrintCommand;

        public ICommand OnPrintCommand
        {
            get { return _onPrintCommand ?? (_onPrintCommand = new RelayCommand(OnPrint)); }
            set { _onPrintCommand = value; }
        }

        private ICommand _onDataGridSelectedCommand;

        public ICommand OnDataGridSelectedCommand
        {
            get { return _onDataGridSelectedCommand ?? (_onDataGridSelectedCommand = new RelayCommand(OnDataGridSelected)); }
            set { _onDataGridSelectedCommand = value; }
        }

        private ICommand _onPrintWithBartenderCommand;

        public ICommand OnPrintWithBartenderCommand
        {
            get { return _onPrintWithBartenderCommand ?? (_onDataGridSelectedCommand = new RelayCommand(OnPrintWithBartenderVertion)); }
            set { _onPrintWithBartenderCommand = value; }
        }
        #endregion


        #region Function
        private void StartWork()
        {
            //Show your wait dialog
            _loadingLabelVisibility = Visibility.Visible;
            _dataGridVisibility = Visibility.Collapsed;
            _printButtonIsEnabled = false;
            _insertPaperList = null;
            _totalRecords = 0;
            _loadingTime = new TimeSpan(0,0,0);
            _stopWatch.Start();
            RaisePropertyChangedEvent(nameof(LoadingLabelVisibility));
            RaisePropertyChangedEvent(nameof(DataGridVisibility));
            RaisePropertyChangedEvent(nameof(PrintButtonIsEnabled));
            RaisePropertyChangedEvent(nameof(InsertPaperList));
            RaisePropertyChangedEvent(nameof(TotalRecords));
            RaisePropertyChangedEvent(nameof(LoadingTime));

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += DoWork;
            worker.RunWorkerCompleted += WorkerCompleted;
            worker.RunWorkerAsync();
        }

        private void WorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _totalRecords = _insertPaperList.Count();
            _loadingLabelVisibility = Visibility.Collapsed;
            _dataGridVisibility = Visibility.Visible;
            _printButtonIsEnabled = true;
            _stopWatch.Stop();
            _loadingTime = _stopWatch.Elapsed;
            _stopWatch.Reset();

            RaisePropertyChangedEvent(nameof(InsertPaperList));
            RaisePropertyChangedEvent(nameof(TotalRecords));
            RaisePropertyChangedEvent(nameof(LoadingLabelVisibility));
            RaisePropertyChangedEvent(nameof(DataGridVisibility));
            RaisePropertyChangedEvent(nameof(PrintButtonIsEnabled));
            RaisePropertyChangedEvent(nameof(LoadingTime));
        }

        private void DoWork(object sender, DoWorkEventArgs e)
        {
            _insertPaperList = BusinessLayerServices.pd_ryo_InsertPaperBL()
                    .GetInsertPaper(_order.ProductionYear, _caseNoFrom, _caseNoTo, _customerCode)
                    .OrderBy(x => x.masterbag_bc)
                    .ToList();
        }

        private void OnGenerate(object e)
        {
            try
            {
                if (_order == null)
                {
                    MessageBoxHelper.Warning("Order cannot be empty.");
                    OnFocusRequested(nameof(Order.PONo));
                    return;
                }

                if (string.IsNullOrEmpty(_customerCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ customer");
                    OnFocusRequested(nameof(CustomerCode));
                    return;
                }

                if (_caseNoFrom <= 0)
                {
                    MessageBoxHelper.Warning("Case no from จะต้องมากกว่า 0");
                    OnFocusRequested(nameof(CaseNoFrom));
                    return;
                }

                if (_caseNoTo <= 0)
                {
                    MessageBoxHelper.Warning("Case no to จะต้องมากกว่า 0");
                    OnFocusRequested(nameof(CaseNoTo));
                    return;
                }

                if (_caseNoFrom > _caseNoTo)
                {
                    MessageBoxHelper.Warning("Case no from จะต้องไม่เกิน case no to");
                    OnFocusRequested(nameof(CaseNoFrom));
                    return;
                }

                if (string.IsNullOrEmpty(_order.ProductionYear))
                    throw new ArgumentException("ไม่ได้ระบุ production year ไว้ใน order");

                if (_caseNoTo - _caseNoFrom + 1 > 50)
                    if (MessageBoxHelper.Question("หากจำนวนกล่องที่ระบุมากกว่า 50 กล่องขึ้นไป " +
                        "อาจใช้เวลาในการแสดงข้อมูลบนหน้าจอ ท่านยืนยันที่จะแสดงข้อมูลตามจำนวนที่ระบุนี้ใช่หรือไม่? ") == MessageBoxResult.No)
                        return;

                StartWork();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnPrint(object e)
        {
            try
            {
                if (_selectedItemList.Count() < 1)
                {
                    MessageBoxHelper.Warning("โปรดเลือก master bag barcode ที่ท่านต้องการจะพิมพ์");
                    return;
                }

                /// Generate info list for fill on the template.
                /// 
                List<m_MasterBagBarcodeTemplate> templateList = new List<m_MasterBagBarcodeTemplate>();
                m_MasterBagBarcodeTemplate obj = new m_MasterBagBarcodeTemplate();
                int columnCount = 1;
                foreach (var item in _selectedItemList)
                {
                    if (columnCount <= 20 && item != null)// !item.Equals(_selectedItemList.Last()))
                    {
                        if (columnCount == 1)
                            obj.Barcode01 = item.masterbag_bc;
                        else if (columnCount == 2)
                            obj.Barcode02 = item.masterbag_bc;
                        else if (columnCount == 3)
                            obj.Barcode03 = item.masterbag_bc;
                        else if (columnCount == 4)
                            obj.Barcode04 = item.masterbag_bc;
                        else if (columnCount == 5)
                            obj.Barcode05 = item.masterbag_bc;
                        else if (columnCount == 6)
                            obj.Barcode06 = item.masterbag_bc;
                        else if (columnCount == 7)
                            obj.Barcode07 = item.masterbag_bc;
                        else if (columnCount == 8)
                            obj.Barcode08 = item.masterbag_bc;
                        else if (columnCount == 9)
                            obj.Barcode09 = item.masterbag_bc;
                        else if (columnCount == 10)
                            obj.Barcode10 = item.masterbag_bc;
                        else if (columnCount == 11)
                            obj.Barcode11 = item.masterbag_bc;
                        else if (columnCount == 12)
                            obj.Barcode12 = item.masterbag_bc;
                        else if (columnCount == 13)
                            obj.Barcode13 = item.masterbag_bc;
                        else if (columnCount == 14)
                            obj.Barcode14 = item.masterbag_bc;
                        else if (columnCount == 15)
                            obj.Barcode15 = item.masterbag_bc;
                        else if (columnCount == 16)
                            obj.Barcode16 = item.masterbag_bc;
                        else if (columnCount == 17)
                            obj.Barcode17 = item.masterbag_bc;
                        else if (columnCount == 18)
                            obj.Barcode18 = item.masterbag_bc;
                        else if (columnCount == 19)
                            obj.Barcode19 = item.masterbag_bc;
                        else if (columnCount == 20)
                        {
                            obj.Barcode20 = item.masterbag_bc;
                            templateList.Add(new m_MasterBagBarcodeTemplate
                            {
                                Barcode01 = obj.Barcode01,
                                Barcode02 = obj.Barcode02,
                                Barcode03 = obj.Barcode03,
                                Barcode04 = obj.Barcode04,
                                Barcode05 = obj.Barcode05,
                                Barcode06 = obj.Barcode06,
                                Barcode07 = obj.Barcode07,
                                Barcode08 = obj.Barcode08,
                                Barcode09 = obj.Barcode09,
                                Barcode10 = obj.Barcode10,
                                Barcode11 = obj.Barcode11,
                                Barcode12 = obj.Barcode12,
                                Barcode13 = obj.Barcode13,
                                Barcode14 = obj.Barcode14,
                                Barcode15 = obj.Barcode15,
                                Barcode16 = obj.Barcode16,
                                Barcode17 = obj.Barcode17,
                                Barcode18 = obj.Barcode18,
                                Barcode19 = obj.Barcode19,
                                Barcode20 = obj.Barcode20
                            });
                            columnCount = 1;
                            obj.Barcode01 = "";
                            obj.Barcode02 = "";
                            obj.Barcode03 = "";
                            obj.Barcode04 = "";
                            obj.Barcode05 = "";
                            obj.Barcode06 = "";
                            obj.Barcode07 = "";
                            obj.Barcode08 = "";
                            obj.Barcode09 = "";
                            obj.Barcode10 = "";
                            obj.Barcode11 = "";
                            obj.Barcode12 = "";
                            obj.Barcode13 = "";
                            obj.Barcode14 = "";
                            obj.Barcode15 = "";
                            obj.Barcode16 = "";
                            obj.Barcode17 = "";
                            obj.Barcode18 = "";
                            obj.Barcode19 = "";
                            obj.Barcode20 = "";
                            continue;
                        }
                        columnCount = columnCount + 1;
                    }
                }
                templateList.Add(new m_MasterBagBarcodeTemplate
                {
                    Barcode01 = obj.Barcode01,
                    Barcode02 = obj.Barcode02,
                    Barcode03 = obj.Barcode03,
                    Barcode04 = obj.Barcode04,
                    Barcode05 = obj.Barcode05,
                    Barcode06 = obj.Barcode06,
                    Barcode07 = obj.Barcode07,
                    Barcode08 = obj.Barcode08,
                    Barcode09 = obj.Barcode09,
                    Barcode10 = obj.Barcode10,
                    Barcode11 = obj.Barcode11,
                    Barcode12 = obj.Barcode12,
                    Barcode13 = obj.Barcode13,
                    Barcode14 = obj.Barcode14,
                    Barcode15 = obj.Barcode15,
                    Barcode16 = obj.Barcode16,
                    Barcode17 = obj.Barcode17,
                    Barcode18 = obj.Barcode18,
                    Barcode19 = obj.Barcode19,
                    Barcode20 = obj.Barcode20
                });

                foreach (var item in templateList)
                    Print(item);

                MessageBoxHelper.Info("การพิมพ์เสร็จสิ้น");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnPrintWithBartenderVertion(object obj)
        {
            try
            {
                var newLine = Environment.NewLine;
                var result = MessageBoxHelper.Question("ท่านต้องการพิมพ์ Insert Paper โดยมีรายละเอียดดังนี้ " + newLine +
                    "Customer Code : " + _customerCode + newLine +
                    "Production Year : " + _order.ProductionYear + newLine +
                    "From CaseNo : " + _caseNoFrom + newLine +
                    "To CaseNo : " + _caseNoTo + newLine +
                    "จำนวน " + (_caseNoTo - _caseNoFrom + 1) * 20 + " แผ่น" + newLine +
                    "กด Yes เพื่อยืนยันการพิมพ์ หรือกด No เพื่อกลับไปตรวจสอบข้อมูลอีกครั้ง");
                if (result == MessageBoxResult.No)
                    return;

                PrintWithBartenderVertion(_order.ProductionYear, _caseNoFrom, _caseNoTo, _customerCode);
                MessageBoxHelper.Info("Print complete");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnDataGridSelected(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                _selectedItemList = items.Cast<sp_pd_ryo_InsertPaper_Result>().ToList();

                _selectedItem = _selectedItemList.Count();
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Print(m_MasterBagBarcodeTemplate model)
        {
            try
            {
                if (string.IsNullOrEmpty(_customerCode))
                    throw new ArgumentException("โปรดระบุ customer");

                var application = new Microsoft.Office.Interop.Word.Application();
                var document = new Microsoft.Office.Interop.Word.Document();

                //00  แชมป์ 25 g.
                //01  แชมป์ 15 g.
                //02  เสือ
                //03  นักมวย
                //04  ช้าง สีแดง
                //05  ช้าง สีเขียว
                //06  รวงข้าว
                //07  มวยไทย
                //08  ชาวไร่

                switch (_customerCode)
                {
                    case "00":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");
                        break;
                    case "01":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Champ15.docx");
                        break;
                    case "02":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Tiger.docx");
                        break;
                    case "03":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Muay.docx");
                        break;
                    case "04":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - ChangRed.docx");
                        break;
                    case "05":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - ChangGreen.docx");
                        break;
                    case "06":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - RuangKhaw.docx");
                        break;
                    case "07":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - MuayThai.docx");
                        break;
                    case "08":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Farmer.docx");
                        break;
                    default:
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");
                        break;
                }

                //document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");

                application.Visible = false;

                foreach (Microsoft.Office.Interop.Word.Shape shape in document.Shapes)
                {
                    if (shape.Name == "MBTextBox1")
                        shape.TextFrame.ContainingRange.Text = model.Barcode01;
                    if (shape.Name == "MBTextBox2")
                        shape.TextFrame.ContainingRange.Text = model.Barcode02;
                    if (shape.Name == "MBTextBox3")
                        shape.TextFrame.ContainingRange.Text = model.Barcode03;
                    if (shape.Name == "MBTextBox4")
                        shape.TextFrame.ContainingRange.Text = model.Barcode04;
                    if (shape.Name == "MBTextBox5")
                        shape.TextFrame.ContainingRange.Text = model.Barcode05;
                    if (shape.Name == "MBTextBox6")
                        shape.TextFrame.ContainingRange.Text = model.Barcode06;
                    if (shape.Name == "MBTextBox7")
                        shape.TextFrame.ContainingRange.Text = model.Barcode07;
                    if (shape.Name == "MBTextBox8")
                        shape.TextFrame.ContainingRange.Text = model.Barcode08;
                    if (shape.Name == "MBTextBox9")
                        shape.TextFrame.ContainingRange.Text = model.Barcode09;
                    if (shape.Name == "MBTextBox10")
                        shape.TextFrame.ContainingRange.Text = model.Barcode10;
                    if (shape.Name == "MBTextBox11")
                        shape.TextFrame.ContainingRange.Text = model.Barcode11;
                    if (shape.Name == "MBTextBox12")
                        shape.TextFrame.ContainingRange.Text = model.Barcode12;
                    if (shape.Name == "MBTextBox13")
                        shape.TextFrame.ContainingRange.Text = model.Barcode13;
                    if (shape.Name == "MBTextBox14")
                        shape.TextFrame.ContainingRange.Text = model.Barcode14;
                    if (shape.Name == "MBTextBox15")
                        shape.TextFrame.ContainingRange.Text = model.Barcode15;
                    if (shape.Name == "MBTextBox16")
                        shape.TextFrame.ContainingRange.Text = model.Barcode16;
                    if (shape.Name == "MBTextBox17")
                        shape.TextFrame.ContainingRange.Text = model.Barcode17;
                    if (shape.Name == "MBTextBox18")
                        shape.TextFrame.ContainingRange.Text = model.Barcode18;
                    if (shape.Name == "MBTextBox19")
                        shape.TextFrame.ContainingRange.Text = model.Barcode19;
                    if (shape.Name == "MBTextBox20")
                        shape.TextFrame.ContainingRange.Text = model.Barcode20;

                    if (shape.Name == "BCTextBox1")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode01 + "*";
                    if (shape.Name == "BCTextBox2")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode02 + "*";
                    if (shape.Name == "BCTextBox3")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode03 + "*";
                    if (shape.Name == "BCTextBox4")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode04 + "*";
                    if (shape.Name == "BCTextBox5")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode05 + "*";
                    if (shape.Name == "BCTextBox6")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode06 + "*";
                    if (shape.Name == "BCTextBox7")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode07 + "*";
                    if (shape.Name == "BCTextBox8")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode08 + "*";
                    if (shape.Name == "BCTextBox9")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode09 + "*";
                    if (shape.Name == "BCTextBox10")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode10 + "*";
                    if (shape.Name == "BCTextBox11")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode11 + "*";
                    if (shape.Name == "BCTextBox12")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode12 + "*";
                    if (shape.Name == "BCTextBox13")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode13 + "*";
                    if (shape.Name == "BCTextBox14")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode14 + "*";
                    if (shape.Name == "BCTextBox15")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode15 + "*";
                    if (shape.Name == "BCTextBox16")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode16 + "*";
                    if (shape.Name == "BCTextBox17")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode17 + "*";
                    if (shape.Name == "BCTextBox18")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode18 + "*";
                    if (shape.Name == "BCTextBox19")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode19 + "*";
                    if (shape.Name == "BCTextBox20")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode20 + "*";
                }

                object saveOption = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                object originalFormat = Microsoft.Office.Interop.Word.WdOriginalFormat.wdOriginalDocumentFormat;
                object routDocument = false;

                object missingValue = Type.Missing;

                object myTrue = true;
                object myFalse = false;

                document.PrintOut(ref myFalse, ref myFalse, ref missingValue, ref missingValue,
                    ref missingValue, missingValue, ref missingValue, ref missingValue, ref missingValue,
                    ref missingValue, ref myFalse, ref missingValue, ref missingValue, ref missingValue);

                document.Close(ref saveOption, ref originalFormat, ref routDocument);

                while (application.BackgroundPrintingStatus > 0)
                {
                    System.Threading.Thread.Sleep(250);
                }

                application.Quit(ref missingValue, ref missingValue, ref missingValue);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintWithBartenderVertion(string productionYear, int fromCaseno, int toCaseno, string customerCode)
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                BarTender.Application btApp;
                BarTender.Format btFormat;
                BarTender.Database btDB;

                btApp = new BarTender.Application();
                btApp.Visible = false;
                btFormat = btApp.Formats.Open(@"\\192.168.0.221\Cut Rag\Template\InsertPaper.btw", false, pd.PrinterSettings.PrinterName);
                btDB = btFormat.Databases.GetDatabase(1);
                btDB.SQLStatement = "EXEC StecDBMS.dbo.sp_pd_ryo_InsertPaper" +
                    " @productionYear = N'" + productionYear + "'," +
                    " @fromCaseNo = " + fromCaseno + "," +
                    " @toCaseNo = " + toCaseno + "," +
                    " @customerCode = N'" + customerCode + "'";
                btFormat.IdenticalCopiesOfLabel = 1;
                btFormat.PrintOut(false, false);
                btFormat.Close(BarTender.BtSaveOptions.btDoNotSaveChanges);
                btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
    }
}
