﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.MasterBagConfig;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.MasterBagConfig
{
    public class vm_MasterBagConfigList : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_MasterBagConfigList()
        {
            MasterBagConfigListBinging();
        }


        #region Properties
        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_MasterBagConfig> _masterBagConfigList;

        public List<pd_ryo_MasterBagConfig> MasterBagConfigList
        {
            get { return _masterBagConfigList; }
            set { _masterBagConfigList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(Add)); }
            set { _onAddCommand = value; }
        }

        private void Add(object obj)
        {
            try
            {
                var window = new AddConfig();
                var vm = new vm_AddConfig();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                MasterBagConfigListBinging();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(Refresh)); }
            set { _onRefreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            MasterBagConfigListBinging();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(Delete)); }
            set { _onDeleteCommand = value; }
        }

        private void Delete(object obj)
        {
            try
            {
                var item = (pd_ryo_MasterBagConfig)obj;
                if (item == null)
                    return;

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == System.Windows.MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MasterBagConfigBL().Delete(item.ConfigCode);
                MasterBagConfigListBinging();
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(Edit)); }
            set { _onEditCommand = value; }
        }

        private void Edit(object obj)
        {
            try
            {
                var item = (pd_ryo_MasterBagConfig)obj;
                if (item == null)
                    return;

                var window = new EditConfig();
                var vm = new vm_EditConfig();
                window.DataContext = vm;
                vm.Window = window;
                vm.Config = item;
                window.ShowDialog();
                MasterBagConfigListBinging();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void MasterBagConfigListBinging()
        {
            try
            {
                _masterBagConfigList = BusinessLayerServices.pd_ryo_MasterBagConfigBL().GetAll();
                _totalRecord = _masterBagConfigList.Count;
                RaisePropertyChangedEvent(nameof(MasterBagConfigList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
