﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.MasterBagConfig
{
    public class vm_AddConfig : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_AddConfig()
        {

        }


        #region Properties
        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private string _configCode;

        public string ConfigCode
        {
            get { return _configCode; }
            set { _configCode = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private decimal _tareWeight;

        public decimal TareWeight
        {
            get { return _tareWeight; }
            set { _tareWeight = value; }
        }

        private decimal _minWeight;

        public decimal MinWeight
        {
            get { return _minWeight; }
            set { _minWeight = value; }
        }

        private decimal _maxWeight;

        public decimal MaxWeight
        {
            get { return _maxWeight; }
            set { _maxWeight = value; }
        }

        private bool _defaultStatus;

        public bool DefaultStatus
        {
            get { return _defaultStatus; }
            set { _defaultStatus = value; }
        }
        #endregion


        #region Command
        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการบันทึกข้อมูลนี้ใช่หรือไม่?") == System.Windows.MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MasterBagConfigBL()
                    .Add(_configCode,
                    _description,
                    _tareWeight,
                    _minWeight,
                    _maxWeight,
                    _defaultStatus,
                    user_setting.User.Username);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            _configCode = null;
            _description = null;
            _minWeight = (decimal)0.0;
            _maxWeight = (decimal)0.0;
            _tareWeight= (decimal)0.0;
            _defaultStatus = false;

            RaisePropertyChangedEvent(nameof(ConfigCode));
            RaisePropertyChangedEvent(nameof(Description));
            RaisePropertyChangedEvent(nameof(MinWeight));
            RaisePropertyChangedEvent(nameof(MaxWeight));
            RaisePropertyChangedEvent(nameof(TareWeight));
            RaisePropertyChangedEvent(nameof(DefaultStatus));
        }
        #endregion
    }
}
