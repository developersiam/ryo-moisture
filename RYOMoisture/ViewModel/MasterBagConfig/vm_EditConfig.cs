﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.MasterBagConfig
{
    internal class vm_EditConfig : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_EditConfig()
        {

        }


        #region Properties
        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private pd_ryo_MasterBagConfig _config;

        public pd_ryo_MasterBagConfig Config
        {
            get { return _config; }
            set { _config = value; }
        }
        #endregion


        #region Command
        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการบันทึกข้อมูลนี้ใช่หรือไม่?") == System.Windows.MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MasterBagConfigBL()
                    .Edit(_config.ConfigCode,
                    _config.Description,
                    _config.TareWeight,
                    _config.MinWeight,
                    _config.MaxWeight,
                    _config.DefaultStatus,
                    user_setting.User.Username);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            try
            {
                _config = BusinessLayerServices.pd_ryo_MasterBagConfigBL()
                    .GetSingle(_config.ConfigCode);
                RaisePropertyChangedEvent(nameof(Config));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion
    }
}
