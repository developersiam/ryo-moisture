﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureBL.Models.Inventory;
using System.Windows;
using RYOMoisture.Helper;
using System.Windows.Input;
using RYOMoisture.View.Inventory.Issued;
using RYOMoistureBL.Helper;
using RYOMoistureBL.InventoryBL;
using RYOMoistureBL;

namespace RYOMoisture.ViewModel.Inventory.Issued
{
    public class vm_IssuedDetail : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_IssuedDetail()
        {
            try
            {
                _issuedDate = DateTime.Now;
                _onhandList = ItemHelper.GetByOnHand();
                _issuedUserList = InventoryService.EmployeeBL()
                    .Get()
                    .OrderBy(x => x.FirstName)
                    .ToList();

                var packedgradeList = InventoryService.packedgradeBL()
                    .GetByCrop(Convert.ToInt16(DateTime.Now.Year))
                    .Where(x => x.form == "RYO" &&
                    !x.packedgrade1.Contains("AB4") &&
                    !x.packedgrade1.Contains("BLEND"));
                var pdsetupList = InventoryService.pdsetupBL()
                    .GetByCrop(Convert.ToInt16(DateTime.Now.Year));

                _pdsetupList = (from a in packedgradeList
                                from b in pdsetupList
                                where a.packedgrade1 == b.packedgrade
                                select new pdsetup
                                {
                                    pdno = b.pdno,
                                    packedgrade = b.pdno + "   /   " + b.packedgrade
                                })
                                .OrderByDescending(x => x.pdno)
                                .ToList();

                RaisePropertyChangedEvent(nameof(OnHandList));
                RaisePropertyChangedEvent(nameof(IssuedDate));
                RaisePropertyChangedEvent(nameof(IssuedUserList));
                RaisePropertyChangedEvent(nameof(pdsetupList));

                MasterBinding();
                IssuedRefBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }



        #region Properties
        private string _refCode;

        public string RefCode
        {
            get { return _refCode; }
            set { _refCode = value; }
        }

        private string _issuedCode;

        public string IssuedCode
        {
            get { return _issuedCode; }
            set
            {
                _issuedCode = value;
                MasterBinding();
            }
        }

        private DateTime _issuedDate;

        public DateTime IssuedDate
        {
            get { return _issuedDate; }
            set { _issuedDate = value; }
        }

        private string _issuedUser;

        public string IssuedUser
        {
            get { return _issuedUser; }
            set { _issuedUser = value; }
        }

        private string _topdno;

        public string Topdno
        {
            get { return _topdno; }
            set
            {
                _topdno = value;

                if (string.IsNullOrEmpty(_topdno))
                {
                    MessageBoxHelper.Warning("โปรดระบุ topdno");
                    OnFocusRequested(nameof(Topdno));
                    return;
                }

                var pdsetup = BusinessLayerServices.PdSetupBL()
                    .GetSinglePDSetup(_topdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบชื่อเกรดของ pdno " + _topdno + " โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                _fromCaseNo = 0;
                _toCaseNo = 0;
                _totalCase = 0;
                RaisePropertyChangedEvent(nameof(FromCaseNo));
                RaisePropertyChangedEvent(nameof(ToCaseNo));
                RaisePropertyChangedEvent(nameof(TotalCase));
            }
        }

        private int _fromCaseNo;

        public int FromCaseNo
        {
            get { return _fromCaseNo; }
            set
            {
                _fromCaseNo = value;

                if (_fromCaseNo <= 0)
                {
                    MessageBoxHelper.Warning("From caseno ไม่ควรน้อยกว่า 0");
                    _toCaseNo = 0;
                    RaisePropertyChangedEvent(nameof(ToCaseNo));
                    OnFocusRequested(nameof(FromCaseNo));
                    return;
                }

                _toCaseNo = _fromCaseNo + _totalCase - 1;

                RaisePropertyChangedEvent(nameof(ToCaseNo));
                RaisePropertyChangedEvent(nameof(TotalCase));
            }
        }

        private int _toCaseNo;

        public int ToCaseNo
        {
            get { return _toCaseNo; }
            set { _toCaseNo = value; }
        }

        private int _totalCase;

        public int TotalCase
        {
            get { return _totalCase; }
            set
            {
                _totalCase = value;
                CalculateCaseNo();
            }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _issuedStatus;

        public bool IssuedStatus
        {
            get { return _issuedStatus; }
            set
            {
                _issuedStatus = value;
                if (_issuedStatus == true)
                    _canEdit = false;
                else
                    _canEdit = true;

                RaisePropertyChangedEvent(nameof(CanEdit));
            }
        }

        private bool _canEdit;

        public bool CanEdit
        {
            get { return _canEdit; }
            set { _canEdit = value; }
        }

        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _itemDescription;

        public string ItemDescription
        {
            get { return _itemDescription; }
            set { _itemDescription = value; }
        }

        private int _onhand;

        public int Onhand
        {
            get { return _onhand; }
            set { _onhand = value; }
        }

        private string _unit;

        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        private int _quantity;

        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private string _lotNumber;

        public string LotNumber
        {
            get { return _lotNumber; }
            set { _lotNumber = value; }
        }

        private m_ItemTransaction _selectedItem;

        public m_ItemTransaction SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                _itemCode = _selectedItem.ItemCode;
                _itemDescription = _selectedItem.Description;
                _unit = _selectedItem.Unit;
                _lotNumber = _selectedItem.LotNumber;
                _onhand = _selectedItem.OnHand;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(Unit));
                RaisePropertyChangedEvent(nameof(LotNumber));
                RaisePropertyChangedEvent(nameof(Onhand));
                OnFocusRequested(nameof(Quantity));
            }
        }

        private IssuedDetail _window;

        public IssuedDetail Window
        {
            get { return _window; }
            set { _window = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Employee> _issuedUserList;

        public List<pd_ryo_inventory_Employee> IssuedUserList
        {
            get { return _issuedUserList; }
            set { _issuedUserList = value; }
        }

        private List<m_ItemTransaction> _issuedDetailList;

        public List<m_ItemTransaction> IssuedDetailList
        {
            get
            {
                if (_issuedDetailList == null)
                    _issuedDetailList = new List<m_ItemTransaction>();
                return _issuedDetailList;
            }
            set { _issuedDetailList = value; }
        }

        private List<pdsetup> _pdsetupList;

        public List<pdsetup> pdsetupList
        {
            get { return _pdsetupList; }
            set { _pdsetupList = value; }
        }

        private List<pd_ryo_inventory_IssuedReference> _issuedRefList;

        public List<pd_ryo_inventory_IssuedReference> IssuedRefList
        {
            get { return _issuedRefList; }
            set { _issuedRefList = value; }
        }

        private List<m_ItemTransaction> _onhandList;

        public List<m_ItemTransaction> OnHandList
        {
            get { return _onhandList; }
            set { _onhandList = value; }
        }

        #endregion



        #region Command
        private ICommand _onItemCodeClickCommand;

        public ICommand OnItemCodeClickCommand
        {
            get { return _onItemCodeClickCommand ?? (_onItemCodeClickCommand = new RelayCommand(OnItemCodeClick)); }
            set { _onItemCodeClickCommand = value; }
        }

        private void OnItemCodeClick(object obj)
        {
            try
            {
                var window = new ItemOnHand();
                var vm = new vm_ItemOnHand();
                window.DataContext = vm;
                vm.ItemList = _onhandList;
                vm.Window = window;
                vm.ItemList = _onhandList;
                window.ShowDialog();

                if (vm.SelectedItem == null)
                    return;

                _selectedItem = vm.SelectedItem;
                ItemDetailBinding(_selectedItem);
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (_quantity <= 0)
                    throw new ArgumentException("Quantity จะต้องมากกว่า 1");

                if (string.IsNullOrEmpty(_lotNumber))
                    throw new ArgumentException("Lot number จะต้องไม่เป็นค่าว่าง");

                var item = _onhandList
                    .SingleOrDefault(x => x.ItemCode == _itemCode &&
                    x.LotNumber == _lotNumber);

                if (item == null)
                    throw new ArgumentException("item cannot be null เกิดข้อผิดพลาดโปรดติดต่อแผนกไอที");

                if (item.Quantity < _quantity)
                {
                    MessageBoxHelper.Warning("จำนวนสินค้า item code " + item.ItemCode +
                        " Lot No. " + _lotNumber + " ที่เหลืออยู่ไม่เพียงพอในการเบิก");
                    return;
                }

                if (_issuedStatus == true)
                    throw new ArgumentException("Issued code นี้ถูกเปลี่ยนสถานะเป็น Finish ไม่สามารถแก้ไขรายการสินค้าที่จะเบิกได้");

                _issuedDetailList
                    .Add(new m_ItemTransaction
                    {
                        ItemCode = _itemCode,
                        Description = _itemDescription,
                        UnitName = _unit,
                        LotNumber = _lotNumber,
                        Quantity = _quantity,
                        UnitPrice = item.UnitPrice
                    });

                OnHandPupulation(item.ItemCode, item.LotNumber, _quantity, "-");
                ClearItemForm();
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onAddByRefCodeCommand;

        public ICommand OnAddByRefCodeCommand
        {
            get { return _onAddByRefCodeCommand ?? (_onAddByRefCodeCommand = new RelayCommand(OnAddByRefCode)); }
            set { _onAddByRefCodeCommand = value; }
        }

        private void OnAddByRefCode(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_refCode))
                    return;

                if (string.IsNullOrEmpty(_topdno))
                    throw new ArgumentException("โปรดระบุ topdno");

                if (_totalCase <= 0)
                    throw new ArgumentException("โปรดระบุจำนวน Total case ของสินค้าที่จะทำการผลิต");

                if (_issuedStatus == true)
                    throw new ArgumentException("Issued code นี้ถูกเปลี่ยนสถานะเป็น Finish ไม่สามารถแก้ไขรายการสินค้าที่จะเบิกได้");

                var refItemList = InventoryService.IssuedReferenceDetailBL()
                    .GetByReferenceCode(_refCode);
                foreach (var item in refItemList)
                {
                    var itemList = _onhandList
                        .Where(x => x.ItemCode == item.ItemCode)
                        .ToList();

                    if (itemList.Count() <= 0)
                    {
                        MessageBoxHelper.Warning("สินค้า item code " + item.ItemCode + " ไม่อยู่ในสต๊อค");
                        continue;
                    }

                    var lotNumber = itemList.Min(x => x.LotNumber);
                    var issuedItem = itemList.SingleOrDefault(x => x.LotNumber == lotNumber);
                    var onhandQty = issuedItem.Quantity;
                    var issuedQty = item.Quantity * _totalCase;
                    if (onhandQty < issuedQty)
                    {
                        MessageBoxHelper.Warning("จำนวนสินค้า item code " + item.ItemCode +
                            " Lot No. " + lotNumber + " ที่เหลืออยู่ไม่เพียงพอในการเบิก " +
                            " โปรดเพิ่มรายการนี้ด้วยตนเองจากฟอร์มบันทึกบนหน้าจอ");
                        continue;
                    }

                    _issuedDetailList
                        .Add(new m_ItemTransaction
                        {
                            ItemCode = issuedItem.ItemCode,
                            Description = issuedItem.Description,
                            UnitName = issuedItem.UnitName,
                            LotNumber = issuedItem.LotNumber,
                            UnitPrice = issuedItem.UnitPrice,
                            Quantity = issuedQty
                        });

                    OnHandPupulation(issuedItem.ItemCode, issuedItem.LotNumber, issuedQty, "-");
                }
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (_fromCaseNo <= 0)
                {
                    MessageBoxHelper.Warning("From caseno ไม่ควรน้อยกว่า 1");
                    OnFocusRequested(nameof(TotalCase));
                    return;
                }

                if (_toCaseNo <= 0)
                {
                    MessageBoxHelper.Warning("To caseno ไม่ควรน้อยกว่า 1");
                    OnFocusRequested(nameof(TotalCase));
                    return;
                }

                if (_totalCase <= 0)
                {
                    MessageBoxHelper.Warning("Total case ไม่ควรน้อยกว่า 1");
                    OnFocusRequested(nameof(TotalCase));
                    return;
                }

                if (string.IsNullOrEmpty(_topdno))
                {
                    MessageBoxHelper.Warning("โปรดระบุ topdno");
                    OnFocusRequested(nameof(Topdno));
                    return;
                }

                if (string.IsNullOrEmpty(_issuedUser))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Issued user");
                    OnFocusRequested(nameof(IssuedUser));
                    return;
                }

                if (string.IsNullOrEmpty(_issuedCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Issued code");
                    OnFocusRequested(nameof(IssuedCode));
                    return;
                }

                if (_issuedDetailList.Count() <= 0)
                    throw new ArgumentException("โปรดเพิ่มรายการสินค้าอย่างน้อย 1 รายการ");

                if (_issuedStatus == true)
                {
                    InventoryService.IssuedBL()
                        .Update(_issuedCode,
                        _issuedUser,
                        _description,
                        _issuedDate,
                        _topdno,
                        _fromCaseNo,
                        _toCaseNo,
                        user_setting.User.Username);
                }
                else
                {
                    if (MessageBoxHelper.Question("โปรดตรวจสอบข้อมูลให้ถูกต้องอีกครั้ง " +
                        "เมื่อบันทึกข้อมูลแล้วจะไม่สามารถแก้ไขข้อมุลย้อนหลังได้อีก " +
                        "หากยืนยันกด Yes เพื่อบันทึกข้อมูล กด No เพื่อตรวจสอบข้อมูลอีกครั้ง") == MessageBoxResult.No)
                        return;

                    InventoryService.IssuedBL()
                        .Add(_issuedCode,
                        _issuedDate,
                        _issuedUser,
                        _topdno,
                        _fromCaseNo,
                        _toCaseNo,
                        _description,
                        user_setting.User.Username);

                    foreach (var item in _issuedDetailList)
                    {
                        InventoryService.IssuedDetailsBL()
                            .Add(new pd_ryo_inventory_IssuedDetail
                            {
                                IssuedCode = _issuedCode,
                                IssuedID = Guid.NewGuid(),
                                ItemCode = item.ItemCode,
                                UnitPrice = item.UnitPrice,
                                Quantity = item.Quantity,
                                LotNo = item.LotNumber,
                                ModifiedBy = user_setting.User.Username,
                                ModifiedDate = DateTime.Now
                            });
                    }
                }

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                MasterBinding();
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            ClearItemForm();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (_issuedStatus == true)
                    throw new ArgumentException("Issued code นี้มีการยืนยันข้อมูลแล้ว ไม่สามารถลบหรือแก้ไขข้อมูลได้");

                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                ///Delete item from this PO.
                ///
                var item = (m_ItemTransaction)obj;
                _issuedDetailList.Remove(item);

                OnHandPupulation(item.ItemCode, item.LotNumber, item.Quantity, "+");
                RaisePropertyChangedEvent(nameof(IssuedDetailList));

                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onIssuedReferenceCommand;

        public ICommand OnIssuedReferenceCommand
        {
            get { return _onIssuedReferenceCommand ?? (_onIssuedReferenceCommand = new RelayCommand(OnIssuedReference)); }
            set { _onIssuedReferenceCommand = value; }
        }

        private void OnIssuedReference(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_refCode))
                    throw new ArgumentException("โปรดระบุ Reference Code โดยเลือกจากตัวเลือกด้านซ้ายมือ");

                var window = new IssuedReferenceDetail();
                var vm = new vm_IssuedReferenceDetail();
                window.DataContext = vm;
                vm.RefCode = _refCode;
                vm.Window = window;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion


        #region Function
        private void MasterBinding()
        {
            try
            {
                if (_issuedCode == null)
                {
                    _issuedCode = InventoryService.IssuedBL().GetNewIssuedCode();
                    _issuedDate = DateTime.Now;
                    _issuedStatus = false;
                    _canEdit = true;
                    _fromCaseNo = 0;
                    _toCaseNo = 0;
                    _totalCase = 0;

                    RaisePropertyChangedEvent(nameof(IssuedCode));
                    RaisePropertyChangedEvent(nameof(IssuedDate));
                    RaisePropertyChangedEvent(nameof(IssuedStatus));
                    RaisePropertyChangedEvent(nameof(CanEdit));
                    RaisePropertyChangedEvent(nameof(FromCaseNo));
                    RaisePropertyChangedEvent(nameof(ToCaseNo));
                    RaisePropertyChangedEvent(nameof(TotalCase));
                }
                else
                {
                    //_issuedCode != "" หรือ not null คือเป็นการ Edit
                    var master = InventoryService.IssuedBL().GetSingle(_issuedCode);
                    if (master == null)
                        throw new ArgumentException("ไม่พบข้อมูล issued code " + _issuedCode +
                            " ในระะบบ โปรดแจ้งแผนกไอทีเพื่อตรวจสอบข้อมูล");

                    _issuedCode = master.IssuedCode;
                    _issuedDate = master.IssuedDate;
                    _issuedUser = master.IssuedUser;
                    _topdno = master.ToPdno;
                    _fromCaseNo = master.FromCaseNo;
                    _toCaseNo = master.ToCaseNo;
                    _totalCase = _toCaseNo - _fromCaseNo + 1;
                    _description = master.Description;
                    _issuedStatus = master.IssuedStatus;
                    _canEdit = false;

                    RaisePropertyChangedEvent(nameof(IssuedCode));
                    RaisePropertyChangedEvent(nameof(IssuedDate));
                    RaisePropertyChangedEvent(nameof(IssuedUser));
                    RaisePropertyChangedEvent(nameof(Topdno));
                    RaisePropertyChangedEvent(nameof(FromCaseNo));
                    RaisePropertyChangedEvent(nameof(ToCaseNo));
                    RaisePropertyChangedEvent(nameof(TotalCase));
                    RaisePropertyChangedEvent(nameof(Description));
                    RaisePropertyChangedEvent(nameof(IssuedStatus));
                    RaisePropertyChangedEvent(nameof(CanEdit));

                    DetailBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailBinding()
        {
            try
            {
                if (_issuedStatus == true)
                    _issuedDetailList = InventoryService.IssuedDetailsBL()
                        .GetByIssuedCode(_issuedCode)
                        .Select(x => new m_ItemTransaction
                        {
                            ItemCode = x.ItemCode,
                            Description = x.pd_ryo_inventory_Item.Description,
                            UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                            UnitPrice = x.UnitPrice,
                            Quantity = x.Quantity,
                            LotNumber = x.LotNo
                        })
                        .OrderBy(x => x.ItemCode)
                        .ThenBy(x => x.LotNumber)
                        .ToList();
                else
                    _issuedDetailList = _issuedDetailList.ToList();

                RaisePropertyChangedEvent(nameof(IssuedDetailList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearItemForm()
        {
            try
            {
                _itemCode = "";
                _itemDescription = "";
                _unit = "";
                _lotNumber = "";
                _onhand = 0;
                _quantity = 0;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(Unit));
                RaisePropertyChangedEvent(nameof(LotNumber));
                RaisePropertyChangedEvent(nameof(Onhand));
                RaisePropertyChangedEvent(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void IssuedRefBinding()
        {
            try
            {
                _issuedRefList = InventoryService.IssuedReferenceBL()
                    .Get()
                    .OrderBy(x => x.ReferenceCode)
                    .ToList();

                RaisePropertyChangedEvent(nameof(IssuedRefList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CalculateCaseNo()
        {
            try
            {
                if (_totalCase <= 0)
                {
                    MessageBoxHelper.Warning("Total case ไม่ควรน้อยกว่า 1");
                    OnFocusRequested(nameof(TotalCase));
                    return;
                }

                _toCaseNo = _fromCaseNo + TotalCase - 1;

                RaisePropertyChangedEvent(nameof(FromCaseNo));
                RaisePropertyChangedEvent(nameof(ToCaseNo));
                OnFocusRequested(nameof(FromCaseNo));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ItemDetailBinding(m_ItemTransaction item)
        {
            try
            {
                if (item == null)
                    return;

                _itemCode = item.ItemCode;
                _itemDescription = item.Description;
                _unit = item.UnitName;
                _lotNumber = item.LotNumber;
                _onhand = item.Quantity;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(Unit));
                RaisePropertyChangedEvent(nameof(LotNumber));
                RaisePropertyChangedEvent(nameof(Onhand));
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnHandPupulation(string itemCode, string lotNumber, int quantity, string assign)
        {
            try
            {
                ///assign parameter is type of populations.
                ///assign [+] is adding a quantity of item to list.
                ///assign [-] remove a quantity of item from list.
                ///

                var item = _onhandList
                    .SingleOrDefault(x => x.ItemCode == itemCode &&
                    x.LotNumber == lotNumber);

                if (item == null)
                    throw new ArgumentException("item cannot be null เกิดข้อผิดพลาดโปรดติดต่อแผนกไอที");

                var temp = item;
                if (assign == "+")
                    temp.Quantity = temp.Quantity + quantity;
                else
                    temp.Quantity = temp.Quantity - quantity;

                _onhandList.Remove(item);
                _onhandList.Add(temp);

                _onhandList = _onhandList.ToList();
                RaisePropertyChangedEvent(nameof(OnHandList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
