﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.Inventory.Issued;
using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Issued
{
    public class vm_IssuedReferenceDetail : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_IssuedReferenceDetail()
        {
            MasterBinding();
        }


        #region Properties
        private pd_ryo_inventory_IssuedReference _issuedRef;

        public pd_ryo_inventory_IssuedReference IssuedRef
        {
            get { return _issuedRef; }
            set { _issuedRef = value; }
        }

        private string _refCode;

        public string RefCode
        {
            get { return _refCode; }
            set
            {
                _refCode = value;
                MasterBinding();
            }
        }

        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _itemDescription;

        public string ItemDescription
        {
            get { return _itemDescription; }
            set { _itemDescription = value; }
        }

        private string _unitCode;

        public string UnitCode
        {
            get { return _unitCode; }
            set { _unitCode = value; }
        }

        private string _unitName;

        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        private int _quantity;

        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private bool _refCodeReadOnly;

        public bool RefCodeReadOnly
        {
            get { return _refCodeReadOnly; }
            set { _refCodeReadOnly = value; }
        }

        private IssuedReferenceDetail _window;

        public IssuedReferenceDetail Window
        {
            get { return _window; }
            set { _window = value; }
        }

        #endregion



        #region List
        private List<m_ItemTransaction> _itemList;

        public List<m_ItemTransaction> ItemList
        {
            get
            {
                if (_itemList == null)
                    _itemList = new List<m_ItemTransaction>();
                return _itemList;
            }
            set { _itemList = value; }
        }

        #endregion



        #region Command

        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Item code.");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (_quantity <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนสินค้าจะต้องมากกว่า 0");
                    OnFocusRequested(nameof(Quantity));
                    return;
                }

                if (_itemList != null)
                    if (_itemList.Where(x => x.ItemCode == _itemCode).Count() > 0)
                        throw new ArgumentException("รายการสินค้านี้ถูกเพิ่มเข้าไปแล้ว โปรดเลือกสินค้าตัวอื่น");

                if (_refCodeReadOnly == true)
                    InventoryService.IssuedReferenceDetailBL()
                        .Add(new pd_ryo_inventory_IssuedReferenceDetail
                        {
                            ReferenceDetailID = Guid.NewGuid(),
                            ReferenceCode = _refCode,
                            ItemCode = _itemCode,
                            Quantity = _quantity,
                            ModifiedBy = user_setting.User.Username,
                            ModifiedDate = DateTime.Now
                        });
                else
                    _itemList
                        .Add(new m_ItemTransaction
                        {
                            ItemCode = _itemCode,
                            Description = _itemDescription,
                            Unit = _unitCode,
                            UnitName = _unitName,
                            Quantity = _quantity,
                            ModifiedDate = DateTime.Now
                        });

                _itemList = _itemList.ToList();
                ResetForm();
                DetailBinding();
                OnFocusRequested(nameof(ItemCode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            ResetForm();
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_refCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Reference Code");
                    OnFocusRequested(nameof(RefCode));
                    return;
                }

                if (_itemList.Count() <= 0)
                    throw new ArgumentException("โปรดใส่รายการสินค้าที่จะรับเข้าระบบอย่างน้อย 1 รายการ");

                ///ถ้าเป็นการสร้างใหม่ให้ลง add master table ก่อนกรณีการแก้ไขให้ข้ามไปได้เลย
                if (_refCodeReadOnly == false)
                {
                    InventoryService.IssuedReferenceBL()
                        .Add(new pd_ryo_inventory_IssuedReference
                        {
                            ReferenceCode = _refCode,
                            Description = _description,
                            ModifiedBy = user_setting.User.Username
                        });

                    foreach (var item in _itemList)
                    {
                        InventoryService.IssuedReferenceDetailBL()
                            .Add(new pd_ryo_inventory_IssuedReferenceDetail
                            {
                                ReferenceCode = _refCode,
                                ItemCode = item.ItemCode,
                                Quantity = item.Quantity,
                                ModifiedBy = user_setting.User.Username,
                                ModifiedDate = DateTime.Now
                            });
                    }
                }
                else
                    InventoryService.IssuedReferenceBL()
                        .Update(new pd_ryo_inventory_IssuedReference
                        {
                            ReferenceCode = _refCode,
                            Description = _description,
                            ModifiedBy = user_setting.User.Username
                        });

                MasterBinding();
                DetailBinding();
                MessageBoxHelper.Info("บันทึกสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCloseCommand;

        public ICommand OnCloseCommand
        {
            get { return _onCloseCommand ?? (_onCloseCommand = new RelayCommand(OnClose)); }
            set { _onCloseCommand = value; }
        }

        private void OnClose(object obj)
        {
            _window.Close();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var item = (m_ItemTransaction)obj;
                if (item == null)
                    return;

                if (_refCodeReadOnly == true)
                {
                    var delete = InventoryService.IssuedReferenceDetailBL()
                        .GetByReferenceCode(_refCode)
                        .SingleOrDefault(x => x.ItemCode == item.ItemCode);

                    InventoryService.IssuedReferenceDetailBL()
                        .Delete(delete.ReferenceDetailID);
                }

                _itemList.Remove(item);
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onMouseClickCommand;

        public ICommand OnMouseClickCommand
        {
            get { return _onMouseClickCommand ?? (_onMouseClickCommand = new RelayCommand(OnMouseClick)); }
            set { _onMouseClickCommand = value; }
        }

        private void OnMouseClick(object obj)
        {
            try
            {
                var window = new View.Inventory.Shared.ItemList();
                var vm = new Shared.vm_ItemList();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                if (vm.SelectedItem == null)
                    return;

                ItemDetailBinding(vm.SelectedItem);
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void MasterBinding()
        {
            try
            {
                if (_refCode == null)
                {
                    _refCodeReadOnly = false;
                    _createDate = DateTime.Now;
                }
                else
                {
                    _issuedRef = InventoryService.IssuedReferenceBL().GetSingle(_refCode);
                    /// กรณี create issued ref detail ให้ข้ามขั้นตอนด้านล่างนี้ไป เพื่อให้สามารถบันทึกข้อมูลได้
                    if (_issuedRef == null)
                        return;

                    _refCodeReadOnly = true;
                    _description = _issuedRef.Description;
                    _createDate = _issuedRef.CreateDate;
                }

                RaisePropertyChangedEvent(nameof(IssuedRef));
                RaisePropertyChangedEvent(nameof(RefCodeReadOnly));
                RaisePropertyChangedEvent(nameof(CreateDate));
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailBinding()
        {
            try
            {
                if (_itemList == null)
                    _itemList = new List<m_ItemTransaction>();

                if (_refCodeReadOnly == true)
                    _itemList = InventoryService.IssuedReferenceDetailBL()
                        .GetByReferenceCode(_refCode)
                        .Select(x => new m_ItemTransaction
                        {
                            ItemCode = x.ItemCode,
                            Description = x.pd_ryo_inventory_Item.Description,
                            Unit = x.pd_ryo_inventory_Item.Unit,
                            UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                            CategoryName = x.pd_ryo_inventory_Item.pd_ryo_inventory_ItemCategory.ItemCategoryName,
                            Quantity = x.Quantity,
                            ModifiedDate = x.ModifiedDate
                        })
                        .ToList();

                _itemList = _itemList.ToList();
                RaisePropertyChangedEvent(nameof(ItemList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ResetForm()
        {
            _itemCode = "";
            _itemDescription = "";
            _unitName = "";
            _quantity = 0;

            RaisePropertyChangedEvent(nameof(ItemCode));
            RaisePropertyChangedEvent(nameof(ItemDescription));
            RaisePropertyChangedEvent(nameof(UnitName));
            RaisePropertyChangedEvent(nameof(Quantity));
        }

        private void ItemDetailBinding(pd_ryo_inventory_Item item)
        {
            try
            {
                if (item == null)
                    return;

                _itemCode = item.ItemCode;
                _itemDescription = item.Description;
                _unitCode = item.Unit;
                _unitName = item.pd_ryo_inventory_Unit.UnitName;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(UnitCode));
                RaisePropertyChangedEvent(nameof(UnitName));
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
