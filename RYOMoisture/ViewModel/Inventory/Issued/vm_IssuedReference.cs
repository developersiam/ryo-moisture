﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.Inventory.Issued;
using RYOMoistureBL.InventoryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Issued
{
    public class vm_IssuedReference : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_IssuedReference()
        {
            DataGridBinding();
        }


        #region Properties
        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_IssuedReference> _issuedRefList;

        public List<pd_ryo_inventory_IssuedReference> IssuedRefList
        {
            get { return _issuedRefList; }
            set { _issuedRefList = value; }
        }
        #endregion



        #region Command
        private ICommand _onCreateCommand;

        public ICommand OnCreateCommand
        {
            get { return _onCreateCommand ?? (_onCreateCommand = new RelayCommand(OnCreate)); }
            set { _onCreateCommand = value; }
        }

        private void OnCreate(object obj)
        {
            try
            {
                var window = new IssuedReferenceDetail();
                var vm = new vm_IssuedReferenceDetail();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DataGridBinding();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var item = (pd_ryo_inventory_IssuedReference)obj;
                InventoryService.IssuedReferenceBL().Delete(item.ReferenceCode);
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDetailCommand;

        public ICommand OnDetailCommand
        {
            get { return _onDetailCommand ?? (_onDetailCommand = new RelayCommand(OnDetail)); }
            set { _onDetailCommand = value; }
        }

        private void OnDetail(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_IssuedReference)obj;
                var window = new IssuedReferenceDetail();
                var vm = new vm_IssuedReferenceDetail();
                window.DataContext = vm;
                vm.RefCode = item.ReferenceCode;
                vm.Window = window;
                window.ShowDialog();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DataGridBinding()
        {
            try
            {
                _issuedRefList = InventoryService.IssuedReferenceBL().Get();
                _totalRecord = _issuedRefList.Count();

                RaisePropertyChangedEvent(nameof(IssuedRefList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
