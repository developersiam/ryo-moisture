﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.Inventory.Issued;
using RYOMoistureBL.Helper;
using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Issued
{
    public class vm_ItemOnHand : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_ItemOnHand()
        {
            DataGridBinding();
        }


        #region Properties
        private m_ItemTransaction _selectedItem;

        public m_ItemTransaction SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }

        private ItemOnHand _window;

        public ItemOnHand Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set { _searchText = value; }
        }
        #endregion


        #region List    
        private List<m_ItemTransaction> _itemList;

        public List<m_ItemTransaction> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<pd_ryo_inventory_ItemCategory> _categoryList;

        public List<pd_ryo_inventory_ItemCategory> CategoryList
        {
            get { return _categoryList; }
            set { _categoryList = value; }
        }
        #endregion


        #region Command
        private ICommand _onSelectedCommand;

        public ICommand OnSelectedCommand
        {
            get { return _onSelectedCommand ?? (_onSelectedCommand = new RelayCommand(OnSelected)); }
            set { _onSelectedCommand = value; }
        }

        private void OnSelected(object obj)
        {
            try
            {
                var item = (m_ItemTransaction)obj;
                if (item == null)
                    return;

                _selectedItem = item;
                RaisePropertyChangedEvent(nameof(SelectedItem));
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            throw new NotImplementedException();
        }

        private ICommand _onSearchTextChangedCommand;

        public ICommand OnSearchTextChangedCommand
        {
            get { return _onSearchTextChangedCommand ?? (_onSearchTextChangedCommand = new RelayCommand(OnSearchTextChanged)); }
            set { _onSearchTextChangedCommand = value; }
        }

        private void OnSearchTextChanged(object obj)
        {
            throw new NotImplementedException();
        }

        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                if (_itemList == null)
                    _itemList = ItemHelper.GetByOnHand().OrderBy(x => x.ItemCode).ToList();

                _totalRecord = _itemList.Count();
                RaisePropertyChangedEvent(nameof(ItemList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
