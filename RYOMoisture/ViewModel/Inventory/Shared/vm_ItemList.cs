﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.Inventory.Shared;
using RYOMoistureBL.InventoryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Shared
{
    public class vm_ItemList : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_ItemList()
        {
            DataGridBinding();
        }


        #region Properties
        private pd_ryo_inventory_Item _selectedItem;

        public pd_ryo_inventory_Item SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }

        private ItemList _window;

        public ItemList Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set { _searchText = value; }
        }
        #endregion


        #region List    
        private List<pd_ryo_inventory_Item> _itemList;

        public List<pd_ryo_inventory_Item> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<pd_ryo_inventory_ItemCategory> _categoryList;

        public List<pd_ryo_inventory_ItemCategory> CategoryList
        {
            get { return _categoryList; }
            set { _categoryList = value; }
        }
        #endregion


        #region Command
        private ICommand _onSelectedCommand;

        public ICommand OnSelectedCommand
        {
            get { return _onSelectedCommand ?? (_onSelectedCommand = new RelayCommand(OnSelected)); }
            set { _onSelectedCommand = value; }
        }

        private void OnSelected(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_Item)obj;
                if (item == null)
                    return;

                _selectedItem = item;
                RaisePropertyChangedEvent(nameof(SelectedItem));
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DataGridBinding();
        }
        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                _itemList = InventoryService.ItemBL()
                        .Get()
                        .OrderBy(x => x.ItemCode)
                        .ToList();
                _totalRecord = _itemList.Count();
                RaisePropertyChangedEvent(nameof(ItemList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
