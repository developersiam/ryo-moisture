﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoisture.View.Inventory.Return;
using RYOMoistureBL.Models.Inventory;
using System.Windows.Input;
using RYOMoisture.Helper;
using RYOMoistureBL.Helper;
using RYOMoistureBL.InventoryBL;
using System.Windows;

namespace RYOMoisture.ViewModel.Inventory.Return
{
    public class vm_ReturnDetail : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_ReturnDetail()
        {
            try
            {
                _returnDate = DateTime.Now;
                _returnUserList = InventoryService.EmployeeBL()
                    .Get()
                    .OrderBy(x => x.FirstName)
                    .ToList();
                _issuedCodeList = InventoryService.IssuedBL()
                    .GetByDateRange(Convert.ToDateTime(DateTime.Now.Year + "-01-01"), DateTime.Now);

                RaisePropertyChangedEvent(nameof(ReturnDate));
                RaisePropertyChangedEvent(nameof(ReturnUserList));
                RaisePropertyChangedEvent(nameof(IssuedList));

                MasterBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #region Properties
        private ReturnDetail _window;

        public ReturnDetail Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private string _returnCode;

        public string ReturnCode
        {
            get { return _returnCode; }
            set
            {
                _returnCode = value;
                MasterBinding();
            }
        }

        private DateTime _returnDate;

        public DateTime ReturnDate
        {
            get { return _returnDate; }
            set { _returnDate = value; }
        }

        private string _returnUser;

        public string ReturnUser
        {
            get { return _returnUser; }
            set { _returnUser = value; }
        }

        private bool _returnStatus;

        public bool ReturnStatus
        {
            get { return _returnStatus; }
            set { _returnStatus = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _issuedCode;

        public string IssuedCode
        {
            get { return _issuedCode; }
            set
            {
                _issuedCode = value;

                if (InventoryService.ReturnBL().IsDupplicateReturnCode(_issuedCode))
                {
                    MessageBoxHelper.Warning("มีการบันทึกการคืนของ issued code " +
                        _issuedCode + " ไปแล้วไม่อนุญาตให้บันทึกข้อมูลซ้ำ");
                    _issuedList = new List<m_ItemTransaction>();
                    RaisePropertyChangedEvent(nameof(IssuedList));
                    OnFocusRequested(nameof(IssuedCode));
                    return;
                }

                IssuedListBinding();
                _returnList = new List<m_ItemTransaction>();
                ReturnListBinding();
                ClearForm();
            }
        }

        private bool _canEdit;

        public bool CanEdit
        {
            get { return _canEdit; }
            set { _canEdit = value; }
        }

        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _itemDescription;

        public string ItemDescription
        {
            get { return _itemDescription; }
            set { _itemDescription = value; }
        }

        private string _unit;

        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        private decimal _unitPrice;

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private string _lotNumber;

        public string LotNumber
        {
            get { return _lotNumber; }
            set { _lotNumber = value; }
        }

        private int _issuedQuantity;

        public int IssuedQuantity
        {
            get { return _issuedQuantity; }
            set { _issuedQuantity = value; }
        }

        private int _returnQuantity;

        public int ReturnQuantity
        {
            get { return _returnQuantity; }
            set
            {
                _returnQuantity = value;
                _usedQuantity = _issuedQuantity - _returnQuantity;

                if (_usedQuantity < 0)
                {
                    MessageBoxHelper.Warning("จำนวนที่จะคืนไม่ควรเกินจำนวนที่เบิกมา");
                    OnFocusRequested(nameof(ReturnQuantity));
                }

                RaisePropertyChangedEvent(nameof(UsedQuantity));

            }
        }

        private int _usedQuantity;

        public int UsedQuantity
        {
            get { return _usedQuantity; }
            set { _usedQuantity = value; }
        }

        private Guid _issuedID;

        public Guid IssuedID
        {
            get { return _issuedID; }
            set { _issuedID = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Employee> _returnUserList;

        public List<pd_ryo_inventory_Employee> ReturnUserList
        {
            get { return _returnUserList; }
            set { _returnUserList = value; }
        }

        private List<pd_ryo_inventory_Issued> _issuedCodeList;

        public List<pd_ryo_inventory_Issued> IssuedCodeList
        {
            get { return _issuedCodeList; }
            set { _issuedCodeList = value; }
        }

        private List<m_ItemTransaction> _issuedList;

        public List<m_ItemTransaction> IssuedList
        {
            get
            {
                if (_issuedList == null)
                    _issuedList = new List<m_ItemTransaction>();
                return _issuedList;
            }
            set { _issuedList = value; }
        }

        private List<m_ItemTransaction> _returnList;

        public List<m_ItemTransaction> ReturnList
        {
            get
            {
                if (_returnList == null)
                    _returnList = new List<m_ItemTransaction>();
                return _returnList;
            }
            set { _returnList = value; }
        }
        #endregion



        #region Command
        private ICommand _onReturnCommand;

        public ICommand OnReturnCommand
        {
            get { return _onReturnCommand ?? (_onReturnCommand = new RelayCommand(Return)); }
            set { _onReturnCommand = value; }
        }

        private void Return(object obj)
        {
            try
            {
                if (_returnStatus == true)
                    throw new ArgumentException("เมื่อมีการบันทึกการคืนไปแล้ว ระบบไม่สามารถลบหรือแก้ไขข้อมูลได้");

                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดคลิกเลือกรายการที่จะทำการคืนจากตารางด้านล่างซ้าย");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (_returnQuantity <= 0)
                {
                    MessageBoxHelper.Warning("โปรดระบุจำนวนที่ต้องการคืน");
                    OnFocusRequested(nameof(ReturnQuantity));
                    return;
                }

                if (_usedQuantity < 0)
                {
                    MessageBoxHelper.Warning("จำนวนที่คืนเกินกว่าจำนวนที่เบิก");
                    OnFocusRequested(nameof(ReturnQuantity));
                    return;
                }

                var returnItem = _returnList
                    .SingleOrDefault(x => x.ItemCode == _itemCode &&
                    x.LotNumber == _lotNumber);

                if (returnItem != null)
                    throw new ArgumentException("มีการบันทึกการคืนไปแล้ว หากต้องการแก้ไขข้อมูลให้ลบข้อมูลจากตารางด้านล่างออกก่อน");

                _returnList.Add(new m_ItemTransaction
                {
                    ItemCode = _itemCode,
                    Description = _itemDescription,
                    UnitName = _unit,
                    UnitPrice = _unitPrice,
                    LotNumber = _lotNumber,
                    Quantity = _returnQuantity
                });

                _returnList = _returnList.ToList();
                ReturnListBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            ClearForm();
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_returnCode))
                    throw new ArgumentException("โปรดระบุ Return code");

                if (string.IsNullOrEmpty(_issuedCode))
                    throw new ArgumentException("โปรดระบุ Issued code");

                if (string.IsNullOrEmpty(_returnUser))
                    throw new ArgumentException("โปรดระบุ Return user");

                if (_returnList.Count() < 1)
                    throw new ArgumentException("โปรดเพิ่มรายการคืนอย่างน้อย 1 รายการ");

                if (_returnStatus == false)
                {
                    InventoryService.ReturnBL()
                        .Add(new pd_ryo_inventory_Return
                        {
                            ReturnCode = _returnCode,
                            ReturnDate = _returnDate,
                            Description = _description,
                            IssuedCode = _issuedCode,
                            ReturnUser = _returnUser,
                            ReturnStatus = true,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = user_setting.User.Username
                        });

                    foreach (var item in _returnList)
                    {
                        InventoryService.ReturnDetailsBL()
                            .Add(new pd_ryo_inventory_ReturnDetail
                            {
                                ReturnID = Guid.NewGuid(),
                                ReturnCode = _returnCode,
                                ItemCode = item.ItemCode,
                                LotNo = item.LotNumber,
                                UnitPrice = item.UnitPrice,
                                Quantity = item.Quantity,
                                ModifiedBy = user_setting.User.Username,
                                ModifiedDate = DateTime.Now
                            });
                    }
                }
                else
                    InventoryService.ReturnBL()
                        .Update(_returnCode,
                        _returnUser,
                        _description,
                        _returnDate,
                        user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                MasterBinding();
                ReturnListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(Delete)); }
            set { _onDeleteCommand = value; }
        }

        private void Delete(object obj)
        {
            try
            {
                var item = (m_ItemTransaction)obj;
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (_returnStatus == true)
                    throw new ArgumentException("เมื่อมีการบันทึกการคืนไปแล้ว ระบบไม่สามารถลบหรือแก้ไขข้อมูลได้");
                //if (MessageBoxHelper.Question("ระบบจะตัดสินค้าออกจากสต็อคตามจำนวนที่รายการที่ถูกลบออกไป " +
                //    "คุณต้องการลบรายการนี้ใช่หรือไม่?") == MessageBoxResult.Yes)
                //    InventoryService.ReturnDetailsBL()
                //        .Delete(_returnCode, item.ItemCode, item.LotNumber);
                else
                    _returnList.Remove(item);

                _returnList = _returnList.ToList();
                ReturnListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onItemSelectedCommand;

        public ICommand OnItemSelectedCommand
        {
            get { return _onItemSelectedCommand ?? (_onItemSelectedCommand = new RelayCommand(ItemSelected)); }
            set { _onItemSelectedCommand = value; }
        }

        private void ItemSelected(object obj)
        {
            try
            {
                var issuedItem = (m_ItemTransaction)obj;
                if (issuedItem == null)
                    throw new ArgumentException("Object cannot be null.");

                var returnItem = new m_ItemTransaction();
                returnItem = _returnList
                    .SingleOrDefault(x => x.ItemCode == issuedItem.ItemCode &&
                    x.LotNumber == issuedItem.LotNumber);

                _itemCode = issuedItem.ItemCode;
                _itemDescription = issuedItem.Description;
                _unit = issuedItem.UnitName;
                _unitPrice = issuedItem.UnitPrice;
                _lotNumber = issuedItem.LotNumber;
                _issuedQuantity = issuedItem.Quantity;
                _returnQuantity = returnItem == null ? 0 : returnItem.Quantity;
                _usedQuantity = issuedItem.Quantity - (returnItem == null ? 0 : returnItem.Quantity);

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(Unit));
                RaisePropertyChangedEvent(nameof(UnitPrice));
                RaisePropertyChangedEvent(nameof(LotNumber));
                RaisePropertyChangedEvent(nameof(IssuedQuantity));
                RaisePropertyChangedEvent(nameof(ReturnQuantity));
                RaisePropertyChangedEvent(nameof(UsedQuantity));
                OnFocusRequested(nameof(ReturnQuantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onIssuedCodeDetailCommand;

        public ICommand OnIssuedCodeDetailCommand
        {
            get { return _onIssuedCodeDetailCommand ?? (_onIssuedCodeDetailCommand = new RelayCommand(IssuedCodeDetail)); }
            set { _onIssuedCodeDetailCommand = value; }
        }

        private void IssuedCodeDetail(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_issuedCode))
                    throw new ArgumentException("โปรดระบุ Issued code โดยเลือกจากตัวเลือกทางด้านซ้าย");

                var window = new View.Inventory.Issued.IssuedDetail();
                var vm = new Issued.vm_IssuedDetail();
                window.DataContext = vm;
                vm.IssuedCode = _issuedCode;
                vm.Window = window;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void MasterBinding()
        {
            try
            {
                if (_returnCode == null)
                {
                    _returnCode = InventoryService.ReturnBL().GetNewReturnCode();
                    _returnDate = DateTime.Now;
                    _canEdit = !_returnStatus;

                    RaisePropertyChangedEvent(nameof(ReturnCode));
                    RaisePropertyChangedEvent(nameof(ReturnDate));
                    RaisePropertyChangedEvent(nameof(CanEdit));
                }
                else
                {
                    var item = InventoryService.ReturnBL().GetSingle(_returnCode);
                    if (item == null)
                        throw new ArgumentException("Object cannot be null.");

                    _returnDate = item.ReturnDate;
                    _returnUser = item.ReturnUser;
                    _description = item.Description;
                    _issuedCode = item.IssuedCode;
                    _returnStatus = item.ReturnStatus;
                    _canEdit = _returnStatus;

                    RaisePropertyChangedEvent(nameof(ReturnDate));
                    RaisePropertyChangedEvent(nameof(ReturnUser));
                    RaisePropertyChangedEvent(nameof(Description));
                    RaisePropertyChangedEvent(nameof(IssuedCode));
                    RaisePropertyChangedEvent(nameof(CanEdit));

                    IssuedListBinding();
                    ReturnListBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void IssuedListBinding()
        {
            try
            {
                _issuedList = InventoryService.IssuedDetailsBL()
                    .GetByIssuedCode(_issuedCode)
                    .Select(x => new m_ItemTransaction
                    {
                        ItemCode = x.ItemCode,
                        Description = x.pd_ryo_inventory_Item.Description,
                        Unit = x.pd_ryo_inventory_Item.Unit,
                        UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                        LotNumber = x.LotNo,
                        UnitPrice = x.UnitPrice,
                        Quantity = x.Quantity,
                    }).ToList();

                RaisePropertyChangedEvent(nameof(IssuedList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReturnListBinding()
        {
            try
            {
                if (_returnStatus == true)
                    _returnList = InventoryService.ReturnDetailsBL()
                        .GetByReturnCode(_returnCode)
                        .Select(x => new m_ItemTransaction
                        {
                            ItemCode = x.ItemCode,
                            Description = x.pd_ryo_inventory_Item.Description,
                            Unit = x.pd_ryo_inventory_Item.Unit,
                            UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                            LotNumber = x.LotNo,
                            UnitPrice = x.UnitPrice,
                            Quantity = x.Quantity
                        }).ToList();
                else
                    _returnList = _returnList.ToList();

                RaisePropertyChangedEvent(nameof(ReturnList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _itemCode = null;
            _itemDescription = null;
            _unit = null;
            _lotNumber = null;
            _issuedQuantity = 0;
            _returnQuantity = 0;
            _usedQuantity = 0;

            RaisePropertyChangedEvent(nameof(ItemCode));
            RaisePropertyChangedEvent(nameof(ItemDescription));
            RaisePropertyChangedEvent(nameof(Unit));
            RaisePropertyChangedEvent(nameof(LotNumber));
            RaisePropertyChangedEvent(nameof(IssuedQuantity));
            RaisePropertyChangedEvent(nameof(ReturnQuantity));
            RaisePropertyChangedEvent(nameof(UsedQuantity));
        }
        #endregion
    }
}
