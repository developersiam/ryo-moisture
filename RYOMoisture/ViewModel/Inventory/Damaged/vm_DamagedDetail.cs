﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.Inventory.Damaged;
using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Damaged
{
    public class vm_DamagedDetail : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DamagedDetail()
        {
            try
            {
                _reportUserList = InventoryService.EmployeeBL()
                    .Get()
                    .OrderBy(x => x.FirstName)
                    .ToList();
                _issuedCodeList = InventoryService.IssuedBL()
                    .GetByDateRange(Convert.ToDateTime(DateTime.Now.Year + "-01-01"), DateTime.Now);
                _createDate = DateTime.Now;

                RaisePropertyChangedEvent(nameof(ReportUserList));
                RaisePropertyChangedEvent(nameof(IssuedCodeList));
                RaisePropertyChangedEvent(nameof(CreateDate));
                MasterBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }



        #region Properties
        private DamagedDetail _window;

        public DamagedDetail Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private string _damagedCode;

        public string DamagedCode
        {
            get { return _damagedCode; }
            set
            {
                _damagedCode = value;
                MasterBinding();
            }
        }

        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        private string _reportUser;

        public string ReportUser
        {
            get { return _reportUser; }
            set { _reportUser = value; }
        }

        private bool _damagedStatus;

        public bool DamagedStatus
        {
            get { return _damagedStatus; }
            set { _damagedStatus = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _issuedCode;

        public string IssuedCode
        {
            get { return _issuedCode; }
            set
            {
                _issuedCode = value;

                if (InventoryService.DamagedBL().IsDupplicateIssuedCode(_issuedCode))
                {
                    MessageBoxHelper.Warning("Issued code รหัส #" + _issuedCode + " ถูกนำไปใช้อ้างอิงแล้ว");
                    _issuedList = new List<m_ItemTransaction>();
                    RaisePropertyChangedEvent(nameof(IssuedList));
                    OnFocusRequested(nameof(IssuedCode));
                    return;
                }

                IssuedListBinding();
                _damagedList = new List<m_ItemTransaction>();
                DamagedListBinding();
                ClearForm();
            }
        }

        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _itemDescription;

        public string ItemDescription
        {
            get { return _itemDescription; }
            set { _itemDescription = value; }
        }

        private string _unit;

        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        private decimal _unitPrice;

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private string _lotNumber;

        public string LotNumber
        {
            get { return _lotNumber; }
            set { _lotNumber = value; }
        }

        private int _issuedQuantity;

        public int IssuedQuantity
        {
            get { return _issuedQuantity; }
            set { _issuedQuantity = value; }
        }

        private int _returnQuantity;

        public int ReturnQuantity
        {
            get { return _returnQuantity; }
            set { _returnQuantity = value; }
        }

        private int _damagedQuantity;

        public int DamagedQuantity
        {
            get { return _damagedQuantity; }
            set { _damagedQuantity = value; }
        }

        private string _damageDescription;

        public string DamagedDescription
        {
            get { return _damageDescription; }
            set { _damageDescription = value; }
        }


        private Guid _issuedID;

        public Guid IssuedID
        {
            get { return _issuedID; }
            set { _issuedID = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Employee> _reportUserList;

        public List<pd_ryo_inventory_Employee> ReportUserList
        {
            get { return _reportUserList; }
            set { _reportUserList = value; }
        }

        private List<pd_ryo_inventory_Issued> _issuedCodeList;

        public List<pd_ryo_inventory_Issued> IssuedCodeList
        {
            get { return _issuedCodeList; }
            set { _issuedCodeList = value; }
        }

        private List<m_ItemTransaction> _issuedList;

        public List<m_ItemTransaction> IssuedList
        {
            get
            {
                if (_issuedList == null)
                    _issuedList = new List<m_ItemTransaction>();
                return _issuedList;
            }
            set { _issuedList = value; }
        }

        private List<m_ItemTransaction> _damagedList;

        public List<m_ItemTransaction> DamagedList
        {
            get { return _damagedList; }
            set { _damagedList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(Add)); }
            set { _onAddCommand = value; }
        }

        private void Add(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดคลิกเลือกรายการที่จะทำการคืนจากตารางด้านล่างซ้าย");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (_damagedQuantity <= 0)
                {
                    MessageBoxHelper.Warning("โปรดระบุจำนวนของที่เสียหาย");
                    OnFocusRequested(nameof(DamagedQuantity));
                    return;
                }

                if (string.IsNullOrEmpty(_damageDescription))
                    throw new ArgumentException("โปรดระบุ Damage remark");

                if (_damagedQuantity > _issuedQuantity - _returnQuantity)
                {
                    MessageBoxHelper.Warning("จำนวนที่เสียหายจะต้องไม่เกิน " +
                        (_issuedQuantity - _returnQuantity).ToString("N0"));
                    OnFocusRequested(nameof(DamagedQuantity));
                    return;
                }

                var damagedItem = _damagedList
                        .SingleOrDefault(x => x.ItemCode == _itemCode &&
                        x.LotNumber == _lotNumber);

                if (damagedItem != null)
                    throw new ArgumentException("มีการนี้อยู่แล้วในระบบ");

                if (_damagedStatus == true)
                    InventoryService.DamagedDetailBL()
                        .Add(new pd_ryo_inventory_DamagedDetail
                        {
                            ID = Guid.NewGuid(),
                            ItemCode = _itemCode,
                            LotNo = _lotNumber,
                            UnitPrice = _unitPrice,
                            Quantity = _damagedQuantity,
                            DamagedCode = _damagedCode,
                            Description = _damageDescription,
                            ModifiedBy = user_setting.User.Username,
                            ModifiedDate = DateTime.Now
                        });
                else
                    _damagedList.Add(new m_ItemTransaction
                    {
                        ItemCode = _itemCode,
                        Description = _itemDescription,
                        UnitName = _unit,
                        UnitPrice = _unitPrice,
                        LotNumber = _lotNumber,
                        DamagedQuantity = _damagedQuantity,
                        DamagedDescription = _damageDescription
                    });

                _damagedList = _damagedList.ToList();
                DamagedListBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            ClearForm();
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_damagedCode))
                    throw new ArgumentException("โปรดระบุ damaged code");

                if (string.IsNullOrEmpty(_issuedCode))
                    throw new ArgumentException("โปรดระบุ Issued code");

                if (string.IsNullOrEmpty(_reportUser))
                    throw new ArgumentException("โปรดระบุ Report user");

                if (_damagedList.Count() < 1)
                    throw new ArgumentException("โปรดเพิ่มรายการคืนอย่างน้อย 1 รายการ");

                if (_damagedStatus == false)
                {
                    if (InventoryService.DamagedBL().IsDupplicateIssuedCode(_issuedCode))
                        throw new ArgumentException("Issued code รหัส #" + _issuedCode + " ถูกนำไปใช้อ้างอิงการคืนแล้ว");

                    InventoryService.DamagedBL()
                        .Add(new pd_ryo_inventory_Damaged
                        {
                            DamagedCode = _damagedCode,
                            Description = _description,
                            IssuedCode = _issuedCode,
                            ReportUser = _reportUser,
                            CreateDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            ModifiedBy = user_setting.User.Username
                        });

                    foreach (var item in _damagedList)
                    {
                        InventoryService.DamagedDetailBL()
                            .Add(new pd_ryo_inventory_DamagedDetail
                            {
                                ID = Guid.NewGuid(),
                                DamagedCode = _damagedCode,
                                ItemCode = item.ItemCode,
                                LotNo = item.LotNumber,
                                UnitPrice = item.UnitPrice,
                                Quantity = item.DamagedQuantity,
                                Description = item.DamagedDescription,
                                ModifiedBy = user_setting.User.Username,
                                ModifiedDate = DateTime.Now
                            });
                    }
                }
                else
                    InventoryService.DamagedBL()
                        .Update(_damagedCode,
                        _reportUser,
                        _description,
                        user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                MasterBinding();
                DamagedListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(Delete)); }
            set { _onDeleteCommand = value; }
        }

        private void Delete(object obj)
        {
            try
            {
                var item = (m_ItemTransaction)obj;
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                if (_damagedStatus == false)
                    _damagedList.Remove(item);
                else
                {
                    if (MessageBoxHelper.Question("คุณต้องการลบรายการนี้ใช่หรือไม่?") == MessageBoxResult.Yes)
                        InventoryService.DamagedDetailBL()
                            .Delete(item.ItemCode, item.LotNumber, _damagedCode);
                }

                _damagedList = _damagedList.ToList();
                DamagedListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onItemSelectedCommand;

        public ICommand OnItemSelectedCommand
        {
            get { return _onItemSelectedCommand ?? (_onItemSelectedCommand = new RelayCommand(ItemSelected)); }
            set { _onItemSelectedCommand = value; }
        }

        private void ItemSelected(object obj)
        {
            try
            {
                var issuedItem = (m_ItemTransaction)obj;
                if (issuedItem == null)
                    throw new ArgumentException("Object cannot be null.");

                var selectedItem = new m_ItemTransaction();
                selectedItem = _issuedList
                    .SingleOrDefault(x => x.ItemCode == issuedItem.ItemCode &&
                    x.LotNumber == issuedItem.LotNumber);

                _itemCode = issuedItem.ItemCode;
                _itemDescription = issuedItem.Description;
                _unit = issuedItem.UnitName;
                _unitPrice = issuedItem.UnitPrice;
                _lotNumber = issuedItem.LotNumber;
                _issuedQuantity = issuedItem.IssuedQuantity;
                _returnQuantity = selectedItem == null ? 0 : selectedItem.ReturnQuantity;
                _damagedQuantity = selectedItem.Quantity;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(Unit));
                RaisePropertyChangedEvent(nameof(UnitPrice));
                RaisePropertyChangedEvent(nameof(LotNumber));
                RaisePropertyChangedEvent(nameof(IssuedQuantity));
                RaisePropertyChangedEvent(nameof(ReturnQuantity));
                RaisePropertyChangedEvent(nameof(DamagedQuantity));
                OnFocusRequested(nameof(DamagedQuantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onIssuedCodeDetailCommand;

        public ICommand OnIssuedCodeDetailCommand
        {
            get { return _onIssuedCodeDetailCommand ?? (_onIssuedCodeDetailCommand = new RelayCommand(IssuedCodeDetail)); }
            set { _onIssuedCodeDetailCommand = value; }
        }

        private void IssuedCodeDetail(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_issuedCode))
                    throw new ArgumentException("โปรดระบุ Issued code โดยเลือกจากตัวเลือกทางด้านซ้าย");

                var window = new View.Inventory.Issued.IssuedDetail();
                var vm = new Issued.vm_IssuedDetail();
                window.DataContext = vm;
                vm.IssuedCode = _issuedCode;
                vm.Window = window;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void MasterBinding()
        {
            try
            {
                if (_damagedCode == null)
                {
                    _damagedCode = InventoryService.DamagedBL().GetNewDamagedCode();
                    _damagedStatus = false;
                    RaisePropertyChangedEvent(nameof(DamagedCode));
                }
                else
                {
                    var item = InventoryService.DamagedBL().GetSingle(_damagedCode);
                    if (item == null)
                        throw new ArgumentException("Object cannot be null.");

                    _damagedStatus = true;
                    _reportUser = item.ReportUser;
                    _description = item.Description;
                    _issuedCode = item.IssuedCode;
                    _damageDescription = "";

                    RaisePropertyChangedEvent(nameof(DamagedStatus));
                    RaisePropertyChangedEvent(nameof(DamagedDescription));
                    RaisePropertyChangedEvent(nameof(ReportUser));
                    RaisePropertyChangedEvent(nameof(Description));
                    RaisePropertyChangedEvent(nameof(IssuedCode));

                    IssuedListBinding();
                    DamagedListBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void IssuedListBinding()
        {
            try
            {
                var issuedList = InventoryService.IssuedDetailsBL()
                    .GetByIssuedCode(_issuedCode)
                    .ToList();

                var returnList = InventoryService.ReturnDetailsBL()
                    .GetByIssuedCode(_issuedCode)
                    .ToList();

                var join = (from a in issuedList
                            join b in returnList
                            on new { A = a.ItemCode, B = a.LotNo } equals
                            new { A = b.ItemCode, B = b.LotNo }
                            into c
                            from result in c.DefaultIfEmpty()
                            select new m_ItemTransaction
                            {
                                ItemCode = a.ItemCode,
                                Description = a.pd_ryo_inventory_Item.Description,
                                UnitName = a.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                                LotNumber = a.LotNo,
                                UnitPrice = a.UnitPrice,
                                IssuedQuantity = a.Quantity,
                                ReturnQuantity = result == null ? 0 : result.Quantity
                            }).ToList();

                _issuedList = join;
                RaisePropertyChangedEvent(nameof(IssuedList));
                RaisePropertyChangedEvent(nameof(returnList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DamagedListBinding()
        {
            try
            {
                if (_damagedStatus == true)
                    _damagedList = InventoryService.DamagedDetailBL()
                             .GetByDamagedCode(_damagedCode)
                             .Select(x => new m_ItemTransaction
                             {
                                 ItemCode = x.ItemCode,
                                 Description = x.pd_ryo_inventory_Item.Description,
                                 Unit = x.pd_ryo_inventory_Item.Unit,
                                 UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                                 LotNumber = x.LotNo,
                                 UnitPrice = x.UnitPrice,
                                 DamagedQuantity = x.Quantity,
                                 DamagedDescription = x.Description
                             }).ToList();
                else
                    _damagedList = _damagedList.ToList();

                RaisePropertyChangedEvent(nameof(DamagedList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _itemCode = null;
            _itemDescription = null;
            _unit = null;
            _lotNumber = null;
            _issuedQuantity = 0;
            _returnQuantity = 0;
            _damagedQuantity = 0;
            _damageDescription = null;

            RaisePropertyChangedEvent(nameof(ItemCode));
            RaisePropertyChangedEvent(nameof(ItemDescription));
            RaisePropertyChangedEvent(nameof(Unit));
            RaisePropertyChangedEvent(nameof(LotNumber));
            RaisePropertyChangedEvent(nameof(IssuedQuantity));
            RaisePropertyChangedEvent(nameof(ReturnQuantity));
            RaisePropertyChangedEvent(nameof(DamagedQuantity));
            RaisePropertyChangedEvent(nameof(DamagedDescription));
        }
        #endregion
    }
}
