﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoisture.View.Inventory.Damaged;
using RYOMoistureBL.InventoryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Damaged
{
    public class vm_DamagedMaster : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DamagedMaster()
        {
            _from = Convert.ToDateTime(DateTime.Now.Year + "-01-01");
            _to = DateTime.Now;
            RaisePropertyChangedEvent(nameof(From));
            RaisePropertyChangedEvent(nameof(To));
            DataGridBinding();
        }



        #region Properties

        private DateTime _from;

        public DateTime From
        {
            get { return _from; }
            set
            {
                _from = value;
                DataGridBinding();
            }
        }

        private DateTime _to;

        public DateTime To
        {
            get { return _to; }
            set
            {
                _to = value;
                DataGridBinding();
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion




        #region List
        private List<pd_ryo_inventory_Damaged> _damagedList;

        public List<pd_ryo_inventory_Damaged> DamagedList
        {
            get { return _damagedList; }
            set { _damagedList = value; }
        }
        #endregion



        #region Command

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DataGridBinding();
        }

        private ICommand _onDetailCommand;

        public ICommand OnDetailCommand
        {
            get { return _onDetailCommand ?? (_onDetailCommand = new RelayCommand(OnDetail)); }
            set { _onDetailCommand = value; }
        }

        private void OnDetail(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_Damaged)obj;
                if (item == null)
                    throw new ArgumentException("ไม่สามารถแสดงข้อมูลได้ โปรดติดต่อแผนกไอที");

                var window = new DamagedDetail();
                var vm = new vm_DamagedDetail();
                window.DataContext = vm;
                vm.Window = window;
                vm.DamagedCode = item.DamagedCode;
                window.ShowDialog();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var item = (pd_ryo_inventory_Return)obj;
                if (item == null)
                    throw new ArgumentException("ไม่สามารถแสดงข้อมูลได้ โปรดติดต่อแผนกไอที");

                InventoryService.ReturnBL().Delete(item.ReturnCode);
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCreateCommand;

        public ICommand OnCreateCommand
        {
            get { return _onCreateCommand ?? (_onCreateCommand = new RelayCommand(OnCreate)); }
            set { _onCreateCommand = value; }
        }

        private void OnCreate(object obj)
        {
            try
            {
                var window = new DamagedDetail();
                var vm = new vm_DamagedDetail();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DataGridBinding()
        {
            try
            {
                _damagedList = InventoryService.DamagedBL()
                    .GetByDateRange(_from, _to)
                    .OrderByDescending(x => x.CreateDate)
                    .ThenByDescending(x => x.DamagedCode)
                    .ToList();
                _totalRecord = _damagedList.Count();

                RaisePropertyChangedEvent(nameof(DamagedList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
