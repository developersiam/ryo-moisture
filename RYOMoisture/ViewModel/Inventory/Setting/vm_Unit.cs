﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoisture.Helper;
using System.Windows.Input;
using System.Windows;
using RYOMoistureBL.InventoryBL;

namespace RYOMoisture.ViewModel.Inventory.Setting
{
    public class vm_Unit : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Unit()
        {
            DataGridBinding();
            ClearForm();            
        }


        #region Properties
        private string _unitCode;

        public string UnitCode
        {
            get { return _unitCode; }
            set { _unitCode = value; }
        }

        private string _unitName;

        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private bool _unitCodeReadOnly;

        public bool UnitCodeReadOnly
        {
            get { return _unitCodeReadOnly; }
            set { _unitCodeReadOnly = value; }
        }

        private pd_ryo_inventory_Unit _selectedItem;

        public pd_ryo_inventory_Unit SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Unit> _unitList;

        public List<pd_ryo_inventory_Unit> UnitList
        {
            get { return _unitList; }
            set { _unitList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_unitCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ unit code.");
                    OnFocusRequested(nameof(UnitCode));
                    return;
                }

                if (string.IsNullOrEmpty(_unitName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ unit name.");
                    OnFocusRequested(nameof(UnitName));
                    return;
                }

                if (string.IsNullOrEmpty(_description))
                {
                    MessageBoxHelper.Warning("โปรดระบุ description.");
                    OnFocusRequested(nameof(Description));
                    return;
                }

                InventoryService.UnitBL()
                    .Add(new pd_ryo_inventory_Unit
                    {
                        UnitCode = _unitCode,
                        UnitName = _unitName,
                        Description = _description,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                //MessageBoxHelper.Info("Add new item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_unitCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ unit code.");
                    OnFocusRequested(nameof(UnitCode));
                    return;
                }

                if (string.IsNullOrEmpty(_unitName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ unit name.");
                    OnFocusRequested(nameof(UnitName));
                    return;
                }

                if (string.IsNullOrEmpty(_description))
                {
                    MessageBoxHelper.Warning("โปรดระบุ description.");
                    OnFocusRequested(nameof(Description));
                    return;
                }

                if (MessageBoxHelper.Question("Do you wnat to edit this item?") ==
                    MessageBoxResult.No)
                    return;

                _selectedItem.UnitName = _unitName;
                _selectedItem.Description = _description;

                InventoryService.UnitBL().Update(_selectedItem);
                //MessageBoxHelper.Info("Edit item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                var item = (pd_ryo_inventory_Unit)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                InventoryService.UnitBL().Delete(item.UnitCode);
                //MessageBoxHelper.Info("Delete item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_Unit)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _unitCode = item.UnitCode;
                _unitName = item.UnitName;
                _description = item.Description;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _unitCodeReadOnly = true;
                _selectedItem = item;

                RaisePropertyChangedEvent(nameof(UnitCode));
                RaisePropertyChangedEvent(nameof(UnitName));
                RaisePropertyChangedEvent(nameof(Description));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(UnitCodeReadOnly));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            DataGridBinding();
            ClearForm();
        }

        private ICommand _unitCodeEnterCommand;

        public ICommand UnitCodeEnterCommand
        {
            get { return _unitCodeEnterCommand ?? (_unitCodeEnterCommand = new RelayCommand(OnClearForm)); }
            set { _unitCodeEnterCommand = value; }
        }

        private void UnitCodeEnter(object obj)
        {
            OnFocusRequested(nameof(UnitName));
        }

        private ICommand _unitNameEnterCommand;

        public ICommand UnitNameEnterCommand
        {
            get { return _unitNameEnterCommand ?? (_unitNameEnterCommand = new RelayCommand(UnitNameEnter)); }
            set { _unitNameEnterCommand = value; }
        }

        private void UnitNameEnter(object obj)
        {
            OnFocusRequested(nameof(Description));
        }
        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                _unitList = InventoryService.UnitBL()
                    .Get()
                    .OrderBy(x => x.UnitCode)
                    .ToList();
                _totalRecord = _unitList.Count();

                RaisePropertyChangedEvent(nameof(UnitList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _unitCode = "";
            _unitName = "";
            _description = "";
            _unitCodeReadOnly = false;
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _selectedItem = null;

            RaisePropertyChangedEvent(nameof(UnitCode));
            RaisePropertyChangedEvent(nameof(UnitName));
            RaisePropertyChangedEvent(nameof(Description));
            RaisePropertyChangedEvent(nameof(UnitCodeReadOnly));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItem));

            OnFocusRequested(nameof(UnitCode));
        }
        #endregion
    }
}
