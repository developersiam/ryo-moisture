﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using System.Windows.Input;
using RYOMoisture.Helper;
using RYOMoistureBL.InventoryBL;
using System.Windows;

namespace RYOMoisture.ViewModel.Inventory.Setting
{
    public class vm_Employee : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Employee()
        {
            DataGridBinding();
            ClearForm();
        }


        #region Properties
        private string _employeeCode;

        public string EmployeeCode
        {
            get { return _employeeCode; }
            set { _employeeCode = value; }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        private string _department;

        public string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private bool _employeeReadOnly;

        public bool EmployeeReadOnly
        {
            get { return _employeeReadOnly; }
            set { _employeeReadOnly = value; }
        }

        private pd_ryo_inventory_Employee _selectedItem;

        public pd_ryo_inventory_Employee SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }

        #endregion



        #region List
        private List<pd_ryo_inventory_Employee> _employeeList;

        public List<pd_ryo_inventory_Employee> EmployeeList
        {
            get { return _employeeList; }
            set { _employeeList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_employeeCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ employee code.");
                    OnFocusRequested(nameof(EmployeeCode));
                    return;
                }

                if (string.IsNullOrEmpty(_firstName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ first name.");
                    OnFocusRequested(nameof(FirstName));
                    return;
                }

                if (string.IsNullOrEmpty(_lastName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ last name.");
                    OnFocusRequested(nameof(LastName));
                    return;
                }

                if (string.IsNullOrEmpty(_department))
                {
                    MessageBoxHelper.Warning("โปรดระบุ department.");
                    OnFocusRequested(nameof(Department));
                    return;
                }

                InventoryService.EmployeeBL()
                    .Add(new pd_ryo_inventory_Employee
                    {
                        EmployeeCode = _employeeCode,
                        FirstName = _firstName,
                        LastName = _lastName,
                        Department = _department,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                //MessageBoxHelper.Info("Add new item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_employeeCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ employee code.");
                    OnFocusRequested(nameof(EmployeeCode));
                    return;
                }

                if (string.IsNullOrEmpty(_firstName))
                {
                    MessageBoxHelper.Warning("โปดระบุ first name.");
                    OnFocusRequested(nameof(FirstName));
                    return;
                }

                if (string.IsNullOrEmpty(_lastName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ last name.");
                    OnFocusRequested(nameof(LastName));
                    return;
                }

                if (string.IsNullOrEmpty(_department))
                {
                    MessageBoxHelper.Warning("โปรดระบุ department.");
                    OnFocusRequested(nameof(Department));
                    return;
                }

                if (MessageBoxHelper.Question("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") ==
                    MessageBoxResult.No)
                    return;

                _selectedItem.FirstName = _firstName;
                _selectedItem.LastName = _lastName;
                _selectedItem.Department = _department;

                InventoryService.EmployeeBL().Update(_selectedItem);
                //MessageBoxHelper.Info("Edit item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                var item = (pd_ryo_inventory_Employee)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                InventoryService.EmployeeBL().Delete(item.EmployeeCode);
                //MessageBoxHelper.Info("Delete item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_Employee)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _employeeCode = item.EmployeeCode;
                _firstName = item.FirstName;
                _lastName = item.LastName;
                _department = item.Department;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _employeeReadOnly = true;
                _selectedItem = item;

                RaisePropertyChangedEvent(nameof(EmployeeCode));
                RaisePropertyChangedEvent(nameof(FirstName));
                RaisePropertyChangedEvent(nameof(LastName));
                RaisePropertyChangedEvent(nameof(Department));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(EmployeeReadOnly));
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            DataGridBinding();
            ClearForm();
        }

        private ICommand _employeeCodeEnterCommand;

        public ICommand EmployeeCodeEnterCommand
        {
            get { return _employeeCodeEnterCommand ?? (_employeeCodeEnterCommand = new RelayCommand(EmployeeCodeEnter)); }
            set { _employeeCodeEnterCommand = value; }
        }

        private void EmployeeCodeEnter(object obj)
        {
            OnFocusRequested(nameof(FirstName));
        }

        private ICommand _firstNameEnterCommand;

        public ICommand FirstNameEnterCommand
        {
            get { return _firstNameEnterCommand ?? (_firstNameEnterCommand = new RelayCommand(FirstNameEnter)); }
            set { _firstNameEnterCommand = value; }
        }

        private void FirstNameEnter(object obj)
        {
            OnFocusRequested(nameof(LastName));
        }

        private ICommand _lastNameEnterCommand;

        public ICommand LastNameEnterCommand
        {
            get { return _lastNameEnterCommand ?? (_lastNameEnterCommand = new RelayCommand(LastNameEnter)); }
            set { _lastNameEnterCommand = value; }
        }

        private void LastNameEnter(object obj)
        {
            OnFocusRequested(nameof(Department));
        }
        #endregion



        #region Function
        private void DataGridBinding()
        {
            try
            {
                _employeeList = InventoryService.EmployeeBL()
                    .Get()
                    .OrderBy(x => x.EmployeeCode)
                    .ToList();
                _totalRecord = _employeeList.Count();

                RaisePropertyChangedEvent(nameof(EmployeeList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _employeeCode = "";
            _firstName = "";
            _lastName = "";
            _department = "";
            _employeeReadOnly = false;
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _selectedItem = null;

            RaisePropertyChangedEvent(nameof(EmployeeCode));
            RaisePropertyChangedEvent(nameof(FirstName));
            RaisePropertyChangedEvent(nameof(LastName));
            RaisePropertyChangedEvent(nameof(Department));
            RaisePropertyChangedEvent(nameof(EmployeeReadOnly));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItem));

            OnFocusRequested(nameof(EmployeeCode));
        }
        #endregion
    }
}
