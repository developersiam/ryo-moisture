﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoisture.Helper;
using System.Windows.Input;
using System.Windows;
using RYOMoistureBL.InventoryBL;

namespace RYOMoisture.ViewModel.Inventory.Setting
{
    public class vm_Item : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Item()
        {
            DataGridBinding();
            ClearForm();
            _unitList = InventoryService.UnitBL()
                .Get()
                .OrderBy(x => x.UnitName)
                .ToList();
            _categoryList = InventoryService.CategoryBL()
                .Get()
                .OrderBy(x => x.ItemCategoryName)
                .ToList();

            RaisePropertyChangedEvent(nameof(UnitList));
            RaisePropertyChangedEvent(nameof(CategoryList));
        }


        #region Properties
        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _unitCode;

        public string UnitCode
        {
            get { return _unitCode; }
            set { _unitCode = value; }
        }

        private string _categoryCode;

        public string CategoryCode
        {
            get { return _categoryCode; }
            set { _categoryCode = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private bool _itemCodeReadOnly;

        public bool ItemCodeReadOnly
        {
            get { return _itemCodeReadOnly; }
            set { _itemCodeReadOnly = value; }
        }

        private pd_ryo_inventory_Item _selectedItem;

        public pd_ryo_inventory_Item SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Unit> _unitList;

        public List<pd_ryo_inventory_Unit> UnitList
        {
            get { return _unitList; }
            set { _unitList = value; }
        }

        private List<pd_ryo_inventory_ItemCategory> _categoryList;

        public List<pd_ryo_inventory_ItemCategory> CategoryList
        {
            get { return _categoryList; }
            set { _categoryList = value; }
        }

        private List<pd_ryo_inventory_Item> _itemList;

        public List<pd_ryo_inventory_Item> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }
        #endregion


        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ item code.");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (string.IsNullOrEmpty(_description))
                {
                    MessageBoxHelper.Warning("โปรดระบุ description.");
                    OnFocusRequested(nameof(Description));
                    return;
                }

                if (string.IsNullOrEmpty(_unitCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ unit.");
                    OnFocusRequested(nameof(UnitCode));
                    return;
                }

                if (string.IsNullOrEmpty(_categoryCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ category.");
                    OnFocusRequested(nameof(CategoryCode));
                    return;
                }

                InventoryService.ItemBL()
                    .Add(new pd_ryo_inventory_Item
                    {
                        ItemCode = _itemCode,
                        Description = _description,
                        Unit = _unitCode,
                        ItemCategoryCode = _categoryCode,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                //MessageBoxHelper.Info("Add new item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ item code.");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (string.IsNullOrEmpty(_description))
                {
                    MessageBoxHelper.Warning("โปรดระบุ description.");
                    OnFocusRequested(nameof(Description));
                    return;
                }

                if (string.IsNullOrEmpty(_unitCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ unit.");
                    OnFocusRequested(nameof(UnitCode));
                    return;
                }

                if (string.IsNullOrEmpty(_categoryCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ category.");
                    OnFocusRequested(nameof(CategoryCode));
                    return;
                }

                if (MessageBoxHelper.Question("Do you wnat to edit this item?") ==
                    MessageBoxResult.No)
                    return;

                _selectedItem.Description = _description;
                _selectedItem.Unit = _unitCode;
                _selectedItem.ItemCategoryCode = _categoryCode;

                InventoryService.ItemBL().Update(_selectedItem);
                //MessageBoxHelper.Info("Edit item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                var item = (pd_ryo_inventory_Item)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                InventoryService.ItemBL().Delete(item.ItemCode);
                //MessageBoxHelper.Info("Delete item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_Item)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _itemCode = item.ItemCode;
                _description = item.Description;
                _unitCode = item.Unit;
                _categoryCode = item.ItemCategoryCode;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _itemCodeReadOnly = true;
                _selectedItem = item;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(Description));
                RaisePropertyChangedEvent(nameof(UnitCode));
                RaisePropertyChangedEvent(nameof(CategoryCode));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(ItemCodeReadOnly));
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            try
            {
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _itemCodeEnterCommand;

        public ICommand ItemCodeEnterCommand
        {
            get { return _itemCodeEnterCommand ?? (_itemCodeEnterCommand = new RelayCommand(ItemCodeEnter)); }
            set { _itemCodeEnterCommand = value; }
        }

        private void ItemCodeEnter(object obj)
        {
            OnFocusRequested(nameof(Description));
        }

        private ICommand _descriptionEnterCommand;

        public ICommand DescriptionEnterCommand
        {
            get { return _descriptionEnterCommand ?? (_descriptionEnterCommand = new RelayCommand(DescriptionEnter)); }
            set { _descriptionEnterCommand = value; }
        }

        private void DescriptionEnter(object obj)
        {
            OnFocusRequested(nameof(UnitCode));
        }
        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                _itemList = InventoryService.ItemBL()
                    .Get()
                    .OrderBy(x => x.ItemCode)
                    .ToList();
                _totalRecord = _itemList.Count();

                RaisePropertyChangedEvent(nameof(ItemList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _itemCode = "";
            _description = "";
            _categoryCode = "";
            _unitCode = "";
            _itemCodeReadOnly = false;
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _selectedItem = null;

            RaisePropertyChangedEvent(nameof(ItemCode));
            RaisePropertyChangedEvent(nameof(Description));
            RaisePropertyChangedEvent(nameof(CategoryCode));
            RaisePropertyChangedEvent(nameof(UnitCode));
            RaisePropertyChangedEvent(nameof(ItemCodeReadOnly));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItem));

            OnFocusRequested(nameof(ItemCode));
        }
        #endregion
    }
}
