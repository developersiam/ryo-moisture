﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RYOMoisture.MVVM;
using DomainModelStecDbms;
using System.Windows.Input;
using RYOMoisture.Helper;
using System.Windows;
using RYOMoistureBL.InventoryBL;

namespace RYOMoisture.ViewModel.Inventory.Setting
{
    public class vm_Category : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Category()
        {
            DataGridBinding();
            ClearForm();
        }


        #region Properties
        private string _categoryCode;

        public string CategoryCode
        {
            get { return _categoryCode; }
            set { _categoryCode = value; }
        }

        private string _categoryName;

        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private bool _categoryCodeReadOnly;

        public bool CategoryCodeReadOnly
        {
            get { return _categoryCodeReadOnly; }
            set { _categoryCodeReadOnly = value; }
        }

        private pd_ryo_inventory_ItemCategory _selectedItem;

        public pd_ryo_inventory_ItemCategory SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_ItemCategory> _categoryList;

        public List<pd_ryo_inventory_ItemCategory> CategoryList
        {
            get { return _categoryList; }
            set { _categoryList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_categoryCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ employee code.");
                    OnFocusRequested(nameof(CategoryCode));
                    return;
                }

                if (string.IsNullOrEmpty(CategoryName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ first name.");
                    OnFocusRequested(nameof(CategoryName));
                    return;
                }

                InventoryService.CategoryBL()
                    .Add(new pd_ryo_inventory_ItemCategory
                    {
                        ItemCategoryCode = _categoryCode,
                        ItemCategoryName = _categoryName,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                //MessageBoxHelper.Info("Add new item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_categoryCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ category code.");
                    OnFocusRequested(nameof(CategoryCode));
                    return;
                }

                if (string.IsNullOrEmpty(_categoryName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ category name.");
                    OnFocusRequested(nameof(CategoryName));
                    return;
                }

                if (MessageBoxHelper.Question("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") ==
                    MessageBoxResult.No)
                    return;

                _selectedItem.ItemCategoryName = _categoryName;
                InventoryService.CategoryBL().Update(_selectedItem);
                //MessageBoxHelper.Info("Edit item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                var item = (pd_ryo_inventory_ItemCategory)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                InventoryService.CategoryBL().Delete(item.ItemCategoryCode);
                //MessageBoxHelper.Info("Delete item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_ItemCategory)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _categoryCode = item.ItemCategoryCode;
                _categoryName = item.ItemCategoryName;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _categoryCodeReadOnly = true;
                _selectedItem = item;

                RaisePropertyChangedEvent(nameof(CategoryCode));
                RaisePropertyChangedEvent(nameof(CategoryName));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(CategoryCodeReadOnly));
                RaisePropertyChangedEvent(nameof(SelectedItem));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            try
            {
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _categoryCodeEnterCommand;

        public ICommand CategoryCodeEnterCommand
        {
            get { return _categoryCodeEnterCommand ?? (_categoryCodeEnterCommand = new  RelayCommand(CategoryCodeEnter)); }
            set { _categoryCodeEnterCommand = value; }
        }

        private void CategoryCodeEnter(object obj)
        {
            OnFocusRequested(nameof(CategoryName));
        }

        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                _categoryList = InventoryService.CategoryBL()
                   .Get()
                   .OrderBy(x => x.ItemCategoryCode)
                   .ToList();
                _totalRecord = _categoryList.Count();

                RaisePropertyChangedEvent(nameof(CategoryList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _categoryCode = "";
            _categoryName = "";
            _categoryCodeReadOnly = false;
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _selectedItem = null;

            RaisePropertyChangedEvent(nameof(CategoryCode));
            RaisePropertyChangedEvent(nameof(CategoryName));
            RaisePropertyChangedEvent(nameof(CategoryCodeReadOnly));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItem));

            OnFocusRequested(nameof(CategoryCode));
        }
        #endregion
    }
}
