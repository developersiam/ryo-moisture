﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoisture.Helper;
using System.Windows.Input;
using System.Windows;
using RYOMoistureBL.InventoryBL;

namespace RYOMoisture.ViewModel.Inventory.Setting
{
    public class vm_supplier : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_supplier()
        {
            DataGridBinding();
            ClearForm();
        }


        #region Properties
        private string _supplierCode;

        public string SupplierCode
        {
            get { return _supplierCode; }
            set { _supplierCode = value; }
        }

        private string _supplierName;

        public string SupplierName
        {
            get { return _supplierName; }
            set { _supplierName = value; }
        }

        private string _address;

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        private string _teleponeNumner;

        public string TelephoneNumber
        {
            get { return _teleponeNumner; }
            set { _teleponeNumner = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private bool _supplierCodeReadOnly;

        public bool SupplierCodeReadOnly
        {
            get { return _supplierCodeReadOnly; }
            set { _supplierCodeReadOnly = value; }
        }

        private pd_ryo_inventory_Supplier _selectedItem;

        public pd_ryo_inventory_Supplier SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Supplier> _supplierList;

        public List<pd_ryo_inventory_Supplier> SupplierList
        {
            get { return _supplierList; }
            set { _supplierList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_supplierCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier code.");
                    OnFocusRequested(nameof(SupplierCode));
                    return;
                }

                if (string.IsNullOrEmpty(_supplierName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier name.");
                    OnFocusRequested(nameof(SupplierName));
                    return;
                }

                if (string.IsNullOrEmpty(_address))
                {
                    MessageBoxHelper.Warning("โปรดระบุ address.");
                    OnFocusRequested(nameof(Address));
                    return;
                }

                if (string.IsNullOrEmpty(_teleponeNumner))
                {
                    MessageBoxHelper.Warning("โปรดระบุ telephone number.");
                    OnFocusRequested(nameof(TelephoneNumber));
                    return;
                }

                InventoryService.SupplierBL()
                    .Add(new pd_ryo_inventory_Supplier
                    {
                        SupplierCode = _supplierCode,
                        Name = _supplierName,
                        Address = _address,
                        TelephoneNo = _teleponeNumner,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                //MessageBoxHelper.Info("Add new item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_supplierCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier code.");
                    OnFocusRequested(nameof(SupplierCode));
                    return;
                }

                if (string.IsNullOrEmpty(_supplierName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier name.");
                    OnFocusRequested(nameof(SupplierName));
                    return;
                }

                if (string.IsNullOrEmpty(_address))
                {
                    MessageBoxHelper.Warning("โปรดระบุ address.");
                    OnFocusRequested(nameof(Address));
                    return;
                }

                if (string.IsNullOrEmpty(_teleponeNumner))
                {
                    MessageBoxHelper.Warning("โปรดระบุ telephone number.");
                    OnFocusRequested(nameof(TelephoneNumber));
                    return;
                }

                if (MessageBoxHelper.Question("Do you wnat to edit this item?") ==
                    MessageBoxResult.No)
                    return;

                _selectedItem.Name = _supplierName;
                _selectedItem.Address = _address;
                _selectedItem.TelephoneNo = _teleponeNumner;

                InventoryService.SupplierBL().Update(_selectedItem);
                //MessageBoxHelper.Info("Edit item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                var item = (pd_ryo_inventory_Supplier)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                InventoryService.SupplierBL().Delete(item.SupplierCode);
                //MessageBoxHelper.Info("Delete item is successfully.");
                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var item = (pd_ryo_inventory_Supplier)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                _supplierCode = item.SupplierCode;
                _supplierName = item.Name;
                _address = item.Address;
                _teleponeNumner = item.TelephoneNo;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _supplierCodeReadOnly = true;
                _selectedItem = item;

                RaisePropertyChangedEvent(nameof(SupplierCode));
                RaisePropertyChangedEvent(nameof(SupplierName));
                RaisePropertyChangedEvent(nameof(Address));
                RaisePropertyChangedEvent(nameof(TelephoneNumber));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(SupplierCodeReadOnly));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            DataGridBinding();
            ClearForm();
        }
        
        private ICommand _supplierCodeEnterCommand;

        public ICommand SupplierCodeEnterCommand
        {
            get { return _supplierCodeEnterCommand ?? (_supplierCodeEnterCommand = new RelayCommand(SupplierCodeEnter)); }
            set { _supplierCodeEnterCommand = value; }
        }

        private void SupplierCodeEnter(object obj)
        {
            OnFocusRequested(nameof(SupplierName));
        }

        private ICommand _supplierNameEnterCommand;

        public ICommand SupplierNameEnterCommand
        {
            get { return _supplierNameEnterCommand ?? (_supplierNameEnterCommand = new RelayCommand(SupplierNameEnter)); }
            set { _supplierNameEnterCommand = value; }
        }

        private void SupplierNameEnter(object obj)
        {
            OnFocusRequested(nameof(Address));
        }

        private ICommand _addressEnterCommand;

        public ICommand AddressEnterCommand
        {
            get { return _addressEnterCommand ?? (_addressEnterCommand = new RelayCommand(AddressEnter)); }
            set { _addressEnterCommand = value; }
        }

        private void AddressEnter(object obj)
        {
            OnFocusRequested(nameof(TelephoneNumber));
        }
        #endregion


        #region Function
        private void DataGridBinding()
        {
            try
            {
                _supplierList = InventoryService.SupplierBL()
                    .Get()
                    .OrderBy(x => x.SupplierCode)
                    .ToList();
                _totalRecord = _supplierList.Count();

                RaisePropertyChangedEvent(nameof(SupplierList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _supplierCode = "";
            _supplierName = "";
            _address = "";
            _teleponeNumner = "";
            _supplierCodeReadOnly = false;
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _selectedItem = null;

            RaisePropertyChangedEvent(nameof(SupplierCode));
            RaisePropertyChangedEvent(nameof(SupplierName));
            RaisePropertyChangedEvent(nameof(Address));
            RaisePropertyChangedEvent(nameof(TelephoneNumber));
            RaisePropertyChangedEvent(nameof(SupplierCodeReadOnly));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItem));

            OnFocusRequested(nameof(SupplierCode));
        }
        #endregion
    }
}
