﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureBL.Models.Inventory;
using System.Windows;
using System.Windows.Input;
using RYOMoisture.Helper;
using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Helper;
using RYOMoisture.View.Inventory.Receive;

namespace RYOMoisture.ViewModel.Inventory.Receive
{
    public class vm_ReceiveDetail : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_ReceiveDetail()
        {
            try
            {
                _receiveTypeList = ReceiveTypeHelper
                    .GetReceiveType()
                    .OrderBy(x => x.Type)
                    .ToList();
                _receiveUserList = InventoryService.EmployeeBL()
                    .Get()
                    .OrderBy(x => x.FirstName)
                    .ToList();
                RaisePropertyChangedEvent(nameof(ReceiveTypeList));
                RaisePropertyChangedEvent(nameof(ReceiveUserList));

                MasterBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }



        #region Properties
        private string _receiveCode;

        public string ReceiveCode
        {
            get { return _receiveCode; }
            set
            {
                _receiveCode = value;
                MasterBinding();
            }
        }

        private DateTime _receiveDate;

        public DateTime ReceiveDate
        {
            get { return _receiveDate; }
            set { _receiveDate = value; }
        }

        private string _receiveType;

        public string ReceiveType
        {
            get { return _receiveType; }
            set
            {
                _receiveType = value;
                _poNo = null;
                _invoiceNo = null;

                if (_receiveType == "PO" && _receiveStatus == false)
                {
                    _poNoReadOnly = false;
                    _visibleBFForm = Visibility.Collapsed;
                    OnFocusRequested(nameof(PONo));
                }
                else
                {
                    _poNoReadOnly = true;
                    _visibleBFForm = Visibility.Visible;
                    OnFocusRequested(nameof(ItemCode));
                    ResetForm();
                }

                _receiveDetailList = new List<m_ItemTransaction>();
                DetailBinding();

                RaisePropertyChangedEvent(nameof(PONo));
                RaisePropertyChangedEvent(nameof(InvoiceNo));
                RaisePropertyChangedEvent(nameof(PONoReadOnly));
                RaisePropertyChangedEvent(nameof(VisibleBFForm));
            }
        }

        private string _poNo;

        public string PONo
        {
            get { return _poNo; }
            set
            {
                _poNo = value;
                try
                {
                    if (_poNo.Length != 11)
                        return;

                    if (InventoryService.ReceiveBL().IsReceived(_poNo))
                    {
                        MessageBoxHelper.Warning("PO รหัส " + _poNo + " ถูกรับเข้าในระบบแล้ว ไม่สามารถรับซ้ำได้");
                        return;
                    }

                    //_receiveDetailList = InventoryService.PODetailsBL()
                    //        .GetByPONo(_poNo)
                    //        .Select(x => new m_ItemTransaction
                    //        {
                    //            ItemCode = x.ItemCode,
                    //            Description = x.pd_ryo_inventory_Item.Description,
                    //            Quantity = x.Quantity,
                    //            UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                    //            UnitPrice = x.UnitPrice,
                    //            Price = x.Quantity * x.UnitPrice,
                    //            LotNumber = InventoryService.ReceiveDetailsBL().GetLotNumber(_itemCode)
                    //        })
                    //        .ToList();

                    //_totalPrice = _receiveDetailList.Sum(x => x.Quantity * x.UnitPrice);
                    //RaisePropertyChangedEvent(nameof(TotalPrice));
                    OnFocusRequested(nameof(InvoiceNo));
                    DetailBinding();

                    if (_receiveDetailList.Count() <= 0)
                    {
                        MessageBoxHelper.Warning("ไม่พบข้อมูลหมายเลข PONo : " + _poNo);
                        OnFocusRequested(nameof(PONo));
                    }
                }
                catch (Exception ex)
                {
                    MessageBoxHelper.Exception(ex);
                }
            }
        }

        private string _invoiceNo;

        public string InvoiceNo
        {
            get { return _invoiceNo; }
            set { _invoiceNo = value; }
        }

        private bool _poNoReadOnly;

        public bool PONoReadOnly
        {
            get { return _poNoReadOnly; }
            set { _poNoReadOnly = value; }
        }

        private decimal _totalPrice;

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }

        private string _receiveUser;

        public string ReceiveUser
        {
            get { return _receiveUser; }
            set { _receiveUser = value; }
        }

        private string _receiveDescription;

        public string ReceiveDescription
        {
            get { return _receiveDescription; }
            set { _receiveDescription = value; }
        }

        private bool _receiveStatus;

        public bool ReceiveStatus
        {
            get { return _receiveStatus; }
            set { _receiveStatus = value; }
        }

        private bool _canEdit;

        public bool CanEdit
        {
            get { return _canEdit; }
            set
            {
                _canEdit = value;
                if (_canEdit == true)
                    _visibleBFForm = Visibility.Visible;
                else
                    _visibleBFForm = Visibility.Collapsed;

                RaisePropertyChangedEvent(nameof(_visibleBFForm));
            }
        }

        private Visibility _visibleBFForm;

        public Visibility VisibleBFForm
        {
            get { return _visibleBFForm; }
            set { _visibleBFForm = value; }
        }

        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _itemDescription;

        public string ItemDescription
        {
            get { return _itemDescription; }
            set { _itemDescription = value; }
        }

        private int _onhand;

        public int Onhand
        {
            get { return _onhand; }
            set { _onhand = value; }
        }

        private string _unitCode;

        public string UnitCode
        {
            get { return _unitCode; }
            set { _unitCode = value; }
        }

        private string _unitName;

        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        private decimal _unitPrice;

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set
            {
                _unitPrice = value;
                _price = _quantity * _unitPrice;
                RaisePropertyChangedEvent(nameof(Price));
            }
        }

        private int _quantity;

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                _price = _quantity * _unitPrice;
                RaisePropertyChangedEvent(nameof(Price));
            }
        }

        private decimal _price;

        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private ReceiveDetail _window;

        public ReceiveDetail Window
        {
            get { return _window; }
            set { _window = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Employee> _receiveUserList;

        public List<pd_ryo_inventory_Employee> ReceiveUserList
        {
            get { return _receiveUserList; }
            set { _receiveUserList = value; }
        }

        private List<m_ReceiveType> _receiveTypeList;

        public List<m_ReceiveType> ReceiveTypeList
        {
            get { return _receiveTypeList; }
            set { _receiveTypeList = value; }
        }

        private List<m_ItemTransaction> _itemList;

        public List<m_ItemTransaction> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<m_ItemTransaction> _receiveDetailList;

        public List<m_ItemTransaction> ReceiveDetailList
        {
            get
            {
                if (_receiveDetailList == null)
                    _receiveDetailList = new List<m_ItemTransaction>();
                return _receiveDetailList;
            }
            set { _receiveDetailList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Item code.");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (_quantity <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนสินค้าจะต้องมากกว่า 0");
                    OnFocusRequested(nameof(Quantity));
                    return;
                }

                if (_unitPrice <= 0)
                {
                    MessageBoxHelper.Warning("Unit price จะต้องมากกว่า 0");
                    OnFocusRequested(nameof(UnitPrice));
                    return;
                }

                if (_receiveDetailList != null)
                    if (_receiveDetailList.Where(x => x.ItemCode == _itemCode).Count() > 0)
                        throw new ArgumentException("รายการสินค้านี้ถูกเพิ่มเข้าไปแล้ว โปรดเลือกสินค้าตัวอื่น");

                ///Add fucntion.
                ///
                var lotNo = InventoryService.ReceiveDetailsBL().GetLotNumber(_itemCode);
                _receiveDetailList
                    .Add(new m_ItemTransaction
                    {
                        ItemCode = _itemCode,
                        Description = _itemDescription,
                        Unit = _unitCode,
                        UnitName = _unitName,
                        Quantity = _quantity,
                        UnitPrice = _unitPrice,
                        Price = _quantity * _unitPrice,
                        LotNumber = lotNo
                    });

                _receiveDetailList = _receiveDetailList.ToList();
                ResetForm();
                DetailBinding();
                OnFocusRequested(nameof(ItemCode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            ResetForm();
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_receiveType))
                {
                    MessageBoxHelper.Warning("โปรดระบุประเภทการรับเข้า");
                    OnFocusRequested(nameof(ReceiveType));
                    return;
                }

                if (string.IsNullOrEmpty(_receiveCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ receive code");
                    OnFocusRequested(nameof(ReceiveCode));
                    return;
                }

                if (string.IsNullOrEmpty(_receiveUser))
                {
                    MessageBoxHelper.Warning("โปรดระบุ receive user");
                    OnFocusRequested(nameof(ReceiveUser));
                    return;
                }

                if (_receiveType == "PO" && string.IsNullOrEmpty(_poNo))
                {
                    MessageBoxHelper.Warning("ถ้าเป็นการรับจากใบสั่งซื้อ โปรดระบุหมายเลข PONo ด้วย");
                    OnFocusRequested(nameof(PONo));
                    return;
                }

                if (_receiveType == "PO" && string.IsNullOrEmpty(_invoiceNo))
                    if (MessageBoxHelper.Question("คุณไม่ต้องการระบุ invoice no ใช่หรือไม่?") == MessageBoxResult.No)
                        return;

                if (_receiveDetailList.Count() <= 0)
                    throw new ArgumentException("โปรดใส่รายการสินค้าที่จะรับเข้าระบบอย่างน้อย 1 รายการ");

                if (_receiveStatus == false)
                {
                    if (MessageBoxHelper.Question("หากบันทึกข้อมูลแล้ว จะไม่สามารถแก้ไขย้อนหลังได้อีก " +
                        "คุณต้องการรับสินค้าตามข้อมูลด้านล่างนี้ใช่หรือไม่?") == MessageBoxResult.No)
                        return;

                    InventoryService.ReceiveBL()
                        .Add(new pd_ryo_inventory_Receive
                        {
                            ReceiveCode = _receiveCode,
                            ReceiveType = _receiveType,
                            ReceiveDate = _receiveDate,
                            ReceiveUser = _receiveUser,
                            ReceiveStatus = true,
                            PONo = _poNo,
                            InvoiceNo = _invoiceNo,
                            Description = _receiveDescription,
                            ModifiedBy = user_setting.User.Username
                        });

                    foreach (var item in _receiveDetailList)
                    {
                        InventoryService.ReceiveDetailsBL()
                            .Add(new pd_ryo_inventory_ReceiveDetail
                            {
                                ReceiveDetailID = Guid.NewGuid(),
                                ReceiveCode = _receiveCode,
                                ItemCode = item.ItemCode,
                                UnitPrice = item.UnitPrice,
                                Quantity = item.Quantity,
                                LotNo = item.LotNumber,
                                ModifiedBy = user_setting.User.Username,
                                ModifiedDate = DateTime.Now
                            });
                    }
                }
                else
                {
                    InventoryService.ReceiveBL()
                        .Edit(new pd_ryo_inventory_Receive
                        {
                            ReceiveCode = _receiveCode,
                            ReceiveDate = _receiveDate,
                            ReceiveUser = _receiveUser,
                            PONo = _poNo,
                            InvoiceNo = _invoiceNo,
                            Description = _receiveDescription,
                            ModifiedBy = user_setting.User.Username
                        });
                }
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");

                MasterBinding();
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (_canEdit == false)
                {
                    MessageBoxHelper.Warning("มีการรับสินค้าเข้าระบบแล้ว ไม่สามารถลบข้อมูลได้");
                    return;
                }

                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var item = (m_ItemTransaction)obj;
                if (item == null)
                    return;

                _receiveDetailList.Remove(item);
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onMouseClickCommand;

        public ICommand OnMouseClickCommand
        {
            get { return _onMouseClickCommand ?? (_onMouseClickCommand = new RelayCommand(OnMouseClick)); }
            set { _onMouseClickCommand = value; }
        }

        private void OnMouseClick(object obj)
        {
            try
            {
                var window = new View.Inventory.Shared.ItemList();
                var vm = new Shared.vm_ItemList();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                if (vm.SelectedItem == null)
                    return;

                ItemDetailBinding(vm.SelectedItem);
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnitPriceGotFocusCommand;

        public ICommand OnUnitPriceGotFocusCommand
        {
            get { return _onUnitPriceGotFocusCommand ?? (_onUnitPriceGotFocusCommand = new RelayCommand(UnitPriceGotFocus)); }
            set { _onUnitPriceGotFocusCommand = value; }
        }

        private void UnitPriceGotFocus(object obj)
        {
            OnFocusRequested(nameof(UnitPrice));
        }

        private ICommand _quantityOnEnterCommand;

        public ICommand QuantityOnEnterCommand
        {
            get { return _quantityOnEnterCommand ?? (_quantityOnEnterCommand = new RelayCommand(QuantityEnterCommand)); }
            set { _quantityOnEnterCommand = value; }
        }

        private void QuantityEnterCommand(object obj)
        {
            OnFocusRequested(nameof(UnitPrice));
        }

        #endregion


        #region Function
        private void MasterBinding()
        {
            try
            {
                if (_receiveCode == null)
                {
                    _receiveCode = InventoryService.ReceiveBL().GetNewReceiveCode();
                    _receiveDate = DateTime.Now;
                    _receiveStatus = false;
                    _canEdit = true;
                    //_receiveType = "PO";

                    RaisePropertyChangedEvent(nameof(ReceiveCode));
                    RaisePropertyChangedEvent(nameof(ReceiveDate));
                    RaisePropertyChangedEvent(nameof(ReceiveStatus));
                    RaisePropertyChangedEvent(nameof(CanEdit));
                    RaisePropertyChangedEvent(nameof(ReceiveType));
                    OnFocusRequested(nameof(PONo));
                }
                else
                {
                    var master = InventoryService.ReceiveBL().GetSingle(_receiveCode);
                    if (master == null)
                        throw new ArgumentException("ไม่พบ receive code หมายเลข #" + _receiveCode + " โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                    _receiveType = master.ReceiveType;
                    _receiveCode = master.ReceiveCode;
                    _receiveDate = master.ReceiveDate;
                    _poNo = master.PONo;
                    _invoiceNo = master.InvoiceNo;
                    _receiveUser = master.ReceiveUser;
                    _receiveDescription = master.Description;
                    _receiveStatus = master.ReceiveStatus;
                    _canEdit = !master.ReceiveStatus;

                    RaisePropertyChangedEvent(nameof(ReceiveType));
                    RaisePropertyChangedEvent(nameof(ReceiveCode));
                    RaisePropertyChangedEvent(nameof(ReceiveDate));
                    RaisePropertyChangedEvent(nameof(PONo));
                    RaisePropertyChangedEvent(nameof(InvoiceNo));
                    RaisePropertyChangedEvent(nameof(ReceiveUser));
                    RaisePropertyChangedEvent(nameof(ReceiveDescription));
                    RaisePropertyChangedEvent(nameof(ReceiveStatus));
                    RaisePropertyChangedEvent(nameof(CanEdit));

                    DetailBinding();
                }

                if (_receiveType == "PO")
                    _visibleBFForm = Visibility.Collapsed;
                else
                    _visibleBFForm = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(VisibleBFForm));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailBinding()
        {
            try
            {
                if (_receiveStatus == true)
                {
                    ///แสดงข้อมูลจาก receive detail.
                    _receiveDetailList = ReceiveDetailHelper
                        .GetByReceiveCode(_receiveCode);
                }
                else
                {
                    if (!string.IsNullOrEmpty(_poNo) && _receiveType == "PO")
                    {
                        ///ดึงข้อมูลจาก PO detail มาแสดงพร้อมทั้งหา Lot Number ล่าสุดให้กับแต่ละ item
                        var list = InventoryService.PODetailsBL().GetByPONo(_poNo);
                        var lotNumber = "";
                        foreach (var item in list)
                        {
                            lotNumber = InventoryService.ReceiveDetailsBL().GetLotNumber(item.ItemCode);
                            _receiveDetailList.Add(new m_ItemTransaction
                            {
                                ItemCode = item.ItemCode,
                                Description = item.pd_ryo_inventory_Item.Description,
                                Unit = item.pd_ryo_inventory_Item.Unit,
                                UnitName = item.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                                Quantity = item.Quantity,
                                UnitPrice = item.UnitPrice,
                                Price = item.Quantity * item.UnitPrice,
                                LotNumber = lotNumber
                            });
                        }
                    }
                }
                _receiveDetailList = _receiveDetailList.ToList();
                _totalPrice = _receiveDetailList.Sum(x => x.Quantity * x.UnitPrice);
                RaisePropertyChangedEvent(nameof(ReceiveDetailList));
                RaisePropertyChangedEvent(nameof(TotalPrice));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ResetForm()
        {
            _itemCode = "";
            _itemDescription = "";
            _onhand = 0;
            _unitName = "";
            _quantity = 0;
            _unitPrice = 0;
            _price = 0;

            RaisePropertyChangedEvent(nameof(ItemCode));
            RaisePropertyChangedEvent(nameof(ItemDescription));
            RaisePropertyChangedEvent(nameof(Onhand));
            RaisePropertyChangedEvent(nameof(UnitName));
            RaisePropertyChangedEvent(nameof(Quantity));
            RaisePropertyChangedEvent(nameof(UnitPrice));
            RaisePropertyChangedEvent(nameof(Price));
        }

        private void ItemDetailBinding(pd_ryo_inventory_Item item)
        {
            try
            {
                if (item == null)
                    return;

                _itemCode = item.ItemCode;
                _itemDescription = item.Description;
                _unitCode = item.Unit;
                _unitName = item.pd_ryo_inventory_Unit.UnitName;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(ItemDescription));
                RaisePropertyChangedEvent(nameof(UnitCode));
                RaisePropertyChangedEvent(nameof(UnitName));
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
