﻿using RYOMoisture.Helper;
using RYOMoisture.MVVM;
using RYOMoistureBL.InventoryBL;
using RYOMoistureBL.Models.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace RYOMoisture.ViewModel.Inventory.Report
{
    public class vm_BalanceStock : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_BalanceStock()
        {
            ItemListBinding();
            DataGridBinding();
        }



        #region Properties
        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set
            {
                _itemCode = value;
                DataGridBinding();
            }
        }

        private long _totalQuantity;

        public long TotalQuantity
        {
            get { return _totalQuantity; }
            set { _totalQuantity = value; }
        }

        private decimal _totalPrice;

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }

        private int _totalItem;

        public int TotalItem
        {
            get { return _totalItem; }
            set { _totalItem = value; }
        }
        #endregion



        #region List
        private List<m_ItemTransaction> _itemList;

        public List<m_ItemTransaction> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<m_ItemTransaction> _reportList;

        public List<m_ItemTransaction> ReportList
        {
            get
            {
                if (_reportList == null)
                    _reportList = new List<m_ItemTransaction>();
                return _reportList;
            }
            set { _reportList = value; }
        }
        #endregion




        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(Refresh)); }
            set { _onRefreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            ItemListBinding();
            DataGridBinding();
        }
        #endregion




        #region Function
        private void ItemListBinding()
        {
            try
            {
                _itemList = InventoryService.ReportBL()
                    .GetBalanceStock()
                    .OrderBy(x => x.ItemCode)
                    .ToList();
                RaisePropertyChangedEvent(nameof(ItemList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                if (_reportList == null)
                    _reportList = new List<m_ItemTransaction>();

                if (!string.IsNullOrEmpty(_itemCode))
                    _reportList = _itemList
                        .Where(x => x.ItemCode == _itemCode).ToList();
                else
                    _reportList = _itemList;

                _totalItem = _reportList.Count();
                _totalQuantity = _reportList.Sum(x => x.Quantity);
                _totalPrice = _reportList.Sum(x => x.Price);

                RaisePropertyChangedEvent(nameof(TotalItem));
                RaisePropertyChangedEvent(nameof(TotalQuantity));
                RaisePropertyChangedEvent(nameof(TotalPrice));

                _reportList.Add(new m_ItemTransaction
                {
                    Quantity = _reportList.Sum(x => x.Quantity),
                    Price = _reportList.Sum(x => x.Price)
                });
                RaisePropertyChangedEvent(nameof(ReportList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
