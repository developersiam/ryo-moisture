﻿using RYOMoisture.MVVM;
using RYOMoistureBL.Models.Inventory;
using DomainModelStecDbms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RYOMoistureBL.InventoryBL;
using RYOMoisture.Helper;
using RYOMoistureBL.Helper;

namespace RYOMoisture.ViewModel.Inventory.Report
{
    public class vm_BalanceScoreCard : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_BalanceScoreCard()
        {
            ItemListBinding();
            DataGridBinding();
        }



        #region Properties
        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set
            {
                _itemCode = value;

                if (string.IsNullOrEmpty(_itemCode))
                    return;

                var item = _itemList.SingleOrDefault(x => x.ItemCode == _itemCode);

                _description = item.Description;
                _unitName = item.pd_ryo_inventory_Unit.UnitName;
                _categoryName = item.pd_ryo_inventory_ItemCategory.ItemCategoryName;

                RaisePropertyChangedEvent(nameof(Description));
                RaisePropertyChangedEvent(nameof(UnitName));
                RaisePropertyChangedEvent(nameof(CategoryName));

                LotNumberBinding();
                DataGridBinding();
            }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _unitName;

        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        private string _categoryName;

        public string CategoryName
        {
            get { return _categoryName; }
            set { _categoryName = value; }
        }

        private string _lotNumber;

        public string LotNumber
        {
            get { return _lotNumber; }
            set
            {
                _lotNumber = value;
                DataGridBinding();
            }
        }

        private int _totalQuantity;

        public int TotalQuantity
        {
            get { return _totalQuantity; }
            set { _totalQuantity = value; }
        }

        private decimal _totalPrice;

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }

        private int _totalItem;

        public int TotalItem
        {
            get { return _totalItem; }
            set { _totalItem = value; }
        }
        #endregion



        #region List
        private List<pd_ryo_inventory_Item> _itemList;

        public List<pd_ryo_inventory_Item> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        private List<m_ItemTransaction> _lotNumberList;

        public List<m_ItemTransaction> LotNumberList
        {
            get { return _lotNumberList; }
            set { _lotNumberList = value; }
        }

        private List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result> _reportList;

        public List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result> ReportList
        {
            get
            {
                if (_reportList == null)
                    _reportList = new List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result>();
                return _reportList;
            }
            set { _reportList = value; }
        }
        #endregion




        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(Refresh)); }
            set { _onRefreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            DataGridBinding();
        }
        #endregion




        #region Function
        private void ItemListBinding()
        {
            try
            {
                _itemList = InventoryService.ItemBL()
                    .Get()
                    .OrderBy(x => x.ItemCode)
                    .ToList();
                RaisePropertyChangedEvent(nameof(ItemList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LotNumberBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                    return;

                if (_lotNumberList == null)
                    _lotNumberList = new List<m_ItemTransaction>();

                _lotNumberList = ItemHelper.GetByOnHand()
                    .Where(x => x.ItemCode == _itemCode)
                    .Select(x => new m_ItemTransaction
                    {
                        LotNumber = x.LotNumber
                    })
                .ToList();
                RaisePropertyChangedEvent(nameof(LotNumberList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode) || string.IsNullOrEmpty(_lotNumber))
                {
                    _reportList = new List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result>();
                    RaisePropertyChangedEvent(nameof(ReportList));
                    return;
                }

                if (_reportList == null)
                    _reportList = new List<sp_pd_ryo_inv_SEL_BalanceScoreCard_Result>();

                _reportList = InventoryService.ReportBL()
                    .GetBalanceScoreCard(_itemCode, _lotNumber)
                    .OrderBy(x => Convert.ToDateTime(x.TransDate).Date)
                    .ThenBy(x => Convert.ToDateTime(x.TransDate).TimeOfDay)
                    .ToList();

                _totalItem = _reportList.Count();
                _totalQuantity = Convert.ToInt32(_reportList.Sum(x => x.BalanceQty));
                _totalPrice = Convert.ToDecimal(_reportList.Sum(x => x.BalanceValue));

                RaisePropertyChangedEvent(nameof(TotalItem));
                RaisePropertyChangedEvent(nameof(TotalQuantity));
                RaisePropertyChangedEvent(nameof(TotalPrice));

                ///Add summary value to a buttom row.
                ///
                _reportList
                    .Add(new sp_pd_ryo_inv_SEL_BalanceScoreCard_Result
                    {
                        BalanceQty = _reportList.Sum(x => x.BalanceQty),
                        BalanceValue = _reportList.Sum(x => x.BalanceValue)
                    });

                RaisePropertyChangedEvent(nameof(ReportList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
