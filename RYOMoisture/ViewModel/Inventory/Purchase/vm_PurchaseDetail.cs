﻿using RYOMoisture.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModelStecDbms;
using RYOMoistureBL.Models.Inventory;
using System.Windows.Input;
using RYOMoisture.Helper;
using RYOMoistureBL.InventoryBL;
using RYOMoisture.View.Inventory.Purchase;
using System.Collections.ObjectModel;

namespace RYOMoisture.ViewModel.Inventory.Purchase
{
    public class vm_PurchaseOrderDetail : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_PurchaseOrderDetail()
        {
            try
            {
                _supplierList = InventoryService.SupplierBL()
                    .Get()
                    .OrderBy(x => x.Name)
                    .ToList();
                _employeeList = InventoryService.EmployeeBL()
                    .Get()
                    .OrderBy(x => x.FirstName)
                    .ToList();
                _faxDate = DateTime.Now;
                _deliveryDate = DateTime.Now.AddDays(7);
                _requestedDate = DateTime.Now;

                RaisePropertyChangedEvent(nameof(SupplierList));
                RaisePropertyChangedEvent(nameof(EmployeeList));
                RaisePropertyChangedEvent(nameof(FaxDate));
                RaisePropertyChangedEvent(nameof(DeliveryDate));
                RaisePropertyChangedEvent(nameof(RequestedDate));

                MasterBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #region Properties
        private string _pono;

        public string PONo
        {
            get { return _pono; }
            set
            {
                _pono = value;
                MasterBinding();
            }
        }

        private string _supplierCode;

        public string SupplierCode
        {
            get { return _supplierCode; }
            set { _supplierCode = value; }
        }

        private string _contractPerson;

        public string ContractPerson
        {
            get { return _contractPerson; }
            set { _contractPerson = value; }
        }

        private string _poDescription;

        public string PODescription
        {
            get { return _poDescription; }
            set { _poDescription = value; }
        }

        private string _requestPerson;

        public string RequestPerson
        {
            get { return _requestPerson; }
            set { _requestPerson = value; }
        }

        private string _quotaionRefNo;

        public string QuotationRefNo
        {
            get { return _quotaionRefNo; }
            set { _quotaionRefNo = value; }
        }

        private string _paymentTerm;

        public string PaymentTerm
        {
            get { return _paymentTerm; }
            set { _paymentTerm = value; }
        }

        private DateTime _faxDate;

        public DateTime FaxDate
        {
            get { return _faxDate; }
            set { _faxDate = value; }
        }

        private DateTime _deliveryDate;

        public DateTime DeliveryDate
        {
            get { return _deliveryDate; }
            set
            {
                if (_deliveryDate <= _requestedDate)
                    throw new ArgumentException("Delivery date should be after requested date.");

                _deliveryDate = value;
            }
        }

        private DateTime _requestedDate;

        public DateTime RequestedDate
        {
            get { return _requestedDate; }
            set
            {
                if (_requestedDate >= _deliveryDate)
                    throw new ArgumentException("Requested date should be before delivery date.");

                _requestedDate = value;
            }
        }

        private bool _poStatus;

        public bool POStatus
        {
            get { return _poStatus; }
            set { _poStatus = value; }
        }

        private bool _canEdit;

        public bool CanEdit
        {
            get { return _canEdit; }
            set { _canEdit = value; }
        }

        private string _itemCode;

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }

        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private string _unitCode;

        public string UnitCode
        {
            get { return _unitCode; }
            set { _unitCode = value; }
        }

        private string _unitName;

        public string UnitName
        {
            get { return _unitName; }
            set { _unitName = value; }
        }

        private decimal _unitPrice;

        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set
            {
                _unitPrice = value;
                _price = _quantity * _unitPrice;
                RaisePropertyChangedEvent(nameof(Price));
            }
        }

        private int _quantity;

        public int Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;
                _price = _quantity * _unitPrice;
                RaisePropertyChangedEvent(nameof(Price));
            }
        }

        private decimal _price;

        public decimal Price
        {
            get { return _price; }
            set { _price = value; }
        }

        private decimal _amount;

        public decimal Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        private decimal _vat;

        public decimal VAT
        {
            get { return _vat; }
            set { _vat = value; }
        }

        private decimal _totalAmount;

        public decimal TotalAmount
        {
            get { return _totalAmount; }
            set { _totalAmount = value; }
        }

        private PurchaseOrederDetail _window;

        public PurchaseOrederDetail Window
        {
            get { return _window; }
            set { _window = value; }
        }
        #endregion



        #region List
        private List<m_ItemTransaction> _poDetailList;

        public List<m_ItemTransaction> PODetailList
        {
            get
            {
                if (_poDetailList == null)
                    _poDetailList = new List<m_ItemTransaction>();

                return _poDetailList;
            }
            set { _poDetailList = value; }
        }

        private List<pd_ryo_inventory_Supplier> _supplierList;

        public List<pd_ryo_inventory_Supplier> SupplierList
        {
            get { return _supplierList; }
            set { _supplierList = value; }
        }

        private List<pd_ryo_inventory_Employee> _employeeList;

        public List<pd_ryo_inventory_Employee> EmployeeList
        {
            get { return _employeeList; }
            set { _employeeList = value; }
        }

        private List<m_ItemTransaction> _itemList;

        public List<m_ItemTransaction> ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }

        public ObservableCollection<string> ItemCodeObservableList
        {
            get
            {
                var list = new ObservableCollection<string>();

                foreach (var item in _itemList)
                    list.Add(item.ItemCode);

                return list;
            }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_itemCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Item code.");
                    OnFocusRequested(nameof(ItemCode));
                    return;
                }

                if (_quantity <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนสินค้าจะต้องมากกว่า 0");
                    OnFocusRequested(nameof(Quantity));
                    return;
                }

                if (_unitPrice <= 0)
                {
                    MessageBoxHelper.Warning("Unit price จะต้องมากกว่า 0");
                    OnFocusRequested(nameof(UnitPrice));
                    return;
                }

                if (_poDetailList != null)
                    if (_poDetailList.Where(x => x.ItemCode == _itemCode).Count() > 0)
                        throw new ArgumentException("มีการเพิ่มรายการสินค้านี้แล้ว โปรดเลือกรายการอื่น");

                ///Add fucntion.
                ///
                _poDetailList.Add(new m_ItemTransaction
                {
                    ItemCode = _itemCode,
                    Description = _description,
                    Unit = _unitCode,
                    UnitName = _unitName,
                    Quantity = _quantity,
                    UnitPrice = _unitPrice,
                    Price = _quantity * _unitPrice
                });

                _poDetailList = _poDetailList.ToList();
                ResetForm();
                DetailBinding();
                OnFocusRequested(nameof(ItemCode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            ResetForm();
        }

        private ICommand _onPreviewCommand;

        public ICommand OnPreviewCommand
        {
            get { return _onPreviewCommand ?? (_onPreviewCommand = new RelayCommand(OnPreview)); }
            set { _onPreviewCommand = value; }
        }

        private void OnPreview(object obj)
        {
            try
            {
                var po = InventoryService.POBL().GetSingle(_pono);
                if (po == null)
                    throw new ArgumentException("โปรดบันทึกข้อมูลใบสั่งซื้อก่อนจึงจะสามารถดูเอกสารนี้ในรูปแบบ excel ได้ กดปุ่ม Save เพื่อบันทึกข้อมูล");

                ExcelHelper.Open(@"C:\RYO\Template\RYOPO.xlsx");
                ExcelHelper.PutDataToCell("D7", po.pd_ryo_inventory_Supplier.Name);
                ExcelHelper.PutDataToCell("D8", _supplierCode);
                ExcelHelper.PutDataToCell("D9", po.ContractPerson);
                ExcelHelper.PutDataToCell("S7", _pono);
                ExcelHelper.PutDataToCell("O8", po.CreateDate.Day.ToString().PadLeft(2, '0') +
                    "/" + po.CreateDate.Month.ToString().PadLeft(2, '0') +
                    "/" + po.CreateDate.Year);
                ExcelHelper.PutDataToCell("S9", _quotaionRefNo);

                int rowNumber = 1;
                int startRow = 12;
                foreach (var item in _poDetailList)
                {
                    ExcelHelper.PutDataToCell("B" + startRow, rowNumber.ToString());
                    ExcelHelper.PutDataToCell("C" + startRow, item.Quantity.ToString("N0"));
                    ExcelHelper.PutDataToCell("E" + startRow, item.Description);
                    ExcelHelper.PutDataToCell("S" + startRow, item.UnitPrice.ToString("N4"));
                    ExcelHelper.PutDataToCell("T" + startRow, (item.Quantity * item.UnitPrice).ToString("N4"));

                    rowNumber++;
                    startRow++;
                }

                ExcelHelper.PutDataToCell("D37", _faxDate.Day.ToString().PadLeft(2, '0') +
                    "/" + _faxDate.Month.ToString().PadLeft(2, '0') +
                    "/" + _faxDate.Year);
                ExcelHelper.PutDataToCell("D38", _deliveryDate.Day.ToString().PadLeft(2, '0') +
                    "/" + _deliveryDate.Month.ToString().PadLeft(2, '0') +
                    "/" + _deliveryDate.Year);
                ExcelHelper.PutDataToCell("D39", _requestedDate.Day.ToString().PadLeft(2, '0') +
                    "/" + _requestedDate.Month.ToString().PadLeft(2, '0') +
                    "/" + _requestedDate.Year);
                ExcelHelper.PutDataToCell("D40", _paymentTerm);
                ExcelHelper.PutDataToCell("T37", _amount.ToString("N4"));
                ExcelHelper.PutDataToCell("T38", _vat.ToString("N4"));
                ExcelHelper.PutDataToCell("T39", _totalAmount.ToString("N4"));
                ExcelHelper.Quit();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_supplierCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier");
                    OnFocusRequested(nameof(SupplierCode));
                    return;
                }

                if (string.IsNullOrEmpty(_contractPerson))
                {
                    MessageBoxHelper.Warning("โปรดระบุ contract person");
                    OnFocusRequested(nameof(ContractPerson));
                    return;
                }

                if (string.IsNullOrEmpty(_pono))
                {
                    MessageBoxHelper.Warning("โปรดระบุ PONo");
                    OnFocusRequested(nameof(PONo));
                    return;
                }

                if (_poDetailList.Count() <= 0)
                    throw new ArgumentException("โปรดเพิ่มรายการสินค้าใดก็ได้อย่างน้อย 1 รายการ");

                if (MessageBoxHelper.Question("คุณต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") ==
                    System.Windows.MessageBoxResult.No)
                    return;

                if (_poStatus == false)
                {
                    InventoryService.POBL()
                        .Add(new pd_ryo_inventory_PO
                        {
                            SupplierCode = _supplierCode,
                            ContractPerson = _contractPerson,
                            Description = _poDescription,
                            PONo = _pono,
                            QuotationRefNo = _quotaionRefNo,
                            PaymentTerm = _paymentTerm,
                            FaxDate = _faxDate,
                            DeliveryDate = _deliveryDate,
                            RequestedDate = _requestedDate,
                            POStatus = true,
                            ModifiedBy = user_setting.User.Username
                        });

                    foreach (var item in _poDetailList)
                    {
                        InventoryService.PODetailsBL()
                            .Add(new pd_ryo_inventory_PODetail
                            {
                                PODetailsID = Guid.NewGuid(),
                                PONo = _pono,
                                ItemCode = item.ItemCode,
                                UnitPrice = item.UnitPrice,
                                Quantity = item.Quantity,
                                ModifiedBy = user_setting.User.Username,
                                ModifiedDate = DateTime.Now
                            });
                    }
                    MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                }
                else
                {
                    InventoryService.POBL()
                        .Edit(new pd_ryo_inventory_PO
                        {
                            SupplierCode = _supplierCode,
                            ContractPerson = _contractPerson,
                            Description = _poDescription,
                            PONo = _pono,
                            QuotationRefNo = _quotaionRefNo,
                            PaymentTerm = _paymentTerm,
                            FaxDate = _faxDate,
                            DeliveryDate = _deliveryDate,
                            RequestedDate = _requestedDate,
                            ModifiedBy = user_setting.User.Username
                        });
                    MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                }

                MasterBinding();
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (_canEdit == false)
                {
                    MessageBoxHelper.Warning("มีการรับสินค้าเข้าระบบแล้ว ไม่สามารถลบข้อมูลได้");
                    return;
                }

                if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") ==
                    System.Windows.MessageBoxResult.No)
                    return;

                ///Delete item from this PO.
                ///
                _poDetailList.Remove((m_ItemTransaction)obj);
                _poDetailList = _poDetailList.ToList();
                DetailBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onMouseClickCommand;

        public ICommand OnMouseClickCommand
        {
            get { return _onMouseClickCommand ?? (_onMouseClickCommand = new RelayCommand(OnMouseClick)); }
            set { _onMouseClickCommand = value; }
        }

        private void OnMouseClick(object obj)
        {
            try
            {
                var window = new View.Inventory.Shared.ItemList();
                var vm = new Shared.vm_ItemList();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                if (vm.SelectedItem == null)
                    return;

                ItemDetailBinding(vm.SelectedItem);
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void MasterBinding()
        {
            try
            {
                if (_pono == null)
                {
                    _pono = InventoryService.POBL().GetNewPONo();
                    _poStatus = false;
                    _canEdit = true;
                    RaisePropertyChangedEvent(nameof(PONo));
                    RaisePropertyChangedEvent(nameof(POStatus));
                    RaisePropertyChangedEvent(nameof(CanEdit));
                }
                else
                {
                    var master = InventoryService.POBL().GetSingle(_pono);

                    _supplierCode = master.SupplierCode;
                    _contractPerson = master.ContractPerson;
                    _poDescription = master.Description;
                    _pono = master.PONo;
                    _quotaionRefNo = master.QuotationRefNo;
                    _paymentTerm = master.PaymentTerm;
                    _faxDate = master.FaxDate;
                    _deliveryDate = master.DeliveryDate;
                    _requestedDate = master.RequestedDate;
                    _poStatus = master.POStatus;
                    _canEdit = !master.POStatus;

                    RaisePropertyChangedEvent(nameof(SupplierCode));
                    RaisePropertyChangedEvent(nameof(ContractPerson));
                    RaisePropertyChangedEvent(nameof(PODescription));
                    RaisePropertyChangedEvent(nameof(PONo));
                    RaisePropertyChangedEvent(nameof(QuotationRefNo));
                    RaisePropertyChangedEvent(nameof(PaymentTerm));
                    RaisePropertyChangedEvent(nameof(FaxDate));
                    RaisePropertyChangedEvent(nameof(DeliveryDate));
                    RaisePropertyChangedEvent(nameof(RequestedDate));
                    RaisePropertyChangedEvent(nameof(POStatus));
                    RaisePropertyChangedEvent(nameof(CanEdit));

                    DetailBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailBinding()
        {
            try
            {
                if (_poStatus == true)
                    _poDetailList = InventoryService.PODetailsBL()
                        .GetByPONo(_pono)
                        .Select(x => new m_ItemTransaction
                        {
                            ItemCode = x.ItemCode,
                            Description = x.pd_ryo_inventory_Item.Description,
                            Quantity = x.Quantity,
                            UnitName = x.pd_ryo_inventory_Item.pd_ryo_inventory_Unit.UnitName,
                            UnitPrice = x.UnitPrice,
                            Price = x.Quantity * x.UnitPrice
                        })
                        .ToList();

                _vat = _poDetailList.Sum(x => x.Quantity * x.UnitPrice) * Convert.ToDecimal(0.07);
                _amount = _poDetailList.Sum(x => x.Quantity * x.UnitPrice);
                _totalAmount = _poDetailList.Sum(x => x.Quantity * x.UnitPrice) + _vat;

                RaisePropertyChangedEvent(nameof(VAT));
                RaisePropertyChangedEvent(nameof(Amount));
                RaisePropertyChangedEvent(nameof(TotalAmount));
                RaisePropertyChangedEvent(nameof(PODetailList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ResetForm()
        {
            _itemCode = "";
            _quantity = 0;
            _unitPrice = 0;
            _price = 0;
            _description = "";
            _unitName = "";

            RaisePropertyChangedEvent(nameof(ItemCode));
            RaisePropertyChangedEvent(nameof(Quantity));
            RaisePropertyChangedEvent(nameof(UnitPrice));
            RaisePropertyChangedEvent(nameof(Price));
            RaisePropertyChangedEvent(nameof(Description));
            RaisePropertyChangedEvent(nameof(UnitName));
        }

        private void ItemDetailBinding(pd_ryo_inventory_Item item)
        {
            try
            {
                if (item == null)
                    return;

                _itemCode = item.ItemCode;
                _description = item.Description;
                _unitCode = item.Unit;
                _unitName = item.pd_ryo_inventory_Unit.UnitName;

                RaisePropertyChangedEvent(nameof(ItemCode));
                RaisePropertyChangedEvent(nameof(Description));
                RaisePropertyChangedEvent(nameof(UnitCode));
                RaisePropertyChangedEvent(nameof(UnitName));
                OnFocusRequested(nameof(Quantity));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
