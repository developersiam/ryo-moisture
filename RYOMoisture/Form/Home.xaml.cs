﻿using DomainModelStecDbms;
using Microsoft.Reporting.WinForms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.Form
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();

            //CropComboBox.ItemsSource = null;
            //CropComboBox.ItemsSource = user_setting.Crops;
            //CropComboBox.SelectedValue = DateTime.Now.Year;

            //CustomerComboBox.ItemsSource = null;
            //CustomerComboBox.ItemsSource = BusinessLayerServices.customerBL()
            //    .Get()
            //    .OrderBy(x => x.code);
        }

        private void ReportBinding()
        {
            try
            {
                //if (CropComboBox.SelectedIndex < 0)
                //    return;

                //if (CustomerComboBox.SelectedIndex < 0)
                //    return;

                //if (CropComboBox.SelectedValue == null)
                //    return;

                //if (CustomerComboBox.SelectedValue == null)
                //    return;

                //List<pd> pdList = new List<pd>();
                //pdList = BusinessLayerServices.ProductBL()
                //    .GetPdInfoByCustomer(Convert.ToInt16(CropComboBox.SelectedValue.ToString()), 
                //    CustomerComboBox.SelectedValue.ToString());

                //ReportDataSource datasource = new ReportDataSource();
                //datasource.Value = pdList;
                //datasource.Name = "pdDataSet";

                //HomeReportViewer.Reset();
                //HomeReportViewer.LocalReport.DataSources.Add(datasource);
                //HomeReportViewer.LocalReport.ReportEmbeddedResource = "RYOMoisture.RDLC.RYO01.rdlc";
                //HomeReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReportBinding();
        }

        private void CropComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ReportBinding();
        }
    }
}
