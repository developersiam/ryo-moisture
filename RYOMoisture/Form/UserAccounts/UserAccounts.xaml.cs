﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.Form.UserAccounts
{
    /// <summary>
    /// Interaction logic for UserRoles.xaml
    /// </summary>
    public partial class UserAccounts : Page
    {
        public UserAccounts()
        {
            InitializeComponent();
            DataGridBinding();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                if (Helper.UserAccountHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบข้อมูลผู้ใช้ " + UsernameTextBox.Text + " ในระบบ");

                BusinessLayerServices.pd_ryo_UserAccountBL().Add(new DomainModelStecDbms.pd_ryo_UserAccount
                {
                    Username = UsernameTextBox.Text,
                    CreateBy = user_setting.User.Username,
                    CreateDate = DateTime.Now
                });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DataGridBinding()
        {
            try
            {
                UserAccountDataGrid.ItemsSource = null;
                UserAccountDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_UserAccountBL().Get();

                TotalTextBlock.Text = UserAccountDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            UsernameTextBox.Clear();
            AddButton.IsEnabled = false;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                if (Helper.UserAccountHelper.ValidateUserNameInActiveDirectory(UsernameTextBox.Text) == false)
                    throw new ArgumentException("ไม่พบข้อมูลผู้ใช้ " + UsernameTextBox.Text + " ในระบบ");

                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_UserAccount)UserAccountDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_UserAccountBL().Delete(model.Username);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddRoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_UserAccount)UserAccountDataGrid.SelectedItem;
                UserRoles window = new UserRoles(model);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
    }
}
