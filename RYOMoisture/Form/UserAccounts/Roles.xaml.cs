﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.UserAccounts
{
    /// <summary>
    /// Interaction logic for Roles.xaml
    /// </summary>
    public partial class Roles : Window
    {
        pd_ryo_Role _role;
        public Roles()
        {
            InitializeComponent();
            DataGridBinding();
            _role = new pd_ryo_Role();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_Role)RoleDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_RoleBL().Delete(model.RoleID);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                RoleNameTextBox.IsEnabled = false;

                _role = (pd_ryo_Role)RoleDataGrid.SelectedItem;

                RoleNameTextBox.Text = _role.RoleName;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก username");

                BusinessLayerServices.pd_ryo_RoleBL().Add(new pd_ryo_Role
                {
                    RoleID = Guid.NewGuid(),
                    RoleName = RoleNameTextBox.Text,
                    CreateBy = user_setting.User.Username,
                    CreateDate = DateTime.Now,
                    ModifiedBy = user_setting.User.Username,
                    ModifiedDate = DateTime.Now
                });

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            RoleNameTextBox.Clear();
            AddButton.IsEnabled = true;
            EditButton.IsEnabled = false;
        }

        private void DataGridBinding()
        {
            try
            {
                RoleDataGrid.ItemsSource = null;
                RoleDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_RoleBL()
                    .Get()
                    .OrderBy(x => x.RoleName);

                TotalTextBlock.Text = RoleDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Role Name");

                var msg = MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                _role.RoleName = RoleNameTextBox.Text;
                _role.ModifiedBy = user_setting.User.Username;
                _role.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_RoleBL().Edit(_role);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
