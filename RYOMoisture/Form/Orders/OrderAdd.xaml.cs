﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Orders
{
    /// <summary>
    /// Interaction logic for AddOrder.xaml
    /// </summary>
    public partial class OrderAdd : Window
    {
        OrderCrop _crop;

        public OrderAdd(short crop)
        {
            try
            {
                InitializeComponent();

                _crop = new OrderCrop();
                _crop.Crop = crop;

                StatusCombobox.ItemsSource = null;
                StatusCombobox.ItemsSource = user_setting.Status;

                CustomerCombobox.ItemsSource = null;
                CustomerCombobox.ItemsSource = BusinessLayerServices.customerBL()
                    .Get()
                    .OrderBy(x => x.code);

                var BEYear = _crop.Crop + 543;
                ProductionYearTextBox.Text = BEYear.ToString().Substring(2, 2);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void QuantityTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (QuantityTextBox.Text == "")
                {
                    UnitPriceTextBox.Clear();
                    AmountTextBox.Clear();
                    ShippingCaseNoToTextBox.Clear();
                    return;
                }

                /// Calculate amount.
                /// 
                AmountTextBox.Text = (Convert.ToDecimal(QuantityTextBox.Text) *
                    Convert.ToDecimal(UnitPriceTextBox.Text == "" ?
                    UnitPriceTextBox.Text = "0" : UnitPriceTextBox.Text)).ToString("N2");

                /// Calculate shipping case no to.
                /// 
                ShippingCaseNoToTextBox.Text = (Convert.ToInt16(QuantityTextBox.Text) +
                    Convert.ToInt16(ShippingCaseNoFromTextBox.Text) - 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UnitPriceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UnitPriceTextBox.Text == "")
                {
                    AmountTextBox.Clear();
                    return;
                }

                /// Calculate amount.
                /// 
                AmountTextBox.Text = (Convert.ToDecimal(QuantityTextBox.Text) *
                    Convert.ToDecimal(UnitPriceTextBox.Text)).ToString("N2");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PONoTextBox.Clear();
                InvoiceNoTextBox.Clear();
                QuantityTextBox.Clear();
                UnitPriceTextBox.Clear();
                AmountTextBox.Clear();
                ProductionYearTextBox.Clear();

                /// Get max shipping case no to.
                /// 

                ShippingCaseNoFromTextBox.Text = BusinessLayerServices.pd_ryo_OrderBL()
                    .GetByCrop(_crop.Crop)
                    .Max(x => x.ShippingCaseTo)
                    .ToString();

                ShippingCaseNoToTextBox.Clear();
                StatusCombobox.SelectedValue = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PONoTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก PO no.");

                if (InvoiceNoTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Invoice no.");

                if (CustomerCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดกรอก Customer.");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Quantity");

                if (UnitPriceTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Unit Price");

                if (ProductionYearTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Production Year");

                if (ShippingCaseNoFromTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Shipping Case No From");

                if (ShippingCaseNoToTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Shipping Case No To");

                if (RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (RegularExpressionHelper.IsDecimalCharacter(UnitPriceTextBox.Text) == false)
                    throw new ArgumentException("Unit price จะต้องเป็นตัวเลขเท่านั้น");

                if (RegularExpressionHelper.IsNumericCharacter(ProductionYearTextBox.Text) == false)
                    throw new ArgumentException("Production year จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    throw new ArgumentException("shippint case no from จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    throw new ArgumentException("shippint case no to จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                string insertPaperBCFrom = ProductionYearTextBox.Text +
                    ShippingCaseNoFromTextBox.Text.PadLeft(5, '0') +
                    "1".PadLeft(2, '0');

                string insertPaperBCTo = ProductionYearTextBox.Text +
                    ShippingCaseNoToTextBox.Text.PadLeft(5, '0') + "20";

                var msg = MessageBoxHelper.Question("รหัสบาร์โค้ต insert paper จะถูกสร้างขึ้นเพื่อใช้ในการพิมพ์ โดยเริ่มตั้งแต่ " +
                    insertPaperBCFrom + " ถึง " + insertPaperBCTo);
                if (msg == MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_OrderBL()
                    .Add(new pd_ryo_Order
                    {
                        PONo = PONoTextBox.Text,
                        InvoiceNo = InvoiceNoTextBox.Text,
                        Crop = _crop.Crop,
                        Customer = CustomerCombobox.SelectedValue.ToString(),
                        DescriptionOfGoods = DescriptionOfGoodsTextBox.Text,
                        Quantity = Convert.ToInt16(QuantityTextBox.Text),
                        UnitPrice = Convert.ToDecimal(UnitPriceTextBox.Text),
                        ProductionYear = ProductionYearTextBox.Text,
                        ShippingCaseFrom = Convert.ToInt16(ShippingCaseNoFromTextBox.Text),
                        ShippingCaseTo = Convert.ToInt16(ShippingCaseNoToTextBox.Text),
                        CustomerPaymentDate = Convert.ToDateTime(CustomerPaymentDatePicker.SelectedDate),
                        DeliveryDate = Convert.ToDateTime(DeliveryDatePicker.SelectedDate),
                        ReceivedDate = Convert.ToDateTime(ReceivedDatePicker.SelectedDate),
                        ArrivalDate = Convert.ToDateTime(ArrivalDatePicker.SelectedDate),
                        Courier = CourierTextBox.Text,
                        CourierPaymentDate = Convert.ToDateTime(CourierPaymentDatePicker.SelectedDate),
                        DeliveryStatus = Convert.ToBoolean(StatusCombobox.SelectedValue),
                        CreateBy = user_setting.User.Username,
                        ModifiedBy = user_setting.User.Username
                    });

                MessageBoxHelper.Info("บันทึกสำเร็จ");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShippingCaseNoFromTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (ShippingCaseNoFromTextBox.Text == "" || QuantityTextBox.Text == "")
                    return;

                ShippingCaseNoToTextBox.Text = (Convert.ToInt16(QuantityTextBox.Text) +
                    Convert.ToInt16(ShippingCaseNoFromTextBox.Text) - 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (CustomerCombobox.SelectedIndex < 0)
                    return;

                var list = BusinessLayerServices.pd_ryo_OrderBL()
                    .GetByCrop(_crop.Crop)
                    .Where(x => x.Customer == CustomerCombobox.SelectedValue.ToString());

                if (list.Count() < 1)
                    ShippingCaseNoFromTextBox.Text = "1";
                else
                    ShippingCaseNoFromTextBox.Text = (list.Max(x => x.ShippingCaseTo) + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
