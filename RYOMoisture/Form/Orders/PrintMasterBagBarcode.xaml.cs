﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.Model;
using RYOMoistureBL;
using RYOMoistureDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Orders
{
    /// <summary>
    /// Interaction logic for PrintMasterBagBarcode.xaml
    /// </summary>
    /// 

    public partial class PrintMasterBagBarcode : Window
    {
        pd_ryo_Order _order;

        public PrintMasterBagBarcode(pd_ryo_Order order)
        {
            InitializeComponent();

            _order = new pd_ryo_Order();
            _order = order;

            PONoTextBox.Text = _order.PONo;
            ShippingCaseNoFromTextBox.Text = _order.ShippingCaseFrom.ToString();
            ShippingCaseNoToTextBox.Text = _order.ShippingCaseTo.ToString();

            CustomerCombobox.ItemsSource = null;
            CustomerCombobox.ItemsSource = BusinessLayerServices.pd_ryo_customer_labelBL().Get();
        }

        private void Print(m_MasterBagBarcodeTemplate model)
        {
            try
            {
                var application = new Microsoft.Office.Interop.Word.Application();
                var document = new Microsoft.Office.Interop.Word.Document();
                var customer = CustomerCombobox.SelectedValue.ToString();

                //00  แชมป์ 25 g.
                //01  แชมป์ 15 g.
                //02  เสือ
                //03  นักมวย
                //04  ช้าง สีแดง
                //05  ช้าง สีเขียว
                //06  รวงข้าว
                //07  มวยไทย
                //08  ชาวไร่

                switch (customer)
                {
                    case "00":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");
                        break;
                    case "01":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Champ15.docx");
                        break;
                    case "02":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Tiger.docx");
                        break;
                    case "03":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Muay.docx");
                        break;
                    case "04":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - ChangRed.docx");
                        break;
                    case "05":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - ChangGreen.docx");
                        break;
                    case "06":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - RuangKhaw.docx");
                        break;
                    case "07":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - MuayThai.docx");
                        break;
                    case "08":
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBC - Farmer.docx");
                        break;
                    default:
                        document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");
                        break;
                }

                //document = application.Documents.Add(Template: @"C:\RYO\MasterBagBarcode.docx");

                application.Visible = false;

                foreach (Microsoft.Office.Interop.Word.Shape shape in document.Shapes)
                {
                    if (shape.Name == "MBTextBox1")
                        shape.TextFrame.ContainingRange.Text = model.Barcode01;
                    if (shape.Name == "MBTextBox2")
                        shape.TextFrame.ContainingRange.Text = model.Barcode02;
                    if (shape.Name == "MBTextBox3")
                        shape.TextFrame.ContainingRange.Text = model.Barcode03;
                    if (shape.Name == "MBTextBox4")
                        shape.TextFrame.ContainingRange.Text = model.Barcode04;
                    if (shape.Name == "MBTextBox5")
                        shape.TextFrame.ContainingRange.Text = model.Barcode05;
                    if (shape.Name == "MBTextBox6")
                        shape.TextFrame.ContainingRange.Text = model.Barcode06;
                    if (shape.Name == "MBTextBox7")
                        shape.TextFrame.ContainingRange.Text = model.Barcode07;
                    if (shape.Name == "MBTextBox8")
                        shape.TextFrame.ContainingRange.Text = model.Barcode08;
                    if (shape.Name == "MBTextBox9")
                        shape.TextFrame.ContainingRange.Text = model.Barcode09;
                    if (shape.Name == "MBTextBox10")
                        shape.TextFrame.ContainingRange.Text = model.Barcode10;
                    if (shape.Name == "MBTextBox11")
                        shape.TextFrame.ContainingRange.Text = model.Barcode11;
                    if (shape.Name == "MBTextBox12")
                        shape.TextFrame.ContainingRange.Text = model.Barcode12;
                    if (shape.Name == "MBTextBox13")
                        shape.TextFrame.ContainingRange.Text = model.Barcode13;
                    if (shape.Name == "MBTextBox14")
                        shape.TextFrame.ContainingRange.Text = model.Barcode14;
                    if (shape.Name == "MBTextBox15")
                        shape.TextFrame.ContainingRange.Text = model.Barcode15;
                    if (shape.Name == "MBTextBox16")
                        shape.TextFrame.ContainingRange.Text = model.Barcode16;
                    if (shape.Name == "MBTextBox17")
                        shape.TextFrame.ContainingRange.Text = model.Barcode17;
                    if (shape.Name == "MBTextBox18")
                        shape.TextFrame.ContainingRange.Text = model.Barcode18;
                    if (shape.Name == "MBTextBox19")
                        shape.TextFrame.ContainingRange.Text = model.Barcode19;
                    if (shape.Name == "MBTextBox20")
                        shape.TextFrame.ContainingRange.Text = model.Barcode20;

                    if (shape.Name == "BCTextBox1")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode01 + "*";
                    if (shape.Name == "BCTextBox2")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode02 + "*";
                    if (shape.Name == "BCTextBox3")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode03 + "*";
                    if (shape.Name == "BCTextBox4")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode04 + "*";
                    if (shape.Name == "BCTextBox5")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode05 + "*";
                    if (shape.Name == "BCTextBox6")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode06 + "*";
                    if (shape.Name == "BCTextBox7")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode07 + "*";
                    if (shape.Name == "BCTextBox8")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode08 + "*";
                    if (shape.Name == "BCTextBox9")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode09 + "*";
                    if (shape.Name == "BCTextBox10")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode10 + "*";
                    if (shape.Name == "BCTextBox11")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode11 + "*";
                    if (shape.Name == "BCTextBox12")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode12 + "*";
                    if (shape.Name == "BCTextBox13")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode13 + "*";
                    if (shape.Name == "BCTextBox14")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode14 + "*";
                    if (shape.Name == "BCTextBox15")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode15 + "*";
                    if (shape.Name == "BCTextBox16")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode16 + "*";
                    if (shape.Name == "BCTextBox17")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode17 + "*";
                    if (shape.Name == "BCTextBox18")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode18 + "*";
                    if (shape.Name == "BCTextBox19")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode19 + "*";
                    if (shape.Name == "BCTextBox20")
                        shape.TextFrame.ContainingRange.Text = "*" + model.Barcode20 + "*";
                }

                object saveOption = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                object originalFormat = Microsoft.Office.Interop.Word.WdOriginalFormat.wdOriginalDocumentFormat;
                object routDocument = false;

                object missingValue = Type.Missing;

                object myTrue = true;
                object myFalse = false;

                document.PrintOut(ref myFalse, ref myFalse, ref missingValue, ref missingValue,
                    ref missingValue, missingValue, ref missingValue, ref missingValue, ref missingValue,
                    ref missingValue, ref myFalse, ref missingValue, ref missingValue, ref missingValue);

                document.Close(ref saveOption, ref originalFormat, ref routDocument);

                while (application.BackgroundPrintingStatus > 0)
                {
                    System.Threading.Thread.Sleep(250);
                }

                application.Quit(ref missingValue, ref missingValue, ref missingValue);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MasterBagBarcodeDataGrid.Items.Count < 1)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการพิมพ์");

                if (MasterBagBarcodeDataGrid.SelectedItems.Count < 1)
                    throw new ArgumentException("โปรดเลือก master bag barcode ที่ท่านต้องการจะพิมพ์");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    throw new ArgumentException("shippint case no from จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    throw new ArgumentException("shippint case no to จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                /// Generate info list for fill on the template.
                /// 
                List<m_MasterBagBarcodeTemplate> templateList = new List<m_MasterBagBarcodeTemplate>();
                m_MasterBagBarcodeTemplate obj = new m_MasterBagBarcodeTemplate();
                int columnCount = 1;
                var list = MasterBagBarcodeDataGrid.SelectedItems.Cast<m_MasterBagBarcode>().ToList();

                foreach (var item in list)
                {
                    if (columnCount <= 20 && !item.Equals(list.Last()))
                    {
                        item.MasterBagCode = item.MasterBagCode;
                        if (columnCount == 1)
                            obj.Barcode01 = item.MasterBagCode;
                        else if (columnCount == 2)
                            obj.Barcode02 = item.MasterBagCode;
                        else if (columnCount == 3)
                            obj.Barcode03 = item.MasterBagCode;
                        else if (columnCount == 4)
                            obj.Barcode04 = item.MasterBagCode;
                        else if (columnCount == 5)
                            obj.Barcode05 = item.MasterBagCode;
                        else if (columnCount == 6)
                            obj.Barcode06 = item.MasterBagCode;
                        else if (columnCount == 7)
                            obj.Barcode07 = item.MasterBagCode;
                        else if (columnCount == 8)
                            obj.Barcode08 = item.MasterBagCode;
                        else if (columnCount == 9)
                            obj.Barcode09 = item.MasterBagCode;
                        else if (columnCount == 10)
                            obj.Barcode10 = item.MasterBagCode;
                        else if (columnCount == 11)
                            obj.Barcode11 = item.MasterBagCode;
                        else if (columnCount == 12)
                            obj.Barcode12 = item.MasterBagCode;
                        else if (columnCount == 13)
                            obj.Barcode13 = item.MasterBagCode;
                        else if (columnCount == 14)
                            obj.Barcode14 = item.MasterBagCode;
                        else if (columnCount == 15)
                            obj.Barcode15 = item.MasterBagCode;
                        else if (columnCount == 16)
                            obj.Barcode16 = item.MasterBagCode;
                        else if (columnCount == 17)
                            obj.Barcode17 = item.MasterBagCode;
                        else if (columnCount == 18)
                            obj.Barcode18 = item.MasterBagCode;
                        else if (columnCount == 19)
                            obj.Barcode19 = item.MasterBagCode;
                        else if (columnCount == 20)
                        {
                            obj.Barcode20 = item.MasterBagCode;
                            templateList.Add(new m_MasterBagBarcodeTemplate
                            {
                                Barcode01 = obj.Barcode01,
                                Barcode02 = obj.Barcode02,
                                Barcode03 = obj.Barcode03,
                                Barcode04 = obj.Barcode04,
                                Barcode05 = obj.Barcode05,
                                Barcode06 = obj.Barcode06,
                                Barcode07 = obj.Barcode07,
                                Barcode08 = obj.Barcode08,
                                Barcode09 = obj.Barcode09,
                                Barcode10 = obj.Barcode10,
                                Barcode11 = obj.Barcode11,
                                Barcode12 = obj.Barcode12,
                                Barcode13 = obj.Barcode13,
                                Barcode14 = obj.Barcode14,
                                Barcode15 = obj.Barcode15,
                                Barcode16 = obj.Barcode16,
                                Barcode17 = obj.Barcode17,
                                Barcode18 = obj.Barcode18,
                                Barcode19 = obj.Barcode19,
                                Barcode20 = obj.Barcode20
                            });
                            columnCount = 1;
                            obj.Barcode01 = "";
                            obj.Barcode02 = "";
                            obj.Barcode03 = "";
                            obj.Barcode04 = "";
                            obj.Barcode05 = "";
                            obj.Barcode06 = "";
                            obj.Barcode07 = "";
                            obj.Barcode08 = "";
                            obj.Barcode09 = "";
                            obj.Barcode10 = "";
                            obj.Barcode11 = "";
                            obj.Barcode12 = "";
                            obj.Barcode13 = "";
                            obj.Barcode14 = "";
                            obj.Barcode15 = "";
                            obj.Barcode16 = "";
                            obj.Barcode17 = "";
                            obj.Barcode18 = "";
                            obj.Barcode19 = "";
                            obj.Barcode20 = "";
                            continue;
                        }
                        columnCount = columnCount + 1;
                    }
                    else
                    {
                        item.MasterBagCode = item.MasterBagCode;
                        if (columnCount == 1)
                            obj.Barcode01 = item.MasterBagCode;
                        else if (columnCount == 2)
                            obj.Barcode02 = item.MasterBagCode;
                        else if (columnCount == 3)
                            obj.Barcode03 = item.MasterBagCode;
                        else if (columnCount == 4)
                            obj.Barcode04 = item.MasterBagCode;
                        else if (columnCount == 5)
                            obj.Barcode05 = item.MasterBagCode;
                        else if (columnCount == 6)
                            obj.Barcode06 = item.MasterBagCode;
                        else if (columnCount == 7)
                            obj.Barcode07 = item.MasterBagCode;
                        else if (columnCount == 8)
                            obj.Barcode08 = item.MasterBagCode;
                        else if (columnCount == 9)
                            obj.Barcode09 = item.MasterBagCode;
                        else if (columnCount == 10)
                            obj.Barcode10 = item.MasterBagCode;
                        else if (columnCount == 11)
                            obj.Barcode11 = item.MasterBagCode;
                        else if (columnCount == 12)
                            obj.Barcode12 = item.MasterBagCode;
                        else if (columnCount == 13)
                            obj.Barcode13 = item.MasterBagCode;
                        else if (columnCount == 14)
                            obj.Barcode14 = item.MasterBagCode;
                        else if (columnCount == 15)
                            obj.Barcode15 = item.MasterBagCode;
                        else if (columnCount == 16)
                            obj.Barcode16 = item.MasterBagCode;
                        else if (columnCount == 17)
                            obj.Barcode17 = item.MasterBagCode;
                        else if (columnCount == 18)
                            obj.Barcode18 = item.MasterBagCode;
                        else if (columnCount == 19)
                            obj.Barcode19 = item.MasterBagCode;
                        else if (columnCount == 20)
                            obj.Barcode20 = item.MasterBagCode;

                        templateList.Add(obj);
                    }
                }

                foreach (var item in templateList)
                    Print(item);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReprintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MasterBagBarcodeDataGrid.SelectedIndex < 0)
                    return;

                var model = (m_MasterBagBarcode)MasterBagBarcodeDataGrid.SelectedItem;

                Print(new m_MasterBagBarcodeTemplate
                {
                    Barcode01 = model.MasterBagCode
                });
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GenerateBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ShippingCaseNoFromTextBox.Text == "" || ShippingCaseNoToTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุเลข Shipping case no from และ to ก่อนการค้นหา");

                if (RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    throw new ArgumentException("shippint case no from จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    throw new ArgumentException("shippint case no to จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                if (CustomerCombobox.SelectedValue == null)
                    throw new ArgumentException("กรุณาระบุ custoer");

                //var pdsetupdef = BusinessLayerServices.PdSetupBL().GetByDefault();
                //if (pdsetupdef == null)
                //    throw new ArgumentException("ไม่พบ def ในระบบ โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบ");

                //var existed = BusinessLayerServices.pd_ryo_customer_label_setupBL().GetSingle(pdsetupdef.pdno);
                //if (existed == null)
                //    throw new ArgumentException("ไม่พบการตั้งค่าข้อมูล Master Box Customer Label โปรดตรวจสอบการตั้งค่าอีกครั้ง");

                //if (existed.customerID == "00")
                //    _cusCode = "";
                //else
                //    _cusCode = existed.customerID;

                GenerateBarcode();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GenerateBarcode()
        {
            try
            {
                if (string.IsNullOrEmpty(ShippingCaseNoFromTextBox.Text) || string.IsNullOrEmpty(ShippingCaseNoToTextBox.Text))
                    return;

                if (RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoFromTextBox.Text) == false)
                    return;

                if (RegularExpressionHelper.IsNumericCharacter(ShippingCaseNoToTextBox.Text) == false)
                    return;

                if (CustomerCombobox.SelectedValue == null || CustomerCombobox.SelectedIndex < 0)
                    return;

                int caseFrom = Convert.ToInt16(ShippingCaseNoFromTextBox.Text);
                int caseTo = Convert.ToInt16(ShippingCaseNoToTextBox.Text);

                if (caseFrom > caseTo)
                    return;

                int rowNumber = 1;
                var _barcodeList = new List<m_MasterBagBarcode>();

                for (int caseno = caseFrom; caseno <= caseTo; caseno++)
                {
                    for (int bag = 1; bag <= 20; bag++)
                    {
                        var test = CustomerCombobox.SelectedValue.ToString() +
                            _order.ProductionYear +
                            caseno.ToString().PadLeft(6, '0') +
                            bag.ToString().PadLeft(2, '0');

                        _barcodeList.Add(new m_MasterBagBarcode
                        {
                            MasterBagCode = test,
                            CreateDate = DateTime.Now,
                            RowNumber = rowNumber
                        });
                        rowNumber = rowNumber + 1;
                    }
                }

                MasterBagBarcodeDataGrid.ItemsSource = null;
                MasterBagBarcodeDataGrid.ItemsSource = _barcodeList;

                TotalTextBlock.Text = MasterBagBarcodeDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MasterBagBarcodeDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectedItemsTextBlock.Text = MasterBagBarcodeDataGrid.SelectedItems.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerCombobox_DropDownClosed(object sender, EventArgs e)
        {
            GenerateBarcode();
        }

        private void ShippingCaseNoFromTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            GenerateBarcode();
        }

        private void ShippingCaseNoToTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            GenerateBarcode();
        }
    }
}
