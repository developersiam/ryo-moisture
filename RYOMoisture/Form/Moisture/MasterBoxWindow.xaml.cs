﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Moisture
{
    /// <summary>
    /// Interaction logic for MasterBoxWindow.xaml
    /// </summary>
    public partial class MasterBoxWindow : Window
    {
        public MasterBoxWindow()
        {
            InitializeComponent();
        }

        public MasterBoxWindow(string bc, string caseno)
        {
            InitializeComponent();
            C48NumberTextBox.Text = caseno;
            BarcodeTextBox.Text = bc;
            ReloadDatagrid();
            UpdateButton.IsEnabled = true;
        }

        private void PackingDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            try
            {
                var date = PackingDatePicker.SelectedDate;
                if (date == null) return;

                GradeCombobox.ItemsSource = null;
                GradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetGroupedGradeByDate(date.Value);
                CasenoCombobox.IsEnabled = false;
                //MasterboxNoTextBox.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GradeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var grade = (pd)GradeCombobox.SelectedItem;
                var packedDate = PackingDatePicker.SelectedDate;
                if (grade == null || packedDate == null) return;

                var productList = BusinessLayerServices.ProductBL().GetPDByGradeAndDate(grade.grade, packedDate.Value);
                CasenoCombobox.ItemsSource = null;
                CasenoCombobox.ItemsSource = productList;
                CasenoCombobox.IsEnabled = productList.Any();
                //MasterboxNoTextBox.Text = "";
                //MasterboxNoTextBox.IsEnabled = productList.Any();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var caseno = (pd)CasenoCombobox.SelectedItem;
                //var masterbox = Convert.ToInt32(MasterboxNoTextBox.Text.Trim());
                var grade = (pd)GradeCombobox.SelectedItem;
                var packedDate = PackingDatePicker.SelectedDate;
                if (grade == null || packedDate == null || caseno == null) throw new Exception("Please mention all required data.");

                var productList = BusinessLayerServices.ProductBL().GetPDByGradeAndDate(grade.grade, packedDate.Value);
                CasenoCombobox.ItemsSource = null;
                CasenoCombobox.ItemsSource = productList;
                CasenoCombobox.IsEnabled = productList.Any();

                var duplicate = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(caseno.bc);
                if (duplicate != null) throw new Exception("Data is duplicate please check");

                var masterbox = new pd_moisture_masterbox
                {
                    from_caseno = Convert.ToInt32(C48NumberTextBox.Text),
                    frombc = BarcodeTextBox.Text,
                    caseno = caseno.caseno.Value,
                    masterbox_bc = caseno.bc
                };

                BusinessLayerServices.MoistureBL().AddMasterBox(masterbox);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadDatagrid()
        {
            try
            {
                var masterLst = BusinessLayerServices.MoistureBL().GetMasterBoxListByBC(BarcodeTextBox.Text);
                InformationDataGrid.ItemsSource = null;
                InformationDataGrid.ItemsSource = masterLst;

                if (masterLst.Any())
                {
                    var x = masterLst.FirstOrDefault();
                    var y = BusinessLayerServices.ProductBL().GetSingleProduct(x.masterbox_bc);
                    PackingDatePicker.SelectedDate = y.packingdate;
                    GradeCombobox.ItemsSource = null;
                    GradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetGroupedGradeByDate(y.packingdate.GetValueOrDefault());
                    GradeCombobox.SelectedValue = y.grade;
                    var productList = BusinessLayerServices.ProductBL().GetPDByGradeAndDate(y.grade, y.packingdate.GetValueOrDefault());
                    CasenoCombobox.ItemsSource = null;
                    CasenoCombobox.ItemsSource = productList;
                }

                PackingDatePicker.IsEnabled = !masterLst.Any();
                GradeCombobox.IsEnabled = !masterLst.Any();
                CasenoCombobox.IsEnabled = masterLst.Any();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteDetailBtton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (pd_moisture_masterbox)InformationDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBoxHelper.Question("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.MoistureBL().DeleteMasterBox(oldItem);
                        MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
                        ReloadDatagrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
