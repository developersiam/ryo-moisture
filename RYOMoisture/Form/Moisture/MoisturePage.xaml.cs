﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelStecDbms;
using RYOMoistureBL;
using RYOMoisture.Model;
using RYOMoisture.Shared;
using RYOMoisture.Helper;

namespace RYOMoisture.Form.Moisture
{
    /// <summary>
    /// Interaction logic for MoisturePage.xaml
    /// </summary>
    public partial class MoisturePage : Page
    {
        public MoisturePage()
        {
            InitializeComponent();
            TargetTextBox.Text = "15.5";
        }

        //private void ProductDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    try
        //    {
        //        Clear();
        //        var productDate = ProductDatePicker.SelectedDate;
        //        GradeCombobox.ItemsSource = null;
        //        if (productDate != null) GradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetGroupedGradeByDate(productDate.Value);
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBoxHelper.Exception(ex);
        //    }
        //}

        private void GradeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            ReloadDatagrid();
        }

        private void ReloadDatagrid()
        {
            try
            {
                Clear();

                var grade = (pd)GradeCombobox.SelectedItem;
                //var productDate = ProductDatePicker.SelectedDate;
                //if (grade == null) return;

                //var productList = BusinessLayerServices.ProductBL().GetPDByGradeAndDate(grade.grade, productDate.Value);
                var productList = BusinessLayerServices.ProductBL().GetPdinfoByGrade(GradeCombobox.Text);

                var vmList = new List<m_MoistureVM>();
                foreach (var i in productList)
                {
                    var moistureList = BusinessLayerServices.MoistureBL().GetMoistureListByBC(i.bc);
                    var packedMoisture = moistureList.SingleOrDefault(s => s.moistureType == 1);
                    var retestMoisture = moistureList.SingleOrDefault(s => s.moistureType == 2);
                    var baggingMoisture = moistureList.SingleOrDefault(s => s.moistureType == 5);
                    var moistureTarget = BusinessLayerServices.MoistureBL().GetMoistureTargetByBC(i.bc);

                    var vm = new m_MoistureVM
                    {
                        Crop = i.crop.Value,
                        Pdno = i.frompdno,
                        TargetMoisture = moistureTarget == null ? null : moistureTarget.target,
                        TestTime = moistureTarget == null ? null : moistureTarget.test_time,
                        Barcode = i.bc,
                        Caseno = i.caseno,
                        PackedMoisture = packedMoisture == null ? null : packedMoisture.moisture,
                        PackedStatus = i.issued.GetValueOrDefault() ? "Blended" : packedMoisture == null ? null : packedMoisture.moisture > moistureTarget.target ? "High" : "Avalible to use",
                        RetestMoisture = retestMoisture == null ? null : retestMoisture.moisture,
                        RestestStatus = i.issued.GetValueOrDefault() ? "Blended" : retestMoisture == null ? null : retestMoisture.moisture > moistureTarget.target ? "High" : "Avalible to use",
                    };
                    vmList.Add(vm);
                }

                InformationDataGrid.ItemsSource = vmList;
                TotalTextBlock.Text = string.Format("Total {0} item{1}", productList.Count, productList.Count > 0 ? "s" : "");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var selected = (m_MoistureVM)InformationDataGrid.SelectedItem;
                if (selected == null) return;

                CropTextBox.Text = selected.Crop.ToString();
                PdnoTextBox.Text = selected.Pdno;
                BarcodeTextBox.Text = selected.Barcode;
                CasenoTextBox.Text = selected.Caseno.ToString();
                TestTimeTextBox.Text = selected.TestTime.ToString();
                TestTimeTextBox.IsEnabled = true;
                TargetTextBox.Text = selected.TargetMoisture == null ? "15.5" : selected.TargetMoisture.Value.ToString("#.##");
                TargetTextBox.IsEnabled = true;
                var packedMoisture = BusinessLayerServices.MoistureBL().GetSingleMoisture(BarcodeTextBox.Text, 1);
                PackedMoistureTextBox.Text = packedMoisture == null ? "" : packedMoisture.moisture.Value.ToString("#.##");
                PackedMoistureTextBox.IsEnabled = true;
                var retestMoisture = BusinessLayerServices.MoistureBL().GetSingleMoisture(BarcodeTextBox.Text, 2);
                RetestMoistureTextBox.Text = retestMoisture == null ? "" : retestMoisture.moisture.Value.ToString("#.##");
                RetestMoistureTextBox.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            //ProductDatePicker.SelectedDate = null;            
        }

        private void Clear()
        {
            CropTextBox.Text = "";
            PdnoTextBox.Text = "";
            PdnoTextBox.IsEnabled = false;
            BarcodeTextBox.Text = "";
            BarcodeTextBox.IsEnabled = false;
            CasenoTextBox.Text = "";
            CasenoTextBox.IsEnabled = false;
            TestTimeTextBox.Text = "";
            TestTimeTextBox.IsEnabled = false;
            TargetTextBox.Text = "15.5";
            TargetTextBox.IsEnabled = false;
            PackedMoistureTextBox.Text = "";
            PackedMoistureTextBox.IsEnabled = false;
            RetestMoistureTextBox.Text = "";
            RetestMoistureTextBox.IsEnabled = false;
            InformationDataGrid.ItemsSource = null;
            TotalTextBlock.Text = string.Format("Total 0 item");
        }

        private void TestTimeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TestTimeTextBox.Text = RYOMoistureService.DataCorrector().FormatTime(TestTimeTextBox.Text);
        }

        private void TestTimeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) TestTimeTextBox.Text = RYOMoistureService.DataCorrector().FormatTime(TestTimeTextBox.Text);
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBoxHelper.Question("ต้องการบันทึกข้อมูลหรือไม่") != MessageBoxResult.Yes)
                    return;

                var crop = CropTextBox.Text;
                var barcode = BarcodeTextBox.Text;
                var caseno = CasenoTextBox.Text;

                if (barcode == "" || caseno == "")
                    throw new Exception("Please choose product on the right side first.");

                var testTimes = TestTimeTextBox.Text;
                var targetMoistures = TargetTextBox.Text;
                var packedMoistures = PackedMoistureTextBox.Text.Trim();
                var retestMoistures = RetestMoistureTextBox.Text.Trim();

                if (testTimes == "" || targetMoistures == "")
                    throw new Exception("Please check on testing Time input or Target Moisute.");

                var target = new pd_moisture_target
                {
                    bc = barcode,
                    caseno = int.Parse(caseno),
                    test_time = TimeSpan.Parse(testTimes),
                    target = decimal.Parse(targetMoistures)
                };
                BusinessLayerServices.MoistureBL().UpdateMoistureTarget(target);

                if (packedMoistures != "")
                {
                    var packedMoisture = new pd_moisture
                    {
                        crop = int.Parse(crop),
                        bc = barcode,
                        caseno = int.Parse(caseno),
                        moistureType = 1,
                        moisture = decimal.Parse(packedMoistures),
                        modifiedDate = DateTime.Now,
                        test_time = TimeSpan.Parse(testTimes)
                    };
                    BusinessLayerServices.MoistureBL().UpdateMoisture(packedMoisture);
                }

                if (retestMoistures != "")
                {
                    var retestMoisture = new pd_moisture
                    {
                        crop = int.Parse(crop),
                        bc = barcode,
                        caseno = int.Parse(caseno),
                        moistureType = 2,
                        moisture = decimal.Parse(retestMoistures),
                        modifiedDate = DateTime.Now,
                        test_time = TimeSpan.Parse(testTimes)
                    };
                    BusinessLayerServices.MoistureBL().UpdateMoisture(retestMoisture);
                }
                else
                {
                    //delete : check before delete
                    var retestMoisture = new pd_moisture
                    {
                        crop = int.Parse(crop),
                        bc = barcode,
                        caseno = int.Parse(caseno),
                        moistureType = 2,
                        modifiedDate = DateTime.Now,
                        test_time = TimeSpan.Parse(testTimes)
                    };
                    BusinessLayerServices.MoistureBL().DeleteMoisture(retestMoisture);
                }

                ReloadDatagrid();
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CropTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //Get all grade by Crop
                if (CropTextBox.Text == "") return;
                GradeCombobox.ItemsSource = null;
                GradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetAB4GradeByCrop(int.Parse(CropTextBox.Text));
            }
        }
    }
}
