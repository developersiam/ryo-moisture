﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.Model;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Moisture
{
    /// <summary>
    /// Interaction logic for BaggingPage.xaml
    /// </summary>
    public partial class BaggingPage : Page
    {
        List<m_Bagging> baggingList;
        List<m_MoistureVM> vmList;
        public BaggingPage()
        {
            InitializeComponent();
            baggingList = new List<m_Bagging>();
            vmList = new List<m_MoistureVM>();
            BindPackedStatus();
        }

        private void BindPackedStatus()
        {
            var packedStsList = new List<pd>();
            packedStsList.Add(new pd { issued = null, issuedreason = "All" });
            packedStsList.Add(new pd { issued = false, issuedreason = "Available" });
            packedStsList.Add(new pd { issued = true, issuedreason = "Issued" });
            PackedStatusCombobox.ItemsSource = null;
            PackedStatusCombobox.ItemsSource = packedStsList;
        }
        private void PackedGradeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var selectedAB4 = (pd)PackedGradeCombobox.SelectedItem;
                if (selectedAB4 == null) return;

                PackedStatusCombobox.SelectedIndex = 0;
                PackedStatusCombobox.IsEnabled = true;

                ReloadPackedDatagrid(selectedAB4);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackedStatusCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var status = (pd)PackedStatusCombobox.SelectedItem;
                var grade = (pd)PackedGradeCombobox.SelectedItem;
                ReloadPackedDatagrid(grade);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddMasterButton_Click(object sender, RoutedEventArgs e)
        {
            //Add and Update Masterbox
            try
            {
                var ab4 = (pd)PackedGradeCombobox.SelectedItem;

                //Add Bagging and Retest Moisture
                if (GradeCombobox.Text != "" & CasenoCombobox.Text != "")
                {
                    var baggingcaseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(GradeCombobox.Text, int.Parse(CasenoCombobox.Text));
                    if (baggingcaseno != null)
                    {
                        var grade = GradeCombobox.Text;
                        var packedDate = PackingDatePicker.SelectedDate;
                        if (grade == null || packedDate == null || baggingcaseno == null) throw new Exception("Please mention all required data.");
                        decimal tempA = 0;
                        decimal tempB = 0;
                        decimal? baggingMoisture = decimal.TryParse(BaggingMoistureTextBox.Text, out tempA) ? tempA : (decimal?)null;
                        decimal? retestedMoisture = decimal.TryParse(RetestedMoistureTextBox.Text, out tempB) ? tempB : (decimal?)null;


                        //Add Bagging moisture for RYO
                        if (BaggingMoistureTextBox.Text != "")
                        {
                            if (Convert.ToDouble(BaggingMoistureTextBox.Text) >= 16.1) throw new Exception("Bagging Target Moisture not allow to over 16.0");
                            UpdateAllMoisture((int)baggingcaseno.crop, baggingcaseno.bc, (int)baggingcaseno.caseno, 4, decimal.Parse(BaggingMoistureTextBox.Text));
                        }

                        //Add Retested moisture for RYO
                        if (RetestedMoistureTextBox.Text != "" & RetestedMoistureTextBox.Text != "0")
                        {
                            if (Convert.ToDouble(RetestedMoistureTextBox.Text) >= 16.1) throw new Exception("Retest Bagging Target Moisture not allow to over 16.0");
                            UpdateAllMoisture((int)baggingcaseno.crop, baggingcaseno.bc, (int)baggingcaseno.caseno, 5, decimal.Parse(RetestedMoistureTextBox.Text));
                        }

                        //-------- Add to master box table ---------
                        if (C48GradeTextBox.Text != "" && C48NumberTextBox.Text != "" && baggingcaseno != null)
                        {
                            var fromcaseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(C48GradeTextBox.Text, int.Parse(C48NumberTextBox.Text));
                            //Add only feeding moisture data
                            if (fromcaseno != null && InputMoistureTextBox.Text != "") UpdateAllMoisture((int)fromcaseno.crop, fromcaseno.bc, (int)fromcaseno.caseno, 3, decimal.Parse(InputMoistureTextBox.Text));

                            //Update Masterbox with AB4
                            if (fromcaseno != null)
                            {
                                UpdateMasterBoxWTAB4((int)fromcaseno.caseno, fromcaseno.bc, Convert.ToInt32(CasenoCombobox.Text), baggingcaseno.bc);
                            }
                        }

                        MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                        //ReloadPackedDatagrid(ab4); 
                        ReloadInfomationDatagrid(GradeCombobox.Text, PackingDatePicker.Text);
                    }
                }
                //Update AB4 : Input moisture
                else
                {
                    //-------- Add to master box table ---------
                    if (C48GradeTextBox.Text != "" && C48NumberTextBox.Text != "")
                    {
                        var fromcaseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(C48GradeTextBox.Text, int.Parse(C48NumberTextBox.Text));
                        //Add only feeding moisture data
                        if (fromcaseno != null && InputMoistureTextBox.Text != "") UpdateAllMoisture((int)fromcaseno.crop, fromcaseno.bc, (int)fromcaseno.caseno, 3, decimal.Parse(InputMoistureTextBox.Text));

                        MessageBoxHelper.Info("บันทึกข้อมูล Input Moisture สำเร็จ");
                        ReloadPackedDatagrid(fromcaseno);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadPackedDatagrid(pd selected)
        {
            List<pd> productList = new List<pd>();
            //Status
            if (PackedStatusCombobox.SelectedIndex > 0)
            {
                var status = (pd)PackedStatusCombobox.SelectedItem;
                productList = BusinessLayerServices.ProductBL().GetPdinfoByGrade(selected.grade).Where(w => w.issued == status.issued).ToList();
            }
            else
                productList = BusinessLayerServices.ProductBL().GetPdinfoByGrade(selected.grade);
          
            //---------------------------- New Grid info -----------------------
            vmList = new List<m_MoistureVM>();
            foreach (var i in productList)
            {
                var moistureList = BusinessLayerServices.MoistureBL().GetMoistureListByBC(i.bc);
                var packedMoisture = moistureList.SingleOrDefault(s => s.moistureType == 1);
                var retestMoisture = moistureList.SingleOrDefault(s => s.moistureType == 2);
                var inputMoisture = moistureList.SingleOrDefault(s => s.moistureType == 3);
                var baggingMoisture = moistureList.SingleOrDefault(s => s.moistureType == 5);
                var moistureTarget = BusinessLayerServices.MoistureBL().GetMoistureTargetByBC(i.bc);

                if (packedMoisture != null || retestMoisture != null || inputMoisture != null)
                {
                    var vm = new m_MoistureVM
                    {
                        Crop = i.crop.Value,
                        Pdno = i.frompdno,
                        TargetMoisture = moistureTarget == null ? null : moistureTarget.target,
                        TestTime = moistureTarget == null ? null : moistureTarget.test_time,
                        Barcode = i.bc,
                        Caseno = i.caseno,
                        PackedMoisture = packedMoisture == null ? null : packedMoisture.moisture,
                        PackedStatus = i.issued.GetValueOrDefault() ? "Issued" : packedMoisture == null ? null : packedMoisture.moisture > moistureTarget.target ? "High" : "Avalible",
                        RetestMoisture = retestMoisture == null ? null : retestMoisture.moisture,
                        RestestStatus = i.issued.GetValueOrDefault() ? "Issued" : retestMoisture == null ? null : retestMoisture.moisture > moistureTarget.target ? "High" : "Avalible",
                        FeedingMoister = inputMoisture == null ? null : inputMoisture.moisture,
                        FeedingStatus = i.issued.GetValueOrDefault() ? "Issued" : inputMoisture == null ? null : inputMoisture.moisture > moistureTarget.target ? "High" : "Avalible"
                    };
                    vmList.Add(vm);
                }
            }

            PackedInformationDataGrid.ItemsSource = vmList;
            TotalTextBlock.Text = string.Format("Total {0} item{1}", vmList.Count, vmList.Count > 0 ? "s" : "");
        }

        private void InformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string C48barcode;
                if (InformationDataGrid.SelectedIndex < 0) return;

                var selected = (m_MoistureVM)InformationDataGrid.SelectedItem;
                if (selected != null)
                {
                    //CasenoCombobox.SelectedValue = selected.Caseno;
                    C48NumberTextBox.Text = selected.Fromcaseno.ToString();
                    var p = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(selected.Barcode);
                    C48GradeTextBox.Text = "";
                    InputMoistureTextBox.Text = "";
                    if (p != null)
                    {
                        C48barcode = p.frombc.ToString();
                        var ab4 = BusinessLayerServices.ProductBL().GetSingleProduct(C48barcode);
                        C48GradeTextBox.Text = ab4.grade;
                        C48NumberTextBox.Text = ab4.caseno.ToString();

                        //input moisture 
                        var inputMS = BusinessLayerServices.MoistureBL().GetSingleMoisture(C48barcode, 3);
                        if (inputMS != null) InputMoistureTextBox.Text = string.Format("{0:N2}", inputMS.moisture);
                    }

                    CasenoCombobox.Text = selected.Caseno.ToString();
                    BaggingMoistureTextBox.Text = string.Format("{0:N2}", selected.MasterBoxMoisture);
                    RetestedMoistureTextBox.Text = string.Format("{0:N2}", selected.BaggingMoisture);

                    AddMasterButton.IsEnabled = true;
                    DeleteMasterButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackedInformationDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PackedInformationDataGrid.SelectedIndex < 0)
                    return;

                var selected = (m_MoistureVM)PackedInformationDataGrid.SelectedItem;
                var caseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(PackedGradeCombobox.Text, (int)selected.Caseno);
                if (selected != null && caseno != null)
                {
                    //PackedCasenoCombobox.SelectedValue = selected.Caseno;
                    C48NumberTextBox.Text = selected.Caseno.ToString();
                    C48GradeTextBox.Text = caseno.grade;
                    InputMoistureTextBox.Text = string.Format("{0:N2}", selected.FeedingMoister);
                }

                //Old code after selected caseno combobox
                var status = (pd)PackedStatusCombobox.SelectedItem;             
                InformationDataGrid.ItemsSource = null;
                var masterLst = BusinessLayerServices.MoistureBL().GetMasterBoxListByBC(caseno.bc);
                if (masterLst.Any())
                {
                    //--------------------- Start to show CHAMP packing and grade -------------------------------
                    var bagginglst = new List<m_MoistureVM>();
                    foreach (var l in masterLst)
                    {
                        PackingDatePicker.IsEnabled = true;
                        //2. Show masterbox Moisture
                        var masterboxList = BusinessLayerServices.MoistureBL().GetMoistureListByBC(l.masterbox_bc);
                        var masterboxMoisture = masterboxList.SingleOrDefault(s => s.moistureType == 4);
                        var retestMoisture = masterboxList.SingleOrDefault(s => s.moistureType == 5);
                        var moistureTarget = BusinessLayerServices.MoistureBL().GetMoistureTargetByBC(caseno.bc);

                        var vm = new m_MoistureVM
                        {
                            TargetMoisture = moistureTarget == null ? null : moistureTarget.target,
                            Fromcaseno = l.from_caseno,
                            Barcode = l.masterbox_bc,
                            Caseno = l.caseno,
                            MasterBoxMoisture = masterboxMoisture == null ? null : masterboxMoisture.moisture,
                            BaggingMoisture = retestMoisture == null ? null : retestMoisture.moisture,
                        };
                        bagginglst.Add(vm);
                    };
                    //3. Show master box
                    InformationDataGrid.ItemsSource = null;
                    InformationDataGrid.ItemsSource = bagginglst;
                    TotalTextBlocks.Text = string.Format("Total {0} item{1}", bagginglst.Count, bagginglst.Count > 0 ? "s" : "");
                }              
 
                PackingDatePicker.IsEnabled = true;
                AddMasterButton.IsEnabled = true;
                DeleteMasterButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void GradeCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                var grade = (pdsetup)GradeCombobox.SelectedItem;
                var packedDate = PackingDatePicker.SelectedDate;
                if (grade == null || packedDate == null) return;

                var productList = BusinessLayerServices.ProductBL().GetPDByGradeAndFromPdDate(grade.packedgrade, packedDate.Value);

                CasenoCombobox.ItemsSource = null;
                CasenoCombobox.ItemsSource = productList;
                CasenoCombobox.IsEnabled = productList.Any();

                //Show all case in grid by Grade and PackedDate
                InformationDataGrid.ItemsSource = null;
                var vmList = new List<m_MoistureVM>();
                foreach (var i in productList)
                {
                    //Get all moisture info
                    var moistureList = BusinessLayerServices.MoistureBL().GetMoistureListByBC(i.bc);
                    var masterboxMoisture = moistureList.SingleOrDefault(s => s.moistureType == 4);
                    var retestMoisture = moistureList.SingleOrDefault(s => s.moistureType == 5);
                    var moistureTarget = BusinessLayerServices.MoistureBL().GetMoistureTargetByBC(i.bc);

                    //Check masterbox come from
                    int? fromcaseno = 0;
                    var masterboxinfo = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(i.bc);
                    if (masterboxinfo != null) fromcaseno = masterboxinfo.from_caseno;

                    var vm = new m_MoistureVM
                    {
                        Crop = i.crop.Value,
                        Pdno = i.frompdno,
                        TargetMoisture = moistureTarget == null ? null : moistureTarget.target,
                        TestTime = moistureTarget == null ? null : moistureTarget.test_time,
                        Barcode = i.bc,
                        Fromcaseno = fromcaseno == 0 ? null : fromcaseno,
                        Caseno = i.caseno,
                        MasterBoxMoisture = masterboxMoisture == null ? null : masterboxMoisture.moisture,
                        BaggingMoisture = retestMoisture == null ? null : retestMoisture.moisture,
                    };
                    vmList.Add(vm);
                }
                InformationDataGrid.ItemsSource = vmList;
                TotalTextBlocks.Text = string.Format("Total {0} item{1}", productList.Count, productList.Count > 0 ? "s" : "");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackingDatePicker_CalendarClosed(object sender, RoutedEventArgs e)
        {
            try
            {
                var date = PackingDatePicker.SelectedDate;
                if (date == null) return;

                GradeCombobox.ItemsSource = null;
                GradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetGradeByfrompdDate(date.Value);
                CasenoCombobox.IsEnabled = false;

                InformationDataGrid.ItemsSource = null;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UpdateAllMoisture(int crop, string bc, int caseno,int mcType, decimal mcValue)
        {
            var mc = new pd_moisture
            {
                crop = (int)crop,
                bc = bc,
                caseno = caseno,
                moistureType = mcType,
                moisture = mcValue,
                modifiedDate = DateTime.Now,
                test_time = TimeSpan.Parse(DateTime.Now.ToString("HH:mm:ss"))
            };
            BusinessLayerServices.MoistureBL().UpdateMoisture(mc);
        }

        private void CropTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //Get all grade by Crop
                PackedGradeCombobox.ItemsSource = null;
                PackedGradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetAB4GradeByCrop(int.Parse(CropTextBox.Text));
            }
        }

        private void CropTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (CropTextBox.Text != "")
            {
                PackedGradeCombobox.ItemsSource = null;
                PackedGradeCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetAB4GradeByCrop(int.Parse(CropTextBox.Text));
            }
        }

        private void DeleteMasterButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var ab4 = (pd)PackedGradeCombobox.SelectedItem;
                //-------- Checking null ---------
                if (C48NumberTextBox.Text == "") throw new Exception("Please check AB4 case no.");
                var fromcaseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(PackedGradeCombobox.Text, int.Parse(C48NumberTextBox.Text));

                if (CasenoCombobox.Text == "") throw new Exception("Please check Master box case no.");

                var baggingcaseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(GradeCombobox.Text, int.Parse(CasenoCombobox.Text));
                var grade = GradeCombobox.Text;
                var packedDate = PackingDatePicker.SelectedDate;
                if (grade == null || packedDate == null || baggingcaseno == null) throw new Exception("Please mention all required data.");

                var duplicate = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(baggingcaseno.bc);
                if (duplicate != null)
                {
                    var masterbox = new pd_moisture_masterbox
                    {
                        from_caseno = Convert.ToInt32(C48NumberTextBox.Text),
                        frombc = fromcaseno.bc,
                        caseno = baggingcaseno.caseno.Value,
                        masterbox_bc = baggingcaseno.bc
                    };
                    BusinessLayerServices.MoistureBL().DeleteMasterBox(masterbox);
                }                 
                MessageBoxHelper.Info("Remove this Master box สำเร็จ");
                //ReloadPackedDatagrid(ab4);
                ReloadInfomationDatagrid(GradeCombobox.Text, PackingDatePicker.Text);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadInfomationDatagrid(string grade, string packingDate)
        {
            var productList = BusinessLayerServices.ProductBL().GetPDByGradeAndFromPdDate(grade, Convert.ToDateTime(packingDate));

            CasenoCombobox.ItemsSource = null;
            CasenoCombobox.ItemsSource = productList;
            CasenoCombobox.IsEnabled = productList.Any();

            //Show all case in grid by Grade and PackedDate
            InformationDataGrid.ItemsSource = null;
            var vmList = new List<m_MoistureVM>();
            foreach (var i in productList)
            {
                //Get all moisture info
                var moistureList = BusinessLayerServices.MoistureBL().GetMoistureListByBC(i.bc);
                var masterboxMoisture = moistureList.SingleOrDefault(s => s.moistureType == 4);
                var retestMoisture = moistureList.SingleOrDefault(s => s.moistureType == 5);
                var moistureTarget = BusinessLayerServices.MoistureBL().GetMoistureTargetByBC(i.bc);

                //Check masterbox come from
                int? fromcaseno = 0;
                var masterboxinfo = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(i.bc);
                if (masterboxinfo != null) fromcaseno = masterboxinfo.from_caseno;

                var vm = new m_MoistureVM
                {
                    Crop = i.crop.Value,
                    Pdno = i.frompdno,
                    TargetMoisture = moistureTarget == null ? null : moistureTarget.target,
                    TestTime = moistureTarget == null ? null : moistureTarget.test_time,
                    Barcode = i.bc,
                    Fromcaseno = fromcaseno == 0 ? null : fromcaseno,
                    Caseno = i.caseno,
                    MasterBoxMoisture = masterboxMoisture == null ? null : masterboxMoisture.moisture,
                    BaggingMoisture = retestMoisture == null ? null : retestMoisture.moisture,
                };
                vmList.Add(vm);
            }
            InformationDataGrid.ItemsSource = vmList;
            TotalTextBlocks.Text = string.Format("Total {0} item{1}", productList.Count, productList.Count > 0 ? "s" : "");
        }

        private void UpdateMasterBoxWTAB4(int AB4_caseno, string AB4_bc, int CH_caseno, string CH_bc)
        {
            string existedissue = "";
            //Find the customer of CUT RAG 
            var baggingcaseno = BusinessLayerServices.ProductBL().GetSinglePdinfoByGrade(GradeCombobox.Text, int.Parse(CasenoCombobox.Text));
            var grade = GradeCombobox.Text;
            var packedDate = PackingDatePicker.SelectedDate;
            if (baggingcaseno == null) throw new Exception("Please mention all required data.");

            //var issueCode = BusinessLayerServices.pd_ryo_MaterialIssuedCodeBL().GetCurrentStatus(baggingcaseno.customer);
            //if (issueCode != null) existedissue = issueCode.IssuedCode;

            var duplicate = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(CH_bc);
            var masterbox = new pd_moisture_masterbox
            {
                from_caseno = Convert.ToInt32(C48NumberTextBox.Text),
                frombc = AB4_bc,
                caseno = Convert.ToInt32(CasenoCombobox.Text),
                masterbox_bc = CH_bc,
                IssuedCode = existedissue
            };
            if (duplicate == null) BusinessLayerServices.MoistureBL().AddMasterBox(masterbox);
            else
            {
                BusinessLayerServices.MoistureBL().DeleteMasterBox(masterbox);
                BusinessLayerServices.MoistureBL().AddMasterBox(masterbox);
            }              
        }
    }
}
