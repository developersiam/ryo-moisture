﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RYOMoistureBL;
using DomainModelStecDbms;
using RYOMoisture.Helper;

namespace RYOMoisture.Form.Production
{
    /// <summary>
    /// Interaction logic for RemnantWindow.xaml
    /// </summary>
    public partial class RemnantWindow : Window
    {
        public DateTime pd_Date;
        public RemnantWindow(string pdno, DateTime pdDate, string grade)
        {
            InitializeComponent();
            pd_Date = pdDate;
            pdDatePicker.Text = pd_Date.ToString("dd/MM/yyyy");
            PdnoTextBox.Text = pdno;
            GradeTextBox.Text = grade;

            PackingDatePicker.SelectedDate = DateTime.Now;
            ReloadDatagrid();
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var packedDate = PackingDatePicker.SelectedDate;
                if (packedDate == null) throw new Exception("กรุณาเลือกวันที่ Packing");

                UpdateRemant();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        public void UpdateRemant()
        {
            try
            {
                var duplicate = BusinessLayerServices.MoistureBL().GetSingleRemnant(PdnoTextBox.Text, (DateTime)PackingDatePicker.SelectedDate);
                if (duplicate != null)
                {
                    duplicate.ryo_box = string.IsNullOrEmpty(boxTextBox.Text) ? 0 : Convert.ToInt16(boxTextBox.Text);
                    duplicate.ryo_bag = string.IsNullOrEmpty(bagTextBox.Text) ? 0 : Convert.ToInt16(bagTextBox.Text);
                    duplicate.ryo_pouch = string.IsNullOrEmpty(pouchTextBox.Text) ? 0 : Convert.ToInt16(pouchTextBox.Text);
                    duplicate.modified_date = DateTime.Now;
                    BusinessLayerServices.MoistureBL().UpdateRemnant(duplicate);
                }
                else
                {
                    var remnant = new pd_ryo_remnant
                    {
                        pdno = PdnoTextBox.Text,
                        pddate = Convert.ToDateTime(pdDatePicker.Text),
                        packingdate = (DateTime)PackingDatePicker.SelectedDate,
                        ryo_box = string.IsNullOrEmpty(boxTextBox.Text) ? 0 : Convert.ToInt16(boxTextBox.Text),
                        ryo_bag = string.IsNullOrEmpty(bagTextBox.Text) ? 0 : Convert.ToInt16(bagTextBox.Text),
                        ryo_pouch = string.IsNullOrEmpty(pouchTextBox.Text) ? 0 : Convert.ToInt16(pouchTextBox.Text),
                        user = "Staff GD12",
                        modified_date = DateTime.Now,
                    };
                    BusinessLayerServices.MoistureBL().AddRemnant(remnant);
                }

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                ReloadDatagrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void ReloadDatagrid()
        {
            remnantDataGrid.ItemsSource = null;
            //var AllRemnant = BusinessLayerServices.MoistureBL().GetRemnant();
            var AllRemnant = BusinessLayerServices.MoistureBL().GetRemnantByPdno(PdnoTextBox.Text);
            if (AllRemnant != null || AllRemnant.Count() > 0) remnantDataGrid.ItemsSource = AllRemnant.OrderByDescending(t => t.packingdate);
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var duplicate = BusinessLayerServices.MoistureBL().GetSingleRemnant(PdnoTextBox.Text, (DateTime)Convert.ToDateTime(PackingDatePicker.SelectedDate.Value.ToString("dd/MM/yyyy")));
            if (duplicate != null) BusinessLayerServices.MoistureBL().RemoveRemnant(duplicate);

            MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            ReloadDatagrid();
            ClearContent();
        }

        private void remnantDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (remnantDataGrid.SelectedIndex < 0) return;
                ClearContent();

                var selected = (pd_ryo_remnant)remnantDataGrid.SelectedItem;
                if (selected != null)
                {
                    PdnoTextBox.Text = selected.pdno;
                    pdDatePicker.Text = selected.pddate.ToShortDateString();
                    PackingDatePicker.Text = selected.packingdate.ToShortDateString();
                    boxTextBox.Text = selected.ryo_box.ToString();
                    bagTextBox.Text = selected.ryo_bag.ToString();
                    pouchTextBox.Text = selected.ryo_pouch.ToString();
                };
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearContent()
        {
            PackingDatePicker.SelectedDate = DateTime.Now;
            boxTextBox.Text = "";
            bagTextBox.Text = "";
            pouchTextBox.Text = "";
        }

        private void pouchTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            UpdateRemant();
        }
    }

 }

