﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RYOMoistureBL;
using DomainModelStecDbms;
using RYOMoisture.Helper;
using System.IO.Ports;
using System.Threading;
using System.Diagnostics;

namespace RYOMoisture.Form.Production
{
    public partial class Packing_MasterBagWindow : Window
    {
        public DateTime productionDate;
        public string curr_pd;
        string masterBoxBC;
        int masterBoxCase;
        pd_ryo_MasterBagConfig _config;
        Stopwatch _stw;

        private delegate void preventCrossThreading(string str);
        private preventCrossThreading accessControlFromCentralThread;

        public Packing_MasterBagWindow(string pdNo, string MboxBC, int MBoxCase, DateTime pdDate)
        {
            try
            {
                InitializeComponent();
                _stw = new Stopwatch();
                productionDate = pdDate;
                curr_pd = pdNo;
                masterBoxBC = MboxBC;
                masterBoxCase = MBoxCase;

                pdDatePicker.Text = pdDate.ToString("dd/MM/yyyy");
                pdNoTextBox.Text = pdNo;

                _config = BusinessLayerServices.pd_ryo_MasterBagConfigBL().GetByDefaultStatus();
                if (_config == null)
                    throw new ArgumentException("ไม่พบข้อมูลการตั้งค่า tare weight ในระบบ โปรดตรวจสอบอีกครั้ง");

                //tareweightTextBox.Text = "28.27"; for CY2021
                //tareweightTextBox.Text = "27.58"; for CY2022
                tareweightTextBox.Text = _config.TareWeight.ToString();

                MasterBoxTextBox.Text = masterBoxCase.ToString();
                packingDatePicker.SelectedDate = DateTime.Today;

                MasterBagBCTextBox.Focus();
                //MasterBagCasenoTextBox.Focus();

                BindingMasterBag();
                if (Properties.Settings.Default.ConnectScale == true)
                    ConnectDigitalScale();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BindingMasterBag()
        {
            //Get Existed Master Bag Data
            MasterBagDataGrid.ItemsSource = null;
            var lstMB = BusinessLayerServices.MoistureBL()
                .GetMasterBagByMasterBox(masterBoxBC);

            if (lstMB.Count() > 0)
            {
                MasterBagDataGrid.ItemsSource = lstMB.OrderByDescending(d => d.masterbag_caseno);
                TotalTextBlocks.Text = string.Format("Total {0} Bag{1}", lstMB.Count, lstMB.Count > 0 ? "s" : "");
                TotalTextBlocks1.Text = string.Format("Total {0} Gram{1}",
                    lstMB.Select(s => s.grossreal).Sum(),
                    lstMB.Select(s => s.grossreal).Sum() > 0 ? "s" : "");
            }
        }

        private void AddMasterButton_Click(object sender, RoutedEventArgs e)
        {
            AddMasterBagInfo();
        }

        private void AddMasterBagInfo()
        {
            try
            {
                if (CheckBeforeAdd())
                {
                    var existedMB = BusinessLayerServices.MoistureBL()
                        .GetSingleMasterBagByBC(MasterBagBCTextBox.Text);

                    if (existedMB != null)
                        UpdateMasterBag(existedMB);
                    else
                        AddNewMasterBag();

                    ClearControl();
                    BindingMasterBag();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddNewMasterBag()
        {
            var model = BusinessLayerServices.pd_ryo_InsertPaperBL()
                .GetSingle(MasterBagBCTextBox.Text);

            if (model == null)
                throw new ArgumentException("Master bag barcode นี้ ไม่ได้ถูกปริ้นท์ออกจากระบบ โปรดตรวจสอบข้อมูลอีกครั้งก่อนการบันทึก");

            var items = new pd_moisture_masterbag
            {
                masterbox_bc = masterBoxBC,
                caseno = masterBoxCase,
                pdno = curr_pd,
                pd_date = productionDate,
                masterbag_bc = MasterBagBCTextBox.Text,
                masterbag_caseno = Convert.ToInt16(MasterBagCasenoTextBox.Text),
                modified_date = DateTime.Now,
                taredef = !string.IsNullOrEmpty(tareweightTextBox.Text) ? Convert.ToDouble(tareweightTextBox.Text) : 0,
                netreal = Convert.ToDouble(weightTextBox.Text) - (!string.IsNullOrEmpty(tareweightTextBox.Text) ? Convert.ToDouble(tareweightTextBox.Text) : 0),
                grossreal = Convert.ToDouble(weightTextBox.Text),
                packing_date = packingDatePicker.SelectedDate
            };
            BusinessLayerServices.MoistureBL().AddMasterBag(items);
        }

        private void UpdateMasterBag(pd_moisture_masterbag item)
        {
            if (item == null)
                return;

            item.taredef = Convert.ToDouble(tareweightTextBox.Text);
            item.netreal = Convert.ToDouble(weightTextBox.Text) - Convert.ToDouble(tareweightTextBox.Text);
            item.grossreal = Convert.ToDouble(weightTextBox.Text);
            item.modified_date = DateTime.Now;
            item.packing_date = Convert.ToDateTime(packingDatePicker.Text);

            BusinessLayerServices.MoistureBL().UpdateMasterBag(item);
        }

        private void MasterBagDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (MasterBagDataGrid.SelectedIndex < 0)
                return;

            ClearControl();

            var selected = (pd_moisture_masterbag)MasterBagDataGrid.SelectedItem;
            {
                MasterBagBCTextBox.Text = selected.masterbag_bc;
                MasterBagCasenoTextBox.Text = selected.masterbag_caseno.ToString();
                tareweightTextBox.Text = selected.taredef.ToString();
                weightTextBox.Text = selected.grossreal.ToString();
                packingDatePicker.SelectedDate = selected.packing_date;
                packingDatePicker.Text = selected.packing_date != null ? selected.packing_date.ToString() : "";
            }
        }

        private void ClearControl()
        {
            MasterBagBCTextBox.Text = "";
            MasterBagCasenoTextBox.Text = "";
            tareweightTextBox.Text = "27.65";
            weightTextBox.Text = "";
            MasterBagBCTextBox.Focus();
        }

        private void MasterBagBCTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                string tmpcase;
                if (MasterBagCasenoTextBox.Text.Length == 0)
                    _stw.Start();

                if (e.Key != Key.Enter)
                    return;

                _stw.Stop();
                var inputTime = _stw.Elapsed.Seconds * 1000 + _stw.Elapsed.Milliseconds;
                if (inputTime > 200)
                {
                    MessageBoxHelper.Warning("ต้องใช้เครื่องสแกนบาร์โค้ตเท่านั้น!!");
                    MasterBagBCTextBox.Clear();
                    MasterBagBCTextBox.Focus();
                    _stw.Reset();
                    return;
                }
                _stw.Reset();
                MasterBagCasenoTextBox.Text = "";

                if (string.IsNullOrEmpty(MasterBagBCTextBox.Text))
                {
                    MessageBoxHelper.Warning("โปรดสแกนหรือกรอกหมายเลขบาร์โค้ต");
                    MasterBagBCTextBox.Focus();
                    return;
                }

                if (MasterBagBCTextBox.Text.Length != 15)
                {
                    MessageBoxHelper.Warning("รหัสบาร์โค้ตจะต้องมีจำนวน 15 หลักเท่านั้น");
                    MasterBagBCTextBox.Clear();
                    MasterBagBCTextBox.Focus();
                    return;
                }

                if (!RegularExpressionHelper.IsNumericCharacter(MasterBagBCTextBox.Text))
                {
                    MessageBoxHelper.Warning("ระบบอนุญาตให้รหัสบาร์โค้ตประกอบด้วยตัวเลขเท่านั้น");
                    MasterBagBCTextBox.Clear();
                    MasterBagBCTextBox.Focus();
                    return;
                }

                tmpcase = MasterBagBCTextBox.Text.Substring(4, 6);
                if (tmpcase != MasterBoxTextBox.Text.PadLeft(6, '0'))
                {
                    MessageBoxHelper.Warning("เช็คเลข Insert paper ให้ตรงกับ Masterbox");
                    MasterBagBCTextBox.Clear();
                    MasterBagBCTextBox.Focus();
                    return;
                }

                if (Convert.ToInt16(MasterBagBCTextBox.Text.Substring(10, 2)) > 20)
                {
                    MessageBoxHelper.Warning("เช็คเลขห่อของรหัสบาร์โค้ตนี้, ไม่ควรเกิน 20 ห่อ");
                    MasterBagBCTextBox.Clear();
                    MasterBagBCTextBox.Focus();
                    return;
                }
                else
                {
                    MasterBagCasenoTextBox.Text = Convert.ToInt16(MasterBagBCTextBox.Text.Substring(10, 2)).ToString();
                    weightTextBox.Focus();
                    weightTextBox.SelectAll();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MasterBagCasenoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            if (string.IsNullOrEmpty(MasterBagCasenoTextBox.Text))
                return;

            weightTextBox.Focus();

        }

        private static string CreateMasterbagBardcoe()
        {
            string tmpBarcode;
            string lastestBC;
            //Get MasterbagBC
            lastestBC = BusinessLayerServices.MoistureBL().GetLastestMasterBagBC().ToString("D6");
            tmpBarcode = "CP" + DateTime.Now.Year.ToString().Substring(2, 2) + "-" + lastestBC;

            return tmpBarcode;
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            var existedMB = BusinessLayerServices.MoistureBL()
                .GetSingleMasterBagByBC(MasterBagBCTextBox.Text);
            if (existedMB != null)
                BusinessLayerServices.MoistureBL().RemoveMasterBag(existedMB);

            MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            BindingMasterBag();
            ClearControl();
        }

        private bool CheckBeforeAdd()
        {
            bool readyForsave = true;
            string tmpcase;
            //Get Barcode for DB when MasterBagBC empty
            if (string.IsNullOrEmpty(MasterBagBCTextBox.Text))
                throw new Exception("กรุณาระบุ Case No."); //MasterBagBCTextBox.Text = CreateMasterbagBardcoe();

            if (string.IsNullOrEmpty(MasterBagCasenoTextBox.Text))
                throw new Exception("กรุณาระบุ Case No.");

            if (string.IsNullOrEmpty(weightTextBox.Text))
                throw new Exception("กรุณาระบุน้ำหนักของ Master Bag");

            if (!RegularExpressionHelper.IsNumericCharacter(MasterBagCasenoTextBox.Text))
            {
                MessageBoxHelper.Warning("ระบบอนุญาตให้หมายเลขห่อยาประกอบด้วยตัวเลขเท่านั้น");
                MasterBagCasenoTextBox.Text = "";
                MasterBagCasenoTextBox.Focus();
                return false;
            }

            if (Convert.ToInt32(MasterBagCasenoTextBox.Text) > 20)
            {
                MessageBoxHelper.Warning("Master Bag ไม่ควรมีเกิน 20 ห่อ, กรุณาเช็คเลข Case No.");
                MasterBagCasenoTextBox.Text = "";
                MasterBagCasenoTextBox.Focus();
                return false;
            }

            if (!RegularExpressionHelper.IsNumericCharacter(weightTextBox.Text))
            {
                MessageBoxHelper.Warning("ระบบอนุญาตให้บันทึกน้ำหนักด้วยจำนวนเต็มเท่านั้น");
                weightTextBox.Text = "";
                weightTextBox.Focus();
                return false;
            }

            //Alert When < 318 g.
            if (Convert.ToDecimal(weightTextBox.Text) < _config.MinWeight)
            {
                MessageBoxHelper.Warning("น้ำหนักของ Master Bag ไม่ควรน้อยกว่า " + _config.MinWeight + " กรัม");
                weightTextBox.Text = "";
                weightTextBox.Focus();
                return false;
            }

            //Alert When > 342
            if (Convert.ToDecimal(weightTextBox.Text) > _config.MaxWeight)
            {
                MessageBoxHelper.Warning("น้ำหนักของ Master Bag ไม่ควรมากกว่า " + _config.MaxWeight + " กรัม");
                weightTextBox.Text = "";
                weightTextBox.Focus();
                return false;
            }

            var existedBC = BusinessLayerServices.ProductBL().GetSingleProduct(masterBoxBC);
            if (existedBC == null)
            {
                MessageBoxHelper.Warning("Barcode ของ Master Box ห่อนี้ไม่มีอยู่ในระบบ กรุณาเช็คอีกครั้ง");
                return false;
            }

            var duplicate = BusinessLayerServices.MoistureBL()
                .GetSingleMasterBagByCase(masterBoxBC, Convert.ToInt32(MasterBagCasenoTextBox.Text));

            if (duplicate != null)
            {
                MessageBoxHelper.Warning("ห่อนี้ได้บันทึกน้ำหนัก ไปเรียบร้อยแล้ว กรุณาเช็คอีกครั้ง");
                return false;
            }

            tmpcase = MasterBagBCTextBox.Text.Substring(4, 6);
            if (tmpcase != MasterBoxTextBox.Text.PadLeft(6, '0'))
            {
                MessageBoxHelper.Warning("เช็คเลข Insert paper ให้ตรงกับ Masterbox");
                return false;
            }

            return readyForsave;
        }

        private void weightTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                AddMasterBagInfo();
        }

        private void RemoveAllButton_Click(object sender, RoutedEventArgs e)
        {
            var lstMB = BusinessLayerServices.MoistureBL().GetMasterBagByMasterBox(masterBoxBC);
            if (lstMB.Count() > 0)
            {
                if (MessageBoxHelper.Question("ต้องการลบข้อมูล Master Bag ทั้งหมด " + lstMB.Count + " ห่อ, ใช่มั้ย") == MessageBoxResult.No)
                    return;

                var existedMB = BusinessLayerServices.MoistureBL().GetSingleMasterBoxByMasterBox(masterBoxBC);
                if (existedMB != null)
                {
                    BusinessLayerServices.MoistureBL().RemoveAllMasterBagByMasterBox(masterBoxBC);
                    MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
                }
                BindingMasterBag();
                ClearControl();
            }

        }

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (!Decimal.TryParse(str, out value))
                {
                    weightTextBox.Dispatcher.Invoke(() =>
                    {
                        weightTextBox.Text = str;
                    });
                    return;
                }

                var _weight = Convert.ToDouble(str);

                if (weightTextBox.Dispatcher.CheckAccess())
                    weightTextBox.Text = _weight.ToString();
                else
                    weightTextBox.Dispatcher.Invoke(() =>
                    {
                        // A number should be an integer (not a decimal).
                        weightTextBox.Text = _weight.ToString("N0");
                    });
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ConnectDigitalScale()
        {
            try
            {
                DigitalScaleHelper.Setup();

                if (DigitalScaleHelper.serialPort.IsOpen == true)
                    throw new ArgumentException("มีโปรแกรมอื่นกำลังใช้งาน port นี้อยู่ ให้ปิดโปรแกรมแล้วลองเข้าใหม่ หรือ restart คอมพิวเตอร์ 1 ครั้ง");

                DigitalScaleHelper.serialPort.Open();
                DigitalScaleHelper.serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                accessControlFromCentralThread = displayTextReadIn;

                weightTextBox.IsReadOnly = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            displayTextReadIn(DigitalScaleHelper.GetWeightResultFromTigerModel());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Helper.DigitalScaleHelper.serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing
                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang
                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close(); //close the serial port
                this.Dispatcher.Invoke(() =>
                {
                    this.Close();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void CloseSerialNonExit()
        {
            try
            {
                Helper.DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                Helper.DigitalScaleHelper.serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }
    }
}
