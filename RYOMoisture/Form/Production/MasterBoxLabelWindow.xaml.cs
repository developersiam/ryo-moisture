﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoisture.Model;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Production
{
    public partial class MasterBoxLabelWindow : Page
    {
        string cusRuning;
        public MasterBoxLabelWindow()
        {
            InitializeComponent();
            BlindingCustomer();
        }

        private void ReloadUserAccountDataGrid()
        {
            if (CropTextBox.Text != "")
            {
                var i = BusinessLayerServices.pd_ryo_customer_label_setupBL().GetByCrop(Convert.ToInt16(CropTextBox.Text));
                if (i.Any())
                {
                    UserAccountDataGrid.ItemsSource = null;
                    UserAccountDataGrid.ItemsSource = i;

                    
                    var labelSetting = new List<m_PdRYOSettingVM>();
                    foreach (var l in i)
                    {
                        var vm = new m_PdRYOSettingVM
                        {
                            crop = l.crop,
                            pdno = l.pdno,
                            pdDate = l.pdDate,
                            packedgrade = l.packedgrade,
                            customerID = l.customerID,
                            customerName = l.customerName,
                            MasterBoxLabel = l.customerID + cusRuning + "000001",
                        };
                        labelSetting.Add(vm);
                    };
   
                    UserAccountDataGrid.ItemsSource = null;
                    UserAccountDataGrid.ItemsSource = labelSetting;

                    TotalTextBlock.Text = string.Format("Total {0} item{1}", i.Count, i.Count > 0 ? "s" : "");
                }
            }
        }

        private void BlindingCustomer()
        {
            var i = BusinessLayerServices.pd_ryo_customer_labelBL().Get();
            CustomerCombobox.ItemsSource = null;
            CustomerCombobox.ItemsSource = i;           
        }

        private void CropTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //Get all grade by Crop
                PdnoCombobox.ItemsSource = null;
                PdnoCombobox.ItemsSource = BusinessLayerServices.PdSetupBL().GetPdNoByCrop(Convert.ToInt16(CropTextBox.Text));

                //Convert year
                cusRuning = (Convert.ToInt16(CropTextBox.Text) + 543).ToString().Substring(2, 2);

                BlindingCustomer();
                ReloadUserAccountDataGrid();
            }
        }

        private void CropTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (CropTextBox.Text != "")
            {
                PdnoCombobox.ItemsSource = null;
                PdnoCombobox.ItemsSource = BusinessLayerServices.PdSetupBL().GetPdNoByCrop(Convert.ToInt16(CropTextBox.Text));

                //Convert year
                cusRuning = (Convert.ToInt16(CropTextBox.Text) + 543).ToString().Substring(2, 2);

                BlindingCustomer();
                ReloadUserAccountDataGrid();
            }
        }

        private void PdnoCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (PdnoCombobox.SelectedValue == null) return;
                GradeTextBox.Text = BusinessLayerServices.PdSetupBL().GetSinglePDSetup(PdnoCombobox.SelectedValue.ToString()).packedgrade;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerCombobox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (CustomerCombobox.SelectedValue == null) return;
                var cust_code = CustomerCombobox.SelectedValue.ToString();
                if (cust_code == "00") cust_code = "";

                FixTextBox.Text = cust_code + cusRuning +"000001";
                //----Prevent duplicate adding----
                AddButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void UserAccountDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //string C48barcode;
            if (UserAccountDataGrid.SelectedIndex < 0) return;

            var selected = (m_PdRYOSettingVM)UserAccountDataGrid.SelectedItem;
            if (selected != null)
            {
                CropTextBox.Text = selected.crop.ToString();
                PdnoCombobox.SelectedValue = selected.pdno;
                GradeTextBox.Text = selected.packedgrade;
                CustomerCombobox.Text = selected.customerName;
                FixTextBox.Text = selected.MasterBoxLabel;

                AddButton.IsEnabled = true;
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropTextBox.Text != "" & PdnoCombobox.Text != "" & CustomerCombobox.Text != "")
                {
                    var setuplst = BusinessLayerServices.pd_ryo_customer_label_setupBL()
                        .GetSingle(PdnoCombobox.SelectedValue.ToString());

                    if (setuplst != null)
                        UpdateLabelSetting();                       
                    else
                        AddLabelSetting();

                    ReloadUserAccountDataGrid();
                    MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                }
                else
                    MessageBoxHelper.Warning("กรุณากรอกข้อมูลให้ครบถ้วนก่อนบันทึก");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddLabelSetting()
        {
            var pdS = BusinessLayerServices.PdSetupBL()
                .GetSinglePDSetup(PdnoCombobox.SelectedValue.ToString());

            var cl = new pd_ryo_customer_label_setup
            {
                crop = Convert.ToInt16(CropTextBox.Text),
                pdno = pdS.pdno,
                pdDate = pdS.date,
                packedgrade = pdS.packedgrade,
                customerID = CustomerCombobox.SelectedValue.ToString(),
                customerName = CustomerCombobox.Text,
                modifiedDate = DateTime.Now,
                User = user_setting.User.Username
            };
            BusinessLayerServices.pd_ryo_customer_label_setupBL().Add(cl);
        }

        private void UpdateLabelSetting()
        {
            var pdS = BusinessLayerServices.PdSetupBL().GetSinglePDSetup(PdnoCombobox.SelectedValue.ToString());

            var cl = new pd_ryo_customer_label_setup
            {
                crop = Convert.ToInt16(CropTextBox.Text),
                pdno = pdS.pdno,
                pdDate = pdS.date,
                packedgrade = pdS.packedgrade,
                customerID = CustomerCombobox.SelectedValue.ToString(),
                customerName = CustomerCombobox.Text,
                modifiedDate = DateTime.Now,
                User = user_setting.User.Username
            };
            BusinessLayerServices.pd_ryo_customer_label_setupBL().Edit(cl);
        }
        private void DeleteDetailBtton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var oldItem = (pd_ryo_customer_label_setup)UserAccountDataGrid.SelectedItem;
                if (oldItem != null)
                {
                    if (MessageBoxHelper.Question("ต้องการที่จะลบข้อมูลนี้ใช่หรือไม่") == MessageBoxResult.Yes)
                    {
                        BusinessLayerServices.pd_ryo_customer_label_setupBL().Delete(oldItem.pdno);
                        MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
                        ReloadUserAccountDataGrid();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
