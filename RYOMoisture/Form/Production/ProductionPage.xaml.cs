﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModelStecDbms;
using RYOMoistureBL;
using RYOMoisture.Helper;

namespace RYOMoisture.Form.Production
{

    public partial class ProductionPage : Page
    {
        public ProductionPage()
        {
            InitializeComponent();
            ShowCurrentProduction();
        }
        private void ShowCurrentProduction()
        {
            ProductionListDataGrid.ItemsSource = null;
            var curr_pd = BusinessLayerServices.ProductBL().GetCurrentProduction();
            if (curr_pd != null) ProductionListDataGrid.ItemsSource = curr_pd;
        }

        private void SelectedButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var pd = (sp_Packing_Get_Current_PD_Result)ProductionListDataGrid.SelectedItem;
                if (pd != null)
                {
                    //string UName = Properties.Settings.Default.UserPacking;                  
                    Packing_ChampBoxWindow w = new Packing_ChampBoxWindow(pd.pdno);
                    w.Title = string.Format("Packing System");
                    w.ShowDialog();             
                }
                else
                {
                    MessageBoxHelper.Warning("กรุณาเลือก Production No.!!");
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
