﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using RYOMoistureBL;
using DomainModelStecDbms;
using RYOMoistureBL.Models;
using RYOMoisture.Helper;

namespace RYOMoisture.Form.Production
{

    public partial class FinishGradeWindow : Window
    {
        public DateTime FinishDate;
        public FinishGradeWindow(DateTime pdDate)
        {
            InitializeComponent();
            FinishDate = pdDate;
            PdDate.Text = pdDate.ToString("dd/MM/yyyy");
            ShowPackedGrade();
        }

        private void ShowPackedGrade()
        {
            string t_status = "";
            FinishDataGrid.ItemsSource = null;
            //var curr_PD = BusinessLayerServices.PdSetupBL().GetProductionByDate();
            var curr_PD = BusinessLayerServices.ProductBL().GetCurrentProduction();
            if (curr_PD != null || curr_PD.Count() > 0)
            {
                List<FinishPacked> tmpCurr_pd = new List<FinishPacked>();
                foreach (var item in curr_PD)
                {
                    t_status = "";
                    if ((bool)item.packinglocked)
                        t_status = "Finish";

                    FinishPacked f = new FinishPacked()
                    {
                        pdno = item.pdno,
                        packedgrade = item.packedgrade,
                        packinglocked = t_status
                    };
                    tmpCurr_pd.Add(f);
                }
                FinishDataGrid.ItemsSource = tmpCurr_pd;
            }
        }
        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            var selected = (FinishPacked)FinishDataGrid.SelectedItem;
            FinishGrade(selected.pdno);

            ShowPackedGrade();

            MessageBoxHelper.Info("Finish Grade Complete");
        }
        private void FinishGrade(string pdno)
        {
            try
            {
                var curr_pdSetup = BusinessLayerServices.PdSetupBL().GetSinglePDSetup(pdno);
                {
                    pdsetup packingLocked = new pdsetup()
                    {
                        crop = curr_pdSetup.crop,
                        date = curr_pdSetup.date,
                        pdno = curr_pdSetup.pdno,
                        packedgrade = curr_pdSetup.packedgrade,
                        def = curr_pdSetup.def,
                        locked = curr_pdSetup.locked,
                        blendinglocked = curr_pdSetup.blendinglocked,
                        packinglocked = true,
                        byproductlocked = curr_pdSetup.byproductlocked,
                        pickinglocked = curr_pdSetup.pickinglocked,
                        dtrecord = curr_pdSetup.dtrecord,
                        user = curr_pdSetup.user,
                        mode = curr_pdSetup.mode,
                        casenost = curr_pdSetup.casenost,
                        pdremark = curr_pdSetup.pdremark,
                        PdRemarkParent = curr_pdSetup.PdRemarkParent,
                        CusRunID = curr_pdSetup.CusRunID,
                    };
                    BusinessLayerServices.PdSetupBL().UpdatePdSetup(packingLocked);
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
