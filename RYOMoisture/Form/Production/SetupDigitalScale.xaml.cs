﻿using RYOMoisture.Helper;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Production
{
    /// <summary>
    /// Interaction logic for SetupDigitalScale.xaml
    /// </summary>
    public partial class SetupDigitalScale : Window
    {
        public string _portName { get; set; }
        public int _buardRate { get; set; }
        public Parity _parity { get; set; }
        public StopBits _stopBit { get; set; }
        public short _dataBit { get; set; }
        public int _threadSleep { get; set; }

        public SetupDigitalScale()
        {
            InitializeComponent();

            _portName = Properties.Settings.Default.PortName;
            _buardRate = Properties.Settings.Default.BaudRate;
            _dataBit = Properties.Settings.Default.DataBit;
            _parity = Properties.Settings.Default.Parity;
            _stopBit = Properties.Settings.Default.StopBits;
            _threadSleep = Properties.Settings.Default.ThreadSleep;

            var portNameList = new List<string>();
            foreach (var item in SerialPort.GetPortNames())
                portNameList.Add(item);
            portNameComboBox.ItemsSource = portNameList;
            portNameComboBox.SelectedValue = _portName;

            var buardRateList = new List<int>();
            buardRateList.Add(115200);
            buardRateList.Add(57600);
            buardRateList.Add(38400);
            buardRateList.Add(19200);
            buardRateList.Add(9600);
            buardRateList.Add(7200);
            buardRateList.Add(4800);
            buardRateList.Add(2400);
            buardRateComboBox.ItemsSource = buardRateList;
            buardRateComboBox.SelectedValue = _buardRate;

            var dataBitList = new List<short>();
            dataBitList.Add(8);
            dataBitList.Add(7);
            dataBitList.Add(6);
            dataBitList.Add(5);
            dataBitComboBox.ItemsSource = dataBitList;
            dataBitComboBox.SelectedValue = _dataBit;

            var parityList = new List<Parity>();
            parityList.Add(Parity.Even);
            parityList.Add(Parity.Odd);
            parityList.Add(Parity.None);
            parityList.Add(Parity.Mark);
            parityList.Add(Parity.Space);
            parityComboBox.ItemsSource = parityList;
            parityComboBox.SelectedValue = _parity;

            var stopBitList = new List<StopBits>();
            stopBitList.Add(StopBits.None);
            stopBitList.Add(StopBits.One);
            stopBitList.Add(StopBits.OnePointFive);
            stopBitList.Add(StopBits.Two);
            stopBitComboBox.ItemsSource = stopBitList;
            stopBitComboBox.SelectedValue = _stopBit;

            var threadSleepList = new List<int>();
            threadSleepList.Add(100);
            threadSleepList.Add(200);
            threadSleepList.Add(300);
            threadSleepList.Add(400);
            threadSleepList.Add(500);
            threadSleepList.Add(600);
            threadSleepList.Add(700);
            threadSleepList.Add(800);
            threadSleepList.Add(900);
            threadSleepList.Add(1000);
            threadSleepComboBox.ItemsSource = threadSleepList;
            threadSleepComboBox.SelectedValue = _threadSleep;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(_portName))
                {
                    MessageBoxHelper.Warning("โปรดระบุ PortName");
                    portNameComboBox.Focus();
                }
                if (string.IsNullOrEmpty(_buardRate.ToString()))
                {
                    MessageBoxHelper.Warning("โปรดระบุ BuardRate");
                    buardRateComboBox.Focus();
                }
                if (string.IsNullOrEmpty(_dataBit.ToString()))
                {
                    MessageBoxHelper.Warning("โปรดระบุ DataBit");
                    dataBitComboBox.Focus();
                }
                if (string.IsNullOrEmpty(_threadSleep.ToString()))
                {
                    MessageBoxHelper.Warning("โปรดระบุ ThreadSleep");
                    threadSleepComboBox.Focus();
                }

                Properties.Settings.Default.PortName = portNameComboBox.SelectedValue.ToString();
                Properties.Settings.Default.BaudRate = (int)(buardRateComboBox.SelectedValue);
                Properties.Settings.Default.StopBits = (StopBits)stopBitComboBox.SelectedValue;
                Properties.Settings.Default.DataBit = (short)dataBitComboBox.SelectedValue;
                Properties.Settings.Default.Parity = (Parity)parityComboBox.SelectedValue;
                Properties.Settings.Default.ThreadSleep = (int)threadSleepComboBox.SelectedValue;

                Properties.Settings.Default.Save();

                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ!");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
