﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DomainModelStecDbms;
using RYOMoistureBL;
using RYOMoisture.Model;
using RYOMoisture.Shared;
using RYOMoistureBL.Models;
using RYOMoisture.Helper;

namespace RYOMoisture.Form.Production
{
    public partial class Packing_ChampBoxWindow : Window
    {
        private string CurrpdNo;
        public string UName;
        private string packedGrade;
        private string mode;
        public string pdMode;
        private int currentYear;
        private string frombc;
        private int fromcaseno;
        private double tmpNetReal = 0;
        private bool tmpBox = true;
        private string tmpBC;

        public Packing_ChampBoxWindow(string pdno)
        {
            InitializeComponent();
            CurrpdNo = pdno;
            ShowProductionDetails();
            ShowProductionBale();
            ChangeProductionPeriod();
            UName = user_setting.User.Username;
            MdcDatePicker.SelectedDate = DateTime.Now;
            BayCombobox.ItemsSource = BusinessLayerServices.ProductBL().GetCutragShippingBay();
            BayCombobox.SelectedIndex = -1;
            thresherTextBox.Text = "Teerapong";
            redryTextBox.Text = "Noraset";
            packingEndTextBox.Text = "Patcharinc";
            pdMode = "Lamina";
            ModeShowTextBox.Text = pdMode;
            modeTextBox.Text = pdMode;
        }

        private void ShowProductionDetails()
        {
            try
            {
                DateTime pdDate;
                var curr_pdSetup = BusinessLayerServices.PdSetupBL().GetSinglePDSetup(CurrpdNo);
                {
                    pdDate = curr_pdSetup.date.HasValue ? curr_pdSetup.date.Value : DateTime.Now;

                    pdNoTextBox.Text = curr_pdSetup.pdno;
                    pdDatePicker.SelectedDate = pdDate;
                    pdDatePicker.Text = pdDate.ToString("dd/MM/yyyy");
                    packGradeTextBox.Text = curr_pdSetup.packedgrade;
                    packedGrade = curr_pdSetup.packedgrade;

                    periodComboBox.ItemsSource = BusinessLayerServices.ProductBL().GetPdHours(curr_pdSetup.pdno);
                    //periodComboBox.SelectedIndex = periodComboBox.Items.Count - 1;
                    periodComboBox.SelectedIndex = 0;
                    pdRemarkTextBox.Text = curr_pdSetup.pdremark;
                    pdRemarkParentTextBox.Text = curr_pdSetup.PdRemarkParent;

                    netAvgTextBox.Text = string.Format("{0:N2}", BusinessLayerServices.ProductBL()
                        .GetNetAvg(packedGrade, pdDate));
                    mode = curr_pdSetup.mode;
                    currentYear = (int)curr_pdSetup.crop;
                }
                //FOR PackedGrade
                ShowPackedDetails(packedGrade);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShowPackedDetails(string Curr_packedGrade)
        {
            try
            {
                var curr_packed = BusinessLayerServices.PackedGradeBL()
                    .GetPackedGrade(Curr_packedGrade);
                {
                    typeTextBox.Text = curr_packed.type;
                    customerTextBox.Text = curr_packed.customer;
                    fromTextBox.Text = curr_packed.form;
                    packingMatTextBox.Text = curr_packed.packingmat;
                    netTextBox.Text = string.Format("{0:N2}", curr_packed.netdef);
                    tareTextBox.Text = string.Format("{0:N2}", curr_packed.taredef);

                    //Grid
                    tareGridTextBox.Text = string.Format("{0:N2}", curr_packed.taredef);
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ShowProductionBale()
        {
            try
            {
                string tmpBox = "";
                var pd_bales = BusinessLayerServices.ProductBL()
                    .GetPdList(CurrpdNo, Convert.ToInt32(periodComboBox.Text))
                    .OrderByDescending(w => w.caseno);

                if (pd_bales != null && pd_bales.Count() > 0)
                {
                    //PackingDataGrid.ItemsSource = pd_bales;
                    List<pdPacked> tmpCurr_pd = new List<pdPacked>();
                    foreach (var item in pd_bales)
                    {
                        tmpBox = "Old Box";
                        if ((bool)item.box)
                            tmpBox = "New Box";

                        //finding AB4 and masterbox 
                        fromcaseno = 0;
                        frombc = "";

                        var MasterBox = BusinessLayerServices.MoistureBL()
                            .GetSingleMasterBoxByMasterBox(item.bc);

                        if (MasterBox != null)
                        {
                            frombc = MasterBox.frombc;
                            fromcaseno = MasterBox.from_caseno;
                        }
                        pdPacked p = new pdPacked()
                        {
                            ab4_bc = frombc,
                            ab4_caseno = fromcaseno == 0 ? 0 : fromcaseno,
                            bc = item.bc,
                            caseno = (int)item.caseno,
                            grossreal = (decimal)item.grossreal,
                            boxtare = item.boxtare,
                            taredef = item.taredef,
                            netreal = item.netreal,
                            packingtime = item.packingtime,
                            box = tmpBox,
                            packingdate = item.packingdate,
                            dtrecord = item.dtrecord,
                            bay = item.bay
                        };
                        tmpCurr_pd.Add(p);
                    }
                    PackingDataGrid.ItemsSource = tmpCurr_pd;

                    var sTime = pd_bales.Select(w => w.packingtime).First();
                    var fTime = pd_bales.Select(w => w.packingtime).Last();
                    startTextBox.Text = string.Format("{0:hh:mm:ss tt}", sTime);
                    FinishTextBox.Text = string.Format("{0:hh:mm:ss tt}", fTime);
                }
                else
                    PackingDataGrid.ItemsSource = null;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PackingDataGrid.SelectedIndex < 0)
                    return;

                var selected = (pdPacked)PackingDataGrid.SelectedItem;
                if (selected != null)
                {
                    fromcasenoTextBox.Text = selected.ab4_caseno.ToString();
                    barcodeTextBox.Text = selected.bc;
                    caseNoTextBox.Text = selected.caseno.ToString();
                    grossTextBox.Text = string.Format("{0:N2}", selected.grossreal);
                    boxTareTextBox.Text = string.Format("{0:N2}", selected.boxtare);
                    tareGridTextBox.Text = string.Format("{0:N2}", selected.taredef);
                    netRealTextBox.Text = string.Format("{0:N2}", selected.netreal);
                    timeTextBox.Text = string.Format("{0:hh:mm:ss tt}", selected.packingtime);
                    boxTextBox.Text = selected.box;
                    BayCombobox.Text = selected.bay;

                    if (selected.caseno == 0 || selected.bc.Substring(0, 3) == "RNR")
                        pdMode = "Remnant";
                    else
                        pdMode = "Lamina";

                    modeTextBox.Text = pdMode;
                    ModeShowTextBox.Text = pdMode;

                    //AB4
                    if (!string.IsNullOrEmpty(selected.ab4_bc))
                        AB4GradeTextBox.Text = BusinessLayerServices.ProductBL()
                            .GetSingleProduct(selected.ab4_bc).grade.ToString();

                    AB4caseNoTextBox.Text = selected.ab4_caseno == 0 ? "" : selected.ab4_caseno.ToString();
                    AB4barcodeTextBox.Text = selected.ab4_bc;
                };
                grossTextBox.SelectAll();
                grossTextBox.Focus();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ClearControl()
        {
            barcodeTextBox.Text = "";
            caseNoTextBox.Text = "";
            grossTextBox.Text = "";
            boxTareTextBox.Text = "";
            netRealTextBox.Text = "";
            timeTextBox.Text = "";
            boxTextBox.Text = "";
            tareGridTextBox.Text = "";

            tareGridTextBox.Text = tareTextBox.Text;
            tareGridTextBox.IsEnabled = false;

            AB4GradeTextBox.Text = "";
            AB4caseNoTextBox.Text = "";
            AB4barcodeTextBox.Text = "";
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            if (PrepareBFSave() == true)
            {
                //Check Existed Barcode before Save
                var existedBC = BusinessLayerServices.ProductBL().GetSingleProduct(barcodeTextBox.Text);
                if (existedBC == null)
                {
                    MessageBoxHelper.Warning("ข้อมูล Barcode นี้ไม่มีอยู่ในระบบ กรุณา Save ก่อน Print");
                    barcodeTextBox.Focus();
                    return;
                }

                if (notPrintCheckBox.IsChecked == false)
                    PrintBarcode();
            }
        }

        private void PrintBarcode()
        {
            //-------- Check Label Setting ---------
            var existed = BusinessLayerServices.pd_ryo_customer_label_setupBL().GetSingle(pdNoTextBox.Text);
            if (existed == null)
                throw new Exception("ยังไม่มีการ setting Customer Label, กรุณา Setup ที่เมนู Customer Label ก่อน");

            //PrintBarcodeTSC();
            PrintBarcodeCHAMP();
        }

        private void PrintBarcodeCHAMP()
        {
            try
            {
                string cusRuning;
                string cusCode;

                //cusRuning = (currentYear + 543).ToString().Substring(2, 2) + Convert.ToInt16(caseNoTextBox.Text).ToString("D6");
                //var pd = BusinessLayerServices.ProductBL().GetSingleProduct(barcodeTextBox.Text);
                //var packingdate = Convert.ToDateTime(pd.packingdate);

                var existed = BusinessLayerServices.pd_ryo_customer_label_setupBL().GetSingle(pdNoTextBox.Text);
                if (existed == null)
                    throw new ArgumentException("ไม่พบการตั้งค่าข้อมูล Master Box Customer Label โปรดตรวจสอบการตั้งค่าอีกครั้ง");

                if (existed.customerID == "00")
                    cusCode = "";
                else
                    cusCode = existed.customerID;

                if (string.IsNullOrEmpty(barcodeTextBox.Text))
                    throw new ArgumentException("ไม่พบรหัสบาร์โค้ตที่ต้องการจะปริ้นท์สติกเกอร์");

                cusRuning = cusCode + (currentYear + 543).ToString().Substring(2, 2) + Convert.ToInt16(caseNoTextBox.Text).ToString("D6");
                var d5 = Convert.ToInt32(cusRuning.Substring(4, 1));
                var d6 = Convert.ToDouble(cusRuning.Substring(5, 1));
                var d7 = Convert.ToDouble(cusRuning.Substring(6, 1));
                var d8 = Convert.ToDouble(cusRuning.Substring(7, 1));
                var d9 = Convert.ToDouble(cusRuning.Substring(8, 1));
                var d10 = Convert.ToDouble(cusRuning.Substring(9, 1));

                var sum5to10 = d5 + d6 + d7 + d8 + d9 + d10;
                var x2 = Math.Pow(sum5to10, 2).ToString().PadLeft(2, '0');
                var last2Digit = x2.Substring(x2.Length - 2, 2);
                var b = Convert.ToDouble(last2Digit.Substring(0, 1));
                var a = Convert.ToDouble(last2Digit.Substring(1, 1));
                var formula = (Math.Pow(2, a) * Math.Pow(3, b)).ToString();
                var R11 = b.ToString();
                var R12 = a.ToString();
                var R13 = formula.ToString().Substring(formula.Length - 1, 1);
                //var test = cusRuning + R11 + R12 + R13;

                //var pd = BusinessLayerServices.PdSetupBL().GetSinglePDSetup(pdNoTextBox.Text);
                //var packingdate = Convert.ToDateTime(pd.date);

                var pd = BusinessLayerServices.ProductBL().GetSingleProduct(barcodeTextBox.Text);
                if (pd == null)
                    throw new ArgumentException("ไม่พบข้อมูลกล่องยาหมายเลขบาร์โค้ตนี้ในระบบ โปรดลองใหม่อีกครั้ง หรือแจ้งแผนกไอทีเพื่อตรวจสอบข้อมูล");

                if (pd.packingdate == null)
                    throw new ArgumentException("ไม่พบข้อมูล packingdate");

                var mdc = Convert.ToDateTime(pd.packingdate);
                var mdcConvert = mdc.Day.ToString().PadLeft(2, '0') + "/" +
                    mdc.Month.ToString().PadLeft(2, '0') + "/" +
                    (mdc.Year + 543).ToString().PadLeft(2, '0');

                RYOsystemModule.openport("TSC TTP-247");
                RYOsystemModule.setup("70", "35", "2.0", "6", "0", "0", "0");
                RYOsystemModule.sendcommand("GAP 2 mm,0");
                RYOsystemModule.sendcommand("DIRECTION 1");
                RYOsystemModule.clearbuffer();

                RYOsystemModule.windowsfont(160, 20, 54, 0, 0, 0, "arial", cusRuning + R11 + R12 + R13);
                RYOsystemModule.barcode("153", "80", "128", "60", "0", "0", "3", "2", cusRuning + R11 + R12 + R13);
                RYOsystemModule.windowsfont(80, 154, 60, 0, 0, 0, "arial", "MDC : " + mdcConvert);
                RYOsystemModule.barcode("142", "216", "128", "40", "0", "0", "2", "2", barcodeTextBox.Text);

                //RYOsystemModule.windowsfont(92, 4, 96, 0, 0, 0, "arial", cusRuning);
                //RYOsystemModule.barcode("200", "96", "128", "80", "0", "0", "2", "2", cusRuning);
                //RYOsystemModule.windowsfont(192, 184, 24, 0, 0, 0, "arial", "MDC : " + mdc);
                //RYOsystemModule.barcode("136", "216", "128", "40", "0", "0", "2", "2", barcodeTextBox.Text);


                RYOsystemModule.printlabel("1", "1");
                RYOsystemModule.closeport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintBarcodeTSC()
        {
            int LengthGrade = 0;
            int LengthCase = 0;
            double gross = 0;
            double netR = 0;
            string grossDefault = "";
            string netDefault = "";
            string netReal = "";
            string tareReal = "";

            LengthGrade = packGradeTextBox.Text.Length;
            LengthCase = caseNoTextBox.Text.Length;

            tmpNetReal = Convert.ToDouble(grossTextBox.Text) - Convert.ToDouble(boxTareTextBox.Text);
            gross = Convert.ToDouble(netTextBox.Text) + Convert.ToDouble(tareTextBox.Text);
            netR = Convert.ToDouble(grossTextBox.Text) - Convert.ToDouble(boxTareTextBox.Text);

            netRealTextBox.Text = string.Format("{0:N2}", tmpNetReal);
            grossDefault = string.Format("{0:N1}", gross);
            netDefault = string.Format("{0:N1}", netTextBox.Text);
            tareReal = string.Format("{0:N2}", boxTareTextBox.Text);
            netReal = string.Format("{0:N2}", netR);


            //================================ TSC Printer ===============================
            RYOsystemModule.openport("TSC TTP-247");
            RYOsystemModule.setup("103", "76", "2.0", "1", "0", "0", "0");
            RYOsystemModule.sendcommand("GAP 3 mm,0");
            RYOsystemModule.sendcommand("DIRECTION 1");
            RYOsystemModule.clearbuffer();

            RYOsystemModule.sendcommand("BAR 50,25,4,510");
            RYOsystemModule.sendcommand("BAR 820,25,4,510");
            RYOsystemModule.sendcommand("BAR 150,210,4,325");
            RYOsystemModule.sendcommand("BAR 50,25,770,4");

            //Grade
            switch (LengthGrade)
            {
                case 4:
                    RYOsystemModule.windowsfont(300, 50, 140, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 5:
                    RYOsystemModule.windowsfont(250, 50, 140, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 6:
                    RYOsystemModule.windowsfont(220, 50, 140, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 7:
                    RYOsystemModule.windowsfont(180, 50, 140, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 8:
                    RYOsystemModule.windowsfont(140, 50, 140, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 9:
                    RYOsystemModule.windowsfont(100, 50, 140, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 10:
                    RYOsystemModule.windowsfont(90, 50, 130, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 11:
                    RYOsystemModule.windowsfont(90, 50, 120, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                case 12:
                    RYOsystemModule.windowsfont(80, 50, 120, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
                default:
                    RYOsystemModule.windowsfont(60, 40, 90, 0, 2, 0, "arial", packGradeTextBox.Text);
                    break;
            }

            RYOsystemModule.barcode("60", "520", "128", "60", "0", "270", "2", "2", barcodeTextBox.Text);
            RYOsystemModule.windowsfont(120, 460, 30, 90, 0, 0, "arial", barcodeTextBox.Text);
            RYOsystemModule.sendcommand("BAR 50,210,770,4");

            //CaseNo
            switch (LengthCase)
            {
                case 1:
                    RYOsystemModule.windowsfont(440, 212, 110, 0, 2, 0, "arial", caseNoTextBox.Text);
                    break;
                case 2:
                    RYOsystemModule.windowsfont(420, 212, 110, 0, 2, 0, "arial", caseNoTextBox.Text);
                    break;
                case 3:
                    RYOsystemModule.windowsfont(390, 212, 110, 0, 2, 0, "arial", caseNoTextBox.Text);
                    break;
                case 4:
                    RYOsystemModule.windowsfont(360, 212, 110, 0, 2, 0, "arial", caseNoTextBox.Text);
                    break;
                default:
                    RYOsystemModule.windowsfont(330, 212, 110, 0, 2, 0, "arial", caseNoTextBox.Text);
                    break;
            }

            RYOsystemModule.sendcommand("BAR 150,315,670,4");
            RYOsystemModule.windowsfont(190, 317, 110, 0, 2, 0, "arial", grossDefault);
            RYOsystemModule.windowsfont(520, 317, 110, 0, 2, 0, "arial", netDefault);
            RYOsystemModule.sendcommand("BAR 150,430,670,4");
            RYOsystemModule.barcode("165", "450", "128", "50", "0", "0", "2", "2", barcodeTextBox.Text);
            RYOsystemModule.windowsfont(220, 500, 30, 0, 0, 0, "arial", barcodeTextBox.Text);

            if (pdRemarkTextBox.Text != "" || pdRemarkTextBox.Text != null)
                RYOsystemModule.windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" + tareReal + ",N:" + netReal + "," + pdRemarkTextBox.Text);
            else
                RYOsystemModule.windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" + tareReal + ",N:" + netReal);

            RYOsystemModule.sendcommand("BAR 50,535,774,4");
            RYOsystemModule.sendcommand("BAR 470,315,4,220");
            RYOsystemModule.windowsfont(65, 540, 30, 0, 0, 0, "arial", "Effective : 28-10-2013");
            RYOsystemModule.windowsfont(650, 545, 30, 0, 0, 0, "arial", "FM-PCS-17");
            RYOsystemModule.windowsfont(65, 565, 30, 0, 0, 0, "arial", "D/M/Y");

            //if (printCheckBox.IsChecked == true) RYOsystemModule.printlabel("1", "2");
            //else
            RYOsystemModule.printlabel("1", "1");

            RYOsystemModule.closeport();
        }

        private bool PrepareBFSave()
        {
            double XA = 0.3;
            tmpNetReal = 0;
            bool readyForSave = true;
            string tmpgrade;

            //Check New Box, Old Box ---> always use New Box
            if (boxTextBox.Text == "New Box")
                tmpBox = true;

            //Prevent user error to put AB4 box in this text
            if (!string.IsNullOrWhiteSpace(barcodeTextBox.Text))
            {
                var existed = BusinessLayerServices.ProductBL().GetSingleProduct(barcodeTextBox.Text);
                if (existed != null)
                {
                    tmpgrade = BusinessLayerServices.ProductBL().GetSingleProduct(barcodeTextBox.Text).grade.ToString();
                    if (tmpgrade.ToLower().Contains("ab4"))
                    {
                        MessageBoxHelper.Warning("Barcode นี้เป็นของ Grade AB4 ไม่สามารถนำมาทำซ้ำได้ กรุณาเช็ค");
                        readyForSave = false;
                        return readyForSave;
                    }
                }
            }

            if (!Helper.RegularExpressionHelper.IsNumericCharacter(caseNoTextBox.Text))
            {
                MessageBoxHelper.Warning("ระบบอนุญาตให้หมายเลขห่อยาประกอบด้วยตัวเลขเท่านั้น");
                caseNoTextBox.Text = "";
                caseNoTextBox.Focus();
                readyForSave = false;
                return readyForSave;
            }

            if (thresherTextBox.SelectedValue == null)
            {
                MessageBoxHelper.Warning("โปรดระบุ Thresher");
                thresherTextBox.Focus();
                readyForSave = false;
                return readyForSave;
            }

            if (redryTextBox.SelectedValue == null)
            {
                MessageBoxHelper.Warning("โปรดระบุ Redryer");
                redryTextBox.Focus();
                readyForSave = false;
                return readyForSave;
            }

            if (packingEndTextBox.SelectedValue == null)
            {
                MessageBoxHelper.Warning("โปรดระบุ Packing End");
                packingEndTextBox.Focus();
                readyForSave = false;
                return readyForSave;
            }

            if (!string.IsNullOrEmpty(AB4barcodeTextBox.Text) &&
                !string.IsNullOrEmpty(AB4caseNoTextBox.Text))
            {
                //MessageBoxHelper.Warning("โปรดระบุ AB4 Barcode", "Warning!",
                //MessageBoxButton.OK, MessageBoxImage.Warning);
                //AB4barcodeTextBox.Focus();
                //readyForSave = false;
                //return readyForSave;

                if (AB4barcodeTextBox.Text.Length > 12 || AB4barcodeTextBox.Text.Length < 12)
                {
                    MessageBoxHelper.Warning("Barcode AB4 ไม่ถูกต้อง, กรุณาเช็ค AB4");
                    AB4barcodeTextBox.Text = "";
                    AB4barcodeTextBox.Focus();
                    readyForSave = false;
                    return readyForSave;
                }
            }

            if (!string.IsNullOrEmpty(grossTextBox.Text) && !string.IsNullOrEmpty(boxTareTextBox.Text))
            {
                if (!RegularExpressionHelper.IsDecimalCharacter(grossTextBox.Text) &&
                    !RegularExpressionHelper.IsDecimalCharacter(boxTareTextBox.Text))
                {
                    MessageBoxHelper.Warning("ช่องน้ำหนัก ต้องเป็นตัวเลขเท่านั้น");
                    grossTextBox.Text = "";
                    grossTextBox.Focus();
                    readyForSave = false;
                    return readyForSave;
                }
                else
                {
                    tmpNetReal = Convert.ToDouble(grossTextBox.Text) - Convert.ToDouble(boxTareTextBox.Text);
                    netRealTextBox.Text = string.Format("{0:N2}", tmpNetReal);
                }
            }
            else
            {
                grossTextBox.Text = "7.15";
                netRealTextBox.Text = "6.00";
            }

            //Check weight for Lamina only
            if (barcodeTextBox.Text.Substring(0, 2) == "LN")
            {
                if (netRealCheckBox.IsChecked == true)
                {
                    if (Convert.ToDouble(netRealTextBox.Text) > Convert.ToDouble(netTextBox.Text) + XA)
                    {
                        MessageBoxHelper.Warning("กรุณาดึงยาออก น้ำหนัก Net(Real) ต้องไม่เกิน " + (Convert.ToDouble(netTextBox.Text) + XA));
                        grossTextBox.Focus();
                        readyForSave = false;
                        return readyForSave;
                    }
                    else if (Convert.ToDouble(netRealTextBox.Text) < Convert.ToDouble(netTextBox.Text) - XA)
                    {
                        MessageBoxHelper.Warning("กรุณาดึงยาออก น้ำหนัก Net(Real) ต้องไม่ต่ำกว่า " + (Convert.ToDouble(netTextBox.Text) - XA));
                        grossTextBox.Focus();
                        readyForSave = false;
                        return readyForSave;
                    }
                }
            }
            return readyForSave;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddNewPacking();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private string CreatingBarcode(int currentYear)
        {
            string tmpPrefix = "";
            string maxBc;
            tmpBC = "";

            if (pdMode == "Lamina")
            {
                if (fromTextBox.Text == "RYO")
                {
                    tmpPrefix = "LNR";
                    tmpBC = tmpPrefix + pdNoTextBox.Text + "-" + caseNoTextBox.Text.PadLeft(4, '0');
                    //Ex. LNR21-001-0001
                }
                else
                {
                    tmpPrefix = "LN";
                    maxBc = GetMaxRunningBC(currentYear).ToString("D6");
                    tmpBC = tmpPrefix + currentYear.ToString().Substring(2, 2) + "-" + maxBc;
                    //Ex. LN21-003268
                }
            }
            else
            {
                if (fromTextBox.Text == "RYO")
                    tmpPrefix = "RNR";
                else
                    tmpPrefix = "RN";

                maxBc = GetMaxRunningBC(currentYear).ToString("D6");
                tmpBC = tmpPrefix + currentYear.ToString().Substring(2, 2) + "-" + maxBc;
                //Ex. RNR21-003268
                //Ex. RN21-003268
            }

            return tmpBC;
        }

        private static Int32 GetMaxRunningBC(int crop)
        {
            var MaxBC = BusinessLayerServices.ProductBL().GetLatestBarcode(crop);
            if (MaxBC > 0)
                return MaxBC + 1;
            else
                return 1;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveNewPack();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveNewPack()
        {
            if (PrepareBFSave() == true)
            {
                //WE MIGHT NEED TO SAVE BOTH IN PD and PD_MOISTURE_MASTERBOX
                SaveNewPacking();
                if (!string.IsNullOrEmpty(AB4barcodeTextBox.Text) &&
                    !string.IsNullOrEmpty(AB4caseNoTextBox.Text))
                    SaveMasterBox();

                //Show Updated list
                ShowProductionBale();
                ChangeProductionPeriod();

                if (notPrintCheckBox.IsChecked == false)
                    PrintBarcode();

                //AddNewPacking();
            }
        }

        private void AddNewPacking()
        {
            if (string.IsNullOrEmpty(BayCombobox.Text))
                throw new ArgumentException("โปรดระบุ Bay ที่จะเก็บ Master Box.");

            if (pdMode == "Remnant")
                caseNoTextBox.Text = "0";
            else
                caseNoTextBox.Text = BusinessLayerServices.ProductBL()
                    .GetCaseNo_Normal(packedGrade).ToString();

            if (!string.IsNullOrEmpty(alertAB4TextBox.Text))
                if (caseNoTextBox.Text == alertAB4TextBox.Text)
                    MessageBoxHelper.Warning("เปลี่ยน AB4 ก่อนเริ่มทำยาห่อใหม่ด้วย");

            barcodeTextBox.Text = CreatingBarcode(currentYear);
            boxTextBox.Text = "New Box";

            //FOR PackedGrade
            ShowPackedDetails(packedGrade);

            boxTareTextBox.Text = tareTextBox.Text;
            boxTareTextBox.IsEnabled = false;

            if (PrepareBFSave() == true)
            {
                SaveNewPacking();

                Packing_MasterBagWindow m =
                        new Packing_MasterBagWindow(CurrpdNo,
                        barcodeTextBox.Text,
                        Convert.ToInt16(caseNoTextBox.Text),
                        Convert.ToDateTime(pdDatePicker.Text));

                m.Title = string.Format("Master Bag Packing");
                m.ShowDialog();

                grossTextBox.Focus();
                grossTextBox.SelectAll();
            }
        }

        private void ChangeProductionPeriod()
        {
            try
            {
                statusTextBox.Text = "UNLOCK";
                statusTextBox.Background = new SolidColorBrush(Colors.YellowGreen);
                saveButton.IsEnabled = true;
                printButton.IsEnabled = true;
                addButton.IsEnabled = true;

                var packingStatus = BusinessLayerServices.ProductBL()
                    .GetPackingStatus(CurrpdNo, Convert.ToInt32(periodComboBox.Text));
                {
                    if (packingStatus.Count > 0)
                    {
                        if ((bool)packingStatus.FirstOrDefault().locked)
                        {
                            statusTextBox.Text = "LOCKED";
                            statusTextBox.Background = new SolidColorBrush(Colors.LightCoral);
                            addButton.IsEnabled = false;
                            saveButton.IsEnabled = false;
                            printButton.IsEnabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveMasterBox()
        {
            try
            {
                //string existedissue = "";
                //var issueCode = BusinessLayerServices.pd_ryo_MaterialIssuedCodeBL()
                //    .GetCurrentStatus(customerTextBox.Text);

                //if (issueCode != null)
                //    existedissue = issueCode.IssuedCode;

                var masterbox = new pd_moisture_masterbox
                {
                    from_caseno = AB4caseNoTextBox.Text == "" ? 0 : Convert.ToInt32(AB4caseNoTextBox.Text),
                    frombc = AB4barcodeTextBox.Text,
                    caseno = Convert.ToInt32(caseNoTextBox.Text),
                    masterbox_bc = barcodeTextBox.Text,
                    IssuedCode = null
                };
                //Get Masterbox list
                var lstMasterBox = BusinessLayerServices.MoistureBL()
                    .GetSingleMasterBoxByMasterBox(barcodeTextBox.Text);
                if (lstMasterBox == null)
                {
                    BusinessLayerServices.MoistureBL().AddMasterBox(masterbox);
                }
                else
                {
                    //delete old masterbox because there are all PK
                    BusinessLayerServices.MoistureBL().DeleteMasterBox(lstMasterBox);
                    BusinessLayerServices.MoistureBL().AddMasterBox(masterbox);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveNewPacking()
        {
            if (string.IsNullOrEmpty(BayCombobox.Text))
                throw new ArgumentException("โปรดระบุ Bay ที่จะเก็บ Master Box.");

            if (PrepareBFSave() == false)
                return;

            var packing = new pd
            {
                bc = barcodeTextBox.Text,
                crop = currentYear,
                type = typeTextBox.Text,
                grade = packedGrade,
                customer = customerTextBox.Text,
                form = fromTextBox.Text,
                packingmat = packingMatTextBox.Text,
                caseno = Convert.ToInt32(caseNoTextBox.Text),
                grossdef = Convert.ToDecimal(netTextBox.Text) + Convert.ToDecimal(tareTextBox.Text),
                taredef = Convert.ToDecimal(tareTextBox.Text),
                netdef = Convert.ToDecimal(netTextBox.Text),
                grossreal = grossTextBox.Text == "" ? 0 : Convert.ToDecimal(grossTextBox.Text),
                netreal = grossTextBox.Text == "" ? 0 : Convert.ToDecimal(grossTextBox.Text) - Convert.ToDecimal(boxTareTextBox.Text),
                malcam = 0,
                rcfrom = "Packing",
                box = tmpBox,
                pdtype = pdMode,
                frompddate = pdDatePicker.SelectedDate,
                frompdno = pdNoTextBox.Text,
                frompdhour = Convert.ToInt16(periodComboBox.SelectedItem),
                fromprgrade = packedGrade,
                //wh = "STEC",
                wh = "BaanKru2",
                bay = BayCombobox.Text,
                packingdate = DateTime.Now.Date,
                packingtime = DateTime.Now,
                dtrecord = DateTime.Now,
                packinguser = UName,//-----> Get Current User
                graders = packedGrade,
                customerrs = customerTextBox.Text,
                thr = thresherTextBox.Text,
                rdy = redryTextBox.Text,
                pend = packingEndTextBox.Text,
                boxtare = Convert.ToDouble(boxTareTextBox.Text),
                pdremark = pdRemarkTextBox.Text,
                frompdremark = pdRemarkTextBox.Text,
                pdremarkrs = pdRemarkTextBox.Text,
                PdRemarkParent = pdRemarkParentTextBox.Text,
                issued = false,
                FullMigration = false,
                cropdef = currentYear
            };

            //Check Existed Barcode before Save
            var existedBC = BusinessLayerServices.ProductBL().GetSingleProduct(barcodeTextBox.Text);
            if (existedBC != null)
                EditPacking(packing);
            else
                InsertNewPacking(packing);

            ShowProductionBale();
            ShowProductionDetails();
        }

        private void InsertNewPacking(pd packing)
        {
            try
            {
                if (!fromTextBox.Text.Contains("RYO") || pdMode == "Remnant")
                {
                    // Get the last bc from pdbc and update the last bc.
                    int latestBC = 0;
                    latestBC = GetMaxRunningBC(currentYear);
                    BusinessLayerServices.ProductBL().UpdatePdBC(currentYear, latestBC);
                }

                // Insert new record in pd.
                BusinessLayerServices.ProductBL().AddNewPacking(packing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void EditPacking(pd packing)
        {
            BusinessLayerServices.ProductBL().UpdatePacking(packing);
        }

        private void DeleteMasterButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //-------- Checking null ---------
                if (AB4barcodeTextBox.Text == "")
                    throw new Exception("Please check AB4 Barcode");

                if (barcodeTextBox.Text == "")
                    throw new Exception("Please check Master box case no.");

                var duplicate = BusinessLayerServices.MoistureBL()
                    .GetSingleMasterBoxByMasterBox(barcodeTextBox.Text);
                if (duplicate != null)
                {
                    var masterbox = new pd_moisture_masterbox
                    {
                        from_caseno = Convert.ToInt32(AB4caseNoTextBox.Text),
                        frombc = AB4barcodeTextBox.Text,
                        caseno = Convert.ToInt32(caseNoTextBox.Text),
                        masterbox_bc = barcodeTextBox.Text
                    };
                    BusinessLayerServices.MoistureBL().DeleteMasterBox(masterbox);
                    MessageBoxHelper.Info("Remove this Master box สำเร็จ");

                    ShowProductionBale();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddMasterButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(AB4barcodeTextBox.Text) &&
                !string.IsNullOrEmpty(AB4caseNoTextBox.Text) &&
                !string.IsNullOrEmpty(barcodeTextBox.Text))
            {
                SaveMasterBox();
                ShowProductionBale();
            };
        }

        private void periodComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ClearControl();
            ChangeProductionPeriod();
            ShowProductionBale();
        }

        private void finishButton_Click(object sender, RoutedEventArgs e)
        {
            FinishGradeWindow f = new FinishGradeWindow(Convert.ToDateTime(pdDatePicker.Text));
            f.ShowDialog();
        }

        private void SelectedButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selected = (pdPacked)PackingDataGrid.SelectedItem;
                if (selected != null)
                {
                    //string UName = Properties.Settings.Default.UserPacking; string pdNo,string MboxBC, int MBoxCase, DateTime pdDate
                    Packing_MasterBagWindow m =
                        new Packing_MasterBagWindow(CurrpdNo,
                        selected.bc,
                        selected.caseno,
                        Convert.ToDateTime(pdDatePicker.Text));

                    m.Title = string.Format("Master Bag Packing");
                    m.ShowDialog();

                    grossTextBox.SelectAll();
                    grossTextBox.Focus();
                }
                else
                    MessageBoxHelper.Warning("กรุณาเลือก Production No.!!");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.F1)
                {
                    pdMode = "Lamina";
                    modeTextBox.Text = pdMode;
                    ModeShowTextBox.Text = pdMode;
                    grossTextBox.Focus();
                    AddNewPacking();
                }
                else if (e.Key == Key.F2)
                {
                    pdMode = "Remnant";
                    modeTextBox.Text = pdMode;
                    ModeShowTextBox.Text = pdMode;
                    AddNewPacking();
                    caseNoTextBox.Text = "0";
                }
                else if (e.Key == Key.F12)
                {
                    SaveNewPack();
                }
                else if (e.Key == Key.F8)
                {
                    AddNewPacking();
                }
                else if (e.Key == Key.F9)
                {
                    PrintBarcodeCHAMP();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void NewButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PackingDataGrid.ItemsSource != null)
                {
                    if (statusTextBox.Text == "UNLOCK")
                        //Insert new pd lock in PackingStatus
                        BusinessLayerServices.ProductBL()
                            .InsertPackingStatus(pdNoTextBox.Text,
                            Convert.ToInt16(periodComboBox.SelectedItem),
                            Convert.ToDateTime(pdDatePicker.SelectedDate));

                    //Add new pdHour in ComboBox
                    periodComboBox.ItemsSource = BusinessLayerServices.ProductBL().GetPdHours(pdNoTextBox.Text);
                    periodComboBox.SelectedIndex = periodComboBox.Items.Count - 1;
                }

                //update PackingDataGrid list
                ShowProductionDetails();
                ShowProductionBale();
                ChangeProductionPeriod();

                grossTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AB4barcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (AB4barcodeTextBox.Text == "")
                    return;

                var existedBC = BusinessLayerServices.ProductBL().GetSingleProduct(AB4barcodeTextBox.Text);
                if (existedBC == null)
                    throw new Exception("เลข Barcode นี้ไม่มีอยู่ในระบบ กรุณาตรวจสอบอีกครั้ง");

                AB4GradeTextBox.Text = existedBC.grade;
                AB4caseNoTextBox.Text = existedBC.caseno.ToString();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RemnantButton_Click(object sender, RoutedEventArgs e)
        {
            RemnantWindow s = new RemnantWindow(pdNoTextBox.Text, Convert.ToDateTime(pdDatePicker.Text), packGradeTextBox.Text);
            s.ShowDialog();
        }

        private void grossTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (grossTextBox.Text == "")
                    return;

                tareGridTextBox.Text = tareTextBox.Text;
                tareGridTextBox.IsEnabled = false;

                boxTareTextBox.Text = tareTextBox.Text;

                if (PrepareBFSave() == true)
                {
                    //WE NEED TO SAVE BOTH IN PD and PD_MOISTURE_MASTERBOX
                    SaveNewPacking();
                    SaveMasterBox();
                    //Show Updated list
                    ShowProductionBale();
                    ChangeProductionPeriod();

                    if (notPrintCheckBox.IsChecked == false)
                        PrintBarcode();

                    //AddNewPacking();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void currentMBButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Check before save in first time
                if (PrepareBFSave() == true)
                {
                    SaveNewPacking();

                    Packing_MasterBagWindow m =
                        new Packing_MasterBagWindow(CurrpdNo,
                        barcodeTextBox.Text,
                        Convert.ToInt16(caseNoTextBox.Text),
                        Convert.ToDateTime(pdDatePicker.Text));

                    m.Title = string.Format("Master Bag Packing");
                    m.ShowDialog();

                    grossTextBox.Focus();
                    grossTextBox.SelectAll();

                    ShowProductionBale();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AB4caseNoTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                AB4barcodeTextBox.Text = "";

                if (string.IsNullOrEmpty(AB4GradeTextBox.Text))
                    throw new Exception("กรุณาใส่ Grade AB4 เพื่อหาเลขกล่อง");

                //Find the AB4 barcode by Grade and case no.
                var findingAB4 = BusinessLayerServices.ProductBL()
                    .GetSingleProductByCaseandGrade(AB4GradeTextBox.Text, Convert.ToInt16(AB4caseNoTextBox.Text));

                if (findingAB4 != null)
                    AB4barcodeTextBox.Text = findingAB4.bc;
                else
                    throw new Exception("ไม่พบข้อมูล AB4 ที่กำลังค้นหา");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
