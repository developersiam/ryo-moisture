﻿using RYOMoisture.Helper;
using RYOMoisture.Model;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for Receiving.xaml
    /// </summary>
    public partial class ReceivingsBalanceStock : Page
    {
        public ReceivingsBalanceStock()
        {
            try
            {
                InitializeComponent();

                CropCombobox.ItemsSource = null;
                CropCombobox.ItemsSource = user_setting.Crops;
                CropCombobox.SelectedValue = DateTime.Now.Year;

                CustomerCombobox.ItemsSource = null;
                CustomerCombobox.ItemsSource = BusinessLayerServices.customerBL()
                    .Get()
                    .OrderBy(x => x.code);

                StockSummaryDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StockSummaryDataGrid.SelectedIndex < 0)
                    return;

                var model = (m_MaterialStockSummary)StockSummaryDataGrid.SelectedItem;
                ReceivingDetails window = new ReceivingDetails(model);
                window.ShowDialog();

                StockSummaryDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void StockSummaryDataGridBinding()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (CropCombobox.SelectedValue == null)
                    return;

                if (CustomerCombobox.SelectedIndex < 0)
                    return;

                if (CustomerCombobox.SelectedValue == null)
                    return;

                StockSummaryDataGrid.ItemsSource = null;
                StockSummaryDataGrid.ItemsSource = Helper.MaterialReceivingHelper
                    .GetCurrentStockByCrop(Convert.ToInt16(CropCombobox.SelectedValue.ToString()));

                TotalTextBlock.Text = StockSummaryDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {

                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือกปีการผลิตก่อนเริ่มการบันทึกข้อมูล Receiving");

                Receivings window = new Receivings(Convert.ToInt16(CropCombobox.SelectedValue.ToString()), CustomerCombobox.SelectedValue.ToString());
                window.ShowDialog();
                StockSummaryDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StockSummaryDataGridBinding();
        }

        private void CustomerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StockSummaryDataGridBinding();
        }
    }
}
