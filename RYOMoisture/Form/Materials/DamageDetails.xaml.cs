﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for DamageDetails.xaml
    /// </summary>
    public partial class DamageDetails : Window
    {
        pd_ryo_MaterialDamage _damage;
        pd_ryo_MaterialDamageDetail _damageDetail;

        public DamageDetails(pd_ryo_MaterialDamage model)
        {
            InitializeComponent();

            _damage = new pd_ryo_MaterialDamage();
            _damage = model;

            ItemCombobox.ItemsSource = null;
            ItemCombobox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialItemBL()
                .Get()
                .OrderBy(x => x.ItemName);

            DamageCodeTextBox.Text = _damage.DamageCode;
            DamageDetailDataGridBinding();
        }

        private void DamageDetailDataGridBinding()
        {
            try
            {
                DamageDetailDataGrid.ItemsSource = null;
                DamageDetailDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_MaterialDamageDetailBL()
                    .GetByDamageCode(_damage.DamageCode)
                    .OrderBy(x => x.pd_ryo_MaterialItem.ItemName);

                TotalTextBlock.Text = DamageDetailDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            try
            {
                EditButton.IsEnabled = false;
                AddButton.IsEnabled = true;
                ItemCombobox.IsEnabled = true;
                QuantityTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Item");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุจำนวน Quantity (Pcs.)");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                var msg = MessageBoxHelper.Question("หากมีการนำข้อมูลนี้ไปใช้ในการอ้างอิงเพื่อตัดสต๊อก material ก่อนหน้านี้แล้ว " +
                    "การเพิ่มข้อมูลนี้อาจทำให้ตัวเลขที่โชว์ในสต๊อกเปลี่ยนแปลงไปจากเดิม" +
                    " ท่านต้องการเพิ่มข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MaterialDamageDetailBL()
                    .Add(new pd_ryo_MaterialDamageDetail
                    {
                        ItemCode = ItemCombobox.SelectedValue.ToString(),
                        DamageCode = _damage.DamageCode,
                        Quantity = Convert.ToInt16(QuantityTextBox.Text),
                        RecordBy = user_setting.User.Username,
                        RecordDate = DateTime.Now,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                DamageDetailDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุจำนวน Quantity Pcs.");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                var msg = MessageBoxHelper.Question("หากมีการนำข้อมูลนี้ไปใช้ในการอ้างอิงเพื่อตัดสต๊อก material ก่อนหน้านี้แล้ว " +
                    "การเพิ่มข้อมูลนี้อาจทำให้ตัวเลขที่โชว์ในสต๊อกเปลี่ยนแปลงไปจากเดิม" +
                    " ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                _damageDetail.Quantity = Convert.ToInt16(QuantityTextBox.Text);
                _damageDetail.ModifiedBy = user_setting.User.Username;
                _damageDetail.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialDamageDetailBL().Edit(_damageDetail);

                DamageDetailDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageDetailDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper
                    .Question("หากข้อมูลนี้ถูกนำไปใช้อ้างอิงในการตัดสต๊อก material ที่ใช้แล้ว" +
                    " การลบข้อมูลอาจทำให้ตัวเลขที่โชว์ในสต๊อกเปลี่ยนแปลงไปจากเดิม ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialDamageDetail)DamageDetailDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialDamageDetailBL().Delete(model.DamageCode, model.ItemCode);

                DamageDetailDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageDetailDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                ItemCombobox.IsEnabled = false;

                _damageDetail = (pd_ryo_MaterialDamageDetail)DamageDetailDataGrid.SelectedItem;

                QuantityTextBox.Text = _damageDetail.Quantity.ToString();
                ItemCombobox.SelectedValue = _damageDetail.ItemCode;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
