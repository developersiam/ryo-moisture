﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for IssuedPerBoxs.xaml
    /// </summary>
    public partial class IssuedPerBoxs : Window
    {
        string _issuedCode;
        pd_ryo_MaterialIssuedPerBox _issuedPerBox;

        public IssuedPerBoxs(string issuedCode)
        {
            try
            {
                InitializeComponent();

                _issuedCode = issuedCode;
                _issuedPerBox = new pd_ryo_MaterialIssuedPerBox();

                ItemCombobox.ItemsSource = null;
                ItemCombobox.ItemsSource = BusinessLayerServices.pd_ryo_MaterialItemBL()
                    .Get()
                    .OrderBy(x => x.ItemName);

                IssuedCodeTextBox.Text = _issuedCode;
                ItemIssuedDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ItemIssuedDataGridBinding()
        {
            try
            {
                ItemIssuedDataGrid.ItemsSource = null;
                ItemIssuedDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_MaterialIssuedPerBoxBL()
                    .GetByIssuedCode(_issuedCode)
                    .OrderBy(x => x.pd_ryo_MaterialItem.ItemName);

                TotalTextBlock.Text = ItemIssuedDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            try
            {
                EditButton.IsEnabled = false;
                AddButton.IsEnabled = true;
                ItemCombobox.IsEnabled = true;
                IssuedQuantityTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IssuedQuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุจำนวน Issued Pcs.");

                if (RegularExpressionHelper.IsNumericCharacter(IssuedQuantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                var msg = MessageBoxHelper.Question("หากมีการนำข้อมูลนี้ไปใช้ในการอ้างอิงเพื่อตัดสต๊อก material ก่อนหน้านี้แล้ว " +
                    "การเพิ่มข้อมูลนี้อาจทำให้ตัวเลขที่โชว์ในสต๊อกเปลี่ยนแปลงไปจากเดิม" +
                    " ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                _issuedPerBox.PcsPerBox = Convert.ToInt16(IssuedQuantityTextBox.Text);
                _issuedPerBox.ModifiedBy = user_setting.User.Username;
                _issuedPerBox.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialIssuedPerBoxBL().Edit(_issuedPerBox);
                ItemIssuedDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ItemIssuedDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดเลือก Item");

                if (IssuedQuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุจำนวน Issued Pcs.");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(IssuedQuantityTextBox.Text) == false)
                    throw new ArgumentException("Quantity จะต้องเป็นตัวเลขจำนวนเต๊มเท่านั้น");

                var msg = MessageBoxHelper.Question("หากมีการนำข้อมูลนี้ไปใช้ในการอ้างอิงเพื่อตัดสต๊อก material ก่อนหน้านี้แล้ว " +
                    "การเพิ่มข้อมูลนี้อาจทำให้ตัวเลขที่โชว์ในสต๊อกเปลี่ยนแปลงไปจากเดิม" +
                    " ท่านต้องการเพิ่มข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                BusinessLayerServices.pd_ryo_MaterialIssuedPerBoxBL()
                    .Add(new pd_ryo_MaterialIssuedPerBox
                    {
                        ItemCode = ItemCombobox.SelectedValue.ToString(),
                        IssuedCode = _issuedCode,
                        PcsPerBox = Convert.ToInt16(IssuedQuantityTextBox.Text),
                        CreateBy = user_setting.User.Username,
                        CreateDate = DateTime.Now,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                ItemIssuedDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemIssuedDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("หากข้อมูลนี้ถูกนำไปใช้อ้างอิงในการตัดสต๊อก material ที่ใช้แล้ว การลบข้อมูลอาจทำให้ตัวเลขที่โชว์ในสต๊อกเปลี่ยนแปลงไปจากเดิม" +
                    " ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialIssuedPerBox)ItemIssuedDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialIssuedPerBoxBL().Delete(model.IssuedCode, model.ItemCode);

                ItemIssuedDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ItemIssuedDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;
                ItemCombobox.IsEnabled = false;

                _issuedPerBox = (pd_ryo_MaterialIssuedPerBox)ItemIssuedDataGrid.SelectedItem;

                IssuedQuantityTextBox.Text = _issuedPerBox.PcsPerBox.ToString();
                ItemCombobox.SelectedValue = _issuedPerBox.ItemCode;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
