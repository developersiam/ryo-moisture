﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for IssuedCodes.xaml
    /// </summary>
    public partial class IssuedCodes : Page
    {
        public IssuedCodes()
        {
            InitializeComponent();

            CropCombobox.ItemsSource = null;
            CropCombobox.ItemsSource = user_setting.Crops;
            CropCombobox.SelectedValue = DateTime.Now.Year;

            CustomerCombobox.ItemsSource = null;
            CustomerCombobox.ItemsSource = BusinessLayerServices.customerBL()
                .Get()
                .OrderBy(x => x.code);

            DataGridBinding();
        }

        private void DataGridBinding()
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    return;

                if (CropCombobox.SelectedValue == null)
                    return;

                if (CustomerCombobox.SelectedIndex < 0)
                    return;

                if (CustomerCombobox.SelectedValue == null)
                    return;

                IssuedCodeDataGrid.ItemsSource = null;
                IssuedCodeDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_MaterialIssuedCodeBL()
                    .GetByCrop(Convert.ToInt16(CropCombobox.SelectedValue.ToString()))
                    .Where(x => x.Customer == CustomerCombobox.SelectedValue.ToString())
                    .OrderByDescending(x => x.CreateDate);

                TotalTextBlock.Text = IssuedCodeDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void GenerateNewCodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropCombobox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุปีการผลิต");

                BusinessLayerServices.pd_ryo_MaterialIssuedCodeBL()
                    .Add(Convert.ToInt16(CropCombobox.SelectedValue.ToString()), CustomerCombobox.SelectedValue.ToString(), user_setting.User.Username);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            DataGridBinding();
        }

        private void IssuedPerBoxButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IssuedCodeDataGrid.SelectedIndex < 0)
                    return;

                var model = (pd_ryo_MaterialIssuedCode)IssuedCodeDataGrid.SelectedItem;

                IssuedPerBoxs window = new IssuedPerBoxs(model.IssuedCode);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IssuedCodeDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialIssuedCode)IssuedCodeDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialIssuedCodeBL().Delete(model.IssuedCode);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CropCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void SetToDefaultButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IssuedCodeDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("เมื่อท่านเปลี่ยนสถานะข้อมูลนี้เป็น default แล้ว การตัดสต็อค material จากนี้จะใช้ข้อมูลนี้เป็นข้อมูลอ้างอิง" +
                    "ท่านต้องการ set to defult ข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialIssuedCode)IssuedCodeDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialIssuedCodeBL()
                    .SetCurrentStatus(model.IssuedCode, user_setting.User.Username);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CustomerCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }
    }
}
