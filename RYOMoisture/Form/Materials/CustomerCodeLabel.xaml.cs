﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for CustomerCodeLabel.xaml
    /// </summary>
    public partial class CustomerCodeLabel : Page
    {
        public CustomerCodeLabel()
        {
            InitializeComponent();
            BlindingDataGrid();
        }

        private void BlindingDataGrid()
        {
            BrandDataGrid.ItemsSource = null;
            BrandDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_customer_labelBL().Get().OrderBy(x => x.customerID);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(BrandnameTextBox.Text) || (string.IsNullOrEmpty(CodeTextBox.Text))) throw new ArgumentException("กรุณาใส่ข้อมูล Brand ให้ครบถ้วนทุกช่อง");

                var setuplst = BusinessLayerServices.pd_ryo_customer_labelBL().GetSingle(CodeTextBox.Text);
                if (setuplst != null) UpdateBrandSetting();
                else AddBrandSetting();

                BlindingDataGrid();
                CodeTextBox.Text = "";
                BrandnameTextBox.Text = "";
                GramTextBox.Text = "";
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddBrandSetting()
        {
            var cl = new pd_ryo_customer_label
            {
                customerID = CodeTextBox.Text,
                customerName = BrandnameTextBox.Text,
                pouch = Convert.ToDouble(GramTextBox.Text),
                modifiedDate = DateTime.Now,
                User = user_setting.User.Username
            };
            BusinessLayerServices.pd_ryo_customer_labelBL().Add(cl);
        }

        private void UpdateBrandSetting()
        {
            var cl = new pd_ryo_customer_label
            {
                customerID = CodeTextBox.Text,
                customerName = BrandnameTextBox.Text,
                pouch = Convert.ToDouble(GramTextBox.Text),
                modifiedDate = DateTime.Now,
                User = user_setting.User.Username
            };
            BusinessLayerServices.pd_ryo_customer_labelBL().Edit(cl);
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BrandDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_customer_label)BrandDataGrid.SelectedItem;
                //Check in Setup first before Delete
                var setup = BusinessLayerServices.pd_ryo_customer_label_setupBL().GetSingleByCode(model.customerID);
                if (setup != null)
                    throw new ArgumentNullException("มีการ setup Brand Code นี้ใน Production แล้ว จึงไม่สามารถลบได้");
                else BusinessLayerServices.pd_ryo_customer_labelBL().Delete(model.customerID);

                BlindingDataGrid();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BrandDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //string C48barcode;
            if (BrandDataGrid.SelectedIndex < 0) return;

            var selected = (pd_ryo_customer_label)BrandDataGrid.SelectedItem;
            if (selected != null)
            {
                BrandnameTextBox.Text = selected.customerName;
                CodeTextBox.Text = selected.customerID;
                GramTextBox.Text = selected.pouch.ToString();

                AddButton.IsEnabled = true;
            }
        }
    }
}
