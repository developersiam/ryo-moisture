﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for DamagePdnos.xaml
    /// </summary>
    public partial class DamagePdnos : Window
    {
        public static short _crop;
        static string _pdno;

        public DamagePdnos(short crop)
        {
            InitializeComponent();

            _crop = crop;
            DataGridBinding();
        }

        private void DataGridBinding()
        {
            try
            {
                pdnoDataGrid.ItemsSource = null;
                pdnoDataGrid.ItemsSource = BusinessLayerServices.PdSetupBL()
                    .GetProductionByCrop(_crop)
                    .OrderByDescending(x => x.date);

                TotalTextBlock.Text = pdnoDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            DataGridBinding();
        }

        private void SelectItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (pdnoDataGrid.SelectedIndex < 0)
                    return;

                var model = (pdsetup)pdnoDataGrid.SelectedItem;

                _pdno = model.pdno;

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public static string GetPdno()
        {
            DamagePdnos window = new DamagePdnos(_crop);
            window.ShowDialog();

            return _pdno;
        }
    }
}
