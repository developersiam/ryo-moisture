﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for DamageReasons.xaml
    /// </summary>
    public partial class DamageReasons : Window
    {
        pd_ryo_MaterialDamageReason _damageReason;
        public DamageReasons()
        {
            InitializeComponent();
            _damageReason = new pd_ryo_MaterialDamageReason();
            DataGridBinding();
        }

        private void DataGridBinding()
        {
            try
            {
                DamageReasonDataGrid.ItemsSource = null;
                DamageReasonDataGrid.ItemsSource = BusinessLayerServices
                    .pd_ryo_MaterialDamageReasonBL()
                    .Get()
                    .OrderBy(x => x.DamageReasonName);

                TotalTextBlock.Text = DamageReasonDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            try
            {
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
                DamageReasonNameTextBox.Clear();

                _damageReason = null;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageReasonNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Item");

                BusinessLayerServices.pd_ryo_MaterialDamageReasonBL()
                    .Add(new pd_ryo_MaterialDamageReason
                    {
                        DamageReasonName = DamageReasonNameTextBox.Text,
                        CreateBy = user_setting.User.Username,
                        CreateDate = DateTime.Now,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageReasonNameTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Reason name");

                var msg = MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                _damageReason.DamageReasonName = DamageReasonNameTextBox.Text;
                _damageReason.ModifiedBy = user_setting.User.Username;
                _damageReason.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialDamageReasonBL().Edit(_damageReason);

                DataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageReasonDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?");
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialDamageReason)DamageReasonDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialDamageReasonBL().Delete(model.DamageReasonID);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DamageReasonDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;

                _damageReason = (pd_ryo_MaterialDamageReason)DamageReasonDataGrid.SelectedItem;

                DamageReasonNameTextBox.Text = _damageReason.DamageReasonName;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
