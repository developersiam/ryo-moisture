﻿using DomainModelStecDbms;
using RYOMoisture.Helper;
using RYOMoistureBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RYOMoisture.Form.Materials
{
    /// <summary>
    /// Interaction logic for Units.xaml
    /// </summary>
    public partial class Units : Window
    {
        pd_ryo_MaterialUnit _unit;

        public Units()
        {
            InitializeComponent();
            UnitDataGridBinding();
            _unit = new pd_ryo_MaterialUnit();
        }

        private void UnitDataGridBinding()
        {
            try
            {
                UnitDataGrid.ItemsSource = null;
                UnitDataGrid.ItemsSource = BusinessLayerServices.pd_ryo_MaterialUnitBL().GetAll();

                TotalTextBlock.Text = UnitDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
                UnitTextBox.Clear();

                _unit = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UnitDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (pd_ryo_MaterialUnit)UnitDataGrid.SelectedItem;
                BusinessLayerServices.pd_ryo_MaterialUnitBL().Delete(model.UnitCode);
                UnitDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UnitTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Unit");

                BusinessLayerServices.pd_ryo_MaterialUnitBL()
                    .Add(new pd_ryo_MaterialUnit
                    {
                        UnitName = UnitTextBox.Text,
                        CreateBy = user_setting.Username,
                        CreateDate = DateTime.Now,
                        ModifiedBy = user_setting.Username,
                        ModifiedDate = DateTime.Now
                    });

                UnitDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UnitDataGrid.SelectedIndex < 0)
                    return;

                EditButton.IsEnabled = true;
                AddButton.IsEnabled = false;

                _unit = (pd_ryo_MaterialUnit)UnitDataGrid.SelectedItem;

                UnitTextBox.Text = _unit.UnitName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UnitTextBox.Text == "")
                    throw new ArgumentException("โปรดกรอก Unit");

                var msg = MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                _unit.UnitName = UnitTextBox.Text;
                _unit.ModifiedBy = user_setting.Username;
                _unit.ModifiedDate = DateTime.Now;

                BusinessLayerServices.pd_ryo_MaterialUnitBL().Edit(_unit);
                UnitDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
