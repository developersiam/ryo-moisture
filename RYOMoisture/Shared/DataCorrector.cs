﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYOMoisture.Shared
{
    public interface IDataCorrector
    {
        string FormatTime(string inputString);
    }
    public class DataCorrector : IDataCorrector
    {
        public string FormatTime(string inputString)
        {
            try
            {
                if (string.IsNullOrEmpty(inputString)) return "00:00:00";

                int hour = 0;
                int minute = 0;
                string[] splited;

                if (inputString.Length <= 2)
                {
                    if (!int.TryParse(inputString, out hour) || hour >= 24) return "00:00:00";
                }
                else
                {
                    inputString = inputString.Replace('.', ':');

                    if (inputString.IndexOf(":") > 0) splited = inputString.Split(':'); //Split with ':'
                    else return "00:00:00";

                    if (splited.Length > 0) int.TryParse(splited[0], out hour);
                    if (splited.Length > 1) int.TryParse(splited[1], out minute);
                }

                TimeSpan result = new TimeSpan(hour, minute, 0);
                return result.ToString();
            }
            catch (Exception)
            {
                return "00:00:00";
            }
        }
    }
}
